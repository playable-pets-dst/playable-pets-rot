
------------------------------------------------------------------
-- Variables
------------------------------------------------------------------
local require = GLOBAL.require
local Ingredient = GLOBAL.Ingredient
-- Add it to the tech trees


local PlayablePets = GLOBAL.PlayablePets
local SETTING = GLOBAL.PP_SETTINGS
local MOBTYPE = GLOBAL.PP_MOBTYPES

local STRINGS = require("pprot_strings")

local TUNING = require("pprot_tuning")

GLOBAL.PPROT_FORGE = require("pprot_forge")
------------------------------------------------------------------
-- Configuration Data
------------------------------------------------------------------

PlayablePets.Init(env.modname)

------------------------------------------------------------------
-- Prefabs
------------------------------------------------------------------

PrefabFiles = {
	-------Houses--------
	"moonspiderdenp",
	"dustmothdenp",
	-------Tools---------
	--"monster_wpnrot",
	--forge
	--"poopweapon_forge",
	-------Mobs----------
	"crabking_clawp",
	"archive_centipede_huskp",
	-------Decor---------

	-------Fx------------
	"moonspider_spikep",

}

-- Centralizes a number of related functions about playable mobs to one convenient table
-- Name is the prefab name of the mob
-- Fancyname is the name of the mob, as seen in-game
-- Gender is fairly self-explanatory
-- Skins is a list of any custom mob skins that may be available, using the Modded Skins API by Fidoop
-- Skins are not required for mobs to function, but they will display a custom portrait when the mob is examined by a player
GLOBAL.PPROT_MobCharacters = {
	spider_wolfp       	       = { fancyname = "Nurse Spider",                               gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"spider_healer"}},
	fruitfly_goodp       	   = { fancyname = "Friendly Fruit Fly",                         gender = "MALE",     mobtype = {}, skins = {}, forge = true, mimic_prefab = {"friendlyfruitfly"}},
	fruitflyp       	       = { fancyname = "Fruit Fly",                                  gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"fruitfly"}},
	fruitflylordp       	   = { fancyname = "Lord of the Fruit Flies",                    gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"lordfruitfly"}},
	wobsterp       	           = { fancyname = "Wobster",                                    gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"wobster"}},
	hermit_crabp       	       = { fancyname = "Crabler",                                    gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"hermitcrab"}},
	crabkingp       	       = { fancyname = "Crab King",                                  gender = "MALE",     mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"crabking"}},
	spider_moonp       	       = { fancyname = "Shattered Spider",                           gender = "MALE",     mobtype = {}, skins = {}, forge = true, mimic_prefab = {"spider_moon"} },
	hound_mutantp              = { fancyname = "Horror Hound",                               gender = "MALE",     mobtype = {}, skins = {}, forge = true, mimic_prefab = {"mutatedhound"} },
	penguin_mutantp            = { fancyname = "Moonrock Pengull",                           gender = "FEMALE",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"mutated_penguin"} },
	fruitdragonp               = { fancyname = "Saladmander",                                gender = "MALE",     mobtype = {}, skins = {}, forge = true, mimic_prefab = {"fruitdragon"} },
	butterfly_moonp            = { fancyname = STRINGS.NAMES.MOONBUTTERFLY,                  gender = "FEMALE",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"moonbutterfly"} },
	carratp                    = { fancyname = STRINGS.NAMES.CARRAT,                         gender = "MALE",     mobtype = {}, skins = {}, forge = true, mimic_prefab = {"carrat"} },
	carrat_shadowp             = { fancyname = "Shadow Carrat",                              gender = "MALE",     mobtype = {}, skins = {}, forge = true, mimic_prefab = {"carrat_ghostracer"} },
	moonmothp                  = { fancyname = "Mothling",                                   gender = "FEMALE",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"critter_lunarmothling"} },
	puffinp                    = { fancyname = "Puffin",                                     gender = "FEMALE",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"puffin"}},
	cookiecutterp              = { fancyname = "Cookie Cutter",                              gender = "MALE",     mobtype = {}, skins = {}, forge = true, mimic_prefab = {"cookiecutter"}},
	malbatrossp                = { fancyname = "Malbatross",                                 gender = "FEMALE",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"malbatross"}},
	merm_guardp                = { fancyname = "Loyal Merm Guard",                           gender = "MALE",     mobtype = {}, skins = {}, forge = true, mimic_prefab = {"mermguard"}},
	squidp                     = { fancyname = "Skitter Squid",                              gender = "MALE",     mobtype = {}, skins = {}, forge = true, mimic_prefab = {"squid"}},
	gnarwailp                  = { fancyname = "Gnarwail",                                   gender = "MALE",     mobtype = {}, skins = {}, forge = true, mimic_prefab = {"gnarwail"}},
	sharkp                     = { fancyname = "Rockjaw",                                    gender = "MALE",     mobtype = {}, skins = {}, forge = true, mimic_prefab = {"shark"}},
	molebatp       	           = { fancyname = STRINGS.CHARACTER_NAMES.molebatp,             gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"molebat"}},
	dustmothp       	       = { fancyname = STRINGS.CHARACTER_NAMES.dustmothp,            gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"dustmoth"}},
	archive_centipedep         = { fancyname = STRINGS.CHARACTER_NAMES.archive_centipedep,   gender = "ROBOT",    mobtype = {}, skins = {}, mimic_prefab = {"archive_centipede"}},
	lightflierp       	       = { fancyname = STRINGS.CHARACTER_NAMES.lightflierp,          gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"lightflier"}},
	mushgnomep       	       = { fancyname = STRINGS.CHARACTER_NAMES.mushgnomep,           gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"mushgnome"}},
	alterguardianp       	   = { fancyname = "Celestial Champion",                         gender = "MALE",     mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"alterguardian_phase1", "alterguardian_phase2", "alterguardian_phase3"}},
	crow_mutantp               = { fancyname = "Moonblind Crow",                             gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"bird_mutant"}},
	robin_mutantp       	   = { fancyname = "Misshapen Bird",                             gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"bird_mutant_spitter"}},
	grassgatorp       	       = { fancyname = STRINGS.CHARACTER_NAMES.grassgatorp,          gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"grassgator"}},
	striderp       	           = { fancyname = STRINGS.CHARACTER_NAMES.striderp,             gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"spider_water"}},
	vargletp       	           = { fancyname = STRINGS.CHARACTER_NAMES.vargletp,             gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"warglet"}},
	powdermonkeyp       	   = { fancyname = STRINGS.CHARACTER_NAMES.powdermonkeyp,        gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"powdermonkey"}},
	primematep       	       = { fancyname = STRINGS.CHARACTER_NAMES.primematep,           gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"prime_mate"}},
	lightcrabp       	       = { fancyname = STRINGS.CHARACTER_NAMES.lightcrabp,           gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"lightcrab"}},
	hound_hedgep               = { fancyname = "Briar Hound",                                gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"hedgehound"}},
	stageusherp                = { fancyname = STRINGS.CHARACTER_NAMES.stageusherp,          gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"stageusher"}},
	shadow_leechp              = { fancyname = STRINGS.CHARACTER_NAMES.shadow_leechp,        gender = "ROBOT",    mobtype = {}, skins = {}, mimic_prefab = {"shadow_leech"}},
	daywalkerp                 = { fancyname = "Nightmare Werepig",                          gender = "MALE",     mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"daywalker"}},
	gestaltp                   = { fancyname = "Gestalt",                                    gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"gestalt"}},
	gestaltguardp              = { fancyname = "Greater Gestalt",                            gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"gestalt_guard"}},
	lunar_grazerp              = { fancyname = "Grazer",                                     gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"lunar_grazer"}},
	froglunarp                 = { fancyname = "Bright-Eyed Frog",                           gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"lunarfrog"}},
	mutatedvargp               = { fancyname = "Possessed Varg",                             gender = "MALE",     mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"mutatedwarg"}},
	mutateddeerclopsp          = { fancyname = "Crystal Deerclops",                          gender = "MALE",     mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"mutateddeerclops"}},
	mutatedbeargerp            = { fancyname = "Armored Bearger",                            gender = "MALE",     mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"mutatedbearger"}},
	fused_shadelingp           = { fancyname = "Fused Shadeling",                            gender = "ROBOT",    mobtype = {}, skins = {}, mimic_prefab = {"fused_shadeling"}},
	shadowthrall_handsp        = { fancyname = "Jitters",                                    gender = "ROBOT",    mobtype = {}, skins = {}, release_id = "R29_SHADOW_RIFT", mimic_prefab = {"shadowthrall_hands"}},
	shadowthrall_wingsp        = { fancyname = "Shriek",                                     gender = "ROBOT",    mobtype = {}, skins = {}, release_id = "R29_SHADOW_RIFT", mimic_prefab = {"shadowthrall_wings"}},
	shadowthrall_hornsp        = { fancyname = "Rasp",                                       gender = "ROBOT",    mobtype = {}, skins = {}, release_id = "R29_SHADOW_RIFT", mimic_prefab = {"shadowthrall_horns"}},
	pollyp       	           = { fancyname = STRINGS.CHARACTER_NAMES.pollyp,               gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"polly_roger"}},
	otterp                     = { fancyname = STRINGS.CHARACTER_NAMES.otterp,               gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"otter"}},
	crabking_mobp              = { fancyname = STRINGS.CHARACTER_NAMES.crabking_mobp,        gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"crabking_mob"}},
	crabking_mob_knightp       = { fancyname = STRINGS.CHARACTER_NAMES.crabking_mob_knightp, gender = "MALE",     mobtype = {}, skins = {}, mimic_prefab = {"crabking_mob_knight"}},
	worm_bossp                 = { fancyname = STRINGS.CHARACTER_NAMES.worm_bossp,           gender = "MALE",     mobtype = {MOBTYPE.GIANT}, skins = {}},
	chest_mimicp               = { fancyname = STRINGS.CHARACTER_NAMES.chest_mimicp,         gender = "ROBOT",    mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"chest_mimic"}},
}

-- Necessary to ensure a specific order when adding mobs to the character select screen. This table is iterated and used to index the one above
PPROT_Character_Order = {
	"vargletp",
	"spider_wolfp",
	"hermit_crabp", 
	"squidp", 
	"wobsterp",
	"gnarwailp", 
	"sharkp", 
	"merm_guardp", 
	"spider_moonp", 
	"hound_mutantp", 
	"penguin_mutantp", 
	"carratp", 
	"carrat_shadowp", 
	"puffinp", 
	"fruitdragonp", 
	"butterfly_moonp", 
	"moonmothp", 
	"cookiecutterp", 
	"molebatp",
	"lightflierp",
	"mushgnomep",
	"dustmothp",
	"archive_centipedep",
	"crow_mutantp",
	"robin_mutantp",
	"striderp",
	"grassgatorp",
	"pollyp",
	"powdermonkeyp",
	"primematep",
	"lightcrabp",
	"fruitflyp",
	"fruitfly_goodp",
	"fruitflylordp",
	"hound_hedgep",
	"stageusherp",
	"otterp",
	"gestaltp",
	"gestaltguardp",
	"lunar_grazerp",
	"froglunarp",
	"shadow_leechp",
	"fused_shadelingp",
	"shadowthrall_handsp",
	"shadowthrall_wingsp",
	"shadowthrall_hornsp",
	"chest_mimicp",
	"worm_bossp",
	"malbatrossp",
	"crabking_mobp",
	"crabking_mob_knightp", 
	"crabkingp",
	"mutatedvargp",
	"mutateddeerclopsp",
	"mutatedbeargerp",
	"alterguardianp",
	"daywalkerp",
} 
------------------------------------------------------------------
-- Assets
------------------------------------------------------------------

Assets = {
	Asset("ANIM", "anim/ghost_monster_build.zip"), -- Might make it not needed on every prefab.
	--------------------------HOMES--------------------------------
	Asset( "IMAGE", "images/inventoryimages/pprot_homes.tex" ),
	Asset( "ATLAS", "images/inventoryimages/pprot_homes.xml" ),
	--------------------------TOOLS---------------------------------
}


AddComponentAction("SCENE", "edible", function(inst, doer, actions, right)
    if inst:HasTag("oceanfish") and doer.can_eat_fish then
        table.insert(actions, GLOBAL.ACTIONS.EAT)
    end
end)

local function GetInkExcludeTags(inst)
	if GLOBAL.TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "squid", "wall", "structure"}
	elseif GLOBAL.TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "wall", "structure"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "squid", "wall", "structure"}
	end
end

AddPrefabPostInit("hermit_pearl", function(inst)
	inst:AddComponent("edible")
	inst.components.edible.foodtype = GLOBAL.FOODTYPE.ELEMENTAL
	inst.components.edible.healthvalue = 10
    inst.components.edible.hungervalue = 20
    inst.components.edible.sanityvalue = 10
end)

AddPrefabPostInit("inksplat", function(inst)
	if not GLOBAL.TheWorld.ismastersim then
        return inst
    end

	inst.components.complexprojectile.onhitfn = function(inst, attacker, target)
		if attacker:HasTag("player") then
			local pt = inst:GetPosition()
			local ents = TheSim:FindEntities(pt.x, pt.y, pt.z, 1, {"_combat"}, GetInkExcludeTags(attacker))
			for i,ent in ipairs(ents) do
				if ent.components.locomotor then
					ent.AnimState:SetMultColour(101/255, 70/255, 163/255, 1)
					ent.components.locomotor:SetExternalSpeedMultiplier(ent, "speed_ink_debuff", 0.3)
					if ent.ink_debuff_task then
						ent.ink_debuff_task:Cancel()
						ent.ink_debuff_task = nil
					end
					ent.ink_debuff_task = ent:DoTaskInTime(5, function(inst)
						ent.AnimState:SetMultColour(1, 1, 1, 1)
						ent.components.locomotor:RemoveExternalSpeedMultiplier(ent, "speed_ink_debuff")
					end)
					if GLOBAL.TheNet:GetServerGameMode() == "lavaarena" then
						if attacker and not attacker.components.health:IsDead() and ent.components.combat and ent.components.combat.target == attacker then
							ent.components.combat.target = nil
							ent.components.combat:SetTarget(attacker) --Makes boarilla kitable with ink
						end
					end
				end
			end
		end	
		GLOBAL.SpawnPrefab("ink_splash").Transform:SetPosition(inst.Transform:GetWorldPosition())
		if inst:IsOnOcean() then
			GLOBAL.SpawnPrefab("ink_puddle_water").Transform:SetPosition(inst.Transform:GetWorldPosition())
		else
			GLOBAL.SpawnPrefab("ink_puddle_land").Transform:SetPosition(inst.Transform:GetWorldPosition())
		end
		local pt = GLOBAL.Vector3(inst.Transform:GetWorldPosition())
		local ents = GLOBAL.TheSim:FindEntities(pt.x, pt.y, pt.z, 1, nil, GetInkExcludeTags(attacker))
		for i,ent in ipairs(ents) do
			if ent.components.inkable then
				ent.components.inkable:Ink()
			end
		end
		
		

		inst:Remove()
	end
end)

------------------------------------------------------------------
-- Custom Recipes
------------------------------------------------------------------




--AddRecipe(prefab_name, ingredients, tab, level, placer, min_spacing, nil, numtogive, builder_tag, atlas, image)
-- Houses
--local citypigrecipe = AddRecipe("citypighousep", {Ingredient("boards", 10),Ingredient("cutstone", 6)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_TWO, "citypighousep_placer", nil, nil, 1, "civilised", "images/inventoryimages/citypighousep.xml", "citypighousep.tex")

--citypigrecipe.sortkey = -16.0

-- Add home crafting recipes if enabled

if MOBHOUSE ~= "Disable" then
	-- Spider nest
	local spiderhomerecipe = AddRecipe("moonspiderdenp", {Ingredient("silk", 4), Ingredient("moonglass", 2), Ingredient("rocks", 6)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "moonspiderdenp_placer", nil, nil, 1 ,nil, "images/inventoryimages/pprot_homes.xml", "spider_moon_homep.tex" )
	spiderhomerecipe.sortkey = -23	

	local dustmothden_recipe = AddRecipe2(
        "dustmothdenp", --the name of the recipe
        {Ingredient("rocks", 30)}, --ingredients
        GLOBAL.TECH.SCIENCE_NONE, --tech tier
        {
            atlas = "images/inventoryimages/pprot_homes.xml", 
            image = "dustmothdenp.tex",
            builder_tag = "dustmoth",
			placer = "dustmothdenp_placer",
			min_spacing = 0.1,
			--nounlock = true,
			--numtogive = 2,
			--testfn = function() end,
			--product = "oinc1p",
			--build_mode = "", -- idk what this is.
			--build_distance = 1, --idk what this does either.
        },{"STRUCTURES"} 
    )
end

------------------------------------------------------------------
-- Component Overrides
------------------------------------------------------------------


------------------------------------------------------------------
-- PostInits
------------------------------------------------------------------	
local State = GLOBAL.State
local TimeEvent = GLOBAL.TimeEvent
local EventHandler = GLOBAL.EventHandler
local FRAMES = GLOBAL.FRAMES


local clack_pre = State({
        name = "funnyidle_clack_pre",
        tags = { "idle", "canrotate", "alert" },

        onenter = function(inst)           
            inst.AnimState:PlayAnimation("idle_clack_pre")
        end,

        events =
        {
            EventHandler("animover", function(inst)            
                inst.sg:GoToState("funnyidle_clack")                
            end),
        },
    })
local clack_loop = State({
        name = "funnyidle_clack",
        tags = { "idle", "canrotate", "alert" },

        onenter = function(inst)
            inst.AnimState:PushAnimation("idle_clack_loop")   
        end,

        timeline = ----jason
        {
            TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent(29*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent((13+31)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent((29+31)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent((13+62)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent((29+62)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent((13+93)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent((29+93)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
        },
		
		events =
        {
            EventHandler("animover", function(inst)            
                inst.sg:GoToState("funnyidle_clack")                
            end),
        },
    })
local tango_pre = State({
        name = "funnyidle_tango_pre",
        tags = { "idle", "canrotate", "alert" },

        onenter = function(inst)           
            inst.AnimState:PlayAnimation("idle_tango_pre")
        end,

        events =
        {
            EventHandler("animover", function(inst)            
                inst.sg:GoToState("funnyidle_tango")                
            end),
        },
    })
local tango_loop = State({
        name = "funnyidle_tango",
        tags = { "idle", "canrotate", "dancing", "alert"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("idle_tango_loop", true)   
        end,

        timeline = 
        {
            TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent(44*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent(52*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent(60*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent(68*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),

            TimeEvent((3+81)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent((11+81)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent((19+81)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent((27+81)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent((44+81)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent((52+81)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent((60+81)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),
            TimeEvent((68+81)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/clap") end),            
        },
		
		events =
        {
            EventHandler("animover", function(inst)            
                inst.sg:GoToState("funnyidle_tango")                
            end),
        },
    })
AddStategraphState("wilson", clack_pre)
AddStategraphState("wilson_client", clack_pre)
AddStategraphState("wilson", clack_loop)
AddStategraphState("wilson_client", clack_loop)
AddStategraphState("wilson", tango_pre)
AddStategraphState("wilson_client", tango_pre)
AddStategraphState("wilson", tango_loop)
AddStategraphState("wilson_client", tango_loop)


GLOBAL.FOODTYPE.TRINKET = "TRINKET"

local function ForceBirdEating(inst, victim)
	if victim and victim.prefab == "malbatrossp" and victim.components.eater and not victim.cannoteat then
		--print("check passed")
		victim.sg:GoToState("eat")
		victim.cannoteat = true
		inst:DoTaskInTime(0.5, function(inst)	inst:Remove() end)
	end
end

local function MakeTrinketEdible(inst)
	if not inst.components.edible then --halloween candy and other food trinkets will not count
		inst:AddComponent("edible")
		inst.components.edible.foodtype = GLOBAL.FOODTYPE.TRINKET
		inst.components.edible.healthvalue = -100
		inst.components.edible.hungervalue = 10
		inst.components.edible.sanityvalue = -20
		
		if inst.components.inventoryitem then
			inst.components.inventoryitem.onpickupfn = ForceBirdEating
		end
	end
end

--Malbatross eating trinkets, I was paid for this...
for i = 1, GLOBAL.NUM_TRINKETS do
	AddPrefabPostInit("trinket_"..i, MakeTrinketEdible)
end

-------------------------------------------------------
--Wardrobe stuff--

--Skin Puppet Stuff
local MobPuppets = require("pprot_puppets")
local MobSkins = require("pprot_skins")
PlayablePets.RegisterPuppetsAndSkins(PPROT_Character_Order, MobPuppets, MobSkins)
------------------------------------------------------------------
-- Commands
------------------------------------------------------------------

------------------------------------------------------------------
-- Asset Population
------------------------------------------------------------------

local assetPaths = { "bigportraits/", "images/map_icons/", "images/avatars/avatar_", "images/avatars/avatar_ghost_" }
local assetTypes = { {"IMAGE", "tex"}, {"ATLAS", "xml"} }

-- Iterate through the player mob table and do the following:
-- 1. Populate the PrefabFiles table with the mob prefab names and their skin prefabs (if applicable)
-- 2. Add an atlas and image for the mob's following assets:
-- 2.1 Character select screen portraits
-- 2.2 Character map icons
-- 2.3 ??? FIXME
-- 2.4 ??? FIXME
--for prefab, mob in pairs(GLOBAL.PPROT_MobCharacters) do
for _, prefab in ipairs(PPROT_Character_Order) do
	local mob = GLOBAL.PPROT_MobCharacters[prefab]

	if PlayablePets.MobEnabled(mob, env.modname) then
		table.insert(PrefabFiles, prefab)
	end
	
	-- Add custom skin prefabs, if available
	-- Example: "dragonplayer_formal"
	for _, skin in ipairs(mob.skins) do
			table.insert(PrefabFiles, prefab.."_"..skin)
	end
	
	for _, path in ipairs(assetPaths) do
		for _, assetType in ipairs(assetTypes) do
			--print("Adding asset: "..assetType[1], path..prefab.."."..assetType[2])
			table.insert( Assets, Asset( assetType[1], path..prefab.."."..assetType[2] ) )
		end
	end
end

------------------------------------------------------------------
-- Mob Character Instantiation
------------------------------------------------------------------
-- Adds a mod character based on an individual mob
-- prefab is the prefab name (e.g. clockwork1player)
-- mob.fancyname is the mob's ingame name (e.g. Knight)
-- mob.gender is fairly self-explanatory
--for prefab, mob in pairs(GLOBAL.PPROT_MobCharacters) do
for _, prefab in ipairs(PPROT_Character_Order) do
	local mob = GLOBAL.PPROT_MobCharacters[prefab]
	PlayablePets.SetGlobalData(prefab, mob)
	--CurrentRelease.PrintID()
	if PlayablePets.MobEnabled(mob, env.modname) then
		AddMinimapAtlas("images/map_icons/"..prefab..".xml")
		AddModCharacter(prefab, mob.gender)		
	end
end