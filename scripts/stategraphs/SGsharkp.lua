require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "shark"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "shark"}
	end
end

local function groundsound(inst)
    local x,y,z = inst.Transform:GetWorldPosition()
    if inst:GetCurrentPlatform() then
        inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/boat_land")
    elseif TheWorld.Map:IsVisualGroundAtPoint(x,y,z) then
        PlayFootstep(inst)
    end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end
--Since Shark does not share a lot of animations between water and land bank
--I make these functions to ensure they use the best state for land/water actions.
local longaction =  function(inst) 
	if inst:HasTag("swimming") then
		return "attack"
	else
		return "action"
	end
end
local shortaction = function(inst) 
	if inst:HasTag("swimming") then
		return "attack"
	else
		return "action"
	end
end
local workaction = function(inst) 
	if inst:HasTag("swimming") then
		return "attack"
	else
		return "work"
	end
end
local otheraction = function(inst) 
	if inst:HasTag("swimming") then
		return "eat"
	else
		return "action"
	end
end



local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if inst:HasTag("swimming") then
			return "attack"
		else
			return "bite"
		end
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	---
	PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{
   State{
        name = "idle",
        tags = { "idle", "canrotate" },
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle", true)
        end,

        timeline =
        {
            TimeEvent(1*FRAMES,  function(inst) groundsound(inst) end),
            TimeEvent(11*FRAMES, function(inst) groundsound(inst) end),
            TimeEvent(23*FRAMES, function(inst) groundsound(inst) end),
            TimeEvent(34*FRAMES, function(inst) groundsound(inst) end),
        },
		
		events =
        {
            EventHandler("animover", function(inst)                 
                inst.sg:GoToState("idle") 
            end),
        }, 

    },
	
	State{
        name = "attack",
        tags = { "busy","attack"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack")
        end,
        timeline =
        {
            TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bark") end),
            TimeEvent(12*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bite")
                inst:PerformBufferedAction()
            end),
        },


        events =
        {
            EventHandler("animover", function(inst)                 
                inst.sg:GoToState("idle") 
            end),
        },        
    },
	
	State{
        name = "bite",
        tags = { "busy","attack"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack")
        end,
        timeline =
        {
            TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bark") end),
            TimeEvent(12*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bite")
                inst.components.combat:DoAreaAttack(inst, 4, nil, nil, nil, GetExcludeTags(inst))
            end),
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bark") end),
            TimeEvent(18*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bite")
                inst.components.combat:DoAreaAttack(inst, 4, nil, nil, nil, GetExcludeTags(inst))
            end),
            TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bark") end),
            TimeEvent(26*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bite")
                inst.components.combat:DoAreaAttack(inst, 4, nil, nil, nil, GetExcludeTags(inst))
            end),
        },


        events =
        {
            EventHandler("animover", function(inst)                 
                inst.sg:GoToState("idle") 
            end),
        },        
    },
	
	State{
        name = "work",
        tags = { "busy","attack"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack")
        end,
        timeline =
        {
            TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bark") end),
            TimeEvent(12*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bite")
                PlayablePets.DoWork(inst, 5)
            end),
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bark") end),
            TimeEvent(18*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bite")
                inst:PerformBufferedAction()
            end),
            TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bark") end),
            TimeEvent(26*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bite")
                inst:PerformBufferedAction()
            end),
        },


        events =
        {
            EventHandler("animover", function(inst)                 
                inst.sg:GoToState("idle") 
            end),
        },        
    },
	
	State{
        name = "hit",
        tags = { "busy", "hit" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/hit")
        end,

        events =
        {
        EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	State{
        name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
			if inst.shouldwalk then
				inst.shouldwalk = false
			else
				inst.shouldwalk = true
			end
			inst.sg:GoToState("idle")
        end,

        timeline =
        {

        },

        events =
        {

        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("dead",false)
            inst.AnimState:PushAnimation("dead_loop",false)
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/death") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "special_atk1",
        tags = { "busy","jumping" },

        onenter = function(inst)
			local ent = FindEntity(inst, 15, nil, {"oceanfish"})
			if inst:HasTag("swimming") and ent then
				inst:ClearBufferedAction()
				inst.foodtoeat = ent
				inst.components.locomotor:RunForward()
				inst.AnimState:PlayAnimation("dive")             
			else
				inst.sg:GoToState("idle")
			end
        end,

        onexit = function(inst)
            inst.Physics:SetActive(true)
        end,

        events =
        {
            EventHandler("animover", function(inst) 
                inst.Physics:SetActive(false) 
                inst.sg:SetTimeout(2) 
            end),
        },
        ontimeout = function(inst)
            local targetpt = Vector3(inst.Transform:GetWorldPosition())
            if inst.foodtoeat and inst.foodtoeat:IsValid() then
                targetpt = Vector3(inst.foodtoeat.Transform:GetWorldPosition())
            end
            inst.Transform:SetPosition(targetpt.x,0,targetpt.z)
            inst.sg:GoToState("eat_pst")
        end,   
    },    
	
	State{
        name = "eat",
        tags = { "busy","jumping" },

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat")
        end,

        timeline =
        {        
            TimeEvent(1*FRAMES, function(inst)
				inst:PerformBufferedAction()
            end),
            TimeEvent(7*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bite") 
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())
            end),         

            TimeEvent(30*FRAMES, function(inst) 
                if inst:HasTag("swimming") then 
                    SpawnPrefab("splash_green_large").Transform:SetPosition(inst.Transform:GetWorldPosition()) 
                else
                    groundsound(inst)
                end
            end),                       
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },   

    State{
        name = "eat_pst",
        tags = { "busy","jumping" },

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat")
        end,

        timeline =
        {        
            TimeEvent(1*FRAMES, function(inst)
                if inst.foodtoeat then
                    inst.foodtoeat:Remove()
                end
				inst.components.hunger:DoDelta(25, false)
                inst.foodtoeat = nil
            end),
            TimeEvent(7*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bite") 
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())
            end),         

            TimeEvent(30*FRAMES, function(inst) 
                if inst:HasTag("swimming") then 
                    SpawnPrefab("splash_green_large").Transform:SetPosition(inst.Transform:GetWorldPosition()) 
                else
                    groundsound(inst)
                end
            end),                       
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },    
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("dead",false)
            inst.AnimState:PushAnimation("dead_loop",false)
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline =
        {
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/death") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					if MOBGHOST== "Enable" then
                     inst:PushEvent(inst.ghostenabled and "makeplayerghost" or "playerdied", { skeleton = false })
					else
						TheWorld:PushEvent("ms_playerdespawnanddelete", inst)
					end
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst)
				PlayablePets.SleepHeal(inst)
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	-- RUN STATES START HERE
    State{
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)           
            inst.components.locomotor:RunForward()
			if inst:HasTag("swimming") then
				inst.AnimState:PlayAnimation("run_pre")
			else
				inst.AnimState:PlayAnimation("jump")     
			end
        end,

        onupdate = function(inst)
            --UpdateRunSpeed(inst)
        end,

        timeline=
        {

        },     

        onexit = function(inst)           

        end,        

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("run") 
            end),
        },
    },

    State{
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            if inst:HasTag("swimming") then
				inst.AnimState:PlayAnimation("run_loop", true)
			else
				inst.AnimState:PlayAnimation("jump_loop", false)
				inst.AnimState:PushAnimation("jump_loop") 
				inst.sg:SetTimeout(1)
			end
        end,  

        timeline =
		{
			TimeEvent(0, function(inst)
				-- inst.SoundEmitter:PlaySound(inst.sounds.growl)
				if inst:HasTag("swimming") then
					inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/jump_small",nil,.25)
					inst.waketask = inst:DoPeriodicTask(0.25, function()
						local wake = SpawnPrefab("wake_small")
						local rotation = inst.Transform:GetRotation()

						local theta = rotation * DEGREES
						local offset = Vector3(math.cos( theta ), 0, -math.sin( theta ))
						local pos = Vector3(inst.Transform:GetWorldPosition()) + offset
						wake.Transform:SetPosition(pos.x,pos.y,pos.z)
						wake.Transform:SetScale(1.35,1.36,1.35)

						wake.Transform:SetRotation(rotation - 90)
					end)
			
				end
	
			end),
			TimeEvent(4 * FRAMES, function(inst)
				if inst:HasTag("swimming") then
					inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/jump_small",nil,.25)
				end
			end),
		},	
		
		onexit = function(inst)
			if inst.waketask then
				inst.waketask:Cancel()
				inst.waketask = nil
			end
		end,
		
		--Shark jump ends
		ontimeout = function(inst)
			inst.sg:GoToState("run_stop")
		end,
		
		events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("run") 
            end),
        },
	},

    State{
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()            
			if inst:HasTag("swimming") then
				inst.AnimState:PlayAnimation("run_pst")
			else
				inst.AnimState:PlayAnimation("jump_pst")     
			end
        end,

        timeline =
        {
		
        },

        events =
        {
			EventHandler("animover", function(inst)
                inst.sg:GoToState("idle") 
            end),
        },
    },   
	
	-- RUN STATES START HERE
    State{
        name = "walk_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)           
            inst.components.locomotor:WalkForward()
			if inst:HasTag("swimming") then
				inst.AnimState:PlayAnimation("walk_pre")
			else
				inst.AnimState:PlayAnimation("idle")     
			end
        end,

        onupdate = function(inst)
            --UpdateRunSpeed(inst)
        end,

        timeline=
        {
            TimeEvent(1*FRAMES,  function(inst) groundsound(inst) end),
            TimeEvent(11*FRAMES, function(inst) groundsound(inst) end),
            TimeEvent(23*FRAMES, function(inst) groundsound(inst) end),
            TimeEvent(34*FRAMES, function(inst) groundsound(inst) end),
        },     

        onexit = function(inst)           

        end,        

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("walk") 
            end),
        },
    },

    State{
        name = "walk",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:WalkForward()
            if inst:HasTag("swimming") then
				inst.AnimState:PlayAnimation("walk_loop")
			else
				inst.AnimState:PlayAnimation("idle")
			end
        end,  

        timeline =
		{
			TimeEvent(1*FRAMES,  function(inst) groundsound(inst) end),
            TimeEvent(11*FRAMES, function(inst) groundsound(inst) end),
            TimeEvent(23*FRAMES, function(inst) groundsound(inst) end),
            TimeEvent(34*FRAMES, function(inst) groundsound(inst) end),
		},	
		
		events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("walk") 
            end),
        },
	},

    State{
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()            
			if inst:HasTag("swimming") then
				inst.AnimState:PlayAnimation("walk_pst")
			else
				inst.AnimState:PlayAnimation("idle")     
			end
        end,

        timeline =
        {
			TimeEvent(1*FRAMES,  function(inst) groundsound(inst) end),
            TimeEvent(11*FRAMES, function(inst) groundsound(inst) end),
            TimeEvent(23*FRAMES, function(inst) groundsound(inst) end),
            TimeEvent(34*FRAMES, function(inst) groundsound(inst) end),
        },

        events =
        {
			EventHandler("animover", function(inst)
                inst.sg:GoToState("idle") 
            end),
        },
    }, 
}
local function GetActionAnim(inst)
	return inst:HasTag("swimming") and "run_pst" or "sleep_pst"
end

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"sleep_pst", nil, nil, "idle", "hit") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/death") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bark") end),
            TimeEvent(12*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bite")
            end),
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bark") end),
            TimeEvent(18*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bite")
            end),
            TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bark") end),
            TimeEvent(26*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dangerous_sea/creatures/shark/bite")
            end),
		},
	
	},
	--anims = 
	{
		corpse = "dead",
		corpse_taunt = "attack"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "idle")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddHomeState(states, nil, "idle", "idle", true)
CommonStates.AddHopStates(states, false, {pre = "jump", loop = "jump_loop", pst = "jump_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "scared_pre",
	plank_idle_loop = "scared_loop",
	plank_idle_pst = "scared_pst",
	
	plank_hop_pre = "jump",
	plank_hop = "jump_loop",
	
	steer_pre = "sleep_pst",
	steer_idle = "idle",
	steer_turning = "sleep_pst",
	stop_steering = "sleep_pst",
	
	row = "sleep_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "jump_pst",
	
	leap_pre = "jump",
	leap_loop = "jump_loop",
	leap_pst = "jump_pst",
	
	lunge_pre = "jump",
	lunge_loop = "jump_loop",
	lunge_pst = "jump_pst",
	
	superjump_pre = "jump",
	superjump_loop = "jump_loop",
	superjump_pst = "jump_pst",
	
	castspelltime = 10,
})
    
CommonStates.AddAmphibiousCreatureHopStates(states, 
{ -- config
	swimming_clear_collision_frame = 9 * FRAMES,
},
{ -- anims
},
{ -- timeline
	hop_pre =
	{
		TimeEvent(0, function(inst) 
			if inst:HasTag("swimming") then 
				SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition()) 
			end
		end),
	},
	hop_pst = {
		TimeEvent(4 * FRAMES, function(inst) 
			if inst:HasTag("swimming") then 
				inst.components.locomotor:Stop()
				SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())                                         
			end
            groundsound(inst)
		end),
		TimeEvent(6 * FRAMES, function(inst) 
			if not inst:HasTag("swimming") then 
                inst.components.locomotor:StopMoving()
			end
		end),
	}
})
	
return StateGraph("sharkp", states, events, "idle", actionhandlers)

