require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if not inst.noactions then
			if inst.willswoop then
				return "swoop_pre"
			else
				return "attack"
			end	
		else
			return "failaction"
		end
	end),
	ActionHandler(ACTIONS.EAT, function(inst)
		if inst.cannoteat then
			return "special_atk1"
		else
			return "eat"
		end	
	end),
	ActionHandler(ACTIONS.MALBATROSS_TELEPORT, "dive"),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "failaction"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end
------------------------------------
--FNs--
local SHAKE_DIST = 40

local function swoopcollision(inst)
	if TheNet:GetServerGameMode() ~= "lavaarena" then
		inst.Physics:ClearCollisionMask()
	end
end

local function resetcollision(inst)
	inst.Physics:CollidesWith((TheWorld.has_ocean and COLLISION.GROUND) or COLLISION.WORLD)
	inst.Physics:CollidesWith(COLLISION.FLYERS)    
end


local function spawnripple(inst)
    if not TheWorld.Map:IsVisualGroundAtPoint(inst.Transform:GetWorldPosition()) and not inst._isflying and TheNet:GetServerGameMode() ~= "lavaarena" then
        inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/ripple")
        --SpawnPrefab("malbatross_ripple").Transform:SetPosition(inst.Transform:GetWorldPosition()) --Doesn't exist?
    end
end

local function spawnsplash(inst, scale, pos)
	if TheNet:GetServerGameMode() ~= "lavaarena" then
		local splashpos = Vector3(inst.Transform:GetWorldPosition())
		if pos then
			splashpos = pos
		end
		if not TheWorld.Map:IsVisualGroundAtPoint(splashpos.x,splashpos.y,splashpos.z) and not TheWorld.Map:GetPlatformAtPoint(splashpos.x,splashpos.z) then
			local fx = SpawnPrefab("splash_green")
			fx.Transform:SetPosition(splashpos.x,splashpos.y,splashpos.z)
			if scale then
				fx.Transform:SetScale(scale,scale,scale)
			end
		end
	end
end

local function spawnwave(inst, time)
    inst.spawnwaves(inst, 12, 360, 4, nil, 2, time or 2, nil, true)
end

local ATTACK_WAVE_SPEED = 5
local ATTACK_WAVE_IDLE_TIME = 1.5

local function wavesetup(inst,point,angle,drift)
    drift = drift or 0
    if not TheWorld.Map:IsVisualGroundAtPoint(point.x,point.y,point.z) and not TheWorld.Map:GetPlatformAtPoint(point.x,point.y,point.z) then
        SpawnAttackWave(point, angle, {ATTACK_WAVE_SPEED,0,drift}, nil, ATTACK_WAVE_IDLE_TIME, true)
    end
end

local function SpawnMalbatrossAttackWaves(inst)                

    local position = inst:GetPosition()
    local angle = inst:GetRotation() + (math.random()*20 -10)

    print(inst:GetRotation(),angle)

    local offset_direction1 = Vector3(math.cos(angle*DEGREES), 0, -math.sin(angle*DEGREES)):Normalize() *1
    local offset_direction2 = Vector3(math.cos((angle + 35)*DEGREES), 0, -math.sin((angle + 35)*DEGREES)):Normalize() *3
    local offset_direction3 = Vector3(math.cos((angle - 35)*DEGREES), 0, -math.sin((angle - 35)*DEGREES)):Normalize() *3

    local offset_direction4 = Vector3(math.cos(angle*DEGREES), 0, -math.sin(angle*DEGREES)):Normalize() *3.5

    local point = position+offset_direction4
    local platform = TheWorld.Map:GetPlatformAtPoint(point.x,point.y,point.z)
    if platform then
            local boat_physics = platform.components.boatphysics
            local theta = inst.Transform:GetRotation() * DEGREES
            local offset = Vector3(1 * math.cos( theta ), 0, -1 * math.sin( theta ))
            boat_physics:ApplyForce(offset.x, offset.z, TUNING.MALBATROSS_BOAT_PUSH)

            if platform.components.hullhealth then
                platform.components.health:DoDelta(-TUNING.MALBATROSS_BOAT_DAMAGE)
            end
    elseif not TheWorld.Map:IsVisualGroundAtPoint(point.x,point.y,point.z) then
        spawnsplash(inst, 3, position+offset_direction4)

        wavesetup(inst,position+offset_direction1,angle)
        wavesetup(inst,position+offset_direction2,angle,-1)
        wavesetup(inst,position+offset_direction3,angle,1)        
    end
end

local function land_without_floater(creature)
    creature:RemoveTag("flying")
    if creature.Physics ~= nil then
        creature.Physics:CollidesWith(COLLISION.LIMITS)
        creature.Physics:ClearCollidesWith(COLLISION.FLYERS)
    end
end

local function raise_without_floater(creature)
    creature:AddTag("flying")
    if creature.Physics ~= nil then
        creature.Physics:ClearCollidesWith(COLLISION.LIMITS)
        creature.Physics:CollidesWith(COLLISION.FLYERS)
    end
end
-------------------------------------

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	--PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
	--CommonHandlers.OnHop(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local function go_to_idle(inst)
    inst.sg:GoToState("idle")
end

 local states=
{
   State{
        name = "idle",
        tags = { "idle", "canrotate" },

        onenter = function(inst, pushanim)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle_loop")
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, spawnripple),
            TimeEvent(10*FRAMES, function(inst) if not inst._isflying then inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/flap") end end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

	 State{
        name = "special_atk1",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

        timeline =
        {
            TimeEvent(5 * FRAMES, function(inst)
				if not inst.cannoteat then
					inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/taunt") 
				end	
			end),
            TimeEvent(8 * FRAMES, spawnripple),
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },
	
	State{
        name = "attack",
        tags = {"busy", "attack"},

        onenter = function(inst)
			inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("atk")
        end,

        timeline =
        {
            TimeEvent(0 * FRAMES, function(inst) 
				inst.sg:AddStateTag("longattack")
				inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/attack_call") 
			end),
			TimeEvent(13 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/attack_swipe_water") end),

			TimeEvent(29 * FRAMES, function(inst)
				if TheNet:GetServerGameMode() == "lavaarena" then
					local buffaction = inst:GetBufferedAction()
					local target = buffaction ~= nil and buffaction.target or nil
					inst.components.combat.areahitdisabled = true
					if inst.bufferedaction.action ~= ACTIONS.ATTACK then
						PlayablePets.DoWork(inst, 8)
					else
						inst.components.combat:DoAttack(target)
						inst.components.combat:DoAreaAttack(target and target or inst, 5, nil, nil, nil, {"player", "companion"})
					end
				else
					inst:PerformBufferedAction()
				end

				SpawnMalbatrossAttackWaves(inst)                        
			end),       
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },
	
	State{
        name = "hit",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("hit")
        end,

        timeline =
        {
            TimeEvent(0 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/hit") end),          
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },
	
	State{
        name = "land",
        tags = {"busy"},

        onenter = function(inst)
			if not TheWorld.Map:IsVisualGroundAtPoint(inst:GetPosition():Get()) and not TheWorld.Map:GetPlatformAtPoint(inst:GetPosition():Get()) then
				inst.sg:GoToState((math.random(1,10) > 5 or inst.cannoteat) and "combatdive_pst" or "eatfish")
			else
			if TheNet:GetServerGameMode() == "lavaarena" then
				if inst._mustlandtask then
					inst._mustlandtask:Cancel()
					inst._mustlandtask = nil
				end
			end
			inst:Show()
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("spawn")
			inst:RemoveTag("noplayerindicator")
			inst.DynamicShadow:Enable(true)
			inst.shouldwalk = true
			inst.noactions = nil
			end
        end,
		
		onexit = function(inst)
			if inst.flytask then
				inst.flytask:Cancel()
				inst.flytask = nil
				inst.flytask = inst:DoTaskInTime(TheNet:GetServerGameMode() == "lavaarena" and PPROT_FORGE.MALBATROSS.SPECIAL_CD or 10, function(inst) inst.taunt2 = true end)
			else
				inst.flytask = inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
			end	
			inst.components.health:SetInvincible(false)
			inst._isflying = nil
		end,

        timeline =
        {
            TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/whoosh") end),          
            TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/flap") end),
            TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/flap") end),            
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },

    State{
        name = "depart",
        tags = {"busy", "nosleep", "swoop"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.combat:DropTarget()
            inst.AnimState:PlayAnimation("despawn")
        end,

        timeline =
        {
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/flap") end),
            TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/flap") end),            
            TimeEvent(32 * FRAMES, function(inst)
                inst.sg:AddStateTag("noattack")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                TheWorld.components.malbatrossspawner:Relocate(inst)
            end),
        },
    },
	
	State{
        name = "special_atk2",
        tags = {"busy", "nosleep", "swoop"},

        onenter = function(inst)
			if not inst.taunt2 then
				inst.sg:GoToState("idle")
			else
				inst.components.locomotor:Stop()
				inst.AnimState:PlayAnimation("despawn")
				inst.components.health:SetInvincible(true)
				inst:AddTag("noplayerindicator")
				inst.taunt2 = false
				inst.noactions = true
				inst._isflying = true
				inst.shouldwalk = false
				if TheNet:GetServerGameMode() == "lavaarena" then
					if inst._mustlandtask then
						inst._mustlandtask:Cancel()
						inst._mustlandtask = nil
					end
					inst._mustlandtask = inst:DoTaskInTime(6, function(inst) inst.sg:GoToState("land") end)
				end
				if not TheWorld.Map:IsVisualGroundAtPoint(inst:GetPosition():Get()) and not TheWorld.Map:GetPlatformAtPoint(inst:GetPosition():Get()) then
					inst.sg:GoToState("combatdive")
				end
			end
        end,

        timeline =
        {
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/flap") end),
            TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/flap") end),            
            TimeEvent(32 * FRAMES, function(inst)
                inst.sg:AddStateTag("noattack")
            end),
        },
		
		onexit = function(inst)
			inst.DynamicShadow:Enable(false)
			inst:Hide()
		end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	 State{
        name = "dive",
        tags = { "busy", "nosleep", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            local ba = inst:GetBufferedAction()
            inst.sg.statemem.tele_pos = (ba ~= nil and ba.pos) or nil

            inst:AddTag("scarytooceanprey")
            inst.AnimState:PlayAnimation("dive")
        end,

        onexit = function(inst)
            inst:RemoveTag("scarytooceanprey")
        end,

        timeline =
        {
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/flap") end),
            TimeEvent(19*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/boss")
                spawnsplash(inst, 4)
                spawnwave(inst, 1)
                inst.sg:AddStateTag("noattack")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                local got_fish = false
                if math.random() < TUNING.MALBATROSS_EATSUCCESS_CHANCE then
                    got_fish = true
                end

                if got_fish then
                    inst.sg:GoToState("eatfish", inst.sg.statemem.tele_pos)
                else
                    inst.sg:GoToState("nofish")
                end
            end),
        },
    },

    State{
        name = "nofish",
        tags = { "busy", "nosleep", "noattack" },

        onenter = function(inst)
			inst.components.health:SetInvincible(false)
			if inst.sg.statemem.tele_pos then
				inst.Physics:Teleport(inst.sg.statemem.tele_pos:Get())
			else
				print("ERROR: No position was obtained for MALBATROSS_TELEPORT action")
			end
            inst.AnimState:PlayAnimation("nofish")
        end,

        timeline =
        {
            TimeEvent(1 * FRAMES, function(inst)
                spawnsplash(inst, 4)
                inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/jump_boss")
            end),
            TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/whoosh") end),
            TimeEvent(4*FRAMES, function(inst) inst.sg:RemoveStateTag("noattack") end),
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },

    State{
        name = "eatfish",
        tags = { "busy", "nosleep", "noattack" },

        onenter = function(inst)
			inst.components.health:SetInvincible(false)
			inst:Show()
			inst.DynamicShadow:Enable(true)
            -- NOTE: we assume we were given a valid fish to eat; validity should be tested before entering this state.
			--if inst.sg.statemem.tele_pos then
				--inst.Physics:Teleport(inst.sg.statemem.tele_pos:Get())
			--else
				--print("ERROR: No position was obtained for MALBATROSS_TELEPORT action")
			--end
            inst.AnimState:PlayAnimation("eatfish")
			
            inst.AnimState:OverrideSymbol("shoal_body", "oceanfish_medium_4", "shoal_body")
            inst.AnimState:OverrideSymbol("shoal_fin", "oceanfish_medium_4", "shoal_fin")
            inst.AnimState:OverrideSymbol("shoal_head", "oceanfish_medium_4", "shoal_head")
			inst:RemoveTag("noplayerindicator")
			inst.DynamicShadow:Enable(true)
			inst.shouldwalk = true
			inst.noactions = nil
        end,

        onexit = function(inst)
            inst.AnimState:ClearOverrideSymbol("shoal_body")
            inst.AnimState:ClearOverrideSymbol("shoal_fin")
            inst.AnimState:ClearOverrideSymbol("shoal_head")
			if inst.flytask then
				inst.flytask:Cancel()
				inst.flytask = nil
				inst.flytask = inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
			else
				inst.flytask = inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
			end	
			inst.components.health:SetInvincible(false)
			inst._isflying = nil
        end,

        timeline =
        {
            TimeEvent(1*FRAMES, function(inst)
                spawnsplash(inst, 4)
				spawnwave(inst, 1)
                inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/jump_boss")
            end),
            TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/whoosh") end),
            TimeEvent(4*FRAMES, function(inst) inst.sg:RemoveStateTag("noattack") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/eat") end),
            TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/beak")
				inst.components.health:DoDelta(20)
				inst.components.hunger:DoDelta(30)
			end),
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },
	
	State{
        name = "eat",
        tags = { "busy", "nosleep", "noattack" },

        onenter = function(inst, fish_to_eat)
            inst.AnimState:PlayAnimation("eatfish")
			inst.AnimState:SetTime(19*FRAMES)
        end,

        onexit = function(inst)
            inst.AnimState:ClearOverrideSymbol("shoal_body")
            inst.AnimState:ClearOverrideSymbol("shoal_fin")
            inst.AnimState:ClearOverrideSymbol("shoal_head")
        end,

        timeline =
        {
            TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/eat") end),
            TimeEvent(21*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/beak") inst:PerformBufferedAction() end),
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },
	
	State{
        name = "swoop_pre",
        tags = {"busy", "canrotate", "swoop"},

        onenter = function(inst, target)
			if TheNet:GetServerGameMode() == "lavaarena" then
				inst.components.health:SetInvincible(true)
			end
			inst.willswoop = false
            inst.Physics:Stop()

            inst.sg.statemem.target = target

            inst.AnimState:PlayAnimation("swoop_pre")
        end,

        onupdate = function(inst)
            if not inst.sg.statemem.stopsteering and inst.sg.statemem.target and inst.sg.statemem.target:IsValid() then
                inst:ForceFacePoint(Vector3(inst.sg.statemem.target.Transform:GetWorldPosition()))
            end
        end,

        timeline =
        {   
            TimeEvent(11 * FRAMES, function(inst) inst.sg.statemem.stopsteering = true end),
            TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/swoop_pre") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("swoop_loop") end),
        },
    },

    State{
        name = "swoop_loop",
        tags = {"busy", "swoop"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("swoop_loop", true)
            inst.Physics:SetMotorVelOverride(15,0,0)
            inst.sg:SetTimeout(TheNet:GetServerGameMode() == "lavaarena" and 1 or 1.5)
            inst.sg.statemem.collisiontime = 0
        end,

        onupdate = function(inst, dt)
            local INTERVAL = 3/30
            if inst.sg.statemem.collisiontime <= 0 then
                local x,y,z = inst.Transform:GetWorldPosition()
                local ents = TheSim:FindEntities(x, y, z, 2, nil, GetExcludeTags(inst))
                for i,ent in ipairs(ents) do
                    inst.oncollide(inst,ent)
                end

                spawnripple(inst)

                inst.sg.statemem.collisiontime = INTERVAL
            end
            inst.sg.statemem.collisiontime = inst.sg.statemem.collisiontime - dt
        end,

        timeline =
        {   
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/swoop") end),
        },

        onexit = function(inst)
			inst.components.combat:SetRange(TUNING.MALBATROSS_ATTACK_RANGE)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.Physics:ClearMotorVelOverride()
            inst.components.locomotor:Stop()
        end,

        ontimeout=function(inst)
            inst.sg:GoToState("swoop_pst")
        end,
    },

    State{
        name = "swoop_pst",
        tags = {"busy", "swoop"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("swoop_pst")
        end,
		
		onexit = function(inst)
			if TheNet:GetServerGameMode() == "lavaarena" then
				inst.components.health:SetInvincible(false)
			end
			if inst._swooptask then
				inst._swooptask:Cancel()
				inst._swooptask = nil
			end
			inst._swooptask = inst:DoTaskInTime(TheNet:GetServerGameMode() == "lavaarena" and PPROT_FORGE.MALBATROSS.SWOOP_CD or 15, function(inst) 
				if inst.components.health:GetPercent() < 0.80 or TheNet:GetServerGameMode() == "lavaarena" then
					inst.willswoop = true
					inst.components.combat:SetRange(8)
				end
			end)
		end,

        timeline=
        {
            TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/flap") end),
            TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/flap") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "wavesplash",
        tags = { "attack", "busy" },

        onenter = function(inst, target)
            if target then
                inst:ForceFacePoint(Vector3(target.Transform:GetWorldPosition()))
            end
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("atk")
            inst.components.combat:StartAttack()
            inst.sg.statemem.target = target
        end,

        timeline =
        {   
            TimeEvent(8 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/attack_call") end),
            TimeEvent(12 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/attack_swipe_water")
                inst.components.combat:DoAttack(inst.sg.statemem.target)

                SpawnMalbatrossAttackWaves(inst)                
            end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end)
        },
    },

    State{
        name = "combatdive",
        tags = { "busy", "nosleep"},

        onenter = function(inst)
			inst.taunt2 = false
			inst.noactions = true
			inst._isflying = true
			inst.shouldwalk = false
			inst:Show()
			inst.DynamicShadow:Enable(true)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("dive")
			
        end,

        timeline =
        {
            TimeEvent(19 * FRAMES, function(inst)
                spawnsplash(inst, 4)
                spawnwave(inst, 1)
                inst.DynamicShadow:Enable(false)
                inst.sg:AddStateTag("noattack")
            end),
        },
		
		onexit = function(inst)
			inst.DynamicShadow:Enable(false)
			inst:Hide()
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("combatdive_pst")
        end,
        
        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end)
        },
    },

    State{
        name = "combatdive_pst",
        tags = { "busy", "nosleep", "noattack" },

        onenter = function(inst)
			inst:Show()
            inst.AnimState:PlayAnimation("nofish")
			inst:RemoveTag("noplayerindicator")
			inst.DynamicShadow:Enable(true)
			inst.shouldwalk = true
			inst.noactions = nil
        end,

        onexit = function(inst)
			if inst.flytask then
				inst.flytask:Cancel()
				inst.flytask = nil
			end	
            inst.flytask = inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
			inst.components.health:SetInvincible(false)
			inst._isflying = nil
		end,

        timeline =
        {
            TimeEvent(1 * FRAMES, function(inst) 
                spawnsplash(inst, 4)
                spawnwave(inst, 1)
                inst.DynamicShadow:Enable(true)
            end),

            TimeEvent(4*FRAMES, function(inst) inst.sg:RemoveStateTag("noattack") end),
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },

    State{
        name = "alert",
        tags = { "idle", "canrotate", "alert" },

        onenter = function(inst, pushanim)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle_loop")
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, spawnripple),
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/flap") end),
        },

        events=
        {
            EventHandler("animover", function(inst)
                local target = nil
                if inst.components.combat and inst.components.combat.target then
                    target = inst.components.combat.target
                end
                if math.random() < 0.2 then
                    inst.sg:GoToState("taunt")
                elseif not inst.components.timer:TimerExists("splashdelay") and target then  
                    inst.components.timer:StartTimer("splashdelay", math.random()*2 +8)
                    inst.sg:GoToState("wavesplash",target)
                else
                    inst.sg:GoToState("alert")
                end
            end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.cannoteat = nil
			if TheWorld.Map:IsVisualGroundAtPoint(inst.Transform:GetWorldPosition()) or inst:GetCurrentPlatform() then
				inst.AnimState:PlayAnimation("death")
				inst.AnimState:PushAnimation("death_idle", false)
			else
				inst.AnimState:PlayAnimation("death_ocean")
				inst.AnimState:PushAnimation("death_ocean_idle", false)
			end
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline =
        {   

            TimeEvent(0 * FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/death") 
			end),
            TimeEvent(33 * FRAMES, function(inst) inst.spawnfeather(inst,0.4) end),
            TimeEvent(34 * FRAMES, function(inst)
                if math.random() < 0.3 then
                    inst.spawnfeather(inst,0.4)
                end
            end),
            TimeEvent(35 * FRAMES, function(inst) inst.spawnfeather(inst,0.45) end),
            TimeEvent(36 * FRAMES, function(inst)
                if math.random() < 0.3 then 
                    inst.spawnfeather(inst,0.4)
                end
            end),
            TimeEvent(38 * FRAMES, function(inst) inst.spawnfeather(inst,0.45) end),
            TimeEvent(42 * FRAMES, function(inst)
				if TheWorld.Map:IsVisualGroundAtPoint(inst.Transform:GetWorldPosition()) or inst:GetCurrentPlatform() then
				
				else
					spawnsplash(inst,5)
					spawnwave(inst)
					inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/boss")
				end	
            end),
			TimeEvent(44 * FRAMES, function(inst) 
				inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
				if TheWorld.Map:IsVisualGroundAtPoint(inst.Transform:GetWorldPosition()) or inst:GetCurrentPlatform() then
					ShakeAllCameras(CAMERASHAKE.FULL, .7, .02, 2, inst, SHAKE_DIST) 
					inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
				end
			end),

        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,
		
		timeline =
		{
			TimeEvent(1 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/jump_boss") end),
			TimeEvent(35 * FRAMES, function(inst)
				land_without_floater(inst)
				if not inst:IsOnPassablePoint() then
					inst.AnimState:PlayAnimation("sleep_ocean_pre")
					inst.AnimState:SetTime(36*FRAMES)
				end
			end),
		},
		
		onexit = raise_without_floater,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("sleep_loop")
				land_without_floater(inst)
				if not inst:IsOnPassablePoint() then
					inst.AnimState:PlayAnimation("sleep_ocean_loop")
				end
			end,
			
		onexit = raise_without_floater,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) 
				PlayablePets.SleepHeal(inst)
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
			land_without_floater(inst)
			if not inst:IsOnPassablePoint() then
				inst.AnimState:PlayAnimation("sleep_ocean_pst")
			end
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,
		
		onexit = raise_without_floater,
		
		timeline =
		{
			TimeEvent(28 * FRAMES, spawnripple),
			TimeEvent(44 * FRAMES, raise_without_floater),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}

CommonStates.AddFrozenStates(states, LandFlyingCreature, RaiseFlyingCreature)

CommonStates.AddWalkStates(states,
{
    starttimeline =
    {
        TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/flap") end),
        TimeEvent(9 * FRAMES, spawnripple),       
    },
    walktimeline =
    {
        TimeEvent(33 * FRAMES, spawnripple),
        TimeEvent(37*FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/flap") end),        
    },
    endtimeline =
    {

    },
})

CommonStates.AddRunStates(states,
{
    starttimeline =
    {
      
    },
    walktimeline =
    {
    
    },
    endtimeline =
    {

    },
})
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0 * FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/death") 
			end),
			TimeEvent(44 * FRAMES, function(inst) 
				ShakeAllCameras(CAMERASHAKE.FULL, .7, .02, 2, inst, SHAKE_DIST) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(5 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("saltydog/creatures/boss/malbatross/taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk_loop", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle_loop",
	plank_idle_loop = "idle_loop",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk_loop",
	
	steer_pre = "walk_pst",
	steer_idle = "idle_loop",
	steer_turning = "walk_pst",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
    
return StateGraph("malbatrossp", states, events, "idle", actionhandlers)

