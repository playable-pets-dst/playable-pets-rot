require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, ""),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    EventHandler("locomote", function(inst) 
        if not inst.sg:HasStateTag("busy") then
			local is_moving = inst.sg:HasStateTag("moving")
			local wants_to_move = inst.components.locomotor:WantsToMoveForward()
			if is_moving ~= wants_to_move then
				if wants_to_move then
					inst.sg:GoToState("moving")
				else
					inst.sg:GoToState("idle")
				end
			end
        end
    end), 
    CommonHandlers.OnFreeze(),
	--CommonHandlers.OnHop(),
	--[[
	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),]]
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	--PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{
   State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_flight_loop", true)
        end,

    },
	
	State{
        name = "moving",
        tags = {"moving", "canrotate"},
        
        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("flight_cycle", true)
        end,
    },

    State{
        name = "attack",
        tags = {"attack"},
		
		onenter = function(inst, target)
			inst.AnimState:PlayAnimation("flight_cycle")
        end,
	
		timeline=
		{

		},

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {

        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("take_off")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
			if IsOceanTile(TheWorld.Map:GetTileAtPoint(inst:GetPosition():Get())) then
				inst.sg:GoToState("idle")
			else
				if inst.components.locomotor ~= nil then
					inst.components.locomotor:StopMoving()
				end
				inst.AnimState:PlayAnimation("land")
			end
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
			PlayablePets.SleepHeal(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("idle", true)
		end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
		
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("take_off")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}

CommonStates.AddFrozenStates(states)
--PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"idle_flight_loop", nil, nil, "idle_flight_loop", "idle_flight_loop") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
		
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.components.health:SetPercent(1) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "take_off"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "idle_flight_loop")
PP_CommonStates.AddOpenGiftStates(states, "idle_flight_loop")
CommonStates.AddHopStates(states, false, {pre = "flight_cycle", loop = "flight_cycle", pst = "flight_cycle"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "land",
	plank_idle_loop = "idle",
	plank_idle_pst = "take_off",
	
	plank_hop_pre = "idle",
	plank_hop = "idle",
	
	steer_pre = "land",
	steer_idle = "idle",
	steer_turning = "taunt",
	stop_steering = "take_off",
	
	row = "flight_cycle",
}
)
    
return StateGraph("butterfly_moonp", states, events, "idle", actionhandlers)

