require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return { "crabking_ally", "INLIMBO", "notarget", "noattack", "flight", "invisible", "playerghost" }
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion",  "crabking_ally", "INLIMBO", "notarget", "noattack", "flight", "invisible", "playerghost" }
	else	
		return {"player", "companion",  "crabking_ally", "INLIMBO", "notarget", "noattack", "flight", "invisible", "playerghost" }
	end
end

local function PlaySound(inst, event)
    inst:PlaySound(event)
end

local function OnAnimOver(state)
    return {
        EventHandler("animover", function(inst) inst.sg:GoToState(state) end),
    }
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.CAST_NET, longaction),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local AOE_RANGE_PADDING = 3
local AOE_TARGET_MUSTHAVE_TAGS = { "_combat" }
local AOE_TARGET_CANT_TAGS = { "crabking_ally", "INLIMBO", "notarget", "noattack", "flight", "invisible", "playerghost" }

local function AOEAttack(inst, dist, radius, targets)
    inst.components.combat.ignorehitrange = true

    local x, y, z = inst.Transform:GetWorldPosition()
    local cos_theta, sin_theta

    if dist ~= 0 then
        local theta = inst.Transform:GetRotation() * DEGREES
        cos_theta = math.cos(theta)
        sin_theta = math.sin(theta)

        x = x + dist * cos_theta
        z = z - dist * sin_theta
    end

    for i, v in ipairs(TheSim:FindEntities(x, y, z, radius + AOE_RANGE_PADDING, AOE_TARGET_MUSTHAVE_TAGS, GetExcludeTags(inst))) do
        if v ~= inst and
            not (targets and targets[v]) and
            v:IsValid() and not v:IsInLimbo() and
            not (v.components.health and v.components.health:IsDead())
        then
            local range = radius + v:GetPhysicsRadius(0)
            local x1, y1, z1 = v.Transform:GetWorldPosition()
            local dx = x1 - x
            local dz = z1 - z

            if dx * dx + dz * dz < range * range and inst.components.combat:CanTarget(v) then
                inst.components.combat:DoAttack(v)

                if targets then
                    targets[v] = true
                end
            end
        end
    end

    inst.components.combat.ignorehitrange = false
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),
	---
	PP_CommonHandlers.OpenGift(),
	--PP_CommonHandlers.OnSink(),
    EventHandler("onsink", function(inst)
        inst.sg:GoToState("dive")
    end),
	PP_CommonHandlers.OnKnockback(),
	EventHandler("locomote", function(inst)
        if not inst.sg:HasStateTag("busy") then
            local is_moving = inst.sg:HasStateTag("moving")
            local wants_to_move = inst.components.locomotor:WantsToMoveForward()
            if not inst.sg:HasStateTag("attack") and is_moving ~= wants_to_move then
                if wants_to_move then
                    inst.sg:GoToState("premoving")
                else
                    inst.sg:GoToState("idle")
                end
            end
        end
    end),
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

------------------------------------------------------------------------------------------------------------------------------------

local function PlaySound(inst, event)
    inst:PlaySound(event)
end

local function OnAnimOver(state)
    return {
        EventHandler("animover", function(inst) inst.sg:GoToState(state) end),
    }
end

local WALK_SOUND_NAME = "footstepsound"

------------------------------------------------------------------------------------------------------------------------------------

local states=
{
    State{
        name = "premoving",
        tags = {"moving", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
        end,

        events = OnAnimOver("moving"),
    },

    State{
        name = "moving",
        tags = {"moving", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PushAnimation("walk_loop")

            if not inst.SoundEmitter:PlayingSound(WALK_SOUND_NAME) then
                inst:PlaySound("walk", WALK_SOUND_NAME)
            end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg.statemem.keepmoving = true
                    inst.sg:GoToState("moving")
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.keepmoving then
                inst.SoundEmitter:KillSound(WALK_SOUND_NAME)
            end
        end,
    },

    State{
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("idle", true)
        end,
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst:PlaySound("death_vocal")
            inst:PlaySound("death_fx") 
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State{
        name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,
        timeline=
        {
            FrameEvent(14, function(inst)  inst:PlaySound("taunt_fx_f14") end),
        },
        events = OnAnimOver("idle"),
    },
	
    State{
        name = "attack",
        tags = {"attack", "busy"},

        onenter = function(inst, target)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("atk1")
            inst:PlaySound("atk_vocal")
            

            inst.components.combat:StartAttack()
            inst.sg.statemem.target = target
        end,

        timeline=
        {
            --SoundFrameEvent(10, "dontstarve/creatures/spider/attack"),
            --SoundFrameEvent(10, "dontstarve/creatures/spider/attack_grunt"),
            FrameEvent(18, function(inst) 
                    inst:PlaySound("f18_atk_fx")
                    
                    PlayablePets.DoWork(inst, inst:HasTag("largecreature") and 3 or 2)
                end),
        },

        events = OnAnimOver("idle"),
    },

    State{
        name = "pp_rangedspecial",
        tags = {"attack", "busy", "spinning", "jumping"},

        onenter = function(inst, pos)
            if inst._rs_cooldown then
                inst.sg:GoToState("idle")
                return
            end
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)

            inst.AnimState:PlayAnimation("atk_pre")

            inst.components.combat:StartAttack()

            if pos then
                inst:ForceFacePoint(pos.x, 0, pos.z)
            end

            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
        end,

        onexit = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.Physics:ClearMotorVelOverride()
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("spin_attack_loop")
        end,
    },

    State{
        name = "spin_attack_loop",
        tags = {"attack", "canrotate", "busy", "spinning", "jumping"},

        onenter = function(inst, targets)
            inst.sg:SetTimeout(0.7)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)
            inst.Physics:SetMotorVelOverride(8,0,0)
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk_loop",true)

            inst.SoundEmitter:PlaySound("meta4/crabcritter/atk2_spin_lp","spin")
            
            inst.sg.statemem.targets = targets or {}
        end,

        onupdate = function(inst, dt)
            AOEAttack(inst, -0.4, 2.5, inst.sg.statemem.targets)
        end,

        onexit = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.Physics:ClearMotorVelOverride()
            inst.SoundEmitter:KillSound("spin")
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("spin_attack_pst")
        end,
    },

    State{
        name = "spin_attack_pst",
        tags = {"attack", "canrotate", "busy", "spinning"},

        onenter = function(inst, target)
            inst.AnimState:PlayAnimation("atk_pst")
            if inst._rs_cooldown_task then
                inst._rs_cooldown_task:Cancel()
                inst._rs_cooldown_task = nil
            end
            inst._rs_cooldown = true
            inst:DoTaskInTime(TUNING.CRABKING_MOB_KNIGHTP.RANGEDSPECIAL_CD, function(inst) inst._rs_cooldown = nil end)
        end,

        events = OnAnimOver("idle"),
    },

    State{
        name = "hit",

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("hit")
            inst:PlaySound("hit_vocal")
            inst:PlaySound("hit")            
        end,

        events = OnAnimOver("idle"),
    },

    State{
        name = "break",
        tags = { "busy", "nosleep", "nofreeze", "noattack" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("break")
            inst.Physics:Stop()
        end,

        events=
        {
            EventHandler("animover", function(inst)
                local is_ocean = TheWorld.Map:IsOceanAtPoint(inst.Transform:GetWorldPosition())

                inst.sg:GoToState(is_ocean and "break_water" or "break_land")
            end ),
        },
    },

    State{
        name = "break_water",
        tags = { "busy", "nosleep", "nofreeze", "noattack" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("break_water")

            inst:PlaySound("break_water_vocal")

            inst.persists = false

            SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())

            if inst.DynamicShadow ~= nil then
                inst.DynamicShadow:Enable(false)
            end
        end,

        timeline =
        {
            TimeEvent(22*FRAMES, function(inst)
                inst:PlaySound("break_water_fx_f22")
            end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst:Remove() end),
        },
    },

    State{
        name = "break_land",
        tags = { "busy" },

        onenter = function(inst)
            inst:PlaySound("break_land_vocal")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("break_land")
        end,

        timeline =
        {
            TimeEvent(22*FRAMES, function(inst)
                inst:PlaySound("break_land_fx_f22")
            end),
        },

        events = OnAnimOver("idle"),
    },

    State{
        name = "dive",
        tags = {"busy", "nomorph", "nosleep", "nofreeze", "noattack"},

        onenter = function(inst)
            inst:Show()
            local platform = inst:GetCurrentPlatform()
            if platform ~= nil then
                local pt = inst:GetPosition()
                local angle = platform:GetAngleToPoint(pt)
                inst.Transform:SetRotation(angle)
            end

            inst:PlaySound("dive_vocal")
            inst:PlaySound("dive_fx")

            inst.AnimState:PlayAnimation("dive")
            
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)
            
            if inst.DynamicShadow ~= nil then
                inst.DynamicShadow:Enable(true)
            end
            if inst:HasTag("swimming") then
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())
                inst.AnimState:SetTime(9* FRAMES)
                inst.Physics:SetMotorVelOverride(8, 0, 0)
            end
            inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
            inst.Physics:ClearCollisionMask()
        end,

        timeline =
        {
            TimeEvent(10*FRAMES, function(inst)
                inst.sg.statemem.collisionmask = inst.Physics:GetCollisionMask()

                inst.Physics:SetCollisionMask(COLLISION.GROUND)

                inst.Physics:SetMotorVelOverride(6, 0, 0)
                inst.sg.statemem.collisionmask = COLLISION.GROUND
            end),

            TimeEvent(30*FRAMES, function(inst)
                inst.Physics:Stop()
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())
            end),
        },

        onexit = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)

            inst.Physics:ClearMotorVelOverride()
            inst.Physics:ClearLocalCollisionMask()

            if inst.sg.statemem.collisionmask ~= nil then
                inst.Physics:SetCollisionMask(inst.sg.statemem.collisionmask)
            end
            ChangeToCharacterPhysics(inst)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                local x, y, z = inst.Transform:GetWorldPosition()
                local on_land = TheWorld.Map:IsVisualGroundAtPoint(x, y, z) or inst:GetCurrentPlatform()

                inst.sg:GoToState(on_land and "dive_pst_land" or "dive_pst_water")
            end),
        },
    },

    State{
        name = "dive_pst_land",
        tags = {"busy"},

        onenter = function(inst)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end

            inst.AnimState:PlayAnimation("dive_pst_land")
            inst.noactions = nil
            inst.components.health:SetInvincible(false)
            inst:RemoveTag("notarget")
            inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, 1.5)
        end,

        events = OnAnimOver("idle"),
    },

    State{
        name = "dive_pst_water",
        tags = { "busy", "nopredict", "nomorph", "drowning", "nointerrupt", "nowake" },

        onenter = function(inst, data)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("dive_pst_water")

            SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())

            if inst.DynamicShadow ~= nil then
                inst.DynamicShadow:Enable(false)
            end
            inst:Hide()
            inst.noactions = true
            inst.components.health:SetInvincible(true)
            inst:AddTag("notarget")
            inst.components.locomotor:SetExternalSpeedMultiplier(inst, 1.5, 1.5)
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "sink",
        tags = { "busy", "nopredict", "nomorph", "drowning", "nointerrupt", "nowake" },

        onenter = function(inst)
            inst.sg:GoToState("dive_pst_water")
        end,
    }
}

CommonStates.AddFrozenStates(states)
CommonStates.AddSleepStates(states,
{
    starttimeline = {
        SoundFrameEvent(0, "meta4/crabcritter/sleep_pre_vocal"),
        SoundFrameEvent(14, "meta4/crabcritter/sleep_pre_fx_f14"),
    },
    sleeptimeline ={
        SoundFrameEvent(35, "meta4/crabcritter/sleep_lp_vocal"),
        FrameEvent(35, PlayablePets.SleepHeal)
    },
    waketimeline = {
        SoundFrameEvent(0, "meta4/crabcritter/sleep_pst_vocal"),
        SoundFrameEvent(8, "meta4/crabcritter/sleep_pst_fx_f8"),
    },
})
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(5 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "taunt")
CommonStates.AddHopStates(states, true, { pre = "walk_pst", loop = "dive", pst = "dive_pst_land"})
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "walk_pre",
	plank_idle_loop = "idle",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pst",
	plank_hop = "dive",
	
	steer_pre = "walk_pst",
	steer_idle = "idle",
	steer_turning = "idle",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_loop",
	
	leap_pre = "walk_pst",
	leap_loop = "dive",
	leap_pst = "dive_pst_land",
	
	castspelltime = 10,
})

CommonStates.AddAmphibiousCreatureHopStates(states, 
{ -- config
	swimming_clear_collision_frame = 9 * FRAMES,
},
{ -- anims
},
{ -- timeline
	hop_pre =
	{
		TimeEvent(0, function(inst) 
			inst.sg:GoToState("dive")
		end),
	},
})	
    
return StateGraph("crabking_mobp", states, events, "idle", actionhandlers)