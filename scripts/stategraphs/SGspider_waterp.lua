require("stategraphs/commonstates")
require("stategraphs/ppstates")
local easing = require("easing")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function go_to_idle(inst)
    inst.sg:GoToState("idle")
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local function CalcJumpSpeed(inst, target)
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (inst:GetPhysicsRadius(0) + target:GetPhysicsRadius(-1))
        if dist > 0 then
            return math.min(8, dist) / (10 * FRAMES)
        end
    end
    return 0
end

local longaction = "taunt"
local shortaction = "action"
local workaction = "attack"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.CAST_NET, longaction),
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if inst:HasTag("spider_warrior") then
			if inst.components.revivablecorpse then
				return"quick_attack"
			else
				return"warrior_attack"
			end
		else
			return "attack"
		end
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    EventHandler("locomote", function(inst) 
        if not inst.sg:HasStateTag("busy") then
            
            local is_moving = inst.sg:HasStateTag("moving")
            local wants_to_move = inst.components.locomotor:WantsToMoveForward()
			--local should_move = inst.components.locomotor:WantsToMoveForward()
            if not inst.sg:HasStateTag("attack") and not (inst.sg:HasStateTag("home") or inst.sg:HasStateTag("sleeping")) and is_moving ~= wants_to_move then
                if wants_to_move then
                    inst.sg:GoToState("premoving")
                else
                    inst.sg:GoToState("idle")
                end
            end
			if inst.sg:HasStateTag("home") or inst.sg:HasStateTag("home_waking") then -- wakeup on locomote
            if inst.sleepingbag ~= nil and inst.sg:HasStateTag("sleeping") and inst.sg:HasStateTag("home") then
                if inst.sleepingbag.components.sleepingbag then
					inst.sleepingbag.components.sleepingbag:DoWakeUp()
				elseif inst.sleepingbag.components.multisleepingbag then
					inst.sleepingbag.components.multisleepingbag:DoWakeUp(inst)
				end
				inst:Show()
				--inst.AnimState:PushAnimation("pig_pickup")
				--SetSleeperAwakeState(inst)
				--inst.AnimState:PushAnimation("pig_pickup")
				inst.sg:GoToState("taunt")
				
            end end
        end
    end),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.AddCommonHandlers(),
	--PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local function SoundPath(event)
    return "waterlogged1/creatures/spider_water/" .. event
end

 local states=
{
	
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound(SoundPath("die"))
            inst.AnimState:PlayAnimation("death")
            inst.AnimState:PushAnimation("death_idle", true)
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))   
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },
	
	State {
        name = "premoving",
        tags = {"moving", "canrotate"},

        onenter = function(inst)
            if inst.components.amphibiouscreature.in_water then
                inst.Physics:Stop()
            else
                inst.components.locomotor:WalkForward()
            end
            inst.AnimState:PlayAnimation("walk_pre")
        end,

        timeline =
        {
            TimeEvent(3*FRAMES, function(inst)
                if not inst.components.amphibiouscreature.in_water then
                    inst.SoundEmitter:PlaySound(SoundPath("walk_spider"))
                end
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("moving")
            end),
        },
    },

    State {
        name = "moving",
        tags = {"moving", "canrotate"},

        onenter = function(inst)
            if inst.components.amphibiouscreature.in_water then
                inst.components.locomotor.runspeed = TUNING.SPIDER_WATER_OCEANFLOATSPEED
                inst.components.locomotor:RunForward()

                inst.SoundEmitter:PlaySound(SoundPath("walk_water"))
            else
                inst.components.locomotor:WalkForward()
            end

            inst.AnimState:PushAnimation("walk_loop")
        end,

        timeline =
        {
            -- ON LAND TIME EVENTS
            TimeEvent(0*FRAMES, function(inst)
                if not inst.components.amphibiouscreature.in_water then
                    inst.SoundEmitter:PlaySound(SoundPath("walk_spider"))
                end
            end),
            TimeEvent(3*FRAMES, function(inst)
                if not inst.components.amphibiouscreature.in_water then
                    inst.SoundEmitter:PlaySound(SoundPath("walk_spider"))
                end
            end),
            TimeEvent(7*FRAMES, function(inst)
                if not inst.components.amphibiouscreature.in_water then
                    inst.SoundEmitter:PlaySound(SoundPath("walk_spider"))
                end
            end),
            TimeEvent(12*FRAMES, function(inst)
                if not inst.components.amphibiouscreature.in_water then
                    inst.SoundEmitter:PlaySound(SoundPath("walk_spider"))
                end
            end),

            -- IN WATER TIME EVENTS
            TimeEvent(7*FRAMES, function(inst)
                if inst.components.amphibiouscreature.in_water then
                    inst.SoundEmitter:PlaySound(SoundPath("walk_water"))
                end
            end),
            TimeEvent(16*FRAMES, function(inst)
                if inst.components.amphibiouscreature.in_water then
                    inst.components.locomotor.runspeed = TUNING.SPIDER_WATER_OCEANDASHSPEED
                    inst.components.locomotor:RunForward()

                    inst.SoundEmitter:PlaySound("turnoftides/common/together/water/swim/walk_water_med")

                    -- Spawn a wake opposite of our movement direction
                    local wake = SpawnPrefab("boat_water_fx")
                    local rotation = inst.Transform:GetRotation() - 180
                    local reverse_rot = rotation - math.floor(rotation/360)*360

                    local theta = reverse_rot * DEGREES
                    local pos = inst:GetPosition() + (Vector3(math.cos(theta), 0, -math.sin(theta)) * 0.5)

                    wake.Transform:SetPosition(pos:Get())
                    wake.Transform:SetRotation(reverse_rot - 90)
                    wake.AnimState:SetScale(0.7, 0.7)
                end
            end),
        },

        onupdate = function(inst, dt)
            if inst.components.amphibiouscreature.in_water then
                local current_speed, y, z = inst.Physics:GetMotorVel()
                if current_speed > TUNING.SPIDER_WATER_OCEANFLOATSPEED then
                    inst.sg.statemem._dashtime = (inst.sg.statemem._dashtime or 0) + dt

                    local new_speed = -1 * easing.inQuad(
                        inst.sg.statemem._dashtime,
                        -TUNING.SPIDER_WATER_OCEANDASHSPEED,
                        (TUNING.SPIDER_WATER_OCEANDASHSPEED - TUNING.SPIDER_WATER_OCEANFLOATSPEED),
                        30*FRAMES
                    )

                    -- Locomotor only updates its speed when RunForward is called,
                    -- but we shouldn't need to call that constantly. Might as well
                    -- keep runspeed updated though.
                    inst.Physics:SetMotorVel(new_speed, 0, 0)
                    inst.components.locomotor.runspeed = new_speed
                end
                if inst:HasTag("swimming") and not inst.noactions then
                    local ent = FindEntity(inst, 1, nil, {"oceanfish"})
                    if ent then
                        inst.sg:GoToState("gobble", ent)
                    end
                end
            end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("moving")
            end),
        },

        onexit = function(inst)
            if inst.components.amphibiouscreature.in_water then
                inst.components.locomotor.runspeed = TUNING.SPIDER_WATER_RUNSPEED
            end
        end,
    },

    State {
        name = "gobble",
        tags = {"busy"},

        onenter = function(inst, ent)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat")
            if ent then
				if ent:HasTag("player") then
                    ent.components.health:Kill()
                    inst.components.hunger:DoDelta(30, false)
                    inst.components.health:DoDelta(12, false)
                else
                    inst.components.hunger:DoDelta(15, false)
                    inst.components.health:DoDelta(2, false)
                    ent:Remove()
                end
			end
        end,

        timeline =
        {
            TimeEvent(6*FRAMES, function(inst)
                if inst.components.amphibiouscreature.in_water then
                    local breach_fx = SpawnPrefab("ocean_splash_small1")
                    breach_fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
                end

                inst.SoundEmitter:PlaySound(SoundPath("eat"), "eating")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("gobble_loop")
            end),
        },

        onexit = function(inst)
            inst:ClearBufferedAction()

            if not inst.sg.statemem._eating then
                inst.SoundEmitter:KillSound("eating")
            end
        end,
    },

    State {
        name = "gobble_loop",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat_loop", true)
            inst.sg:SetTimeout(1)
        end,

        ontimeout = function(inst)
			inst.AnimState:PlayAnimation("eat_pst")
        end,

        onexit = function(inst)
            inst.SoundEmitter:KillSound("eating")
        end,
		
		events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },


    State {
        name = "eat",
        tags = {"busy"},

        onenter = function(inst, forced)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("eat")
        end,

        timeline =
        {
            TimeEvent(6*FRAMES, function(inst)
                if inst.components.amphibiouscreature.in_water then
                    local breach_fx = SpawnPrefab("ocean_splash_small1")
                    breach_fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
                end

                inst.SoundEmitter:PlaySound(SoundPath("eat"), "eating")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("eat_loop")
            end),
        },

        onexit = function(inst)
            inst:ClearBufferedAction()

            if not inst.sg.statemem._eating then
                inst.SoundEmitter:KillSound("eating")
            end
        end,
    },

    State {
        name = "eat_loop",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat_loop", true)
            inst.sg:SetTimeout(1)
        end,

        ontimeout = function(inst)
			inst.AnimState:PlayAnimation("eat_pst")
        end,

        onexit = function(inst)
            inst.SoundEmitter:KillSound("eating")
        end,
		
		events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State {
        name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            inst.SoundEmitter:PlaySound(SoundPath("scream"))
        end,

        events=
        {
            EventHandler("animover", go_to_idle),
        },
    },
	
	State {
        name = "taunt",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            inst.SoundEmitter:PlaySound(SoundPath("scream"))
			inst:PerformBufferedAction()
        end,

        events=
        {
            EventHandler("animover", go_to_idle),
        },
    },
	
	State{
        name = "special_atk2",
        tags = {"busy", "idle"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("cower")
            inst.AnimState:PushAnimation("cower_loop", true)
        end,
        
		timeline = 
		{
			TimeEvent(5*FRAMES, function(inst) inst.sg:RemoveStateTag("busy") end),
		},
    },  

    State {
        name = "investigate",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            inst.SoundEmitter:PlaySound(SoundPath("scream"))
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst:PerformBufferedAction()
                inst.sg:GoToState("idle")
            end),
        },
    },

    State {
        name = "attack",
        tags = {"attack", "busy"},

        onenter = function(inst, target)
            inst.Physics:Stop()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk")
            inst.sg.statemem.target = target
        end,

        timeline=
        {
            TimeEvent(10*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(SoundPath("Attack"))
                inst.SoundEmitter:PlaySound(SoundPath("attack_grunt"))
            end),
            TimeEvent(16*FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
        },

        events=
        {
            EventHandler("animover", go_to_idle),
        },
    },

    State {
        name = "hit",

        onenter = function(inst)
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()
        end,

        events=
        {
            EventHandler("animover", go_to_idle),
        },
    },
	
	State {
        name = "idle",
		tags = {"idle"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("idle")
            inst.Physics:Stop()
        end,

        events=
        {
            EventHandler("animover", go_to_idle),
        },
    },

    State {
        name = "dropper_enter",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("enter")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/spider/descend")
        end,

        events=
        {
            EventHandler("animqueueover", go_to_idle),
        },
    },

    State {
        name = "trapped",
        tags = { "busy", "trapped" },

        onenter = function(inst)
            inst.Physics:Stop()

            inst:ClearBufferedAction()

            inst.AnimState:PlayAnimation("cower")
            inst.AnimState:PushAnimation("cower_loop", true)

            inst.sg:SetTimeout(1)
        end,

        ontimeout = go_to_idle,
    },

    State {
        name = "mutate",
        tags = {"busy", "mutating"},

        onenter = function(inst)
            inst.Physics:Stop()

            inst.AnimState:PlayAnimation("mutate_pre")

            inst.SoundEmitter:PlaySound("webber2/common/mutate")
        end,

        events =
        {
            EventHandler("animover", function(inst) 
                local fx = SpawnPrefab("spider_mutate_fx")
                fx.Transform:SetPosition(inst.Transform:GetWorldPosition())

                inst:DoTaskInTime(0.25, do_mutate)
            end),
        },
    },

    State {
        name = "mutate_pst",
        tags = {"busy", "mutating"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("mutate_pst")
        end,

        events =
        {
            EventHandler("animqueueover", go_to_idle),
        },
    },
	
	------------------SLEEPING-----------------
	State {
        name = "sleep",
        tags = { "busy", "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,
		
		timeline = 
		{
		TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath("fallAsleep")) end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline =
		{
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath("sleeping")) end ),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end

            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,
		
		timeline = 
		{
		TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath("wakeUp")) end ),
		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },    
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "taunt", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "die")) end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "scream")) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "taunt")
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk_loop", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "cower",
	plank_idle_loop = "cower_loop",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk_loop",
	
	steer_pre = "walk_pst",
	steer_idle = "idle",
	steer_turning = "taunt",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_pst",
	
	leap_pre = "walk_pre",
	leap_loop = "walk_loop",
	leap_pst = "walk_pst",
	
	castspelltime = 10,
})
CommonStates.AddAmphibiousCreatureHopStates(states,
{ -- config
    swimming_clear_collision_frame = 5*FRAMES,
},
nil,
{ -- timelines
    hop_pre =
    {
        TimeEvent(0, function(inst)
            if inst:HasTag("swimming") then
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())
            end
        end),
    },
    hop_pst = {
        TimeEvent(4 * FRAMES, function(inst)
            if inst:HasTag("swimming") then
                inst.components.locomotor:Stop()
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())
            end
        end),
        TimeEvent(6 * FRAMES, function(inst)
            if not inst:HasTag("swimming") then
                inst.components.locomotor:StopMoving()
            end
        end),
    }
})

CommonStates.AddWalkStates(states)
    
return StateGraph("spider_waterp", states, events, "special_atk1", actionhandlers)

