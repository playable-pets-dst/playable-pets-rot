require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "lunar_aligned"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "lunar_aligned", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "lunar_aligned"}
	end
end

local function TryStagger(inst)
	inst.sg:GoToState("stagger_pre")
	return true
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "eat_pre"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.PICKUP, "attack"),
	ActionHandler(ACTIONS.HARVEST, "attack"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function SpawnCloseEmberFX(inst, angle)
	local x, y, z = inst.Transform:GetWorldPosition()
	angle = (inst.Transform:GetRotation() + angle) * DEGREES
	x = x + math.cos(angle) * 3.5
	z = z - math.sin(angle) * 3.5
	angle = math.random() * PI2
	x = x + math.cos(angle) * 0.6
	z = z - math.sin(angle) * 0.6

	if not TheWorld.Map:IsPassableAtPoint(x, 0, z) then
		return
	end

	local fx = table.remove(inst.ember_pool)
	if fx == nil then
		fx = SpawnPrefab("warg_mutated_ember_fx")
		fx:SetFXOwner(inst)
	end
	fx.Transform:SetPosition(x, 0, z)
	fx:RestartFX(1.7 + math.random() * 0.3, "nofade")
	fx:DoTaskInTime(math.random(18, 22) * FRAMES, fx.KillFX)
end

local function SpawnBreathFX(inst, angle, dist, targets)
	local fx = table.remove(inst.flame_pool)
	if fx == nil then
		fx = SpawnPrefab("warg_mutated_breath_fx")
		fx:SetFXOwner(inst)
	end

	local scale = (1.4 + math.random() * 0.25)
	if dist < 6 then
		scale = scale * 1.2
	elseif dist > 7 then
		scale = scale * (1 + (dist - 7) / 6)
	end

	local fadeoption = (dist < 6 and "nofade") or (dist <= 7 and "latefade") or nil

	local x, y, z = inst.Transform:GetWorldPosition()
	angle = (inst.Transform:GetRotation() + angle) * DEGREES
	x = x + math.cos(angle) * dist
	z = z - math.sin(angle) * dist
	dist = dist / 20
	angle = math.random() * PI2
	x = x + math.cos(angle) * dist
	z = z - math.sin(angle) * dist
	if fx and fx:IsValid() then
		fx.Transform:SetPosition(x, 0, z) --There is a rare crash that occurs here, try and figure out what it is.
		fx:RestartFX(scale, fadeoption, targets)
	end
end

local AOE_OFFSET = 3
local AOE_RANGE = 1.7
local AOE_RANGE_PADDING = 3
local AOE_TARGET_TAGS = { "_combat" }
local AOE_TARGET_CANT_TAGS = GetExcludeTags()
local MULTIHIT_FRAMES = 10

--NOTE: This is for close range that the breath fx doesn't fully cover
local function DoFlamethrowerAOE(inst, angle, targets)
	inst.components.combat.ignorehitrange = true
	inst.components.combat.ignoredamagereflect = true

	local tick = GetTick()
	local x, y, z = inst.Transform:GetWorldPosition()
	angle = (inst.Transform:GetRotation() + angle) * DEGREES
	x = x + math.cos(angle) * AOE_OFFSET
	z = z - math.sin(angle) * AOE_OFFSET
	local ents = TheSim:FindEntities(x, 0, z, AOE_RANGE + AOE_RANGE_PADDING, AOE_TARGET_TAGS, AOE_TARGET_CANT_TAGS)
	for i, v in ipairs(ents) do
		if v:IsValid() and not v:IsInLimbo() and not (v.components.health ~= nil and v.components.health:IsDead()) then
			local range = AOE_RANGE + v:GetPhysicsRadius(0)
			if v:GetDistanceSqToPoint(x, 0, z) < range * range then
				local target_data = targets[v]
				if target_data == nil then
					target_data = {}
					targets[v] = target_data
				end
				if target_data.tick ~= tick then
					target_data.tick = tick
					--Supercool
					if v.components.temperature ~= nil then
						local newtemp = math.max(v.components.temperature.mintemp, TUNING.MUTATED_WARG_COLDFIRE_TEMPERATURE)
						if newtemp < v.components.temperature:GetCurrent() then
							v.components.temperature:SetTemperature(newtemp)
						end
					end
					--Hit
					if (target_data.hit_tick == nil or target_data.hit_tick + MULTIHIT_FRAMES < tick) and inst.components.combat:CanTarget(v) then
						target_data.hit_tick = tick
						inst.components.combat:DoAttack(v)
					end
				end
			end
		end
	end

	inst.components.combat.ignorehitrange = false
	inst.components.combat.ignoredamagereflect = false
end

local function RetargetForgeFn(inst)
    if (inst.components.follower.leader and inst.components.follower.leader.components.health:IsDead()) or (inst.daddy and inst.daddy.components.health:IsDead()) then
		return FindEntity(inst, 30, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() end, nil, { "structure", "player", "companion" })
	elseif inst.components.follower.leader and inst.components.follower.leader.components.combat.target then	
		return inst.components.follower.leader.components.combat.target
	end
end

local function KeepForgeTarget(inst, target)
    return target:IsValid() and 
		target.sg ~= nil and not 
		((target.sg:HasStateTag("sleeping") or target:HasTag("_isinheals")) or target.sg:HasStateTag("fossilized")) and not target:HasTag("player")
end

local function RelinktoLeader(inst)
	if inst.daddy and inst.daddy.components.leader then
		inst.daddy.components.leader:AddFollower(inst)
	end
end

local function NoDmgFromPlayers(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
    return afflicter ~= nil and (afflicter:HasTag("player") or afflicter:HasTag("companion"))
end

local function OnForgeAttacked(inst, data)
	if not (data.attacker:HasTag("player") or data.attacker:HasTag("companion") or data.attacker:HasTag("hound")) then
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST,
        function(dude)
            return not (dude.components.health ~= nil and dude.components.health:IsDead())
                and (dude:HasTag("hound") or dude:HasTag("houndfriend"))
                and data.attacker ~= (dude.components.follower ~= nil and dude.components.follower.leader or nil)
        end, 5)
	end	
end

--old
local function OnAttacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST,
        function(dude)
            return not (dude.components.health ~= nil and dude.components.health:IsDead())
                and (dude:HasTag("hound") or dude:HasTag("houndfriend"))
                and data.attacker ~= (dude.components.follower ~= nil and dude.components.follower.leader or nil)
        end, 5)
end

local events=
{
    EventHandler("attacked", function(inst)
        if not inst.components.health:IsDead() and
            (not inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("sleeping")) and
            (inst.sg.mem.last_hit_time or 0) + inst.hit_recovery < GetTime() then
				inst.sg:GoToState("hit")
        end
    end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.AddCommonHandlers(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
}


 local states=
{
	
	State
	{
		name = "idle",
		tags = {"idle"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle_loop")
			inst.SoundEmitter:PlaySound(inst.sounds.idle)
		end,

		events = 
		{
			EventHandler("animover", function(inst) 
				inst.sg:GoToState("idle") 
			end)
		},
	},
	
	 State{
        name = "attack",
        tags = {"busy", "attack"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk")
        end,

        timeline =
        {
            TimeEvent(12*FRAMES, function(inst) PlayablePets.DoWork(inst, 5) end),
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack) end)
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	State{
        name = "hit",
        tags = {"busy", "hit", "canrotate"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst.sg.mem.last_hit_time = GetTime()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound(inst.sounds.hit)
        end,

        timeline =
        {
            
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "eat_pre",
        tags = { "chewing", "busy" },
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.components.combat:StartAttack()
			inst.AnimState:PlayAnimation("eat_pre")
			inst.SoundEmitter:PlaySound(inst.sounds.attack)

			local buffaction = inst:GetBufferedAction()
			local target = buffaction.target
			if target ~= nil and target:IsValid() then
				inst.components.combat:StartAttack()
				inst:ForceFacePoint(target.Transform:GetWorldPosition())
			end
		end,

        timeline =
		{

		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("eat_loop")
				end
			end),
		},
	},

	State{
		name = "eat_loop",
		tags = { "chewing", "busy" },

		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("eat_loop")
			inst.SoundEmitter:PlaySound("rifts3/chewing/warg")
		end,

		timeline =
		{
			FrameEvent(7, function(inst)
				inst.sg:AddStateTag("caninterrupt")
				inst.sg:AddStateTag("wantstoeat")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("eat_pst", true)
				end
			end),
		},
	},

	State{
		name = "eat_pst",
		tags = { "busy", "caninterrupt" },

		onenter = function(inst, chewing)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("eat_pst")
            PlayablePets.DoWork(inst, 2.5)
		end,

		timeline =
		{
			FrameEvent(6, function(inst)

			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},

		onexit = function(inst)
			inst.SoundEmitter:KillSound("loop")
		end,
	},

	State
	{
		name = "special_atk1",
		tags = {"busy"},

		onenter = function(inst)
			if inst.taunt == false then
				inst.sg:GoToState("idle")
			end
			inst.taunt = false
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("howl")
			inst.SoundEmitter:PlaySound(inst.sounds.howl)
		end,

		timeline = 
		{
			TimeEvent(10*FRAMES, function(inst) inst:SpawnHounds() end),
		},

		events = 
		{
			EventHandler("animover", function(inst) 
				inst:DoTaskInTime(8, function() inst.taunt = true end)
				inst.sg:GoToState("idle") 
			end)
		},
	},

	State{
		name = "special_atk2",
		tags = { "attack", "busy" },

		onenter = function(inst, target)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("atk_breath_pre")
			inst:SwitchToEightFaced()
			local dir
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target ~= nil and target:IsValid() then
				inst.components.combat:StartAttack()
				inst.sg.statemem.target = target
				inst.sg.statemem.targetpos = target:GetPosition()
				dir = inst:GetAngleToPoint(inst.sg.statemem.targetpos)
			else
				dir = inst.Transform:GetRotation()
			end
			--snap to 45's
			inst.Transform:SetRotation(math.floor(dir / 45 + .5) * 45)

			inst.components.combat:SetDefaultDamage(TUNING.MUTATED_WARG_FLAMETHROWER_DAMAGE)
		end,

		onupdate = function(inst)
			local target = inst.sg.statemem.target
			if target ~= nil then
				if target:IsValid() then
					local p = inst.sg.statemem.targetpos
					p.x, p.y, p.z = target.Transform:GetWorldPosition()
					local rot = inst.Transform:GetRotation()
					local rot1 = inst:GetAngleToPoint(p)
					local drot = ReduceAngle(rot1 - rot)
					if math.abs(drot) < 90 then
						rot1 = rot + math.clamp(drot / 2, -1, 1)
						--snap to 45's
						inst.Transform:SetRotation(math.floor(rot1 / 45 + .5) * 45)
					end
				else
					inst.sg.statemem.target = nil
				end
			elseif inst.sg.statemem.angle ~= nil then
				DoFlamethrowerAOE(inst, inst.sg.statemem.angle, inst.sg.statemem.targets)
			end
		end,

		timeline =
		{
			FrameEvent(0, function(inst) inst.SoundEmitter:PlaySound("rifts3/mutated_varg/blast_pre_f0") end),
			FrameEvent(16, function(inst)
				inst.sg.statemem.target = nil
				inst.sg.statemem.targets = {}
			end),

			FrameEvent(17, function(inst)
				inst.SoundEmitter:PlaySound("rifts3/mutated_varg/blast_pre_f17")
				inst.SoundEmitter:PlaySound("rifts3/mutated_varg/blast_lp", "loop")
			end),
			FrameEvent(19, function(inst) SpawnBreathFX(inst, -40, 4, inst.sg.statemem.targets) end),

			FrameEvent(20, function(inst) inst.sg.statemem.angle = -45 end),
			FrameEvent(21, function(inst) SpawnBreathFX(inst, -45, 6, inst.sg.statemem.targets) end),
			FrameEvent(24, function(inst) SpawnBreathFX(inst, -45, 8, inst.sg.statemem.targets) end),
			FrameEvent(27, function(inst) SpawnBreathFX(inst, -45, 9, inst.sg.statemem.targets) end),

			FrameEvent(29, function(inst) SpawnCloseEmberFX(inst, -45) end),
			FrameEvent(26, function(inst) SpawnBreathFX(inst, -45, 5, inst.sg.statemem.targets) end),
			FrameEvent(29, function(inst) SpawnBreathFX(inst, -45, 7, inst.sg.statemem.targets) end),

			--frame 30 is start of "flamethrower_loop"
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg.statemem.attacking = true
					inst.sg:GoToState("flamethrower_loop", inst.sg.statemem.targets)
				end
			end),
		},

		onexit = function(inst)
			inst.taunt2 = false
			if inst.taunt2_task then
				inst.taunt2_task:Cancel()
				inst.taunt2_task = nil
			end
			inst.taunt2_task = inst:DoTaskInTime(TUNING.MUTATED_WARG_FLAMETHROWER_CD, function(inst) inst.taunt2 = true end)
			if not inst.sg.statemem.attacking then
				inst:SwitchToSixFaced()
				inst.components.combat:SetDefaultDamage(inst.mob_table.damage)
				inst.SoundEmitter:KillSound("loop")
			end
		end,
	},

	State{
		name = "flamethrower_loop",
		tags = { "attack", "busy" },

		onenter = function(inst, targets)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("atk_breath_loop")
			inst:SwitchToEightFaced()
			inst.sg.statemem.targets = targets or {}
			inst.sg.statemem.angle = -45
			--inst.sg.statemem.loop = true
			inst.components.timer:StopTimer("flamethrower_cd")
			inst.components.timer:StartTimer("flamethrower_cd", TUNING.MUTATED_WARG_FLAMETHROWER_CD + math.random() * 2)
			inst.components.combat:SetDefaultDamage(TUNING.MUTATED_WARG_FLAMETHROWER_DAMAGE)
			if not inst.SoundEmitter:PlayingSound("loop") then
				inst.SoundEmitter:PlaySound("rifts3/mutated_varg/blast_lp", "loop")
			end
		end,

		onupdate = function(inst)
			DoFlamethrowerAOE(inst, inst.sg.statemem.angle, inst.sg.statemem.targets)
		end,

		timeline =
		{
			--FrameEvent(-1, function(inst) SpawnCloseEmberFX(inst, -45) end),
			--FrameEvent(-4, function(inst) SpawnBreathFX(inst, -45, 5, inst.sg.statemem.targets) end),
			--FrameEvent(-1, function(inst) SpawnBreathFX(inst, -45, 7, inst.sg.statemem.targets) end),
			FrameEvent(3, function(inst) SpawnBreathFX(inst, -45, 9, inst.sg.statemem.targets) end),

			FrameEvent(2, function(inst) inst.sg.statemem.angle = -27 end),
			FrameEvent(3, function(inst) SpawnCloseEmberFX(inst, -27) end),
			FrameEvent(0, function(inst) SpawnBreathFX(inst, -27, 5, inst.sg.statemem.targets) end),
			FrameEvent(3, function(inst) SpawnBreathFX(inst, -27, 7, inst.sg.statemem.targets) end),
			FrameEvent(7, function(inst) SpawnBreathFX(inst, -27, 9, inst.sg.statemem.targets) end),

			FrameEvent(4, function(inst) inst.sg.statemem.angle = -9 end),
			FrameEvent(5, function(inst) SpawnCloseEmberFX(inst, -9) end),
			FrameEvent(2, function(inst) SpawnBreathFX(inst, -9, 5, inst.sg.statemem.targets) end),
			FrameEvent(5, function(inst) SpawnBreathFX(inst, -9, 7, inst.sg.statemem.targets) end),
			FrameEvent(9, function(inst) SpawnBreathFX(inst, -9, 9, inst.sg.statemem.targets) end),

			FrameEvent(6, function(inst) inst.sg.statemem.angle = 9 end),
			FrameEvent(7, function(inst) SpawnCloseEmberFX(inst, 9) end),
			FrameEvent(4, function(inst) SpawnBreathFX(inst, 9, 5, inst.sg.statemem.targets) end),
			FrameEvent(7, function(inst) SpawnBreathFX(inst, 9, 7, inst.sg.statemem.targets) end),
			FrameEvent(11, function(inst) SpawnBreathFX(inst, 9, 9, inst.sg.statemem.targets) end),

			FrameEvent(9, function(inst) inst.sg.statemem.angle = 27 end),
			FrameEvent(10, function(inst) SpawnCloseEmberFX(inst, 27) end),
			FrameEvent(7, function(inst) SpawnBreathFX(inst, 27, 5, inst.sg.statemem.targets) end),
			FrameEvent(10, function(inst) SpawnBreathFX(inst, 27, 7, inst.sg.statemem.targets) end),
			FrameEvent(14, function(inst) SpawnBreathFX(inst, 27, 9, inst.sg.statemem.targets) end),

			FrameEvent(12, function(inst) inst.sg.statemem.angle = 45 end),
			FrameEvent(13, function(inst) SpawnCloseEmberFX(inst, 45) end),
			FrameEvent(10, function(inst) SpawnBreathFX(inst, 45, 5, inst.sg.statemem.targets) end),
			FrameEvent(13, function(inst) SpawnBreathFX(inst, 45, 7, inst.sg.statemem.targets) end),
			FrameEvent(17, function(inst) SpawnBreathFX(inst, 45, 9, inst.sg.statemem.targets) end),

			FrameEvent(15, function(inst) inst.sg.statemem.angle = 27 end),
			FrameEvent(16, function(inst) SpawnCloseEmberFX(inst, 27) end),
			FrameEvent(13, function(inst) SpawnBreathFX(inst, 27, 5, inst.sg.statemem.targets) end),
			FrameEvent(16, function(inst) SpawnBreathFX(inst, 27, 7, inst.sg.statemem.targets) end),
			FrameEvent(20, function(inst) SpawnBreathFX(inst, 27, 9, inst.sg.statemem.targets) end),

			FrameEvent(18, function(inst) inst.sg.statemem.angle = 9 end),
			FrameEvent(19, function(inst) SpawnCloseEmberFX(inst, 9) end),
			FrameEvent(16, function(inst) SpawnBreathFX(inst, 9, 5, inst.sg.statemem.targets) end),
			FrameEvent(19, function(inst) SpawnBreathFX(inst, 9, 7, inst.sg.statemem.targets) end),
			FrameEvent(23, function(inst) SpawnBreathFX(inst, 9, 9, inst.sg.statemem.targets) end),

			FrameEvent(21, function(inst) inst.sg.statemem.angle = -9 end),
			FrameEvent(22, function(inst) SpawnCloseEmberFX(inst, -9) end),
			FrameEvent(19, function(inst) SpawnBreathFX(inst, -9, 5, inst.sg.statemem.targets) end),
			FrameEvent(22, function(inst) SpawnBreathFX(inst, -9, 7, inst.sg.statemem.targets) end),
			FrameEvent(26, function(inst) SpawnBreathFX(inst, -9, 9, inst.sg.statemem.targets) end),

			FrameEvent(24, function(inst) inst.sg.statemem.angle = -27 end),
			FrameEvent(25, function(inst) SpawnCloseEmberFX(inst, -27) end),
			FrameEvent(22, function(inst) SpawnBreathFX(inst, -27, 5, inst.sg.statemem.targets) end),
			FrameEvent(25, function(inst) SpawnBreathFX(inst, -27, 7, inst.sg.statemem.targets) end),
			--FrameEvent(29, function(inst) SpawnBreathFX(inst, -27, 9, inst.sg.statemem.targets) end),

			FrameEvent(27, function(inst) inst.sg.statemem.angle = -45 end),
			FrameEvent(28, function(inst) SpawnCloseEmberFX(inst, -45) end),
			FrameEvent(25, function(inst) SpawnBreathFX(inst, -45, 5, inst.sg.statemem.targets) end),
			FrameEvent(28, function(inst) SpawnBreathFX(inst, -45, 7, inst.sg.statemem.targets) end),
			--FrameEvent(32, function(inst) SpawnBreathFX(inst, -45, 9, inst.sg.statemem.targets) end),

			--frame 29 and beyond goes to "flamethrower_pst"
		},

		events =
		{
			EventHandler("attacked", function(inst, data)
				if not inst.components.health:IsDead() and
					data ~= nil and data.spdamage ~= nil and data.spdamage.planar ~= nil
				then
					if not inst.sg.mem.dostagger then
						inst.sg.mem.dostagger = true
						inst.sg.statemem.staggertime = GetTime() + 0.3
					elseif GetTime() > inst.sg.statemem.staggertime then
						inst.sg:GoToState("hit")
					end
				end
				return true
			end),
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg.statemem.attacking = true
					if inst.sg.statemem.loop then
						SpawnBreathFX(inst, -27, 9, inst.sg.statemem.targets)
						inst.sg:GoToState("flamethrower_loop", inst.sg.statemem.targets)
					else
						inst.sg:GoToState("flamethrower_pst", inst.sg.statemem.targets)
					end
				end
			end),
		},

		onexit = function(inst)
			if not inst.sg.statemem.attacking then
				inst:SwitchToSixFaced()
				inst.components.combat:SetDefaultDamage(TUNING.WARG_DAMAGE)
				inst.SoundEmitter:KillSound("loop")
			elseif not inst.sg.statemem.loop then
				inst.components.combat:SetDefaultDamage(TUNING.WARG_DAMAGE)
			end
		end,
	},

	State{
		name = "flamethrower_pst",
		tags = { "attack", "busy" },

		onenter = function(inst, targets)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("atk_breath_pst")
			inst:SwitchToEightFaced()
			inst.SoundEmitter:PlaySound("rifts3/mutated_varg/blast_pst")
			inst.sg.statemem.targets = targets or {}
		end,

		timeline =
		{
			FrameEvent(0, function(inst) SpawnBreathFX(inst, -27, 9, inst.sg.statemem.targets) end),

			FrameEvent(3, function(inst) SpawnBreathFX(inst, -45, 9, inst.sg.statemem.targets) end),

			FrameEvent(4, function(inst)
				if inst.sg.mem.dostagger and TryStagger(inst) then
					return
				end
				inst.sg:AddStateTag("caninterrupt")
			end),
			FrameEvent(6, function(inst) inst.SoundEmitter:KillSound("loop") end),
			FrameEvent(13, function(inst)
				inst.sg:RemoveStateTag("busy")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},

		onexit = function(inst)
			inst:SwitchToSixFaced()
			inst.SoundEmitter:KillSound("loop")
		end,
	},

	State{
		name = "stagger_pre",
		tags = { "staggered", "busy", "nosleep" },

		onenter = function(inst)
			inst.sg.mem.dostagger = nil
			inst.sg.mem.dohowl = nil
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("stagger_pre")
			inst.components.timer:StopTimer("stagger")
			inst.components.timer:StartTimer("stagger", TUNING.MUTATED_WARG_STAGGER_TIME)
			inst.components.timer:StopTimer("flamethrower_cd")
			inst.components.timer:StartTimer("flamethrower_cd", TUNING.MUTATED_WARG_STAGGER_TIME + TUNING.MUTATED_WARG_FLAMETHROWER_CD * (0.5 + math.random() * 0.5))
			inst.SoundEmitter:PlaySound(inst.sounds.hit)
		end,

		timeline =
		{
			FrameEvent(16, function(inst)
			end),
			FrameEvent(24, function(inst)
				inst.sg:AddStateTag("caninterrupt")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState(inst.components.timer:TimerExists("stagger") and "stagger_idle" or "stagger_pst")
				end
			end),
		},
	},

	State{
		name = "stagger_idle",
		tags = { "staggered", "busy", "caninterrupt", "nosleep" },

		onenter = function(inst)
			if not inst.components.timer:TimerExists("stagger") then
				inst.sg:GoToState("stagger_pst")
				return
			end
			inst.AnimState:PlayAnimation("stagger", true)
		end,

		events =
		{
			EventHandler("timerdone", function(inst, data)
				if data ~= nil and data.name == "stagger" then
					inst.sg:GoToState("stagger_pst")
				end
			end),
		},
	},

	State{
		name = "stagger_hit",
		tags = { "staggered", "busy", "hit", "nosleep" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("stagger_hit")
		end,

		timeline =
		{
			FrameEvent(9, function(inst)
				if inst.components.timer:TimerExists("stagger") then
					inst.sg:AddStateTag("caninterrupt")
				end
			end),
			FrameEvent(16, function(inst)
				if not inst.components.timer:TimerExists("stagger") then
					inst.sg:GoToState("stagger_pst", true)
					return
				end
				inst.sg.statemem.cangetup = true
			end),
		},

		events =
		{
			EventHandler("timerdone", function(inst, data)
				if data ~= nil and data.name == "stagger" and inst.sg.statemem.cangetup then
					inst.sg:GoToState("stagger_pst", true)
				end
			end),
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					if inst.components.timer:TimerExists("stagger") then
						inst.sg:GoToState("stagger_idle")
					else
						inst.sg:GoToState("stagger_pst", true)
					end
				end
			end),
		},
	},

	State{
		name = "stagger_pst",
		tags = { "staggered", "busy", "nosleep" },

		onenter = function(inst, nohit)
			inst.AnimState:PlayAnimation("stagger_pst")
			if not nohit then
				inst.sg:AddStateTag("caninterrupt")
			end
			if inst.components.sleeper ~= nil then
				inst.components.sleeper:WakeUp()
			end
		end,

		timeline =
		{
			FrameEvent(10, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.sleep) end),
			FrameEvent(11, function(inst)
				inst.sg:RemoveStateTag("staggered")
				inst.sg:RemoveStateTag("caninterrupt")
			end),
			CommonHandlers.OnNoSleepFrameEvent(66, function(inst)
				if inst.sg.mem.dostagger and TryStagger(inst) then
					return
				end
				inst.sg:RemoveStateTag("nosleep")
				inst.sg:AddStateTag("caninterrupt")
			end),
			FrameEvent(39, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.idle) end),
			FrameEvent(99, function(inst)
				inst.sg:RemoveStateTag("busy")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},
	},

	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/sleep") end)
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
            inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)
			
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline=
		{
			
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },
	
}





  
CommonStates.AddRunStates(states, 
{	
	starttimeline = {},
    runtimeline = 
    { 
        TimeEvent(5*FRAMES, 
        function(inst) 
        	PlayFootstep(inst)
        	inst.SoundEmitter:PlaySound(inst.sounds.idle)
        end),
    },
	endtimeline = {},
})
CommonStates.AddFrozenStates(states)
local simpleanim = "run_pst"
local idleanim = "idle_loop"
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	simpleanim, nil, nil, "idle_loop", simpleanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end)
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.howl) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "howl"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
CommonStates.AddHopStates(states, false, {pre = "run_pre", loop = "run_loop", pst = "run_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "run_pst",
	plank_idle_loop = "idle_loop",
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = "run_pre",
	plank_hop = "run_loop",
	
	steer_pre = simpleanim,
	steer_idle = idleanim,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "run_pre",
	leap_loop = "run_loop",
	leap_pst = "run_pst",
	
	lunge_pre = "run_pre",
	lunge_loop = "run_loop",
	lunge_pst = "run_pst",
	
	superjump_pre = "run_pre",
	superjump_loop = "run_loop",
	superjump_pst = "run_pst",
	
	castspelltime = 10,
})   
    
return StateGraph("vargplayer", states, events, "idle", actionhandlers)

