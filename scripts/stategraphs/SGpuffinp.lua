require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action2"
local shortaction = "action2"
local workaction = "action2"
local otheraction = "action2"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.CAST_NET, longaction),
}

local function play_carrat_scream(inst)
    inst.SoundEmitter:PlaySound(inst.sounds.scream)
end

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	--CommonHandlers.OnHop(),
	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{
    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst, pushanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle")
        end,
        
        timeline = 
        {

        },
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	   State {
		name = "frozen",
		tags = {"busy"},
		
        onenter = function(inst)
            inst.AnimState:PlayAnimation("frozen")
            inst.Physics:Stop()
        end,
		
    },
	
	 State{
        name = "special_atk1",
        tags = {"busy"},
        onenter= function(inst)
			inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("caw")
			inst:PerformBufferedAction()
            inst.SoundEmitter:PlaySound("turnoftides/birds/chirp_puffin")
        end,
        
		events =
        {
            EventHandler("animover", function(inst)
				inst.sg:GoToState("idle")
            end),
        },

    },
	
    State {
        name = "attack",
		tags = { "busy" },
        onenter = function(inst)
            inst:PerformBufferedAction()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("peck")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State {
        name = "eat",
		tags = { "busy" },
		
        onenter = function(inst)
            inst:PerformBufferedAction()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat_pre", false)
            inst.AnimState:PushAnimation("eat_loop", false)
            inst.AnimState:PushAnimation("eat_pst", false)
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State{
        name = "land",
        tags = {"busy", "flight", "landing"},
        onenter= function(inst)
			if inst.amphibious or not IsOceanTile(TheWorld.Map:GetTileAtPoint(inst:GetPosition():Get())) then
				inst.components.locomotor:StopMoving()
				inst:Show()
				local pt = Point(inst.Transform:GetWorldPosition())
				pt.y = 25
				inst.Physics:Stop()
				inst.components.locomotor:StopMoving()
				inst.Physics:Teleport(pt.x,pt.y,pt.z)
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("glide", true)
				inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
			
				inst:RemoveTag("notarget")
				inst.components.locomotor:StopMoving()
				inst.components.locomotor.disable = true
				inst.Physics:SetMotorVel(0,-20+math.random()*10,0)
				inst.SoundEmitter:PlaySound("dontstarve/birds/flyin", "flyin")
			else
				--print("DEBUG: failed, going to glide state")
				inst.sg:GoToState("glide")
			end
        end,
		
		onexit = function(inst)
			if inst.amphibious or not IsOceanTile(TheWorld.Map:GetTileAtPoint(inst:GetPosition():Get())) then --this should assume that the state failed, thus not setting the mobs to landform internally
				inst._isflying = false
				inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
				inst.components.health:SetInvincible(false)
				inst.components.locomotor.disable = false
			end
		end,	
		
		 timeline = 
        {
            TimeEvent(5, function(inst) --added as a failsafe. Teleports to the ground if takes too long.
				local pt = Point(inst.Transform:GetWorldPosition())
				pt.y = 0
                inst.components.locomotor:StopMoving()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.AnimState:PlayAnimation("land")
	            inst.DynamicShadow:Enable(true)
				inst.components.health:SetInvincible(false)
                inst.sg:GoToState("idle", true)
			end),
        },
        
        onupdate= function(inst)
            local pt = Point(inst.Transform:GetWorldPosition())
            inst.Physics:SetMotorVel(0,-20+math.random()*10,0)
            if pt.y <= 1 then
                pt.y = 0
                inst.components.locomotor:StopMoving()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.AnimState:PlayAnimation("land")
	            inst.DynamicShadow:Enable(true)
				inst.components.health:SetInvincible(false)
                inst.sg:GoToState("idle", true)
            end
        end,
		

        ontimeout = function(inst)
             inst.SoundEmitter:PlaySound("dontstarve/birds/flyin", "flyin")
             --inst.sg:GoToState("idle")
        end,
    },    
	 
	 State{
        name = "special_atk2",
        tags = {"flight", "busy"}, --sliding happens
        onenter = function(inst)
			if not inst.taunt2 then
				inst.sg:GoToState("idle")
			end
			inst.taunt2 = false
            inst.components.locomotor:StopMoving()
            inst.sg:SetTimeout(.1+math.random()*.2)
            inst.sg.statemem.vert = math.random() > .5
			
	        inst.DynamicShadow:Enable(false)
			inst.components.health:SetInvincible(true)
            inst.SoundEmitter:PlaySound("turnoftides/birds/takeoff_puffin")
            inst.AnimState:PlayAnimation("takeoff_vertical_pre")
        end,
        
        ontimeout= function(inst)
             inst.AnimState:PushAnimation("takeoff_vertical_loop", true)
			 inst.components.locomotor.disable = true
             inst.Physics:SetMotorVel(0,15+math.random()*5,0)
        end,
        
        timeline = 
        {
            TimeEvent(1.5, function(inst) 
				inst:Hide()
				inst._isflying = true
				inst.components.locomotor.disable = false
				if TheNet:GetServerGameMode() == "lavaarena" then
					inst:DoTaskInTime(5, function(inst) if inst._isflying == true and not inst.sg:HasStateTag("landing") then inst.sg:GoToState("land") end end)
				end	
				inst.sg:GoToState("glide") 
			end)
        }
        
    },	
		
	State{
        name = "glide",
        tags = {"flight2", "canrotate"},
        onenter= function(inst)
			
			local pt = Point(inst.Transform:GetWorldPosition())
			if pt.y > .1 then
                pt.y = 0
                inst.Physics:Stop()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
				
			end	
			inst._isflying = true
			--inst.Physics:SetMotorVel(8,0,0)
            inst.AnimState:PlayAnimation("idle", true)
        end,
        
        onupdate= function(inst)
        end,
		

        ontimeout = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/birds/flyin", "flyin")
        end,
    },    
		
	 State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()
            local pt = Vector3(inst.Transform:GetWorldPosition())
            if pt.y > 1 then
                inst.sg:GoToState("fall")
            end
        end,
        
        events=
        {
            EventHandler("animover", function(inst) 
                    inst.sg:GoToState("idle") 
            end ),
        },        
    },    	
	
	 State{
        name = "fall",
        tags = {"busy"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("fall_loop", true)
        end,
        
        onupdate = function(inst)
            local pt = Vector3(inst.Transform:GetWorldPosition())
            if pt.y <= .2 then
                pt.y = 0
                inst.Physics:Stop()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
	            inst.DynamicShadow:Enable(false)
                inst.sg:GoToState("stunned")
            end
        end,
    },    
		
	State{
        name = "stunned",
        tags = {"busy"},
        
        onenter = function(inst) 
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stunned_loop", true)
            inst.sg:SetTimeout(GetRandomWithVariance(6, 2) )
        end,
        
        ontimeout = function(inst) inst.sg:GoToState("idle") end,
    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
			PlayablePets.SleepHeal(inst)
		end,

		timeline=
        {

        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "run_start",
        tags = {"moving", "canrotate", "hopping"},
        
        onenter = function(inst) 
            inst.AnimState:PlayAnimation("hop")
            inst.components.locomotor:RunForward()
        end,
        
        timeline=
        {
            TimeEvent(8*FRAMES, function(inst) 
				if inst._isflying == false then
					inst.Physics:Stop() 
				end
            end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        }
    },
        
    State{            
        name = "run",
        tags = {"moving", "canrotate","running"},
        
        onenter = function(inst) 
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_loop")
        end,          
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),        
        },
        timeline=
        {

        },
    },      
    
    State{            
        name = "run_stop",
        tags = {"canrotate"},
        
        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
           -- inst.AnimState:PlayAnimation("idle")            
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    },
	
	State{            
        name = "action2",
        tags = {"busy"},
        
        onenter = function(inst) 
			if inst._isflying == true then
				inst.sg:GoToState("idle")
			else
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("peck")    
			inst:PerformBufferedAction()
			end
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    },
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"peck", nil, nil, "peck", "peck") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{

		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "distress"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "hop")
PP_CommonStates.AddOpenGiftStates(states, "idle")
CommonStates.AddHopStates(states, false, {pre = "hop", loop = "hop", pst = "idle"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "hop",
	plank_idle_loop = "idle",
	plank_idle_pst = "hop",
	
	plank_hop_pre = "hop",
	plank_hop = "hop",
	
	steer_pre = "hop",
	steer_idle = "idle",
	steer_turning = "idle",
	stop_steering = "hop",
	
	row = "hop",
}
)
    
return StateGraph("puffinp", states, events, "idle", actionhandlers)

