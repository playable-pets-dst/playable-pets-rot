require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function SetLightValue(inst, val)
    if inst.Light ~= nil then
        inst.Light:SetIntensity(.6 * val * val)
        inst.Light:SetRadius(8 * val)
        inst.Light:SetFalloff(3 * val)
    end
end

local function SetLightValueAndOverride(inst, val, override)
    if inst.Light ~= nil then
        inst.Light:SetIntensity(.6 * val * val)
        inst.Light:SetRadius(8 * val)
        inst.Light:SetFalloff(3 * val)
        inst.AnimState:SetLightOverride(override)
    end
end

local function SetLightColour(inst, val)
    if inst.Light ~= nil then
        inst.Light:SetColour(val, 0, 0)
    end
end

local longaction = "action"
local shortaction = "action"
local workaction = "work"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.REVIVE_CORPSE, "idle"), --no reviving
	ActionHandler(ACTIONS.PP_DESTROY, "attack"), --no reviving
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local SHAKE_DIST = 40

local function OnDestroy(inst)
	inst.components.health:DoDelta(50)
	inst.components.sanity:DoDelta(25)
end

local function DeerclopsFootstep(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/step")
    for i, v in ipairs(AllPlayers) do
        v:ShakeCamera(CAMERASHAKE.VERTICAL, .25, .03, 1, inst, SHAKE_DIST)
    end

	if not noice and inst.sg.mem.circle ~= nil then
		inst.sg.mem.circle:KillFX()
		inst.sg.mem.circle = SpawnPrefab("deerclops_aura_circle_fx")
		local x, y, z = inst.Transform:GetWorldPosition()
		if moving then
			local rot = inst.Transform:GetRotation() * DEGREES
			x = x + math.cos(rot) * 1.5
			z = z - math.sin(rot) * 1.5
		end
		inst.sg.mem.circle.Transform:SetPosition(x, 0, z)
		inst.sg.mem.circle.SoundEmitter:PlaySound("dontstarve/common/break_iceblock", nil, 0.4)
	end
end

local AREAATTACK_MUST_TAGS = { "_combat" }
local AREA_EXCLUDE_TAGS = { "INLIMBO", "notarget", "noattack", "flight", "invisible", "playerghost" }
local ICESPAWNTIME =  0.25

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return  {"notarget", "wall"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall", "battlestandard"}
	end
end
    
local function DoSpawnIceSpike(inst, x, z)
    SpawnPrefab("icespike_fx_"..tostring(math.random(1, 4))).Transform:SetPosition(x, 0, z)

    local ents = TheSim:FindEntities(x,0,z,1.5,AREAATTACK_MUST_TAGS,GetExcludeTagsp(inst))
    if #ents > 0 then
        for i,ent in ipairs(ents)do
            if ent ~= inst then
                if not inst._icespikeshit_targets[ent.GUID] and inst.components.combat:CanTarget(ent) and not ent.deerclopsattacked then
					inst.components.combat:DoAttack(ent)
					inst._icespikeshit = true
	                inst._icespikeshit_targets[ent.GUID] = true
                end
                ent.deerclopsattacked = true
                ent:DoTaskInTime(ICESPAWNTIME +0.03,function() ent.deerclopsattacked = nil end)
            end
        end
    end
end

local function CheckForIceSpikesMiss(inst)
	if inst._icespikeshit_task ~= nil then
		inst._icespikeshit_task:Cancel()
		inst._icespikeshit_task = nil
	end

	if not inst._icespikeshit then
        inst:PushEvent("onmissother") -- for ChaseAndAttack
	end
end

local function SpawnIceFx(inst, target)
	inst._icespikeshit_targets = {}

	local AOEarc = 35

    local x, y, z = inst.Transform:GetWorldPosition()
    local angle = inst.Transform:GetRotation()

    local num = 3
    for i=1,num do
        local newarc = 180 - AOEarc
        local theta =  inst.Transform:GetRotation()*DEGREES
        local radius = TUNING.DEERCLOPS_ATTACK_RANGE - ( (TUNING.DEERCLOPS_ATTACK_RANGE/num)*i )
        local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))
        inst:DoTaskInTime(math.random() * .25, DoSpawnIceSpike, x+offset.x, z+offset.z)
    end

    for i=math.random(12,17),1,-1 do
        local theta =  ( angle + math.random(AOEarc *2) - AOEarc ) * DEGREES
        local radius = TUNING.DEERCLOPS_ATTACK_RANGE * math.sqrt(math.random())
        local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))
        inst:DoTaskInTime(math.random() * ICESPAWNTIME, DoSpawnIceSpike, x+offset.x, z+offset.z)
    end

    for i=math.random(5,8),1,-1 do
        local newarc = 180 - AOEarc
        local theta =  ( angle -180 + math.random(newarc *2) - newarc ) * DEGREES
        local radius = 2 * math.random() +1
        local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))
        inst:DoTaskInTime(math.random() * ICESPAWNTIME, DoSpawnIceSpike, x+offset.x, z+offset.z)
    end 

	inst._icespikeshit = false
	if inst._icespikeshit_task ~= nil then
		inst._icespikeshit_task:Cancel()
	end
	inst._icespikeshit_task = inst:DoTaskInTime(ICESPAWNTIME + FRAMES, CheckForIceSpikesMiss)
end

local ICE_LANCE_RADIUS = 5.5

local function DoIceLanceAOE(inst, pt, targets)
	inst.components.combat.ignorehitrange = true
	local dist = math.sqrt(inst:GetDistanceSqToPoint(pt))
	local ents = TheSim:FindEntities(pt.x, 0, pt.z, ICE_LANCE_RADIUS, AREAATTACK_MUST_TAGS, AREA_EXCLUDE_TAGS)
	for i, v in ipairs(ents) do
		if not targets[v] and v:IsValid() and not v:IsInLimbo() and
			not (v.components.health ~= nil and v.components.health:IsDead()) and
			inst.components.combat:CanTarget(v)
		then
			local wasfrozen = v.components.freezable ~= nil and v.components.freezable:IsFrozen()
			inst.components.combat:DoAttack(v)
			if wasfrozen then
				v:PushEvent("knockback", { knocker = inst, radius = dist + ICE_LANCE_RADIUS })
			end
			targets[v] = true
		end
	end
	inst.components.combat.ignorehitrange = false
end

local function TryIceGrow(inst)
	local burning = inst.components.burnable:IsBurning()
	local shouldgrowice

	
		--in combat:
		--  -when EYE spike is NOT burning
		--    -either summon circle if needed (can be burning)
		--    -or regrow missing ice when not burning
	shouldgrowice =
		not (burning and inst.sg.mem.noice == 1 and not inst.sg.mem.noeyeice) and
		(
			(inst.hasiceaura and inst.sg.mem.circle == nil) or
			(not burning and inst.sg.mem.noice ~= nil)
		)

	if shouldgrowice then
		inst.sg:GoToState("icegrow")
		return true
	end
	inst.sg.mem.doicegrow = nil
	return false
end

local function TryStagger(inst)
	if inst.sg.mem.noice == 1 and not inst.sg.mem.noeyeice and inst.components.burnable:IsBurning() then
		inst.sg:GoToState("struggle_pre")
		return true
	end
	inst.sg.mem.dostagger = nil
	return false
end

local function ChooseAttack(inst, target)
	target = target or inst.components.combat.target
	if target ~= nil and not target:IsValid() then
		target = nil
	end

	if inst.hasicelance and inst.sg.mem.noice ~= 1 and
		(
			inst.components.burnable:IsBurning() or
			(target ~= nil and not inst:IsNear(target, TUNING.MUTATED_DEERCLOPS_ICELANCE_RANGE.min))
		)
	then
		inst.sg:GoToState("icelance", target)
		return true
	end
	if inst.haslaserbeam then
		local isfrozen, shouldfreeze = false, false
		if target ~= nil and target.components.freezable ~= nil then
			if target.components.freezable:IsFrozen() then
				isfrozen = true
			elseif target.components.freezable:ResolveResistance() - target.components.freezable.coldness <= 2 then
				shouldfreeze = true
			end
		end
		if isfrozen or not (shouldfreeze or inst.components.timer:TimerExists("laserbeam_cd")) then
			inst.sg:GoToState("laserbeam", target)
			return true
		end
	end
	inst.sg:GoToState("attack", target)
	return true
end

local function StartAttackCooldown(inst)
	if inst.sg.mem.combo ~= nil then
		inst.sg.mem.combo = inst.sg.mem.combo + 1
		if inst.sg.mem.combo == 1 then
			inst.components.combat:SetAttackPeriod(1)
		elseif inst.sg.mem.combo == 3 or math.random() < 0.5 then
			inst.sg.mem.combo = 0
			inst.components.combat:SetAttackPeriod(TUNING.MUTATED_DEERCLOPS_COMBO_ATTACK_PERIOD)
		end
	end
	inst.components.combat:StartAttack()
end

local function StartFrenzy(inst)
	if inst.hasfrenzy and not inst.frenzied then
		inst:SetFrenzied(true)
		inst.sg.mem.combo = 0
	end
end

local function StopFrenzy(inst)
	if inst.frenzied then
		inst:SetFrenzied(false)
		inst.sg.mem.combo = nil
		inst.components.combat:SetAttackPeriod(TUNING.MUTATED_DEERCLOPS_ATTACK_PERIOD)
	end
end

local function DeerclopsFootstep(inst, moving, noice)
	inst.SoundEmitter:PlaySound(inst.sounds.step)
	ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, .03, 1, inst, SHAKE_DIST)

	if not noice and inst.sg.mem.circle ~= nil then
		inst.sg.mem.circle:KillFX()
		inst.sg.mem.circle = SpawnPrefab("deerclops_aura_circle_fx")
		local x, y, z = inst.Transform:GetWorldPosition()
		if moving then
			local rot = inst.Transform:GetRotation() * DEGREES
			x = x + math.cos(rot) * 1.5
			z = z - math.sin(rot) * 1.5
		end
		inst.sg.mem.circle.Transform:SetPosition(x, 0, z)
		inst.sg.mem.circle.SoundEmitter:PlaySound("dontstarve/common/break_iceblock", nil, 0.4)
	end
end

local events=
{
    CommonHandlers.OnLocomote(false,true),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
    CommonHandlers.OnAttack(),
    EventHandler("attacked", function(inst, data)
		if inst.components.health ~= nil and not inst.components.health:IsDead() and (
			not inst.sg:HasStateTag("busy") or
			inst.sg:HasStateTag("caninterrupt") or
			inst.sg:HasStateTag("frozen")
		) then
			if inst.sg:HasStateTag("staggered") then
				inst.sg.statemem.staggered = true
				inst.sg:GoToState("stagger_hit")
			elseif not CommonHandlers.HitRecoveryDelay(inst) then
				--hit out of struggle state lowers priority for chain re-entering struggle state
				inst.sg:GoToState("hit", inst.sg:HasStateTag("struggle"))
			end
		end
	end),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
    --Mutated
	EventHandler("doicegrow", function(inst)
		if not (inst.sg:HasStateTag("icegrow") or inst.components.health:IsDead()) then
			if inst.sg:HasStateTag("busy") then
				inst.sg.mem.doicegrow = true
			else
				TryIceGrow(inst)
			end
		end
	end),
	EventHandler("onignite", function(inst)
		if inst.sg.mem.noice == 1 and not inst.sg.mem.noeyeice and inst.components.burnable:IsBurning() and
			not (inst.sg:HasStateTag("staggered") or inst.components.health:IsDead())
		then
			if inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("caninterrupt") then
				inst.sg.mem.dostagger = true
			else
				TryStagger(inst)
			end
		end
	end),
}

local states=
{  
    State{
        name = "idle",
        tags = { "idle", "canrotate" },

        onenter = function(inst)
			if (inst.sg.mem.doicegrow and TryIceGrow(inst)) or
				(inst.sg.mem.dostagger and TryStagger(inst)) then
				return
			end

            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

		onexit = function(inst)
			inst:SwitchToFourFaced()
		end,
    },	
	State{
        name = "taunt",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

        onupdate = function(inst)
            if inst.sg.statemem.lightval ~= nil then
                inst.sg.statemem.lightval = inst.sg.statemem.lightval * .99
                SetLightValue(inst, inst.sg.statemem.lightval)
            end
        end,

        timeline =
        {
            TimeEvent(2 * FRAMES, function(inst) SetLightColour(inst, .9) end),
            TimeEvent(3 * FRAMES, function(inst) SetLightColour(inst, .87) end),
            TimeEvent(4 * FRAMES, function(inst) SetLightColour(inst, .845) end),
            TimeEvent(5 * FRAMES, function(inst)
                SetLightColour(inst, .825)
				inst.SoundEmitter:PlaySound(inst.sounds.taunt_grrr)
            end),
            TimeEvent(6 * FRAMES, function(inst) SetLightColour(inst, .81) end),
            TimeEvent(7 * FRAMES, function(inst) SetLightColour(inst, .8) end),
            TimeEvent(13 * FRAMES, function(inst)
                inst.sg.statemem.lightval = 1
            end),
            TimeEvent(16 * FRAMES, function(inst)
				inst.SoundEmitter:PlaySound(inst.sounds.taunt_howl)
            end),
            TimeEvent(24 * FRAMES, function(inst)
                inst.sg.statemem.lightval = nil
            end),
            TimeEvent(41 * FRAMES, function(inst)
                SetLightValue(inst, .98)
                SetLightColour(inst, .95)
            end),
            TimeEvent(42 * FRAMES, function(inst)
                SetLightValue(inst, 1)
                SetLightColour(inst, 1)
            end),
			FrameEvent(43, function(inst)
				if inst.sg.mem.dostagger and TryStagger(inst) then
					return
				end
				inst.sg:AddStateTag("caninterrupt")
			end),
			FrameEvent(46, function(inst)
				inst.sg:RemoveStateTag("busy")
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },

        onexit = function(inst)
            SetLightValue(inst, 1)
            SetLightColour(inst, 1)
        end,
    },
    State{
        name = "special_atk1",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

        onupdate = function(inst)
            if inst.sg.statemem.lightval ~= nil then
                inst.sg.statemem.lightval = inst.sg.statemem.lightval * .99
                SetLightValue(inst, inst.sg.statemem.lightval)
            end
        end,

        timeline =
        {
            TimeEvent(2 * FRAMES, function(inst) SetLightColour(inst, .9) end),
            TimeEvent(3 * FRAMES, function(inst) SetLightColour(inst, .87) end),
            TimeEvent(4 * FRAMES, function(inst) SetLightColour(inst, .845) end),
            TimeEvent(5 * FRAMES, function(inst)
                SetLightColour(inst, .825)
				inst.SoundEmitter:PlaySound(inst.sounds.taunt_grrr)
            end),
            TimeEvent(6 * FRAMES, function(inst) SetLightColour(inst, .81) end),
            TimeEvent(7 * FRAMES, function(inst) SetLightColour(inst, .8) end),
            TimeEvent(13 * FRAMES, function(inst)
                inst.sg.statemem.lightval = 1
            end),
            TimeEvent(16 * FRAMES, function(inst)
				inst.SoundEmitter:PlaySound(inst.sounds.taunt_howl)
            end),
            TimeEvent(24 * FRAMES, function(inst)
                inst.sg.statemem.lightval = nil
            end),
            TimeEvent(41 * FRAMES, function(inst)
                SetLightValue(inst, .98)
                SetLightColour(inst, .95)
            end),
            TimeEvent(42 * FRAMES, function(inst)
                SetLightValue(inst, 1)
                SetLightColour(inst, 1)
            end),
			FrameEvent(43, function(inst)
				if inst.sg.mem.dostagger and TryStagger(inst) then
					return
				end
				inst.sg:AddStateTag("caninterrupt")
			end),
			FrameEvent(46, function(inst)
				inst.sg:RemoveStateTag("busy")
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },

        onexit = function(inst)
            SetLightValue(inst, 1)
            SetLightColour(inst, 1)
        end,
    },
	
	State{
		name = "hit",
		tags = { "hit", "busy" },

		onenter = function(inst, ignorestagger)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound(inst.sounds.hurt)
			CommonHandlers.UpdateHitRecoveryDelay(inst)
			inst.sg.statemem.ignorestagger = ignorestagger
		end,

		timeline =
		{
			FrameEvent(8, function(inst)
				if (inst.sg.mem.dostagger and not inst.sg.statemem.ignorestagger) or
					(inst.sg.statemem.doicegrow and TryIceGrow(inst)) then
					return
				end
				inst.sg.statemem.doicegrow = nil
				inst.sg.statemem.canicegrow = true
			end),
			FrameEvent(10, function(inst)
				if (inst.sg.mem.dostagger and not inst.sg.statemem.ignorestagger and TryStagger(inst)) or
					(inst.sg.statemem.doattack ~= nil and ChooseAttack(inst, inst.sg.statemem.doattack)) then
					return
				end
				inst.sg.statemem.doattack = nil
				inst.sg:RemoveStateTag("busy")
			end),
		},

		events =
		{
			EventHandler("doattack", function(inst, data)
				if not inst.sg.mem.dostagger or inst.sg.statemem.ignorestagger then
					if not inst.sg:HasStateTag("busy") then
						ChooseAttack(inst, data ~= nil and data.target or nil)
					else
						inst.sg.statemem.doattack = data ~= nil and data.target or nil
					end
				end
				return true
			end),
			EventHandler("doicegrow", function(inst)
				if not inst.sg.mem.dostagger or inst.sg.statemem.ignorestagger then
					if inst.sg.statemem.canicegrow then
						TryIceGrow(inst)
					else
						inst.sg.statemem.doicegrow = true
					end
				end
				return true
			end),
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},
	},
	
	State{
        name = "attack",
        tags = {"attack", "busy"},
        
        onenter = function(inst)
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.components.combat:StartAttack()
            if target ~= nil then
                if target:IsValid() then
                    inst:FacePoint(target:GetPosition())
                    inst.sg.statemem.attacktarget = target
                end
            end
            if target ~= nil and target:HasTag("wall") then
                local isname = inst:GetDisplayName()
                local selfpos = inst:GetPosition()
                print("LOGGED: "..isname.." attacked at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)
            end
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("atk")
        end,
        
        onexit = function(inst)
            
        end,
        
        timeline =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/attack") end),
			TimeEvent(29*FRAMES, function(inst) 
                SpawnIceFx(inst, inst.sg.statemem.attacktarget) 
            end),
			TimeEvent(35*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/swipe")
                --buffered action checks no longer work for some reason. Figure out why.
                --[[
                local bufferedaction = inst:GetBufferedAction()
				if bufferedaction and (bufferedaction.action == ACTIONS.HAMMER or bufferedaction.action == ACTIONS.PP_DESTROY) then
					bufferedaction.action = ACTIONS.HAMMER
                    print("Hammer check passed")
					PlayablePets.DoWork(inst, 99)
					OnDestroy(inst) --assuming it'll be destroyed
				elseif bufferedaction and bufferedaction ~= ACTIONS.ATTACK then
                    print("Non Attack check passed")
					PlayablePets.DoWork(inst, 8)
				end]]
				for i, v in ipairs(AllPlayers) do
					v:ShakeCamera(CAMERASHAKE.FULL, .5, .05, 2, inst, SHAKE_DIST)
				end
			end),
			TimeEvent(36*FRAMES, function(inst) inst.sg:RemoveStateTag("attack") end),
		},
     
		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

    State{
        name = "work",
        tags = {"attack", "busy"},
        
        onenter = function(inst)
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.components.combat:StartAttack()
            if target ~= nil then
                if target:IsValid() then
                    inst:FacePoint(target:GetPosition())
                    inst.sg.statemem.attacktarget = target
                end
            end
            if target ~= nil and target:HasTag("wall") then
                local isname = inst:GetDisplayName()
                local selfpos = inst:GetPosition()
                print("LOGGED: "..isname.." attacked at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)
            end
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("atk")
        end,
        
        onexit = function(inst)
            
        end,
        
        timeline =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/attack") end),
			TimeEvent(29*FRAMES, function(inst) 
                SpawnIceFx(inst, inst.sg.statemem.attacktarget) 
            end),
			TimeEvent(35*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/swipe")
                --buffered action checks no longer work for some reason. Figure out why.
				PlayablePets.DoWork(inst, 99)
				OnDestroy(inst) --assuming it'll be destroyed
				for i, v in ipairs(AllPlayers) do
					v:ShakeCamera(CAMERASHAKE.FULL, .5, .05, 2, inst, SHAKE_DIST)
				end
			end),
			TimeEvent(36*FRAMES, function(inst) inst.sg:RemoveStateTag("attack") end),
		},
     
		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State 
	{
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

		timeline =
		{
			
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/wakeup")
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    State{
		name = "walk_start",
		tags = { "moving", "canrotate" },

		onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_pre")
		end,

		timeline =
		{
			FrameEvent(7, function(inst)
				if (inst.sg.statemem.doicegrow and TryIceGrow(inst)) or
					(inst.sg.statemem.doattack ~= nil and ChooseAttack(inst, inst.sg.statemem.doattack)) then
					return
				end
				inst.sg.statemem.doicegrow = nil
				inst.sg.statemem.doattack = nil
				inst.sg.statemem.canact = true
				DeerclopsFootstep(inst, true)
			end),
		},

		events =
		{
			EventHandler("doattack", function(inst, data)
				if inst.sg.mem.circle ~= nil then
					if inst.sg.statemem.canact then
						ChooseAttack(inst, data ~= nil and data.target or nil)
					else
						inst.sg.statemem.doattack = data ~= nil and data.target or nil
					end
					return true
				end
			end),
			EventHandler("doicegrow", function(inst)
				if inst.sg.statemem.canact then
					TryIceGrow(inst)
				else
					inst.sg.statemem.doicegrow = true
				end
				return true
			end),
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg.statemem.walking = true
					inst.sg:GoToState("walk")
				end
			end),
		},

		onexit = function(inst)
			if not inst.sg.statemem.walking then
				DeerclopsFootstep(inst, false)
			end
		end,
	},

	State{
		name = "walk",
		tags = { "moving", "canrotate" },

		onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop", true)
			inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
			inst.sg.statemem.canact = true
			if inst.sounds.walk ~= nil then
				inst.SoundEmitter:PlaySound(inst.sounds.walk)
			end
		end,

		timeline =
		{
			FrameEvent(1, function(inst)
				inst.sg.statemem.canact = false
			end),
			FrameEvent(23, function(inst)
				if (inst.sg.statemem.doicegrow and TryIceGrow(inst)) or
					(inst.sg.statemem.doattack ~= nil and ChooseAttack(inst, inst.sg.statemem.doattack)) then
					return
				end
				inst.sg.statemem.doicegrow = nil
				inst.sg.statemem.doattack = nil
				inst.sg.statemem.canact = true
				DeerclopsFootstep(inst, true)
			end),
			FrameEvent(25, function(inst)
				inst.sg.statemem.canact = false
			end),
			--
			FrameEvent(47, function(inst)
				if (inst.sg.statemem.doicegrow and TryIceGrow(inst)) or
					(inst.sg.statemem.doattack ~= nil and ChooseAttack(inst, inst.sg.statemem.doattack)) then
					return
				end
				inst.sg.statemem.doicegrow = nil
				inst.sg.statemem.doattack = nil
				inst.sg.statemem.canact = true
				DeerclopsFootstep(inst, true)
			end),
		},

		ontimeout = function(inst)
			inst.sg.statemem.walking = true
			inst.sg:GoToState("walk")
		end,

		events =
		{
			EventHandler("doattack", function(inst, data)
				if inst.sg.mem.circle ~= nil then
					if inst.sg.statemem.canact then
						ChooseAttack(inst, data ~= nil and data.target or nil)
					else
						inst.sg.statemem.doattack = data ~= nil and data.target or nil
					end
					return true
				end
			end),
			EventHandler("doicegrow", function(inst)
				if inst.sg.statemem.canact then
					TryIceGrow(inst)
				else
					inst.sg.statemem.doicegrow = true
				end
				return true
			end),
		},

		onexit = function(inst)
			if not inst.sg.statemem.walking then
				DeerclopsFootstep(inst, false)
			end
		end,
    },

	State{
		name = "walk_stop",
		tags = { "canrotate" },

		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
		end,

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},
	},

    State{
		name = "pp_rangedspecial",
		tags = { "attack", "busy" },

		onenter = function(inst, pos)
			inst.components.locomotor:Stop()
			if inst.sg.mem.noice and inst.sg.mem.noice == 1 then
				--you ain't got no ice brotha
				inst.sg:GoToState("idle")
				return
			end
			inst:SwitchToEightFaced()
			if inst.sg.mem.noice == nil then
				inst.AnimState:PlayAnimation("throw")
			else
				if inst.sg.mem.noice == 1 then
					inst.sg.mem.noice = 0
					inst.AnimState:Show("ice_1")
				end
				inst.AnimState:PlayAnimation("throw_2")
			end
			StartAttackCooldown(inst)
			inst.sg.statemem.targetpos = pos or inst:GetPosition()
			inst:ForceFacePoint(pos)
		end,

		onupdate = function(inst)
			--[[
			local target = inst.sg.statemem.target
			if target ~= nil then
				if target:IsValid() then
					local p = inst.sg.statemem.targetpos
					p.x, p.y, p.z = target.Transform:GetWorldPosition()
					local rot = inst.Transform:GetRotation()
					local rot1 = inst:GetAngleToPoint(p)
					local drot = ReduceAngle(rot1 - rot)
					if math.abs(drot) < 90 then
						rot1 = rot + math.clamp(drot / 2, -1, 1)
						inst.Transform:SetRotation(rot1)
					end
				else
					inst.sg.statemem.target = nil
				end
			end]]
		end,

		timeline =
		{
			FrameEvent(8, function(inst) inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_throw_f13") end),
			FrameEvent(30, function(inst)
				inst.sg.statemem.target = nil

				local range = TUNING.MUTATED_DEERCLOPS_ICELANCE_RANGE
				local p = inst.sg.statemem.targetpos
				local x, y, z = inst.Transform:GetWorldPosition()
				local rot = inst.Transform:GetRotation() * DEGREES
				local dist
				if p ~= nil then
					local dx = p.x - x
					local dz = p.z - z
					if dx ~= 0 or dz ~= 0 then
						local rot1 = math.atan2(-dz, dx)
						local diff = DiffAngleRad(rot, rot1)
						if diff * RADIANS < 90 then
							dist = math.sqrt(dx * dx + dz * dz) * math.cos(diff)
							dist = math.clamp(dist, range.min, range.max)
						else
							dist = range.min
						end
					else
						dist = range.min
					end
					p.y = 0
				else
					dist = (range.min + range.max) * 0.5
					p = Vector3(0, 0, 0)
					inst.sg.statemem.targetpos = p
				end
				p.x = x + math.cos(rot) * dist
				p.z = z - math.sin(rot) * dist

				inst.sg.statemem.ping = SpawnPrefab("deerclops_icelance_ping_fx")
				inst.sg.statemem.ping.Transform:SetPosition(p:Get())
			end),
			FrameEvent(34, function(inst)
				inst.components.burnable:Extinguish()
				inst.sg.mem.noice = inst.sg.mem.noice == nil and 0 or 1
			end),
			FrameEvent(47, function(inst) inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_throw_f47") end),
			FrameEvent(56, function(inst)
				DeerclopsFootstep(inst, false, true)
			end),
			FrameEvent(60, function(inst)
				inst.SoundEmitter:PlaySound(inst.sounds.attack)
				inst.sg.statemem.ping:KillFX()
				inst.sg.statemem.ping = nil

				local lance = SpawnPrefab("deerclops_impact_circle_fx")
				lance.Transform:SetPosition(inst.sg.statemem.targetpos:Get())
				inst.sg.statemem.targets = {}
				inst.sg.statemem.freezepower = 99
				inst.components.combat:SetDefaultDamage(TUNING.MUTATED_DEERCLOPS_ICELANCE_DAMAGE)
				DoIceLanceAOE(inst, inst.sg.statemem.targetpos, inst.sg.statemem.targets)
			end),
			FrameEvent(61, function(inst)
				DoIceLanceAOE(inst, inst.sg.statemem.targetpos, inst.sg.statemem.targets)
			end),
			FrameEvent(62, function(inst)
				DoIceLanceAOE(inst, inst.sg.statemem.targetpos, inst.sg.statemem.targets)
				if next(inst.sg.statemem.targets) == nil then
					inst:PushEvent("onmissother", { target = inst.sg.statemem.original_target }) --for ChaseAndAttack
				end
			end),
			FrameEvent(72, function(inst)
				if inst.sg.mem.dostagger and TryStagger(inst) then
					return
				end
				inst.sg:AddStateTag("caninterrupt")
			end),
			FrameEvent(76, function(inst)
				inst.sg:RemoveStateTag("busy")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg.statemem.keepfacing = true
					inst.sg:GoToState("idle")
				end
			end),
		},

		onexit = function(inst)
			inst:SwitchToFourFaced()
			if inst.sg.mem.noice == 0 then
				inst.AnimState:Hide("ice_0")
			elseif inst.sg.mem.noice == 1 then
				inst.AnimState:Hide("ice_1")
				inst.components.combat:SetRange(inst.mob_table.range)
			end
			if inst.sg.statemem.ping ~= nil then
				inst.sg.statemem.ping:KillFX()
			end
			inst.components.combat:SetDefaultDamage(inst.mob_table.damage)
		end,
	},

	State{
		name = "icegrow",
		tags = { "icegrow", "busy" },

		onenter = function(inst)
			inst.sg.mem.doicegrow = nil
			inst.sg.mem.noeyeice = true
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("ice_grow")
			local growice
			if inst.sg.mem.noice == nil then
				inst.AnimState:Hide("grow_ice_0")
				inst.AnimState:Hide("grow_ice_1")
			else
				inst.AnimState:Show("grow_ice_0")
				if inst.sg.mem.noice == 1 then
					inst.AnimState:Show("grow_ice_1")
				else
					inst.AnimState:Hide("grow_ice_1")
				end
				inst.sg.statemem.didgrowice = true
				growice = true
			end
			if inst.sg.mem.noeyeice then
				inst.sg.statemem.groweyeice = true
				growice = true
				inst.sg.statemem.didgrowice = true
			end
			if growice then
				inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_crackling_LP", "loop")
			end
			inst.sg.statemem.burninterrupt = true
		end,

		timeline =
		{
			FrameEvent(7, function(inst)
				DeerclopsFootstep(inst, false, true)
			end),
			FrameEvent(9, function(inst)
				inst.sg.statemem.burninterrupt = nil
				inst.components.burnable:Extinguish()
				inst.components.burnable:SetBurnTime(0)
				if inst.hasiceaura and inst.sg.mem.circle == nil and (not inst.frenzied or inst.sg.statemem.groweyeice) then
					inst.sg.mem.circle = SpawnPrefab("deerclops_aura_circle_fx")
					local x, y, z = inst.Transform:GetWorldPosition()
					inst.sg.mem.circle.Transform:SetPosition(x, 0, z)
					inst.sg.mem.circle:GrowFX()
					inst.sg.statemem.newcircle = true
				end
				inst.sg.mem.dostagger = nil
			end),
			FrameEvent(5, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.taunt_grrr) end),
			FrameEvent(6, function(inst)
				if inst.sg.mem.noice == 1 then
					inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_grow_4f_leadin")
				end
			end),
			FrameEvent(10, function(inst)
				if inst.sg.mem.noice == 1 then
					inst.sg.mem.noice = 0
				end
			end),
			FrameEvent(9, function(inst)
				if inst.sg.mem.noice == 0 then
					inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_grow_4f_leadin")
				end
			end),
			FrameEvent(13, function(inst)
				if inst.sg.mem.noice == 0 then
					inst.sg.mem.noice = nil
					if not inst.sg.statemem.groweyeice then
						inst.SoundEmitter:KillSound("loop")
					end
				end
			end),
			FrameEvent(35, function(inst)
				if inst.sg.statemem.groweyeice then
					inst.sg.statemem.didgrowice = true
					inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_grow_4f_leadin")
				end
			end),
		},

		events =
		{
			EventHandler("onignite", function(inst)
				if inst.sg.statemem.burninterrupt and inst.sg.mem.noice == 1 and not inst.sg.mem.noeyeice and inst.components.burnable:IsBurning() then
					inst.sg:GoToState("hit")
					--don't return true, let stategraph "onignite" handler manage stagger
				end
			end),
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					if inst.sg.statemem.newcircle or inst.sg.statemem.groweyeice then
						inst.sg.statemem.icegrow = true
						inst.sg:GoToState("icegrow2")
					else
						inst.sg:GoToState("icegrow_pst")
					end
				end
			end),
		},

		onexit = function(inst)
			if inst.sg.mem.noice ~= 1 then
				inst.AnimState:Show("ice_1")
				if inst.sg.mem.noice ~= 0 then
					inst.AnimState:Show("ice_0")
				end
				inst.components.combat:SetRange(TUNING.MUTATED_DEERCLOPS_ATTACK_RANGE)
			end
			if not inst.sg.statemem.icegrow then
				inst.SoundEmitter:KillSound("loop")
				inst.components.burnable:SetBurnTime(10)
			end
			if inst.sg.statemem.didgrowice then
				inst.taunt2 = false
				if inst.taunt2_task then
					inst.taunt2_task:Cancel()
					inst.taunt2_task = nil
				end
				inst.taunt2_task = inst:DoTaskInTime(15, function(inst) inst.taunt2 = true end)
			end
			inst.sg.statemem.didgrowice = nil
		end,
	},

	State{
		name = "special_atk2",
		tags = { "icegrow", "busy" },

		onenter = function(inst)
			inst.sg.mem.doicegrow = nil
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("ice_grow")
			local growice
			if inst.sg.mem.noice == nil then
				inst.AnimState:Hide("grow_ice_0")
				inst.AnimState:Hide("grow_ice_1")
			else
				inst.AnimState:Show("grow_ice_0")
				if inst.sg.mem.noice == 1 then
					inst.AnimState:Show("grow_ice_1")
				else
					inst.AnimState:Hide("grow_ice_1")
				end
				inst.sg.statemem.didgrowice = true
				growice = true
			end
			if inst.sg.mem.noeyeice then
				inst.sg.statemem.groweyeice = true
				growice = true
				inst.sg.statemem.didgrowice = true
			end
			if growice then
				inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_crackling_LP", "loop")
			end
			inst.sg.statemem.burninterrupt = true
		end,

		timeline =
		{
			FrameEvent(7, function(inst)
				DeerclopsFootstep(inst, false, true)
			end),
			FrameEvent(9, function(inst)
				inst.sg.statemem.burninterrupt = nil
				inst.components.burnable:Extinguish()
				inst.components.burnable:SetBurnTime(0)
				if inst.hasiceaura and inst.sg.mem.circle == nil and (not inst.frenzied or inst.sg.statemem.groweyeice) then
					inst.sg.mem.circle = SpawnPrefab("deerclops_aura_circle_fx")
					local x, y, z = inst.Transform:GetWorldPosition()
					inst.sg.mem.circle.Transform:SetPosition(x, 0, z)
					inst.sg.mem.circle:GrowFX()
					inst.sg.statemem.newcircle = true
				end
				inst.sg.mem.dostagger = nil
			end),
			FrameEvent(5, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.taunt_grrr) end),
			FrameEvent(6, function(inst)
				if inst.sg.mem.noice == 1 then
					inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_grow_4f_leadin")
				end
			end),
			FrameEvent(10, function(inst)
				if inst.sg.mem.noice == 1 then
					inst.sg.mem.noice = 0
				end
			end),
			FrameEvent(9, function(inst)
				if inst.sg.mem.noice == 0 then
					inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_grow_4f_leadin")
				end
			end),
			FrameEvent(13, function(inst)
				if inst.sg.mem.noice == 0 then
					inst.sg.mem.noice = nil
					if not inst.sg.statemem.groweyeice then
						inst.SoundEmitter:KillSound("loop")
					end
				end
			end),
			FrameEvent(35, function(inst)
				if inst.sg.statemem.groweyeice then
					inst.sg.statemem.didgrowice = true
					inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_grow_4f_leadin")
				end
			end),
		},

		events =
		{
			EventHandler("onignite", function(inst)
				if inst.sg.statemem.burninterrupt and inst.sg.mem.noice == 1 and not inst.sg.mem.noeyeice and inst.components.burnable:IsBurning() then
					inst.sg:GoToState("hit")
					--don't return true, let stategraph "onignite" handler manage stagger
				end
			end),
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					if inst.sg.statemem.newcircle or inst.sg.statemem.groweyeice then
						inst.sg.statemem.icegrow = true
						inst.sg:GoToState("icegrow2")
					else
						inst.sg:GoToState("icegrow_pst")
					end
				end
			end),
		},

		onexit = function(inst)
			if inst.sg.mem.noice ~= 1 then
				inst.AnimState:Show("ice_1")
				if inst.sg.mem.noice ~= 0 then
					inst.AnimState:Show("ice_0")
				end
				inst.components.combat:SetRange(TUNING.MUTATED_DEERCLOPS_ATTACK_RANGE)
			end
			if not inst.sg.statemem.icegrow then
				inst.SoundEmitter:KillSound("loop")
				inst.components.burnable:SetBurnTime(10)
			end
			if inst.sg.statemem.didgrowice then
				inst.taunt2 = false
				if inst.taunt2_task then
					inst.taunt2_task:Cancel()
					inst.taunt2_task = nil
				end
				inst.taunt2_task = inst:DoTaskInTime(15, function(inst) inst.taunt2 = true end)
			end
			inst.sg.statemem.didgrowice = nil
		end,
	},

	State{
		name = "icegrow2",
		tags = { "busy" },

		onenter = function(inst)
			inst.AnimState:PlayAnimation("ice_grow_2")
			inst.SoundEmitter:PlaySound(inst.sounds.taunt_howl)
			if not inst.SoundEmitter:PlayingSound("loop") then
				inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_crackling_LP", "loop")
			end
			if inst.sg.mem.noeyeice then
				inst.AnimState:Show("grow_ice_2")
				inst.AnimState:Hide("gestalt_eye")
			else
				inst.AnimState:Hide("grow_ice_2")
			end
		end,

		timeline =
		{
			FrameEvent(3, function(inst)
				inst.sg.mem.noeyeice = nil
				StopFrenzy(inst)
			end),
			FrameEvent(8, function(inst) inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_grow_4f_leadin", nil, 0.5) end),
			FrameEvent(17, function(inst) inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_grow_4f_leadin", nil, 0.6) end),
			FrameEvent(21, function(inst)
				inst.SoundEmitter:KillSound("loop")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("icegrow_pst")
				end
			end),
		},

		onexit = function(inst)
			if inst.sg.mem.noeyeice then
				inst.AnimState:Show("gestalt_eye")
			else
				inst.AnimState:Show("ice_2")
			end
			inst.components.burnable:SetBurnTime(10)
			inst.SoundEmitter:KillSound("loop")
		end,
	},

	State{
		name = "icegrow_pst",
		tags = { "busy" },

		onenter = function(inst)
			inst.AnimState:PlayAnimation("ice_grow_pst")
		end,

		timeline =
		{
			FrameEvent(12, function(inst)
				if inst.sg.mem.dostagger and TryStagger(inst) then
					return
				end
				inst.sg:AddStateTag("caninterrupt")
			end),
			FrameEvent(14, function(inst)
				inst.sg:GoToState("idle", true)
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},
	},

	--------------------------------------------------------------------------

	State{
		name = "struggle_pre",
		tags = { "struggle", "busy" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("struggle_pre")
			inst.SoundEmitter:PlaySound(inst.sounds.hurt)
			if inst.sg.mem.noice ~= 1 then
				inst.sg.mem.noice = 1
				inst.AnimState:Hide("ice_0")
				inst.AnimState:Hide("ice_1")
			end
		end,

		timeline =
		{
			FrameEvent(14, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack, nil, 0.5) end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState(inst.components.burnable:IsBurning() and "struggle_loop" or "struggle_pst")
				end
			end),
		},
	},

	State{
		name = "struggle_loop",
		tags = { "struggle", "busy" },

		onenter = function(inst, loops)
			inst.components.locomotor:Stop()
			if not inst.AnimState:IsCurrentAnimation("struggle_loop") then
				inst.AnimState:PlayAnimation("struggle_loop", true)
			end
			inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
			inst.sg.statemem.loops = (loops or 0) + 1
			if inst.sg.mem.noice ~= 1 then
				inst.sg.mem.noice = 1
				inst.AnimState:Hide("ice_0")
				inst.AnimState:Hide("ice_1")
			end
		end,

		timeline =
		{
			FrameEvent(0, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.hurt) end),
			FrameEvent(10, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack, nil, 0.5) end),
		},

		ontimeout = function(inst)
			if not inst.components.burnable:IsBurning() then
				inst.sg:GoToState("struggle_pst")
			elseif inst.sg.statemem.loops >= 2 then
				inst.sg:GoToState("stagger_pre")
			else
				inst.sg:GoToState("struggle_loop", inst.sg.statemem.loops)
			end
		end,
	},

	State{
		name = "struggle_pst",
		tags = { "struggle", "busy" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("struggle_pst")
			inst.components.burnable:Extinguish()
			inst.SoundEmitter:PlaySound(inst.sounds.hurt)
			if inst.sg.mem.noice ~= 1 then
				inst.sg.mem.noice = 1
				inst.AnimState:Hide("ice_0")
				inst.AnimState:Hide("ice_1")
			end
		end,

		timeline =
		{
			FrameEvent(7, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.taunt_grrr, nil, .6) end),
			FrameEvent(21, function(inst)
				if inst.sg.statemem.doattack == nil then
					if inst.sg.mem.dostagger and TryStagger(inst) then
						return
					end
					inst.sg:AddStateTag("caninterrupt")
				end
			end),
		},

		events =
		{
			EventHandler("doattack", function(inst, data)
				inst.sg.statemem.doattack = data ~= nil and data.target or nil
				inst.sg:RemoveStateTag("caninterrupt")
				return true
			end),
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					if inst.sg.statemem.doattack ~= nil and ChooseAttack(inst, inst.sg.statemem.doattack) then
						return
					end
					inst.sg:GoToState("idle")
				end
			end),
		},
	},

	--------------------------------------------------------------------------

	State{
		name = "stagger_pre",
		tags = { "staggered", "busy", "nosleep" },

		onenter = function(inst)
			inst.sg.mem.dostagger = nil
			inst.sg.mem.doicegrow = nil
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("stagger_pre")
			inst.components.timer:StopTimer("stagger")
			inst.components.timer:StartTimer("stagger", TUNING.MUTATED_DEERCLOPS_STAGGER_TIME)
			if inst.components.burnable:IsBurning() then
				inst.components.burnable:SetBurnTime(8 * FRAMES)
				inst.components.burnable:ExtendBurning()
			end
			if inst.sg.mem.noice ~= 1 then
				inst.sg.mem.noice = 1
				inst.AnimState:Hide("ice_0")
				inst.AnimState:Hide("ice_1")
			end
		end,

		timeline =
		{
			FrameEvent(3, function(inst)
				if not inst.sg.mem.noeyeice then
					inst.sg.statemem.shatter = true
					inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/stunned_pre_break_f13")
				end
			end),
			FrameEvent(8, function(inst)
				inst.components.burnable:Extinguish()
				inst.components.burnable:SetBurnTime(10)
				inst.sg.mem.noeyeice = true
			end),
			FrameEvent(24, function(inst)
				DeerclopsFootstep(inst, false, true)
			end),
			FrameEvent(26, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/common/iceboulder_smash", nil, .5) end),
			FrameEvent(27, function(inst)
				if inst.sg.mem.circle ~= nil then
					inst.sg.mem.circle:KillFX(true)
					inst.sg.mem.circle = nil
				end
			end),
			FrameEvent(44, function(inst)
				if TheWorld.state.snowlevel > 0.02 then
					inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_snow")
				else
					inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt")
				end
				ShakeAllCameras(CAMERASHAKE.FULL, .7, .02, 2, inst, SHAKE_DIST)
			end),
			FrameEvent(46, function(inst)
				inst.sg:AddStateTag("caninterrupt")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg.statemem.staggered = true
					inst.sg:GoToState(inst.components.timer:TimerExists("stagger") and "stagger_idle" or "stagger_pst")
				end
			end),
		},

		onexit = function(inst)
			if inst.sg.mem.noeyeice then
				inst.AnimState:Hide("ice_2")
				inst.AnimState:Show("gestalt_eye")
			end
			inst.components.burnable:SetBurnTime(10)
		end,
	},

	State{
		name = "stagger_idle",
		tags = { "staggered", "busy", "caninterrupt", "nosleep" },

		onenter = function(inst)
			if not inst.components.timer:TimerExists("stagger") then
				inst.sg.statemem.staggered = true
				inst.sg:GoToState("stagger_pst")
				return
			end
			inst.AnimState:PlayAnimation("stagger", true)
			if inst.sg.mem.noice ~= 1 then
				inst.sg.mem.noice = 1
				inst.AnimState:Hide("ice_0")
				inst.AnimState:Hide("ice_1")
			end
			if not inst.sg.mem.noeyeice then
				inst.sg.mem.noeyeice = true
				inst.AnimState:Hide("ice_2")
				inst.AnimState:Show("gestalt_eye")
			end
			inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
		end,

		events =
		{
			EventHandler("timerdone", function(inst, data)
				if data ~= nil and data.name == "stagger" then
					inst.sg.statemem.staggered = true
					inst.sg:GoToState("stagger_pst", true)
				end
			end),
		},
	},

	State{
		name = "stagger_hit",
		tags = { "staggered", "busy", "hit", "nosleep" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("stagger_hit")
			if inst.sg.mem.noice ~= 1 then
				inst.sg.mem.noice = 1
				inst.AnimState:Hide("ice_0")
				inst.AnimState:Hide("ice_1")
			end
			if not inst.sg.mem.noeyeice then
				inst.sg.mem.noeyeice = true
				inst.AnimState:Hide("ice_2")
				inst.AnimState:Show("gestalt_eye")
			end
		end,

		timeline =
		{
			FrameEvent(8, function(inst)
				if inst.components.timer:TimerExists("stagger") then
					inst.sg:AddStateTag("caninterrupt")
				end
			end),
			FrameEvent(21, function(inst)
				if not inst.components.timer:TimerExists("stagger") then
					inst.sg.statemem.staggered = true
					inst.sg:GoToState("stagger_pst", true)
					return
				end
				inst.sg.statemem.cangetup = true
			end),
		},

		events =
		{
			EventHandler("timerdone", function(inst, data)
				if data ~= nil and data.name == "stagger" and inst.sg.statemem.cangetup then
					inst.sg.statemem.staggered = true
					inst.sg:GoToState("stagger_pst", true)
				end
			end),
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg.statemem.staggered = true
					if inst.components.timer:TimerExists("stagger") then
						inst.sg:GoToState("stagger_idle")
					else
						inst.sg:GoToState("stagger_pst", true)
					end
				end
			end),
		},
	},

	State{
		name = "stagger_pst",
		tags = { "staggered", "busy", "nosleep" },

		onenter = function(inst, nohit)
			inst.AnimState:PlayAnimation("stagger_pst")
			inst.AnimState:Show("ice_0")
			inst.AnimState:Show("ice_1")
			inst.sg.statemem.groweyeice = false --hard-coded toggle
			if inst.sg.statemem.groweyeice then
				inst.AnimState:Show("ice_2")
				inst.AnimState:Hide("gestalt_eye")
			else
				inst.AnimState:Hide("ice_2")
				inst.AnimState:Show("gestalt_eye")
				inst.sg.mem.doicegrow = nil
			end
			inst.sg.mem.noice = 1
			inst.sg.mem.noeyeice = true
			if not nohit then
				inst.sg:AddStateTag("caninterrupt")
			end
			if inst.components.sleeper ~= nil then
				inst.components.sleeper:WakeUp()
			end
			inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/stunned_pst_f0")
		end,
		timeline =
		{
			FrameEvent(33, function(inst)
				inst.sg:RemoveStateTag("staggered")
				inst.sg:RemoveStateTag("caninterrupt")
				inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_crackling_LP", "loop")
			end),
			FrameEvent(51, function(inst)
				if inst.sg.statemem.groweyeice then
					inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_grow_4f_leadin")
				end
			end),
			FrameEvent(55, function(inst)
				if inst.sg.statemem.groweyeice then
					inst.components.burnable:Extinguish()
					inst.components.burnable:SetBurnTime(0)
					inst.sg.mem.noeyeice = nil
				end
			end),
			FrameEvent(52, function(inst) inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/ice_grow_4f_leadin") end),
			FrameEvent(56, function(inst)
				inst.SoundEmitter:KillSound("loop")
				if inst.sg.statemem.groweyeice then
					inst.components.burnable:SetBurnTime(10)
				else
					inst.components.burnable:Extinguish()
				end
				inst.sg.mem.noice = nil
			end),
			FrameEvent(70, function(inst) inst.SoundEmitter:PlaySound("rifts3/mutated_deerclops/stunned_pst_f70") end),
			FrameEvent(73, function(inst)
				DeerclopsFootstep(inst, false, true)
			end),
			CommonHandlers.OnNoSleepFrameEvent(92, function(inst)
				if inst.sg.mem.dostagger and TryStagger(inst) then
					return
				end
				inst.sg:RemoveStateTag("nosleep")
				inst.sg:AddStateTag("caninterrupt")
			end),
			FrameEvent(100, function(inst)
				inst.sg:RemoveStateTag("busy")
				if inst.sg.mem.noeyeice then
					StartFrenzy(inst)
					inst.sg.mem.doicegrow = nil
				end
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},

		onexit = function(inst)
			inst.SoundEmitter:KillSound("loop")
			if inst.sg.mem.noice ~= nil then
				inst.AnimState:Hide("ice_0")
				if inst.sg.mem.noice ~= 0 then
					inst.AnimState:Hide("ice_1")
				end
			end
			if inst.sg.mem.noeyeice then
				inst.AnimState:Hide("ice_2")
				inst.AnimState:Show("gestalt_eye")
				if not inst.sg:HasStateTag("staggered") then
					StartFrenzy(inst)
					inst.sg.mem.doicegrow = nil
				end
			else
				inst.AnimState:Show("ice_2")
				inst.AnimState:Hide("gestalt_eye")
			end
			inst.components.burnable:SetBurnTime(10)
		end,
	},
	State{
        name = "death",
        tags = {"busy", "pausepredict", "nomorph"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()
            inst.SoundEmitter:PlaySound(inst.sounds.death)

            inst.AnimState:PlayAnimation("death")
			--inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.lootdropper:DropLoot(inst:GetPosition())
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
            if inst.components.burnable.nocharring then
				inst.components.burnable.fastextinguish = true
				inst.components.burnable:SetBurnTime(0)
			end
        end,
		
		 timeline=
    {
        TimeEvent(50*FRAMES, function(inst)
            if TheWorld.state.snowlevel > 0.02 then
                inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_snow")
            else
                inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt")
            end
            for i, v in ipairs(AllPlayers) do
                v:ShakeCamera(CAMERASHAKE.FULL, .7, .02, 3, inst, SHAKE_DIST)
            end
			if inst.sg.mem.circle ~= nil then
				inst.sg.mem.circle:KillFX(true)
				inst.sg.mem.circle = nil
			end
        end),
    },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)			
                end
            end),
        },
}

}
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/death") end),
			TimeEvent(50*FRAMES, function(inst)
            if TheWorld.state.snowlevel > 0.02 then
                inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_snow")
            else
                inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt")
            end
            for i, v in ipairs(AllPlayers) do
                v:ShakeCamera(CAMERASHAKE.FULL, .7, .02, 3, inst, SHAKE_DIST)
            end
        end),
		},
		
		corpse_taunt =
		{
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/taunt_grrr") end),
            TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/taunt_howl") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)

return StateGraph("deerplayer", states, events, "icegrow", actionhandlers)

