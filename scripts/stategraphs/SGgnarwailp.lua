require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.CAST_NET, longaction),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local BOAT_ATTACK_DISTANCESQ = (TUNING.GNARWAIL.TARGET_DISTANCE + TUNING.MAX_WALKABLE_PLATFORM_RADIUS) ^ 2
local RUNNING_DIVE_DISTANCESQ = 144

local function return_to_idle(inst)
    inst.sg:GoToState("idle")
end

local function return_to_emerge(inst)
    inst.sg:GoToState("emerge")
end

local function body_slam_attack(inst)
    local combat = inst.components.combat
    if combat.target and combat.target:IsValid() then
        combat:DoAttack()
    end
end

local function spawn_body_slam_waves(inst)
    SpawnAttackWaves(inst:GetPosition(), nil, 1, 6, nil, 3, nil, 1, true)
    inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/large")
end

local function IsOnLand(target)
	--We don't attack users on land, but we do attack users on boats.
	if TheNet:GetServerGameMode() == "lavaarena" then
		return false --ocean mobs are on land in forge so might as well always return false
	end
	return TheWorld.Map:IsVisualGroundAtPoint(target:GetPosition():Get()) and not TheWorld.Map:GetPlatformAtPoint(target:GetPosition():Get())
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	--CommonHandlers.OnHop(),
	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{
     State {
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
            inst.sg:SetTimeout(1 + math.random() * 1)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState((math.random() > 0.5 and "headbang_idle") or "idle")
        end,
    },

    State {
        name = "emerge",
        tags = {"busy", "noattack"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("emerge")
        end,

        events =
        {
            EventHandler("animover", return_to_idle),
        },

        timeline =
        {
            TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/common/together/water/emerge/medium") end),
            TimeEvent(6*FRAMES, function(inst)
                inst.sg:RemoveStateTag("noattack")
            end),
        },
    },

    State {
        name = "headbang_idle",
        tags = {"idle", "busy", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("idle2_loop")
        end,

        events =
        {
            EventHandler("animover", return_to_idle),
        },

        timeline= 
        {
            TimeEvent(8*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("turnoftides/common/together/water/splash/jump_small")
            end),
            TimeEvent(10*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("hookline/creatures/gnarwail/idle", {timeoffset=math.random()})
            end),
            TimeEvent(20*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("turnoftides/common/together/water/splash/jump_small")
            end),
        }
    },

    State {
        name = "headbang",
        tags = {"idle"},

        onenter = function(inst, timeout)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("idle2_loop", true)

            if timeout then
                inst.sg:SetTimeout(timeout)
            end
        end,

        timeline= 
        {
            TimeEvent(9*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("hookline/creatures/gnarwail/idle", {timeoffset=math.random()})
            end),
            TimeEvent(22*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("hookline/creatures/gnarwail/idle", {timeoffset=math.random()})
            end),
        },

        ontimeout = return_to_idle,
    },
	
	State {
        name = "eat",
		tags = { "busy" },
		
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat_pre", false)
            inst.AnimState:PushAnimation("eat_loop", false)
            inst.AnimState:PushAnimation("eat_pst", false)
        end,

		timeline =
        {
            TimeEvent(3*FRAMES, function(inst)
				  inst:PerformBufferedAction()
                inst.SoundEmitter:PlaySound(inst.sounds.eat)
            end)
        },
		
        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound(inst.sounds.hit)
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.AnimState:PlayAnimation("dead", false)
            inst:PushAnimation("dead_loop", true)
            
            inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/death")
			
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

		timeline=
        {
            TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/common/together/water/submerge/medium",nil,.5) end),
            TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/common/together/water/emerge/medium",nil,.5) end),
        },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "special_atk1",
        tags = {"busy", "canrotate"},

        onenter = function(inst, target_position)
            inst.components.locomotor:Stop()

            if target_position ~= nil then
                inst:ForceFacePoint(target_position)
            end
			
            inst.AnimState:PlayAnimation("submerge")
            inst:PushAnimation("taunt_2", false)
        end,

        events =
        {
            EventHandler("animqueueover", return_to_emerge),
        },

        timeline =
        {
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/run") end),
			TimeEvent(30*FRAMES, function(inst)
				local ent = FindEntity(inst, 15, nil, {"oceanfish"})
				if ent then
					inst.sg:GoToState("attack_horn", ent)
				end
			end)
        },
    },
	
	State {
        name = "special_atk2",
        tags = {"busy", "eating"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("walk_pst")
			if inst.shouldwalk then
				inst.shouldwalk = false
			else
				inst.shouldwalk = true
			end
        end,

        timeline =
        {
		
        },

        events =
        {
            EventHandler("animover", return_to_idle),
        },
    },
	
	 State {
        name = "eat",
        tags = { "busy", "eating" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            
            inst.AnimState:PlayAnimation("bite")
        end,

        timeline =
        {
            TimeEvent(0*FRAMES, function(inst)
                inst:PerformBufferedAction()
                --inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/chew")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("chewing", GetRandomMinMax(2, 4))
            end),
        },

        onexit = function(inst)
            inst:ClearBufferedAction()
        end,
    },

    State {
        name = "chewing",
        tags = {"busy", "eating"},

        onenter = function(inst, iterations)
            inst.AnimState:PlayAnimation("chew")

            inst.sg.statemem.its_remaining = (iterations and iterations - 1) or 0
        end,

        timeline =
        {
            TimeEvent(20*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/chew")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.sg.statemem.its_remaining > 0 then
                    inst.sg:GoToState("chewing", inst.sg.statemem.its_remaining)
                else
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        timeline =
		{
			TimeEvent(0, function(inst)
				if inst._water_shadow ~= nil then
					inst._water_shadow.AnimState:PlayAnimation("sleep_pre")
				end
			end),
			TimeEvent(6*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/yawn")
			end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
		end,

		timeline=
        {
			TimeEvent(0, function(inst)
				if inst._water_shadow ~= nil then
					inst._water_shadow.AnimState:PlayAnimation("sleep_loop")
				end
			end),
			TimeEvent(6*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/sleep_out")
			end),
			TimeEvent(18*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/sleep_in")
				PlayablePets.SleepHeal(inst)
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

	    -- WALK
    State
    {
        name = "walk_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
        end,

        timeline =
        {
            TimeEvent(8*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/jump_small",nil,.25)
                inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/hop")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("walk")
                end
            end),
        },
    },

    State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_loop")
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
        end,

        timeline =
        {
            TimeEvent(6*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/jump_small",nil,.25)
                inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/hop")
            end),
        },

        ontimeout = function(inst)
            inst.sg:GoToState("walk")
        end,
    },

    State
    {
        name = "walk_stop",
        tags = { "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            local run_anim_time_remaining = inst.AnimState:GetCurrentAnimationTime() % inst.AnimState:GetCurrentAnimationLength()
            inst.sg:SetTimeout(run_anim_time_remaining + 1*FRAMES)

            inst:PushAnimation("walk_pst", false)
        end,

        ontimeout = function(inst)
            inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/jump_small",nil,.25)
            inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/hop")
        end,

        events =
        {
            EventHandler("animqueueover", return_to_idle),
        },
    },

    -- ROLLING RUN
    State {
        name = "run_start",
        tags = {"moving", "running", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("submerge")
        end,

        timeline =
        {
            TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/run") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("run")
                end
            end),
        },
    },

    State {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("run", true)
        end,

        timeline =
        {
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/run") end),
            TimeEvent(76*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/run") end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end),
        },
    },

    State {
        name = "run_stop",
        tags = {"idle"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            -- We want to play the emerge sound 3 frames into the emerge animation, but we're softstopping, so we could be any number of frames
            -- into the run animation when we get here. So we calculate that and play the sound as a timeout trigger instead.
            local run_anim_time_remaining = inst.AnimState:GetCurrentAnimationTime() % inst.AnimState:GetCurrentAnimationLength()
            inst.sg:SetTimeout(run_anim_time_remaining + 3*FRAMES)

            inst:PushAnimation("emerge", false)
        end,

        ontimeout = function(inst)
            inst.SoundEmitter:PlaySound("turnoftides/common/together/water/emerge/medium")
        end,

        events =
        {
            EventHandler("animqueueover", return_to_idle),
        },
    },
	
    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

		timeline = 
		{
			TimeEvent(0, function(inst)
				if inst._water_shadow ~= nil then
					inst._water_shadow.AnimState:PlayAnimation("sleep_pst")
				end
			end),
			TimeEvent(4*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/hop")
			end),
		},
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	-- HIT
    State
    {
        name = "hit",
        tags = { "hit", "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            inst.AnimState:PlayAnimation("hit")
        end,

        events =
        {
            EventHandler("animover", return_to_idle),
        },
    },
	
	State
    {
        name = "attack",
        tags = { "attack", "busy" },

        onenter = function(inst, target)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target ~= nil then
				if target:IsValid() then
					inst:FacePoint(target:GetPosition())
					inst.sg.statemem.attacktarget = target
				end
			end
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("submerge")
			inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength() + 1.3)
        end,
		
		timeline =
        {
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/run") end),
        },

        ontimeout = function(inst)
			local target = inst.sg.statemem.attacktarget
			if target and not target.components.health:IsDead() and not IsOnLand(target) then
				local pos = target:GetPosition()
				inst.sg:GoToState(TheWorld.Map:GetPlatformAtPoint(pos:Get()) and "attack_boat" or "attack_horn", inst.sg.statemem.attacktarget)
			else
				inst.sg:GoToState("emerge")
			end
		end
    },
	
	State
    {
        name = "attack_horn",
        tags = { "attack", "busy" },

        onenter = function(inst, target)
			if target then
				inst.sg.statemem.attacktarget = target
				inst.Transform:SetPosition(target:GetPosition():Get()) 
			end
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("toss")
        end,
		
		timeline =
        {
            TimeEvent(3*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("turnoftides/common/together/water/emerge/medium")
            end),
            TimeEvent(6*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("turnoftides/common/together/water/emerge/medium")
            end),
            TimeEvent(19*FRAMES, function(inst)
				if inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget:HasTag("oceanfish") then
					inst:TossItem(inst.sg.statemem.attacktarget, inst.sg.statemem.attacktarget)
				end
                inst:PerformBufferedAction()
				
            end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline/creatures/gnarwail/toss") end),
        },

        events =
        {
            EventHandler("animover", return_to_idle),
        },
    },

    State {
        name = "attack_boat",
        tags = { "attack", "busy" },

        onenter = function(inst, target)
            local pos = target:GetPosition()

            local horn_attack_prefab = SpawnPrefab("gnarwail_attack_horn")
            horn_attack_prefab.Transform:SetPosition(pos:Get())

            -- If the target is still near our exit point, damage it.
            if target and target:IsValid() then
                if target:GetDistanceSqToPoint(pos:Get()) < TUNING.GNARWAIL.BOATATTACK_RADIUSSQ then
                    target.components.combat:GetAttacked(horn_attack_prefab, TUNING.GNARWAIL.DAMAGE)
                end
            end

            -- Also damage the boat we just pierced.
            local platform = TheWorld.Map:GetPlatformAtPoint(pos:Get())
            if platform and platform.components.hullhealth and platform.components.health then
                platform.components.health:DoDelta(-TUNING.GNARWAIL.HORN_BOAT_DAMAGE)
            end
			
			inst.sg:SetTimeout(5)

            horn_attack_prefab.SoundEmitter:PlaySoundWithParams("turnoftides/common/together/boat/damage", {intensity=.8})
        end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("emerge")
		end,
    },
}
local function frozen_onoverridesymbols(inst)
    if inst._water_shadow then
        if inst.sg:HasStateTag("frozen") then
            inst._water_shadow.AnimState:PlayAnimation("frozen", true)
        else
            inst._water_shadow.AnimState:PlayAnimation("frozen_loop_pst", true)
        end
    end
end

CommonStates.AddFrozenStates(states, frozen_onoverridesymbols)

PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "emerge"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
CommonStates.AddHopStates(states, false, {pre = "run_pre", loop = "run", pst = "run_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "run_pst",
	plank_idle_loop = "idle_loop",
	plank_idle_pst = "run_pst",
	
	plank_hop_pre = "run_pre",
	plank_hop = "run",
	
	steer_pre = "run_pst",
	steer_idle = "idle_loop",
	steer_turning = "idle2",
	stop_steering = "run_pst",
	
	row = "run_pst",
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "run_pre",
	leap_loop = "run_loop",
	leap_pst = "run_loop",
	
	castspelltime = 10,
})
    
return StateGraph("gnarwailp", states, events, "idle", actionhandlers)

