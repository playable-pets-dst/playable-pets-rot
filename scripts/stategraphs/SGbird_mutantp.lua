require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif PP_FORGE_ENABLED then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "attack"
local shortaction = "attack"
local workaction = "attack"
local otheraction = "attack"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		return inst:HasTag("bird_mutant_spitter") and "shoot" or "attack"
	end),
}

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    EventHandler("doattack", function(inst, data) inst.sg:GoToState("attack", data.target) end),
    PP_CommonHandlers.OnDeath(),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OpenGift(),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function SetFlyingState(inst)
	inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
	if TheNet:GetServerGameMode() ~= "lavaarena" and TheNet:GetServerGameMode() ~= "quagmire" then
		inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
	end
	inst.components.locomotor:SetExternalSpeedMultiplier(inst, 1.5, 1.5)
	inst.DynamicShadow:Enable(false)
	inst.components.health:SetInvincible(true)
	inst:AddTag("notarget")
end

local function SetLandState(inst)
	inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
	inst.Physics:CollidesWith(COLLISION.LIMITS)
	inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, 1.5)
	inst.DynamicShadow:Enable(true)
	inst.components.health:SetInvincible(false)
	inst:RemoveTag("notarget")
end

local states=
{
    
    
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition())) --no loot, instead we spawn its corpse.
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline = 
		{
		
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },
    },    
	
	State {
		name = "frozen",
		tags = {"busy"},
		
        onenter = function(inst)
            inst.AnimState:PlayAnimation("frozen")
            inst.Physics:Stop()
        end,
		
    },
	
	 State{
        name = "special_atk1",
        tags = {"busy"},
        onenter= function(inst)	
            inst.AnimState:PlayAnimation("caw")
            inst.SoundEmitter:PlaySound(inst.sounds.chirp)
        end,
        
		events =
        {
            EventHandler("animover", function(inst)
				inst.sg:GoToState("idle")
            end),
        },

    },
    
	State{
        name = "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst, pushanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle")
        end,
        
        timeline = 
        {

        },
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
   },

	 State{
        name = "run_start",
        tags = {"moving", "canrotate", "hopping"},
        
        onenter = function(inst) 
            inst.AnimState:PlayAnimation("hop")
            inst.components.locomotor:RunForward()
        end,
        
        timeline=
        {
            TimeEvent(8*FRAMES, function(inst) 
				if inst._isflying == false then
					inst.Physics:Stop() 
				end
            end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        }
    },
        
    State{            
        name = "run",
        tags = {"moving", "canrotate","running"},
        
        onenter = function(inst) 
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_loop")
        end,          
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),        
        },
        timeline=
        {

        },
    },      
    
    State{            
        name = "run_stop",
        tags = {"canrotate"},
        
        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
           -- inst.AnimState:PlayAnimation("idle")            
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    },    
	
	State{
        name = "attack",
        tags = { "busy", "attack"},
		
        onenter = function(inst)
			if inst._isflying then
				inst.sg:GoToState("idle")
			else
				if inst:HasTag("bird_mutant_spitter") then
					inst.AnimState:PlayAnimation("caw")
				else
					inst.AnimState:PlayAnimation("attack")
					inst.components.locomotor:Stop()
					if inst.components.combat.target ~= nil then
						inst:ForceFacePoint(inst.components.combat.target.Transform:GetWorldPosition())
					end
					inst.components.combat:StartAttack()
					inst.SoundEmitter:PlaySound(inst.sounds.attack)
				end
			end
		end,
        
        timeline=
        {
            TimeEvent(8*FRAMES, function(inst) 
					inst:PerformBufferedAction()
                    inst.sg:RemoveStateTag("attack")
                    inst.sg:RemoveStateTag("busy")
                    inst.components.combat.target = nil
				end ),
        },
        
        events =
        {
            EventHandler("animover", function(inst) 
				inst.sg:GoToState("idle") 
			end),
        },
    },

    State{
        name = "shoot",
        tags = { "attack", "busy" },

        onenter = function(inst)
			if inst._isflying then
				inst.sg:GoToState("idle")
			else
				local buffaction = inst:GetBufferedAction()
				local target = buffaction ~= nil and buffaction.target or nil
				if target then
					inst.sg.statemem.target = target
				end

				inst.Physics:Stop()
				inst.AnimState:PlayAnimation("attack")
				inst.SoundEmitter:PlaySound(inst.sounds.spit_pre)
				inst.components.combat:StartAttack()
			end
        end,

        timeline =
        {   
            TimeEvent(14*FRAMES, function(inst)
                if inst.sg.statemem.target and inst.sg.statemem.target:IsValid() then
                    inst.sg.statemem.spitpos = Vector3(inst.sg.statemem.target.Transform:GetWorldPosition())            
                    inst:LaunchProjectile(inst.sg.statemem.spitpos)

                    inst.SoundEmitter:PlaySound(inst.sounds.chirp)

                    inst.components.timer:StopTimer("spit_cooldown")
                    inst.components.timer:StartTimer("spit_cooldown", 3 + math.random()*3)
                end
            end),
        },

        events =
        {
            EventHandler("animover",function(inst) 
                inst.sg:GoToState("idle")
            end),
        },
    },
	 
	 State{
        name = "land",
        tags = {"busy", "flight"},
        onenter= function(inst)
            print("Trying to land!")
			local pos = inst:GetPosition()
			if inst._isflying and not PlayablePets.IsAboveLand(pos) then
				inst.sg:GoToState("glide")
			else
				inst.components.locomotor:StopMoving()
				inst:Show()
				local pt = Point(inst.Transform:GetWorldPosition())
				pt.y = 25
				inst.Physics:Stop()
				inst.components.locomotor:StopMoving()
				inst.Physics:Teleport(pt.x,pt.y,pt.z)
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("glide", true)
				inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
				inst.components.locomotor:StopMoving()
				inst.components.locomotor.disable = true
				inst.Physics:SetMotorVel(0,-20+math.random()*10,0)
			end
        end,
		
		onexit = function(inst)
			if not inst._cannot_land then
			inst._isflying = false
			inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
			SetLandState(inst)
			inst.components.locomotor.disable = false
			else
				inst._cannot_land = nil
			end
		end,	
		
		 timeline = 
        {
            TimeEvent(5, function(inst) --added as a failsafe. Teleports to the ground if takes too long.
				local pt = Point(inst.Transform:GetWorldPosition())
				pt.y = 0
                inst.components.locomotor:StopMoving()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.AnimState:PlayAnimation("land")
	            inst.DynamicShadow:Enable(true)
				inst.components.health:SetInvincible(false)
                inst.sg:GoToState("idle", true)
			end),
        },
        
        onupdate= function(inst)
            local pt = Point(inst.Transform:GetWorldPosition())
            inst.Physics:SetMotorVel(0,-20+math.random()*10,0)
            if pt.y <= 1 then
                pt.y = 0
                inst.components.locomotor:StopMoving()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.AnimState:PlayAnimation("land")
	            SetLandState(inst)
                inst.sg:GoToState("idle", true)
            end
        end,
		

        ontimeout = function(inst)
             --inst.sg:GoToState("idle")
        end,
    },    
	 
	 State{
        name = "special_atk2",
        tags = {"flight", "busy"}, --sliding happens
        onenter = function(inst)
			if not inst.taunt2 then
				inst.sg:GoToState("idle")
			else
				inst.taunt2 = false
				inst.components.locomotor:StopMoving()
				inst.sg:SetTimeout(.1+math.random()*.2)
				inst.sg.statemem.vert = math.random() > .5
			
				SetFlyingState(inst)
				inst.AnimState:PlayAnimation("takeoff_vertical_pre")
			end
        end,
        
        ontimeout= function(inst)
             inst.AnimState:PushAnimation("takeoff_vertical_loop", true)
			 inst.components.locomotor.disable = true
			 inst.components.locomotor:StopMoving()
             inst.Physics:SetMotorVel(0,15+math.random()*5,0)
        end,

        timeline = 
        {
            TimeEvent(1.5, function(inst) 
				inst:Hide()
				inst.components.locomotor.disable = false
				if TheNet:GetServerGameMode() == "lavaarena" then
					inst:DoTaskInTime(5, function(inst) if inst._isflying == true and not inst.sg:HasStateTag("landing") then inst.sg:GoToState("land") end end)
				end	
				inst.sg:GoToState("glide") 
			end)
        }
        
    },	
		
	State{
        name = "glide",
        tags = {"flight2", "canrotate"},
        onenter= function(inst)
			
			local pt = Point(inst.Transform:GetWorldPosition())
			if pt.y > .1 then
                pt.y = 0
                inst.Physics:Stop()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
				
			end	
			inst._isflying = true
			--inst.Physics:SetMotorVel(8,0,0)
            inst.AnimState:PlayAnimation("idle", true)
        end,
        
        onupdate= function(inst)
        end,
		

        ontimeout = function(inst)

        end,
    },    
		
	State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()
        end,
        
        events=
        {
            EventHandler("animover", function(inst) 
                    inst.sg:GoToState("idle") 
            end ),
        },        
    },    	
		
	State{
        name = "stunned",
        tags = {"busy"},
        
        onenter = function(inst) 
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stunned_loop", true)
            inst.sg:SetTimeout(GetRandomWithVariance(6, 2) )
        end,
        
        ontimeout = function(inst) inst.sg:GoToState("idle") end,
    },	
		
	State {
        name = "sleep",
        tags = { "busy", "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/fallasleep")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,
		
		timeline = 
		{
			--TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/grass_gekko/sleep_pre") end)
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline = 
		{
           --TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/grass_gekko/sleep") end),
		   --TimeEvent(28*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/grass_gekko/sleep") end)
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/wakeup")
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,
		
		timeline = 
		{
			
		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
}

--CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"attack", nil, nil, "idle", "attack") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{

		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "caw"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "hop")
PP_CommonStates.AddOpenGiftStates(states, "idle")
CommonStates.AddHopStates(states, false, {pre = "hop", loop = "hop", pst = "idle"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "hop",
	plank_idle_loop = "idle",
	plank_idle_pst = "hop",
	
	plank_hop_pre = "hop",
	plank_hop = "hop",
	
	steer_pre = "hop",
	steer_idle = "idle",
	steer_turning = "idle",
	stop_steering = "hop",
	
	row = "hop",
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "hop",
	
	castspelltime = 10,
})	
    
return StateGraph("bird_mutantp", states, events, "idle", actionhandlers)

