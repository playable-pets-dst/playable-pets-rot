require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.CAST_NET, longaction),
}

local function play_carrat_scream(inst)
    inst.SoundEmitter:PlaySound(inst.sounds.scream)
end

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local random_emotes =
{
	"alert_direction",
	"alert_reaction",
	"alert_speed",
	"alert_stamina"
}

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{
    State {
        name = "idle",
        tags = { "idle", "canrotate" },
        onenter = function(inst, playanim)
            inst.Physics:Stop()

            local play_special_idle = (math.random() > 0.85)
            if play_special_idle then
                inst.AnimState:PlayAnimation("idle2", false)
                inst.AnimState:PushAnimation("idle1", true)
            else
                inst.AnimState:PlayAnimation("idle1", true)
            end
            inst.sg:SetTimeout(1 + math.random()*1)
        end,

        ontimeout= function(inst)
            inst.sg:GoToState("idle")
        end,
    },

    State {
        name = "attack",
		tags = { "busy" },
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat_pre", false)
			inst.AnimState:PushAnimation("eat_pst", false)
        end,

		timeline =
        {
            TimeEvent(3*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.eat)
            end),
			TimeEvent(5*FRAMES, function(inst)
				inst:PerformBufferedAction()
			end)
        },
		
        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State {
        name = "eat",
		tags = { "busy" },
		
        onenter = function(inst)
            inst:PerformBufferedAction()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat_pre", false)
            inst.AnimState:PushAnimation("eat_loop", false)
            inst.AnimState:PushAnimation("eat_pst", false)
        end,

		timeline =
        {
            TimeEvent(3*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.eat)
            end)
        },
		
        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound(inst.sounds.hit)
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "special_atk1",
        tags = { "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
             if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.Physics:Stop()
            inst.Physics:SetActive(false)
			-- Ensure that we're facing to the right, to match what a planted carrat looks like after
            -- submerging. This prevents us from flipping much more obviously after we've prefabbed swapped
            -- in the "submerged" state.
            inst.Transform:SetNoFaced()

            inst.AnimState:PlayAnimation("submerge")
        end,

        timeline =
        {
			TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.submerge)
            end),
            TimeEvent(30*FRAMES, function(inst)
                inst.DynamicShadow:Enable(false)
            end),
        },
		
		onexit = function(inst)
            inst.Physics:SetActive(true)
            inst.Transform:SetSixFaced()
			inst.DynamicShadow:Enable(true)
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("special_atk1_loop") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "special_atk1_loop",
        tags = { "busy", "special_toggle"},
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.Physics:SetActive(false)
				inst.Transform:SetNoFaced()
				inst.DynamicShadow:Enable(false)
				inst.AnimState:PlayAnimation("planted", false)
			end,
			
		onexit = function(inst)
			inst.Physics:SetActive(true)
            inst.Transform:SetSixFaced()
			inst.DynamicShadow:Enable(true)
		end,
    },

    State
    {
        name = "special_atk1_pst",
        tags = { "busy" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.Physics:SetActive(false)
			inst.SoundEmitter:PlaySound(inst.sounds.emerge)
            inst.AnimState:PlayAnimation("emerge_fast")
			inst.DynamicShadow:Enable(false)
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        timeline =
        {
            TimeEvent(5*FRAMES, function(inst)
                inst.DynamicShadow:Enable(true)
            end),
        },		

		onexit = function(inst)
			inst.Physics:SetActive(true)
            inst.Transform:SetSixFaced()
			inst.DynamicShadow:Enable(true)
		end,
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("walk_pst")
			inst.components.locomotor:StopMoving()
			if not inst.shouldwalk or inst.shouldwalk == false then
				inst.shouldwalk = true
			else
				inst.shouldwalk = false
			end
        end,

        timeline=
        {

        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State {
        name = "happy",
        tags = { "idle", "canrotate", "alert" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("victory_pre")
            inst.AnimState:PushAnimation("victory_loop", false)
            inst.AnimState:PushAnimation("victory_post", false)
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("happy")
            end),
		},
		
        timeline =
        {
			TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.reaction) end),
			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.reaction) end),
			TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.emerge) end),
        },
    },
	
	State {
        name = "sad",
        tags = { "idle", "canrotate", "alert" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("lose_small_pre")
			inst.AnimState:PushAnimation("lose_small_loop", false)
            inst.AnimState:PushAnimation("small_big_trans", false)
            inst.AnimState:PushAnimation("lose_big_loop", false)
            inst.AnimState:PushAnimation("lose_big_loop", false)
            inst.AnimState:PushAnimation("lose_big_loop", false)
            inst.AnimState:PushAnimation("lose_big_post", false)
		end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("sad")
            end),
		},
		
        timeline =
        {
			TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.emerge) end),
			TimeEvent(39*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.reaction)end),
			TimeEvent(45*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.reaction) end),
			TimeEvent(50*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.stunned, "cry") end),
        },

        onexit = function(inst)
			inst.SoundEmitter:KillSound("cry")
        end,
    },
	
	  State {
        name = "alert_speed",
        tags = { "idle", "canrotate", "alert" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("speed")
			
			inst.sg.statemem.step_sound_task = inst:DoPeriodicTask(4*FRAMES, function() inst.SoundEmitter:PlaySound(inst.sounds.step) end)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("alert_speed")
                end
            end),
		},
		
        timeline =
        {
			TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.reaction) end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.emerge) end),

			TimeEvent(70*FRAMES, function(inst)
				if inst.sg.statemem.step_sound_task ~= nil then
					inst.sg.statemem.step_sound_task:Cancel()
					inst.sg.statemem.step_sound_task = nil
				end
			end),
		},
		
        onexit = function(inst)
			if inst.sg.statemem.step_sound_task ~= nil then
				inst.sg.statemem.step_sound_task:Cancel()
				inst.sg.statemem.step_sound_task = nil
			end
        end,
    },
	
	State {
        name = "alert_direction",
        tags = { "idle", "canrotate", "alert" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("direction")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("alert_direction")
                end
            end),
		},
		
        timeline =
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.reaction) end),
			TimeEvent(52*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.emerge) end),
			TimeEvent(100*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.reaction) end),
        },
    },
	
	State {
        name = "alert_reaction",
        tags = { "idle", "canrotate", "alert" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("reaction")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("alert_reaction")
                end
            end),
		},
		
        timeline =
        {
			TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.eat) end),
			TimeEvent(89*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.eat) end),
			TimeEvent(100*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.eat) end),
        },
    },

    State {
        name = "alert_stamina",
        tags = { "idle", "canrotate", "alert" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("stamina")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("alert_stamina")
                end
            end),
		},
		
        timeline =
        {
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.reaction) end),
			TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.emerge) end),
			TimeEvent(56*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
			TimeEvent(75*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.reaction) end),
        },
    },
	
	State {
        name = "exhausted",
        tags = { "exhausted" },
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("exhausted_pre", false)
        end,

        timeline =
        {
           TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.submerge) end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("exhausted_loop")
            end),
        },
    },

    State {
        name = "exhausted_loop",
        tags = { "exhausted" },
        onenter = function(inst, count)
            inst.components.locomotor:StopMoving()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("exhausted_loop")
        end,

        timeline =
        {
--            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.idle) end),
        },

        events =
        {
            EventHandler("animover", function(inst)
				if inst.components.yotc_racecompetitor ~= nil and inst.components.yotc_racecompetitor:IsExhausted() then
	                inst.sg:GoToState("exhausted_loop")
				else
					if inst.components.yotc_racestats ~= nil and inst.components.sleeper ~= nil and inst.components.yotc_racestats:GetStaminaModifier() == 0 and math.random() < TUNING.YOTC_RACER_STAMINA_SLEEP_CHANCE then
                        inst:PushEvent("carrat_error_sleeping")
						inst.components.sleeper:GoToSleep(TUNING.YOTC_RACER_STAMINA_SLEEP_TIME + math.random() * TUNING.YOTC_RACER_STAMINA_SLEEP_TIME_VAR)
					end
	                inst.sg:GoToState("exhausted_pst")
				end
            end),
        },
    },

    State {
        name = "exhausted_pst",
        tags = { "exhausted" },
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("exhausted_pst", false)
        end,

        timeline =
        {
--            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.idle) end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
	                inst.sg:GoToState("idle")
				end
            end),
        },
    },
	
	State{
        name= "emote",
        tags = {"idle"},
        
        onenter = function(inst, data)
			inst.Physics:Stop()
			
			if data.anim and data.anim == "emoteXL_happycheer" then
				inst.sg:GoToState("happy")
			elseif data.anim and data.anim == "emoteXL_sad" then 
				inst.sg:GoToState("sad")
			else
				inst.sg:GoToState(random_emotes[math.random(1, #random_emotes)])
			end	
        end,
        
        events=
        {
            --EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
		end,

		timeline=
        {
			TimeEvent(30*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound(inst.sounds.sleep)
				PlayablePets.SleepHeal(inst)
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}

CommonStates.AddFrozenStates(states)
CommonStates.AddWalkStates(states)
CommonStates.AddRunStates(states,
{
    starttimeline =
    {
        TimeEvent(0, function(inst)
			
            inst.SoundEmitter:PlaySound(inst.sounds.stunned)
        end),
    },
	endtimeline =
    {
        TimeEvent(0, PlayFootstep),
    },
})
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"run_pst", nil, nil, "idle1", "run_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "stunned_pst"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "taunt")
CommonStates.AddHopStates(states, false, {pre = "run_pre", loop = "run_loop", pst = "run_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "run_pst",
	plank_idle_loop = "idle1",
	plank_idle_pst = "run_pst",
	
	plank_hop_pre = "run_pre",
	plank_hop = "run_loop",
	
	steer_pre = "run_pst",
	steer_idle = "idle1",
	steer_turning = "idle2",
	stop_steering = "run_pst",
	
	row = "run_pst",
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "run_pre",
	leap_loop = "run_loop",
	leap_pst = "run_loop",
	
	castspelltime = 10,
})
    
return StateGraph("carratp", states, events, "idle", actionhandlers)

