require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local function StartFlapping(inst)
    inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/together/dragonling/fly_LP", "flying")
end

local function RestoreFlapping(inst)
    if not inst.SoundEmitter:PlayingSound("flying") then
        StartFlapping(inst)
    end
end

local function StopFlapping(inst)
    inst.SoundEmitter:KillSound("flying")
end

local function CleanupIfSleepInterrupted(inst)
    if not inst.sg:HasStateTag("sleeping") then
        RestoreFlapping(inst)
    end
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, "idle"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(), 
    CommonHandlers.OnFreeze(),
	--CommonHandlers.OnHop(),
	--[[
	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),]]
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst)
			end),	
}

local states=
{	
	State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			local emotechance = math.random(3)
			if emotechance == 1 then
				inst.sg:GoToState("taunt1")
			elseif emotechance == 2 then
				inst.sg:GoToState("taunt2")
			elseif emotechance == 3 then
				inst.sg:GoToState("taunt3")
			else
				inst.sg:GoToState("taunt1") --incase something goes wrong.				
			end				
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("distress")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/pupington/bark")
        end,

		timeline=
 		{
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/distress") end),
		},

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "taunt1",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			--inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("emote1")
        end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/emote1") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "taunt2",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			--inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("emote2")
        end,

		timeline=
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/emote2") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "taunt3",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			--inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("emote_nuzzle")
        end,

		timeline=
        {
			TimeEvent(52*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/emote_nuzzle") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "eat",
        tags = {"busy"},

          onenter = function(inst, pushanim)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end

            inst.AnimState:PlayAnimation("eat_pre")
			inst:PerformBufferedAction()
            inst.AnimState:PushAnimation("eat_loop", false)
            inst.AnimState:PushAnimation("eat_pst", false)
        end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/eat_pre") end),
			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/vo_cute") end),

            TimeEvent((28+0)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/eat_LP") end),
        
			TimeEvent((28+10)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/eat_LP") end),

			TimeEvent((28+24+0)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/eat_LP") end),
        },

        events=
        {
			--EventHandler("animqueueover", function(inst) if inst:PerformBufferedAction() then inst.components.combat:SetTarget(nil)  inst.sg:GoToState("idle", "atk_pst") end end),
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
		},
    },
	
	State{
        name = "moving",
        tags = {"moving", "canrotate"},
        
        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("flight_cycle", true)
        end,
    },

    State{
        name = "attack",
        tags = {"attack"},
		
		onenter = function(inst, target)
			inst.AnimState:PlayAnimation("walk_pst")
        end,
	
		timeline=
		{

		},

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("sleep_pre")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {

        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("walk_pst")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
			local pos = inst:GetPosition()
			if IsOceanTile(TheWorld.Map:GetTileAtPoint(inst:GetPosition():Get())) and not TheWorld.Map:GetPlatformAtPoint(pos.x, pos.z) then
				inst.sg:GoToState("idle")
			else
				if inst.components.locomotor ~= nil then
					inst.components.locomotor:StopMoving()
				end
				inst.AnimState:PlayAnimation("sleep_pre")
			end
        end,
		
		timeline =
		{
			TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/sleep_pre") end),
            TimeEvent(44*FRAMES, StopFlapping),
			TimeEvent(31*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/sleep_in") LandFlyingCreature(inst) end),
		},

		onexit = CleanupIfSleepInterrupted,
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
			PlayablePets.SleepHeal(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop", true)
		end,
			
		onexit = CleanupIfSleepInterrupted,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/sleep_out") end),
			TimeEvent(32*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/sleep_in") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
			RaiseFlyingCreature(inst)
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

		onexit = RestoreFlapping,
		
        timeline = 
		{
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/creatures/together/lunarmothling/sleep_pst") end),
            TimeEvent(12*FRAMES, StartFlapping),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_pre")
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/sheepington/walk") end),		
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = 
		{
		
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")            
        end,

		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/sheepington/walk") end),		
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}
--CommonStates.AddFrozenStates(states)
--PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
		
		},
		
		corpse_taunt =
		{
				
		},
	
	},
	--anims = 
	{
		corpse = "sleep_pre",
		corpse_taunt = "sleep_pst"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
--CommonStates.AddHopStates(states, false, {pre = "walk_pst", loop = "walk_pst", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "walk_pst",
	plank_idle_loop = "idle_loop",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk_loop",
	
	steer_pre = "walk_pst",
	steer_idle = "idle_loop",
	steer_turning = "walk_pst",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
    
return StateGraph("moonmothp", states, events, "idle", actionhandlers)

