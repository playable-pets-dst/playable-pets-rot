require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "cookiecutter"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "cookiecutter"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "cookiecutter"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "eat"
local shortaction = "eat"
local workaction = "eat"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, longaction),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif inst.sg:HasStateTag("drilling") then
			inst.sg:GoToState("drill_hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 	
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
	CommonHandlers.OnHop(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local function RestoreCollidesWith(inst)
	inst.Physics:CollidesWith(COLLISION.WORLD 
						+ COLLISION.OBSTACLES
						+ COLLISION.SMALLOBSTACLES
						+ COLLISION.CHARACTERS
						+ COLLISION.GIANTS)
end

local function SetSortOrderIsInWater(inst, in_water)
	if in_water then
		inst.sg.mem.in_water = true
		inst.AnimState:SetSortOrder(ANIM_SORT_ORDER_BELOW_GROUND.BOAT_LIP)
		inst.AnimState:SetLayer(LAYER_BELOW_GROUND)
	elseif not in_water then
		inst.sg.mem.in_water = false
		inst.AnimState:SetSortOrder(0)
		inst.AnimState:SetLayer(LAYER_WORLD)
		RestoreCollidesWith(inst)
	end
end

local function UpdateWalkSpeedAndHopping(inst)
	inst.components.locomotor.walkspeed = (inst.target_wood ~= nil and inst.target_wood:IsValid()) and TUNING.COOKIECUTTER.APPROACH_SPEED or TUNING.COOKIECUTTER.WANDER_SPEED
end

 local states=
{
   State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle", true)
        end,

    },

	State{
        name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("walk_pst")
			inst.components.locomotor:StopMoving()
			if not inst.shouldwalk or inst.shouldwalk == false then
				inst.shouldwalk = true
			else
				inst.shouldwalk = false
			end
        end,

        timeline=
        {

        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("walk_pst")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "eat",
        tags = { "busy", "jumping" },

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("jump_pre", false)
			inst.AnimState:PushAnimation("jumping", false)
            inst.AnimState:PushAnimation("jump_pst_water", false)
        end,

        timeline =
        {
			TimeEvent(10*FRAMES, function(inst)
				SpawnPrefab("splash").Transform:SetPosition(inst.Transform:GetWorldPosition())
				inst.SoundEmitter:PlaySound(inst.sounds.jump)
			end),
			TimeEvent(12*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound(inst.sounds.attack)
			end),
			TimeEvent(17*FRAMES, function(inst)
				inst:setsortorderisinwaterfn(false)
			end),
            TimeEvent(28*FRAMES, function(inst)
				if inst:PerformBufferedAction() then
					inst.SoundEmitter:PlaySound(inst.sounds.eat_item)
				end
			end),
			TimeEvent(30*FRAMES, function(inst)
				SpawnPrefab("splash").Transform:SetPosition(inst.Transform:GetWorldPosition())
				inst.SoundEmitter:PlaySound(inst.sounds.splash)
				inst:setsortorderisinwaterfn(true)
			end),
        },

        events =
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "drill",
		tags = { "drilling" },

        onenter = function(inst, cb)
			if not inst.SoundEmitter:PlayingSound("eat_LP") then inst.SoundEmitter:PlaySound(inst.sounds.eat, "eat_LP") end

			if inst.components.cookiecutterdrill:GetIsDoneDrilling() then
				inst.sg:GoToState("leaveboat")
			else
				if inst.attackdata.wants_to_attack and not inst.attackdata.on_cooldown then
					inst.sg:GoToState("areaattack")
				else
					inst.components.cookiecutterdrill:StartDrilling()

					inst.AnimState:PlayAnimation("eat_loop", true)
					SpawnPrefab("wood_splinter_drill").Transform:SetPosition(inst.Transform:GetWorldPosition())
				end
			end
		end,

		onexit = function(inst)
			inst.components.cookiecutterdrill:StopDrilling()
		end,

        events =
        {
            EventHandler("animover", function(inst)
				inst.sg:GoToState("drill")
			end),
        },
    },

    State{
        name = "leaveboat",
        tags = { "busy", "drilling", "leavingboat" },

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat_pst")
			inst.should_drill = false
		end,

        timeline =
        {
			TimeEvent(3*FRAMES, function(inst)
				inst.components.cookiecutterdrill:FinishDrilling()
			end),
        },

        events =
        {
            EventHandler("animover", function(inst)
				inst:ClearBufferedAction()
				inst:PushEvent("onsubmerge")
                inst.SoundEmitter:KillSound("eat_LP")
				inst.SoundEmitter:PlaySound(inst.sounds.eat_finish)
			end),
        },
    },

    State{
        name = "areaattack",
        tags = { "attack", "busy" },

        onenter = function(inst, target)
			inst.attackdata.wants_to_attack = false
			inst.attackdata.on_cooldown = true
			inst:DoTaskInTime(inst.attackdata.cooldown_duration, function() inst.attackdata.on_cooldown = false end)

            inst.sg.statemem.target = target
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack_loop")
        end,

		onexit = function(inst)
			inst.components.cookiecutterdrill:StopDrilling()
		end,

        timeline =
        {
			TimeEvent(0, function(inst)
				inst.SoundEmitter:PlaySound(inst.sounds.attack)
			end),

			TimeEvent(5*FRAMES, function(inst)
				inst.components.combat:DoAreaAttack(inst, TUNING.COOKIECUTTER.ATTACK_RADIUS, nil, nil, nil, { "ghost", "playerghost", "cookiecutter" })
			end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("drill") end),
        },
    },

    State{
        name = "surface",
        tags = { "busy" },

        onenter = function(inst, cb)
            inst.AnimState:PlayAnimation("resurface")
		end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	State{
        name = "gohome",
        tags = { "busy" },

        onenter = function(inst, cb)
            inst.AnimState:PlayAnimation("submerge")
		end,

        events =
        {
            EventHandler("animover", function(inst)
				inst:doreturnfn()
			end),
        },
	},

    State{
        name = "hit_land", -- Skips straight to running away if hit in water
        tags = { "busy", "hit" },

        onenter = function(inst)
            inst.Physics:Stop()
			inst.components.cookiecutterdrill:StartDrilling()
			inst.AnimState:PlayAnimation("hit")
			if inst.SoundEmitter:PlayingSound("eat_LP") then inst.SoundEmitter:KillSound("eat_LP") end
        end,

		onexit = function(inst)
			inst.components.cookiecutterdrill:StopDrilling()
		end,

        events =
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation(inst:HasTag("swimming") and "death" or "boat_death")
			if inst.SoundEmitter:PlayingSound("eat_LP") then inst.SoundEmitter:KillSound("eat_LP") end
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) 
				PlayablePets.SleepHeal(inst)
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "hop_antic",
        tags = { "busy", "canrotate", "nosleep", "noattack", "nointerrupt" },

        onenter = function(inst, target_pt)
			inst.sg:GoToState("jump_pre")
        end,
    },
	
	State{
        name = "jump_pre",
        tags = { "busy", "canrotate", "nosleep", "noattack", "nointerrupt" },

        onenter = function(inst, target_pt)
			SetSortOrderIsInWater(inst, true)


			if target_pt ~= nil then
				inst.sg.statemem.target_pt = target_pt
				inst:ForceFacePoint(inst.sg.statemem.target_pt:Get())
			end
            inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("jump_pre")
        end,

		timeline = 
		{
			TimeEvent(10*FRAMES, function(inst)
				SpawnPrefab("splash").Transform:SetPosition(inst.Transform:GetWorldPosition())
				inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/small")
			end),
		},

        events =
        {
            EventHandler("animover", function(inst)
				inst.sg:GoToState("jumping", 7 + math.random(1,5))
			end),
        },
    },

	State{
        name = "resurface",
        tags = { "busy", "noattack", "nosleep", "nointerrupt" },

        onenter = function(inst, should_relocate)
			SetSortOrderIsInWater(inst, true)
			inst:AddTag("NOCLICK")
            inst.AnimState:PlayAnimation("resurface")

			if should_relocate then

				local pt = inst:GetPosition()
				local a = math.random() * 2 - 1
				local r = math.random() * 2
				local boat = inst:GetCurrentPlatform()
				local boat_dir = (boat ~= nil and boat.components.boatphysics ~= nil) and Vector3(boat.components.boatphysics.velocity_x, 0, boat.components.boatphysics.velocity_z) or nil
				if boat_dir ~= nil then
					pt = pt + boat_dir * -TUNING.MAX_WALKABLE_PLATFORM_RADIUS
				end
				local start_angle = (inst.Transform:GetRotation() + 180 + a*a*a*30) * DEGREES
				local min_dist_to_boat = TUNING.MAX_WALKABLE_PLATFORM_RADIUS + inst:GetPhysicsRadius(0) + 1
				local function testfn(new_pt) return #TheSim:FindEntities(new_pt.x, 0, new_pt.z, min_dist_to_boat, {"walkableplatform"}) == 0 end

				local offset = FindSwimmableOffset(pt, start_angle, TUNING.MAX_WALKABLE_PLATFORM_RADIUS + r, 16, true, nil, testfn, true) -- allowing boats because testfn will handle it
										or FindSwimmableOffset(pt, start_angle, TUNING.MAX_WALKABLE_PLATFORM_RADIUS + r + 3, 16, true, nil, testfn, true)
										or FindSwimmableOffset(pt, start_angle, TUNING.MAX_WALKABLE_PLATFORM_RADIUS + r + 6, 16, true, nil, testfn, true)

				if offset ~= nil then
					inst.Transform:SetPosition(pt.x + offset.x, 0, pt.z + offset.z)
				end
			end
		end,

		timeline = 
		{
			TimeEvent(9*FRAMES, function(inst)
				inst:RemoveTag("NOCLICK")
				inst.sg:RemoveStateTag("noattack")
				inst.sg:RemoveStateTag("nosleep")
				inst.sg:RemoveStateTag("nointerrupt")
			end),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },

		onexit = function(inst)
			inst.components.embarker:Cancel()
			inst:RemoveTag("NOCLICK")
		end,
    },
	
	State{
        name = "jumping",
        tags = { "busy", "jumping", "nosleep", "noattack", "nointerrupt" },

        onenter = function(inst, motor_speed)
			SetSortOrderIsInWater(inst, false)

			inst.AnimState:PlayAnimation("jumping")

            inst.components.locomotor:Stop()

	        inst.Physics:SetCollisionMask(COLLISION.GROUND)

			if motor_speed ~= nil then
	            inst.Physics:SetMotorVelOverride(motor_speed, 0, 0)
			end
        end,

        timeline =
        {
			TimeEvent(12, function(inst)
	            inst.Physics:ClearMotorVelOverride()
			end),

			TimeEvent(14, function(inst)
		        inst.Physics:SetCollisionMask(COLLISION.WORLD) -- collide with the ground and limits, this will let physics push the cookie cutter on or off boats
			end),

			TimeEvent(15*FRAMES, function(inst)
                inst.components.combat:DoAreaAttack(inst, TUNING.COOKIECUTTER.JUMP_ATTACK_RADIUS, nil, nil, nil, { "cookiecutter", "INLIMBO", "invisible", "noattack", "flight", "playerghost", "shadow", "shadowchesspiece", "shadowcreature" })
			end),
        },

        events =
        {
            EventHandler("animover", function(inst)
				if inst:GetCurrentPlatform() ~= nil then
					inst.sg.statemem.collisionmask = nil
					inst.target_wood = inst:GetCurrentPlatform()
					inst.sg:GoToState("jump_pst_boat")
				else
					if inst:GetBufferedAction() ~= nil then
						inst:PerformBufferedAction()
					end
					inst.sg:GoToState("jump_pst_water")
				end
			end),
        },

		onexit = function(inst)
            inst.Physics:ClearMotorVelOverride()
			RestoreCollidesWith(inst)
			if inst:GetBufferedAction() ~= nil then
				inst:ClearBufferedAction()
			end
		end,
    },

	State{
        name = "jump_pst_water",
        tags = { "busy", "nosleep", "noattack", "nointerrupt" },

        onenter = function(inst)
			SetSortOrderIsInWater(inst, true)
            inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("jump_pst_water")

			SpawnPrefab("splash").Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/small")
        end,

        events =
        {
            EventHandler("animover", function(inst)
				inst.sg:GoToState("idle")
			end),
        },
    },

	State{
        name = "jump_pst_boat",
        tags = { "busy", "nosleep", "drilling" },

        onenter = function(inst)
			SetSortOrderIsInWater(inst, false)
            inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("jump_pst_boat")
			inst.SoundEmitter:PlaySound("turnoftides/common/together/boat/damage_small")
			inst.components.cookiecutterdrill:ResetDrilling()
	        inst.Physics:SetCollisionMask(COLLISION.GROUND)
        end,

        events =
        {
            EventHandler("animover", function(inst)
				inst.sg.statemem.notinterupted = true
				inst.sg:GoToState("drill")
			end),
        },

		onexit = function(inst)
			SetSortOrderIsInWater(inst, not inst.sg.statemem.notinterupted)
			RestoreCollidesWith(inst)
		end,
    },

    State{
        name = "drill",
		tags = { "drilling", "nosleep", "busy"},

        onenter = function(inst)
			SetSortOrderIsInWater(inst, false)
	        inst.Physics:SetCollisionMask(COLLISION.GROUND)
			if inst.target_wood ~= nil and inst.target_wood:IsValid() and inst.target_wood:HasTag("boat") then
				inst.AnimState:PlayAnimation("drill_loop", true)
				inst.SoundEmitter:PlaySound("saltydog/creatures/cookiecutter/eat_LP", "eat_LP")

				inst.components.cookiecutterdrill:ResumeDrilling()

				inst.sg.statemem.fx_task = inst:DoPeriodicTask(inst.AnimState:GetCurrentAnimationLength(), function(i)
					SpawnPrefab("wood_splinter_drill").Transform:SetPosition(i.Transform:GetWorldPosition()) 
					inst.components.hunger:DoDelta(10, false)
				end, 0)
			else
				inst.sg:GoToState("drill_pst")
			end
		end,

        onupdate = function(inst)
			if inst.components.cookiecutterdrill:GetIsDoneDrilling() then
				inst.sg:GoToState("drill_pst")
			end
		end,

		onexit = function(inst)
			if inst.sg.statemem.fx_task ~= nil then
				inst.sg.statemem.fx_task:Cancel()
			end
			RestoreCollidesWith(inst)
			inst.SoundEmitter:KillSound("eat_LP")
			inst.components.cookiecutterdrill:PauseDrilling()
		end,
    },

    State{
        name = "drill_pst",
        tags = { "busy", "drilling", "drilling_pst", "nosleep", "noattack", "nointerrupt" },

        onenter = function(inst)
			SetSortOrderIsInWater(inst, false)
	        inst.Physics:SetCollisionMask(COLLISION.GROUND)
			inst:AddTag("NOCLICK")

            inst.Physics:Stop()
    		inst.AnimState:PlayAnimation("drill_pst")
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength() + 1.0 + math.random() * 0.5)

			if inst.target_wood ~= nil and inst.target_wood:IsValid() and inst.target_wood:HasTag("boat") then
				inst.SoundEmitter:PlaySound("saltydog/creatures/cookiecutter/attack")
			end
		end,

        timeline =
        {
			TimeEvent(3*FRAMES, function(inst)
				if inst.components.eater ~= nil then
					inst.components.eater.lasteattime = GetTime()
				end
				inst.components.cookiecutterdrill:FinishDrilling()
			end),
        },

        events =
        {
            EventHandler("animover", function(inst)
				inst:Hide()
			end),
        },

        ontimeout = function(inst)
			inst.sg:GoToState("resurface", true)
		end,

		onexit = function(inst)
			inst.wood_target = nil
			RestoreCollidesWith(inst)
			inst:RemoveTag("NOCLICK")
			inst:Show()
		end,
    },

    State{
        name = "drill_hit",
        tags = { "busy", "hit", "drilling" },

        onenter = function(inst)
			SetSortOrderIsInWater(inst, false)
	        inst.Physics:SetCollisionMask(COLLISION.GROUND)

            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("hit")
            inst.AnimState:PushAnimation("attack_loop", false)
			inst.SoundEmitter:PlaySound("saltydog/creatures/cookiecutter/hit")

        end,

        timeline =
        {
			TimeEvent(19*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("saltydog/creatures/cookiecutter/attack")
			end),
			TimeEvent(25*FRAMES, function(inst)
                inst.components.combat:DoAreaAttack(inst, TUNING.COOKIECUTTER.ATTACK_RADIUS, nil, nil, nil, GetExcludeTags(inst))
			end),
			TimeEvent(27*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("saltydog/creatures/cookiecutter/attack")
			end),
        },

        events =
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("drill") end),
        },

		onexit = function(inst)
			RestoreCollidesWith(inst)
		end,
    },
}

CommonStates.AddFrozenStates(states)

CommonStates.AddWalkStates(states, {}, nil, true)

CommonStates.AddRunStates(states,
{
	starttimeline = {
		
	},
	runtimeline = {
		
	},
}, nil, nil, nil,
{
	startonexit = function(inst)
		
	end,
	runonexit = function(inst)
		
	end
})
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "walk_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.components.health:SetPercent(1) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "walk_pst"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
--CommonStates.AddHopStates(states, false, {pre = "jumpout", loop = "jump_loop", pst = "jump_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle",
	plank_idle_loop = "idle",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk",
	
	steer_pre = "walk_pst",
	steer_idle = "idle",
	steer_turning = "walk_pst",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
    
return StateGraph("cookiecutterp", states, events, "idle", actionhandlers)

