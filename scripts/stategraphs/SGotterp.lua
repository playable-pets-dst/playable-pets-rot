require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local function FindFish(inst)
    local posx, posy, posz = inst.Transform:GetWorldPosition()
    local angle = -inst.Transform:GetRotation() * DEGREES
    local offset = 1
    local targetpos = {x = posx + (offset * math.cos(angle)), y = 0, z = posz + (offset * math.sin(angle))} 
    local ents = TheSim:FindEntities(targetpos.x, 0, targetpos.z, 3, {"oceanfishable_creature"}) 
    return ents[1] or nil
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "eat_pre"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.DROP, "drop"),
    ActionHandler(ACTIONS.PICKUP, "pickup"),
    ActionHandler(ACTIONS.PICK, "pickup"),
    ActionHandler(ACTIONS.HARVEST, "pickup"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	--CommonHandlers.OnHop(),
	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),
	---
	PP_CommonHandlers.OpenGift(),
	--PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local function go_to_idle(inst)
    inst.sg:GoToState("idle")
end
local BUSY_TAGS = {"busy"}
local EATING_TAGS = {"busy", "eating"}
local ATTACK_FRAME = 15
 local states=
{
    State {
        name = "idle",
        tags = { "idle", "canrotate" },
        onenter = function(inst, playanim)
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State {
        name = "attack",
		tags = { "busy" },
        onenter = function(inst)
            inst.Physics:Stop()
			inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("bite")
        end,

		timeline =
        {
			SoundFrameEvent(0, "meta4/otter/attack_f0"),
            SoundFrameEvent(4, "meta4/otter/vo_attack_f4"),
            FrameEvent(ATTACK_FRAME, function(inst)
                inst:PerformBufferedAction()
            end),
        },
		
        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State {
        name = "eat",
		tags = { "busy" },
		
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("eat_pst", false)
        end,

		timeline =
        {
            FrameEvent(1, function(inst)
                if inst:PerformBufferedAction() then
                    inst.SoundEmitter:PlaySound("meta4/otter/eat_chomp_f17")
                else
                    inst.sg:GoToState("eat_fail")
                end
            end),
            SoundFrameEvent(9, "meta4/otter/vo_eat_f21"),
            SoundFrameEvent(26, "meta4/otter/eat_pst_f40"),
        },
		
        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },

    State {
        name = "special_atk2",
        tags = BUSY_TAGS,

        onenter = function(inst)
            if inst:HasTag("swimming") then
                inst.components.locomotor:StopMoving()

                inst.AnimState:PlayAnimation("attack")

                local target = FindFish(inst)

                if target then
                    inst.sg.statemem.toss_target = target
                end
            else
                inst.sg:GoToState("idle")
            end
        end,

        timeline =
        {
            FrameEvent(6, function(inst)
                local target = inst.sg.statemem.toss_target
                if target and target:IsValid() and target:IsOnOcean(false) then
                    inst:TossFish(target)
                end

                inst.SoundEmitter:PlaySound("meta4/otter/vo_taunt_f8")
            end),
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        timeline = {
            SoundFrameEvent(4, "meta4/otter/vo_death_f4"),
            SoundFrameEvent(30, "meta4/otter/death_impact_f30"),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "special_atk1",
        tags = { "busy" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

        timeline =
        {
            SoundFrameEvent(0, "meta4/otter/taunt_f0"),
            SoundFrameEvent(8, "meta4/otter/vo_taunt_f8"),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State {
        name = "sleep",
        tags = { "sleeping" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        timeline = {
            SoundFrameEvent(5, "meta4/otter/sleep_pre_f5"),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
		end,

		timeline=
        {
			TimeEvent(5*FRAMES, function(inst) 
				PlayablePets.SleepHeal(inst)
			end),
            SoundFrameEvent(0, "meta4/otter/vo_sleep_loop_f0"),
            SoundFrameEvent(22, "meta4/otter/vo_sleep_loop_pst_f22"),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        timeline = {
            SoundFrameEvent(22, "meta4/otter/sleep_pst_f22"),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}

CommonStates.AddFrozenStates(states)
CommonStates.AddSimpleActionState(states, "pickup", "pickup", nil, BUSY_TAGS, nil, {
    SoundFrameEvent(5, "meta4/otter/pickup_f5"),
    FrameEvent(10, function(inst)
        inst:PerformBufferedAction()
    end),
})
CommonStates.AddSimpleActionState(states, "drop", "drop", nil, BUSY_TAGS, nil, {
    SoundFrameEvent(7, "meta4/otter/drop_f7"),
    FrameEvent(40, function(inst)
        inst.SoundEmitter:PlaySound("meta4/otter/attack_f0")
        inst:PerformBufferedAction()
    end),
})

CommonStates.AddSimpleRunStates(states, nil, {
    starttimeline = {
        FrameEvent(3, function(inst)
            if not inst.components.amphibiouscreature.in_water then
                inst.SoundEmitter:PlaySound("meta4/otter/vo_run_pre_f3")
            end
        end),
        FrameEvent(5, function(inst)
            if not inst.components.amphibiouscreature.in_water then
                inst.SoundEmitter:PlaySound("meta4/otter/run_pre_f5")
            end
        end),
    },
    runtimeline = {
        FrameEvent(8, function(inst)
            if not inst.components.amphibiouscreature.in_water then
                inst.SoundEmitter:PlaySound("meta4/otter/run_lp_f8")
            else
                inst.SoundEmitter:PlaySound("turnoftides/common/together/water/submerge/medium")
            end
        end),
    },
    endtimeline = {
        FrameEvent(4, function(inst)
            if not inst.components.amphibiouscreature.in_water then
                inst.SoundEmitter:PlaySound("meta4/otter/run_pst_f4")
            end
        end),
    },
})
CommonStates.AddSimpleState(states, "eat_pre", "eat_pre", EATING_TAGS, "eat", {
    SoundFrameEvent(6, "meta4/otter/vo_eat_pre_f6"),
})
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"run_pst", nil, nil, "idle", "run_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "taunt")
CommonStates.AddHopStates(states, false, {pre = "jumpout", loop = "jump_loop", pst = "jumpin"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "run_pst",
	plank_idle_loop = "idle",
	plank_idle_pst = "run_pst",
	
	plank_hop_pre = "run_pre",
	plank_hop = "run_loop",
	
	steer_pre = "run_pst",
	steer_idle = "idle",
	steer_turning = "idle",
	stop_steering = "run_pst",
	
	row = "run_pst",
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "run_pre",
	leap_loop = "run_loop",
	leap_pst = "run_loop",
	
	castspelltime = 10,
})

CommonStates.AddAmphibiousCreatureHopStates(states,
{ -- Config
    swimming_clear_collision_frame = 9 * FRAMES,
},
{ -- Anims
},
{ -- Timelines
    hop_pre =
    {
        FrameEvent(0, function(inst)
            if inst:HasTag("swimming") then
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())
            end
        end),
    },
    hop_pst = {
        FrameEvent(4, function(inst)
            if inst:HasTag("swimming") then
                inst.components.locomotor:Stop()
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())
            end
        end),
        FrameEvent(6, function(inst)
            if not inst:HasTag("swimming") then
                inst.components.locomotor:StopMoving()
            end
        end),
    }
})
    
return StateGraph("otterp", states, events, "idle", actionhandlers)

