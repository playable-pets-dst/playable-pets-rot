require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function spawnsplash(inst)
    local theta = (inst.Transform:GetRotation()-180) * DEGREES
    local dist = 1
    local offset = Vector3(dist * math.cos( theta ), 0, -dist * math.sin( theta ))
    local fx = SpawnPrefab("weregoose_splash")
    local newpt = offset + Vector3(inst.Transform:GetWorldPosition())
    fx.Transform:SetPosition(newpt:Get())
end

local longaction = "action2"
local shortaction = "action2"
local workaction = "hammer"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
    ActionHandler(ACTIONS.ROW, "row2"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local states=
{
    State{

        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle", true)
            inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/idle")
        end,

        timeline =
        {

        },

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State{
        name = "hammer",
        tags = {"busy", "action"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("atk")
           -- inst.SoundEmitter:PlaySound("dontstarve/wilson/make_trap", "make")
        end,
        onexit = function(inst)
        end,

        timeline =
        {
            TimeEvent(17*FRAMES, function(inst)
                PlayablePets.DoWork(inst, 3)
            end),
        },

        events=
        {
            EventHandler("animover", function (inst)
                inst.sg:GoToState("idle")
            end),
        }
    },

    State{

        name = "action2",
        tags = {"busy", "action"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("action_pre")
            inst.AnimState:PushAnimation("action",false)
        end,

        onexit = function(inst)
        end,

        timeline =
        {
            TimeEvent(6*FRAMES, function(inst)
                if inst:GetBufferedAction() and inst:GetBufferedAction().target and inst:GetBufferedAction().target.components.boatcannon then
                    if inst.cannon then inst.cannon.operator = nil end
                    inst.cannon = nil
                end
                inst:PerformBufferedAction()
            end),
        },

        events=
        {
            EventHandler("animqueueover", function (inst)
                inst.sg:GoToState("idle")
            end),
        }
    },

    State{
        name = "special_atk2",
        tags = {"busy"},
        onenter = function(inst, data)
            inst.Physics:Stop()

            inst.victory = true

            inst.AnimState:OverrideSymbol("swap_item", "cave_banana", "cave_banana01")
            inst.AnimState:PlayAnimation("action_victory_pre")

            inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/victory_pre")
        end,

        events=
        {
            EventHandler("animover", function (inst)
                inst.sg:GoToState("victory_pst")
            end),
        }
    },

    State{
        name = "victory_pst",
        tags = {"busy"},
        onenter = function(inst, data)
            inst.Physics:Stop()

            inst.AnimState:PlayAnimation("victory")
            inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/victory")

            inst.components.talker:Say(STRINGS["MONKEY_BATTLECRY_VICTORY_CHEER"][math.random(1,#STRINGS["MONKEY_BATTLECRY_VICTORY_CHEER"])])

        end,

        timeline =
        {
            TimeEvent(9*FRAMES, function(inst)
                PlayFootstep(inst)
            end),

            TimeEvent(25*FRAMES, function(inst)
                if inst.components.crewmember and inst.components.crewmember.boat then
                    inst.components.crewmember.boat.components.boatcrew:CrewCheer()
                end
                PlayFootstep(inst)
            end),

            TimeEvent(40*FRAMES, function(inst)
                PlayFootstep(inst)
            end),

            TimeEvent(54*FRAMES, function(inst)
                PlayFootstep(inst)
            end),
        },

        events=
        {
            EventHandler("animover", function (inst)
                inst.sg:GoToState("idle")
            end),
        }
    },

    State{

        name = "eat",
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat", true)
        end,

        onexit = function(inst)
            inst:PerformBufferedAction()
        end,

        timeline =
        {
            TimeEvent(8*FRAMES, function(inst)
                local waittime = FRAMES*8
                for i = 0, 3 do
                    inst:DoTaskInTime((i * waittime), function() inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/eat") end)
                end
            end)
        },

        events=
        {
            EventHandler("animover", function (inst)
                inst.sg:GoToState("idle")
            end),
        }
    },

    State{
        name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, data)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            inst.sg.statemem.say = data and data.say
        end,

        timeline =
        {
            TimeEvent(8*FRAMES, function(inst)
                local bc = inst.components.crewmember and inst.components.crewmember.boat and inst.components.crewmember.boat.components.boatcrew or nil                
                if inst.sg.statemem.say then
                    inst.components.talker:Say(inst.sg.statemem.say)
                elseif bc and bc.status == "retreat" then
                    inst.components.talker:Say(STRINGS["MONKEY_TALK_RETREAT"][math.random(1,#STRINGS["MONKEY_TALK_RETREAT"])])
                else
                    inst.components.talker:Say(STRINGS["MONKEY_BATTLECRY"][math.random(1,#STRINGS["MONKEY_BATTLECRY"])])
                end
                
                inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/taunt")
            end)
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "row2",
        tags = {"busy"},
        onenter = function(inst, playanim)
            local boat = inst:GetCurrentPlatform()
            if boat then
                inst.Transform:SetRotation(inst:GetAngleToPoint(boat.Transform:GetWorldPosition()))
            end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("row_pre",false)
            inst.AnimState:PushAnimation("row_loop",false)
            inst.AnimState:PushAnimation("row_pst",false)

            inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/row")

        end,

        onexit = function(inst)
        end,

        timeline =
        {
            TimeEvent(7*FRAMES, function(inst)
                spawnsplash(inst)
                inst:PerformBufferedAction()
            end),
        },

        events=
        {
            EventHandler("animqueueover", function (inst)
                inst.sg:GoToState("idle")
            end),
        }
    },


    State{
        name = "dive",
        tags = {"busy", "nomorph"},

        onenter = function(inst)
            local platform = inst:GetCurrentPlatform()
            if platform then
                local pt = Vector3(inst.Transform:GetWorldPosition())
                local angle = platform:GetAngleToPoint(pt)
                inst.Transform:SetRotation(angle)
            end

            inst.AnimState:PlayAnimation("dive")
            inst.Physics:Stop()
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)

            inst.SoundEmitter:PlaySound("monkeyisland/primemate/dive")
        end,

        timeline =
        {
            TimeEvent(10*FRAMES, function(inst)

                inst.sg.statemem.collisionmask = inst.Physics:GetCollisionMask()
                inst.Physics:SetCollisionMask(COLLISION.GROUND)
                if not TheWorld.ismastersim then
                    inst.Physics:SetLocalCollisionMask(COLLISION.GROUND)
                end

                inst.Physics:SetMotorVelOverride(5,0,0)
            end),

            TimeEvent(30*FRAMES, function(inst)
                inst.Physics:Stop()
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())
            end),
        },

        onexit = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.Physics:ClearMotorVelOverride()

            inst.Physics:ClearLocalCollisionMask()
            if inst.sg.statemem.collisionmask ~= nil then
                inst.Physics:SetCollisionMask(inst.sg.statemem.collisionmask)
            end
        end,

        events=
        {
            EventHandler("animover", function(inst)
                if TheWorld.Map:IsVisualGroundAtPoint(inst.Transform:GetWorldPosition()) or inst:GetCurrentPlatform() then
                    inst.sg:GoToState("dive_pst_land")
                else
                    SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())

                    if TheWorld.components.piratespawner then
                        TheWorld.components.piratespawner:StashLoot(inst)
                    end
                    inst:Remove()
                end
            end),
        },
    },

    State{
        name = "dive_pst_land",
        tags = {"busy"},

        onenter = function(inst)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("dive_pst_land")
            PlayFootstep(inst)
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,
		
		timeline = {
			TimeEvent(1*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/sleep_pre") 
            end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
		end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			TimeEvent(1*FRAMES, function(inst)
                PlayablePets.SleepHeal(inst)
                inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/sleep_lp", "sleep_lp") 
            end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

		timeline = {
            TimeEvent(1*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/sleep_pst") 
            end),
		},
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}

CommonStates.AddWalkStates(states,
{
    starttimeline =
    {

    },

	walktimeline =
    {
        TimeEvent(5*FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(13*FRAMES, function(inst) PlayFootstep(inst) end),
	},

    endtimeline =
    {
        TimeEvent(5*FRAMES, function(inst) PlayFootstep(inst) end),
    },
})

CommonStates.AddCombatStates(states,
{
    attacktimeline =
    {
        TimeEvent(14*FRAMES, function(inst)
            inst:PerformBufferedAction()

            if inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then
                inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/attack_sword")
            else
                inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/attack_unarmed")
            end

        end),
    },

    hittimeline =
    {
        TimeEvent(1*FRAMES, function(inst)
            inst.components.timer:StartTimer("hit",2+(math.random()*2))
            inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/hit") end),
    },

    deathtimeline =
    {
        TimeEvent(1*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/death") 
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end),
        TimeEvent(50*FRAMES, PlayablePets.DoDeath)
    },
},nil,{
    attackanimfn = function(inst) 
        if inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then
            return "atk"
        else
            return "unequipped_atk"
        end
    end
})

local moveanim = "walk"
local idleanim = "idle"
local actionanim = "walk_pst"
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	actionanim, nil, nil, "walk_pst", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/death") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("monkeyisland/powdermonkey/taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, "taunt")
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim.."_loop", pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim.."_loop",
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim.."_loop",
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim.."_loop",
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim.."_loop",
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})
	
return StateGraph("powdermonkeyp", states, events, "idle", actionhandlers)

