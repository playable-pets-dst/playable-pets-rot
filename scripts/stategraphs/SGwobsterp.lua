require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "squid"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "squid"}
	end
end

local function testExtinguish(inst)
    if inst:HasTag("swimming") then 
        if inst.components.burnable:IsBurning() then inst.components.burnable:Extinguish() end
    end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if TheNet:GetServerGameMode() == "lavaarena" then
			return "action"
		else
			return "idle"
		end	
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if not inst:HasTag("swimming") then
			if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
				inst.sg:GoToState("hit")
			elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
				inst.sg:GoToState("hit", data.stimuli)
			elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
				inst.sg:GoToState("hit") 
			end 
		end
	end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.AddCommonHandlers(),
	--PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

 local states=
{
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
        end,
		
		TimeEvent(0, function(inst)
			inst.SoundEmitter:PlaySound(inst._hit_sound)
		end),

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "special_atk1",
        tags = {"idle"},

        onenter = function(inst)
			if inst:HasTag("swimming") then
				inst.sg:GoToState("idle")
			else
				inst.components.locomotor:Stop()
				inst.AnimState:PushAnimation("stunned_loop", true)
			end
        end,

        timeline =
        {
            TimeEvent(0, function(inst)
                inst.SoundEmitter:PlaySound(inst._hit_sound)
            end),
        },
    },
	
	State{
		name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst, cb)
			if not inst:HasTag("swimming") then
				if inst.shouldwalk  then
					inst.shouldwalk = false
				else
					inst.shouldwalk = true
				end
			end
			inst.AnimState:PlayAnimation("run_pst")
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) 
				inst.sg:GoToState("idle") 
			end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.AnimState:PlayAnimation("death")
			inst.sg:SetTimeout(1.5)
			if inst:HasTag("swimming") then
				inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			end
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline =
        {
            TimeEvent(0, function(inst)
                inst.SoundEmitter:PlaySound("hookline_2/creatures/wobster/death")
            end),
        },

		ontimeout = function(inst)
			if not inst:HasTag("swimming") then
				inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			end
			PlayablePets.DoDeath(inst)
		end,
    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			if inst:HasTag("swimming") then
				inst.sg:GoToState("idle")
			else
				inst.AnimState:PlayAnimation("sleep_pre")
			end
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			TimeEvent(5*FRAMES, function(inst) 
				PlayablePets.SleepHeal(inst)
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

		timeline = {
		},
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}

CommonStates.AddFrozenStates(states)
local function play_run_step(inst)
    PlayFootstep(inst, 0.25)
end
CommonStates.AddIdle(states, false, "idle")
CommonStates.AddRunStates(states,
{
    starttimeline =
    {
        TimeEvent(2*FRAMES, play_run_step),
    },
    runtimeline =
    {
        TimeEvent(4*FRAMES, play_run_step),
        TimeEvent(8*FRAMES, play_run_step),
    },
    endtimeline =
    {
        TimeEvent(2*FRAMES, play_run_step),
    },
})
CommonStates.AddWalkStates(states,
{
	
})
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"run_pst", nil, nil, "idle", "run_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0, function(inst)
                inst.SoundEmitter:PlaySound("hookline_2/creatures/wobster/death")
            end),
		},
		
		corpse_taunt =
		{

		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "idle"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddHomeState(states, nil, "idle", "run_pst", true)
CommonStates.AddHopStates(states, false, {pre = "jump", loop = "jump_loop", pst = "jump_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "run_pst",
	plank_idle_loop = "idle",
	plank_idle_pst = "run_pst",
	
	plank_hop_pre = "jump",
	plank_hop = "jump_pst",
	
	steer_pre = "run_pst",
	steer_idle = "idle",
	steer_turning = "run_pst",
	stop_steering = "run_pst",
	
	row = "run_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "jump",
	leap_loop = "jump_loop",
	leap_pst = "jump_pst",
	
	lunge_pre = "run_pst",
	lunge_loop = "run_pst",
	lunge_pst = "run_pst",
	
	superjump_pre = "jump",
	superjump_loop = "jump_loop",
	superjump_pst = "jump_pst",
	
	castspelltime = 10,
})
    
CommonStates.AddAmphibiousCreatureHopStates(states, 
{ -- config
    swimming_clear_collision_frame = 23 * FRAMES,
},
{ -- anims
},
{ -- timeline
    hop_pre =
    {
        TimeEvent(0, function(inst) 
			inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
            if inst:HasTag("swimming") then 
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition()) 
            end
        end),
    },
    hop_pst = {
        TimeEvent(4 * FRAMES, function(inst) 
            if inst:HasTag("swimming") then 
                inst.components.locomotor:Stop()
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition()) 
                testExtinguish(inst)
            end
        end),
        TimeEvent(30 * FRAMES, function(inst) 
            if not inst:HasTag("swimming") then 
               inst.components.locomotor:StopMoving()
            end
			inst.Physics:CollidesWith(COLLISION.LIMITS)
        end), 
    }
})	
	
return StateGraph("wobsterp", states, events, "idle", actionhandlers)

