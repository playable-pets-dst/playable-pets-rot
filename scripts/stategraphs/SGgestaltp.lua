require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "lunar_aligned"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "lunar_aligned"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "lunar_aligned"}
	end
end

local function DoAoe(inst)
    local pos = inst:GetPosition()
    local ents = TheSim:FindEntities(pos.x, pos.y, pos.z, inst.mob_table.hit_range, {"_health", "_combat"},  GetExcludeTags(inst))
    if #ents > 0 and inst._aoetargets then
        for i, v in ipairs(ents) do
            if v:IsValid() and not inst._aoetargets[v] then
                inst.components.combat:DoAttack(v)
                if inst.prefab == "gestaltp" and v.components.grogginess then
                    v.components.grogginess:AddGrogginess(TUNING.GESTALT_ATTACK_DAMAGE_GROGGINESS, TUNING.GESTALT_ATTACK_DAMAGE_KO_TIME)
                end
                inst._aoetargets[v] = true
            end
        end
    end
end

local function DoSpecialAttack(inst)
	PlayablePets.DoWork(inst, 3)
    --[[
	local grogginess = target.components.grogginess
	if grogginess ~= nil then
		grogginess:AddGrogginess(TUNING.GESTALT_ATTACK_DAMAGE_GROGGINESS, TUNING.GESTALT_ATTACK_DAMAGE_KO_TIME)
		
	else
		target:PushEvent("attacked", {attacker = inst, damage = 0})
	end]]
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local states=
{
    State{
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("idle")
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "emerge",
        tags = {"busy", "noattack", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("emerge")
        end,

        timeline=
        {
            --TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/rabbit/hop") end ),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("melt")
            inst.Physics:Stop()		
			RemovePhysicsColliders(inst)
			inst.components.lootdropper:DropLoot()
            inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        timeline =
		{

		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },

    State{
        name = "relocate",
        tags = {"busy", "noattack", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("melt")
        end,

        timeline=
        {
            --TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/rabbit/hop") end ),
        },

        events =
        {
            EventHandler("animover", function(inst)
				inst.sg:GoToState("relocating")
			end),
        },
    },

    State{
        name = "relocating",
        tags = {"busy", "noattack", "hidden"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
			inst:Hide()
            inst.sg:SetTimeout(math.random() * 0.5 + 0.25)
        end,

        ontimeout = function(inst)
			inst.sg.statemem.dest = inst:FindRelocatePoint()
			if inst.sg.statemem.dest ~= nil then
				inst.sg:GoToState("emerge")
			else
				inst:Remove()
			end
		end,

		onexit = function(inst)
			if inst.sg.statemem.dest ~= nil then
				inst.Transform:SetPosition(inst.sg.statemem.dest:Get())
				inst:Show()
			else
				inst:Remove()
			end
		end
    },

    State{
        name = "attack",
        tags = { "busy", "noattack", "attack", "jumping" },

        onenter = function(inst)
            if inst.noactions then
                inst.sg:GoToState("idle")
                return
            end
            local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target ~= nil then
				if target:IsValid() then
					inst:ForceFacePoint(target:GetPosition())
					inst.sg.statemem.attacktarget = target
				end
			end
            inst._aoetargets = {}
            inst.AnimState:PlayAnimation("attack")

			inst.components.locomotor:Stop()
	        inst.components.combat:StartAttack()
		end,

        onupdate = function(inst)
            if inst.sg.statemem.enable_attack then
                DoAoe(inst, inst.sg.statemem.attacktarget)
            end
        end,

        timeline=
        {
            TimeEvent(15*FRAMES, function(inst)
					inst.Physics:SetMotorVelOverride(inst.prefab == "gestaltp" and 20 or 30, 0, 0)
					inst.sg.statemem.enable_attack = true
				end ),
            TimeEvent(25*FRAMES, function(inst)
					inst.Physics:ClearMotorVelOverride()
					inst.components.locomotor:Stop()
					inst.sg.statemem.enable_attack = false
				end ),
        },

        events =
        {
            EventHandler("animover", function(inst)
				inst.sg:GoToState("idle")
			end),
        },

        onexit = function(inst)
			inst.Physics:ClearMotorVelOverride()
			inst.components.locomotor:Stop()
            inst._aoetargets = {}
		end,
    },

    State{
        name = "guardattack",
        tags = { "busy", "noattack", "attack", "jumping" },

        onenter = function(inst)
            local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
            if target and target:IsValid() then
                inst.sg.state.mem.attacktarget = target
            end
            if inst.noactions then
                inst.sg:GoToState("idle")
                return
            end
            inst.AnimState:PlayAnimation("attack")
			inst.components.locomotor:Stop()
			if inst.components.combat.target ~= nil then
				inst:ForceFacePoint(inst.components.combat.target.Transform:GetWorldPosition())
			end
	        inst.components.combat:StartAttack()
		end,

        timeline=
        {
            TimeEvent(8*FRAMES, function(inst)
					if inst.components.combat.target ~= nil then
						inst:ForceFacePoint(inst.components.combat.target.Transform:GetWorldPosition())
					end
					inst.Physics:SetMotorVelOverride(30, 0, 0)
					inst.sg.statemem.enable_attack = true
				end),
            TimeEvent(19*FRAMES, function(inst)
					inst.Physics:ClearMotorVelOverride()
					inst.components.locomotor:Stop()
					inst.sg.statemem.enable_attack = false
					inst.components.combat:DropTarget()
				end),
        },

        onupdate = function(inst)
			if inst.sg.statemem.enable_attack then
				local target = inst.sg.state.mem.attacktarget
				if target ~= nil and target:IsValid() and inst:GetDistanceSqToInst(target) <= TUNING.GESTALT_ATTACK_HIT_RANGE_SQ then
                    if inst.components.combat:CanTarget(target) then
						PlayablePets.DoWork(inst, 4)
                        if inst.prefab == "gestaltp" and target.component.grogginess then
                            target.component.grogginess:AddGrogginess(TUNING.GESTALT_ATTACK_DAMAGE_GROGGINESS, TUNING.GESTALT_ATTACK_DAMAGE_KO_TIME)
                        end
						inst.sg:GoToState("mutate_pre", 6)
					end
				end
			end
        end,

        events =
        {
            EventHandler("animover", function(inst)
				inst.sg:GoToState("idle")
			end),
        },

        onexit = function(inst)
			inst.Physics:ClearMotorVelOverride()
			inst.components.locomotor:Stop()
		end,
    },

    State{
        name = "mutate_pre",
        tags = {"busy", "noattack", "jumping"},

        onenter = function(inst, speed)
			inst.Physics:SetMotorVelOverride(speed or 2, 0, 0)
            inst.AnimState:PlayAnimation("mutate")
			inst.persists = false
        end,

        timeline=
        {
            --TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/rabbit/hop") end ),
        },

        events =
        {
            EventHandler("animover", function(inst)
				inst:Remove()
			end),
        },

    },
    State
    {
        name = "special_sleep",
		tags = {"busy", "hiding"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("melt")
			inst:AddTag("invisible")
			inst:AddTag("notarget")
			inst.components.health:SetInvincible(true)
			inst.noactions = true
			inst:RemoveTag("scarytoprey")
            local speed = inst.prefab == "gestaltp" and 6 or 3
            inst.components.locomotor:SetExternalSpeedMultiplier(inst, speed, speed)          
        end,
		
		events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					inst:Hide()
					inst.specialsleep = false
					inst.sg:GoToState("idle")					
                end
            end),
        },

        
    },
	
	State
    {
        name = "special_wake",
		tags = {"busy"},
        onenter = function(inst)
			inst:Show()
            local speed = inst.prefab == "gestaltp" and 6 or 3
            inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, speed)
			inst.components.health:SetInvincible(false)
            inst.AnimState:PlayAnimation("emerge")
			inst.specialsleep = true
			inst.noactions = nil
			inst:RemoveTag("invisible")
			inst:RemoveTag("notarget")
			inst:AddTag("scarytoprey") 
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
}
local function SpawnTrail(inst)
	if not inst._notrail then
		local trail = SpawnPrefab("gestalt_trail")
		trail.Transform:SetPosition(inst.Transform:GetWorldPosition())
		trail.Transform:SetRotation(inst.Transform:GetRotation())
	end
end

CommonStates.AddWalkStates(states,
{
    starttimeline =
    {
    },
    walktimeline =
    {
        TimeEvent(0*FRAMES, SpawnTrail),
        --TimeEvent(5*FRAMES, SpawnTrail),
    },
    endtimeline =
    {
    },
}
, nil, nil, true)
PP_CommonStates.AddHomeState(states, nil, "walk_pst", "walk_pst", true)
PP_CommonStates.AddKnockbackState(states, nil, "walk_loop", nil, {onenter = function(inst)  end}) --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			
		},
		
		corpse_taunt =
		{
            
		},
	
	},
	--anims = 
	{
		corpse = "melt",
		corpse_taunt = "emerge"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk_loop", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle",
	plank_idle_loop = "walk_loop",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk_loop",
	
	steer_pre = "walk_pst",
	steer_idle = "idle",
	steer_turning = "walk_pst",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_pst",
	
	leap_pre = "walk_pre",
	leap_loop = "walk_loop",
	leap_pst = "walk_pst",
	
	castspelltime = 10,
})
    
return StateGraph("gestaltp", states, events, "idle", actionhandlers)

