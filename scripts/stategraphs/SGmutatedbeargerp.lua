require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "steal"
local shortaction = "action"
local workaction = "steal"
local otheraction = "action2"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.REVIVE_CORPSE, "failaction"), --no reviving
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function ShakeIfClose_Pound(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .7, .025, 1.25, inst, 40)
end

local SHAKE_DIST = 40

local COLLAPSIBLE_WORK_ACTIONS =
{
	CHOP = true,
	DIG = true,
	HAMMER = true,
	MINE = true,
}
local COLLAPSIBLE_TAGS = { "NPC_workable" }
for k, v in pairs(COLLAPSIBLE_WORK_ACTIONS) do
	table.insert(COLLAPSIBLE_TAGS, k.."_workable")
end
local NON_COLLAPSIBLE_TAGS = { "FX", --[["NOCLICK",]] "DECOR", "INLIMBO" }

local function DestroyStuff(inst, dist, radius, arc, nofx)
	if MOBFIRE ~= "Enable" then
		return
	end
    local x, y, z = inst.Transform:GetWorldPosition()
	local rot = inst.Transform:GetRotation() * DEGREES
	if dist ~= 0 then
		x = x + dist * math.cos(rot)
		z = z - dist * math.sin(rot)
	end
	for i, v in ipairs(TheSim:FindEntities(x, y, z, radius, nil, NON_COLLAPSIBLE_TAGS, COLLAPSIBLE_TAGS)) do
		if v:IsValid() and not v:IsInLimbo() and v.components.workable ~= nil then
			local x1, y1, z1 = v.Transform:GetWorldPosition()
			if arc == nil or ((x1 ~= x or z1 ~= z) and DiffAngleRad(rot, math.atan2(z - z1, x1 - x)) < arc) then
				local work_action = v.components.workable:GetWorkAction()
				--V2C: nil action for NPC_workable (e.g. campfires)
				if (work_action == nil and v:HasTag("NPC_workable")) or
					(v.components.workable:CanBeWorked() and work_action ~= nil and COLLAPSIBLE_WORK_ACTIONS[work_action.id])
				then
					if not nofx then
						SpawnPrefab("collapse_small").Transform:SetPosition(x1, y1, z1)
					end
					v.components.workable:Destroy(inst)
				end
			end
		end
    end
end

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return  {"notarget", "wall"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall", "battlestandard"}
	end
end

local ARC = 90 * DEGREES --degrees to each side
local AOE_RANGE_PADDING = 3
local AOE_TARGET_MUSTHAVE_TAGS = { "_combat" }
local AOE_TARGET_CANT_TAGS = GetExcludeTagsp()
local MAX_SIDE_TOSS_STR = 0.8

local function DoArcAttack(inst, dist, radius, heavymult, mult, forcelanded, targets)
	inst.components.combat.ignorehitrange = true
	local x, y, z = inst.Transform:GetWorldPosition()
	local rot = inst.Transform:GetRotation() * DEGREES
	local x0, z0
	if dist ~= 0 then
		if dist > 0 and ((mult ~= nil and mult > 1) or (heavymult ~= nil and heavymult > 1)) then
			x0, z0 = x, z
		end
		x = x + dist * math.cos(rot)
		z = z - dist * math.sin(rot)
	end
	for i, v in ipairs(TheSim:FindEntities(x, y, z, radius + AOE_RANGE_PADDING, AOE_TARGET_MUSTHAVE_TAGS, AOE_TARGET_CANT_TAGS)) do
		if v ~= inst and
			not (targets ~= nil and targets[v]) and
			v:IsValid() and not v:IsInLimbo()
			and not (v.components.health ~= nil and v.components.health:IsDead())
		then
			local range = radius + v:GetPhysicsRadius(0)
			local x1, y1, z1 = v.Transform:GetWorldPosition()
			local dx = x1 - x
			local dz = z1 - z
			local distsq = dx * dx + dz * dz
			if distsq > 0 and distsq < range * range and
				DiffAngleRad(rot, math.atan2(-dz, dx)) < ARC and
				inst.components.combat:CanTarget(v)
			then
				inst.components.combat:DoAttack(v)
				if mult ~= nil then
					local strengthmult = (v.components.inventory ~= nil and v.components.inventory:ArmorHasTag("heavyarmor") or v:HasTag("heavybody")) and heavymult or mult
					if strengthmult > MAX_SIDE_TOSS_STR and x0 ~= nil then
						--Don't toss as far to the side for frontal attacks
						dx = x1 - x0
						dz = z1 - z0
						if dx ~= 0 or dz ~= 0 then
							local rot1 = math.atan2(-dz, dx) + PI
							local k = math.max(0, math.cos(math.min(PI, DiffAngleRad(rot1, rot) * 2)))
							strengthmult = MAX_SIDE_TOSS_STR + (strengthmult - MAX_SIDE_TOSS_STR) * k * k
						end
					end
					v:PushEvent("knockback", { knocker = inst, radius = radius + dist, strengthmult = strengthmult, forcelanded = forcelanded })
				end
				if targets ~= nil then
					targets[v] = true
				end
			end
		end
	end
	inst.components.combat.ignorehitrange = false
end

local COMBO_ARC_OFFSET = 0.5

local function DoComboArcAttack(inst, targets)
	DoArcAttack(inst, COMBO_ARC_OFFSET, TUNING.BEARGER_MELEE_RANGE, 1, 1, nil, targets)
end

local function DoComboArcWork(inst)
	DestroyStuff(inst, COMBO_ARC_OFFSET, TUNING.BEARGER_MELEE_RANGE, ARC, true)
end

local function DoAOEAttack(inst, dist, radius, heavymult, mult, forcelanded, targets)
	inst.components.combat.ignorehitrange = true
	local x, y, z = inst.Transform:GetWorldPosition()
	local rot = inst.Transform:GetRotation() * DEGREES
	if dist ~= 0 then
		x = x + dist * math.cos(rot)
		z = z - dist * math.sin(rot)
	end
	for i, v in ipairs(TheSim:FindEntities(x, y, z, radius + AOE_RANGE_PADDING, AOE_TARGET_MUSTHAVE_TAGS, AOE_TARGET_CANT_TAGS)) do
		if v ~= inst and
			not (targets ~= nil and targets[v]) and
			v:IsValid() and not v:IsInLimbo()
			and not (v.components.health ~= nil and v.components.health:IsDead())
		then
			local range = radius + v:GetPhysicsRadius(0)
			local distsq = v:GetDistanceSqToPoint(x, y, z)
			if distsq < range * range and inst.components.combat:CanTarget(v) then
				inst.components.combat:DoAttack(v)
				if mult ~= nil then
					v:PushEvent("knockback", { knocker = inst, radius = radius + dist, strengthmult = (v.components.inventory ~= nil and v.components.inventory:ArmorHasTag("heavyarmor") or v:HasTag("heavybody")) and heavymult or mult, forcelanded = forcelanded })
				end
				if targets ~= nil then
					targets[v] = true
				end
			end
		end
	end
	inst.components.combat.ignorehitrange = false
end

local function TryStagger(inst)
	inst.sg:GoToState("stagger_pre")
	return true
end

local TRACKING_ARC = 90

local function StartTrackingTarget(inst, target)
	if target ~= nil and target:IsValid() then
		inst.sg.statemem.target = target
		inst.sg.statemem.targetpos = target:GetPosition()
		inst.sg.statemem.tracking = true
		local x1, y1, z1 = target.Transform:GetWorldPosition()
		local rot = inst.Transform:GetRotation()
		local rot1 = inst:GetAngleToPoint(x1, y1, z1)
		local diff = DiffAngle(rot, rot1)
		if diff < TRACKING_ARC then
			inst.Transform:SetRotation(rot1)
		end
	end
end

local function UpdateTrackingTarget(inst)
	if inst.sg.statemem.tracking then
		if inst.sg.statemem.target ~= nil then
			if inst.sg.statemem.target:IsValid() then
				local p = inst.sg.statemem.targetpos
				p.x, p.y, p.z = inst.sg.statemem.target.Transform:GetWorldPosition()
			else
				inst.sg.statemem.target = nil
			end
		end
		if inst.sg.statemem.targetpos ~= nil then
			local rot = inst.Transform:GetRotation()
			local rot1 = inst:GetAngleToPoint(inst.sg.statemem.targetpos)
			local drot = ReduceAngle(rot1 - rot)
			if math.abs(drot) < TRACKING_ARC then
				rot1 = rot + math.clamp(drot / 2, -1, 1)
				inst.Transform:SetRotation(rot1)
			end
		end
	end
end

local function StopTrackingTarget(inst)
	inst.sg.statemem.tracking = false
end

local function ShouldComboTarget(inst, target)
	if target and target:IsValid() and not IsEntityDeadOrGhost(target) and inst.combo_attacks < inst.max_combo then
		local x, y, z = inst.Transform:GetWorldPosition()
		local x1, y1, z1 = target.Transform:GetWorldPosition()
		local dx = x1 - x
		local dz = z1 - z
		local distsq = dx * dx + dz * dz
		if distsq > 0 and distsq < inst.components.combat:CalcAttackRangeSq(target) then
			local rot = inst.Transform:GetRotation()
			local rot1 = math.atan2(-dz, dx) * RADIANS
			return DiffAngle(rot, rot1) < TRACKING_ARC
		end
	end
    inst.combo_attacks = 0
	return false
end

local function ShouldButtTarget(inst, target)
	if target ~= nil and target:IsValid() and not IsEntityDeadOrGhost(target) then
		local x, y, z = inst.Transform:GetWorldPosition()
		local x1, y1, z1 = target.Transform:GetWorldPosition()
		local dx = x1 - x
		local dz = z1 - z
		local distsq = dx * dx + dz * dz
		if distsq > 0 and distsq < 64 then
			local rot = inst.Transform:GetRotation() + 180
			local rot1 = math.atan2(-dz, dx) * RADIANS
			return DiffAngle(rot, rot1) < TRACKING_ARC
		end
	end
	return false
end

local function TryButt(inst)
	if inst:IsButtRecovering() then
		return false
	elseif ShouldButtTarget(inst, inst.sg.statemem.target) then
		inst.sg:GoToState("special_atk3", inst.sg.statemem.target)
		return true
	end
	local target = inst.components.combat.target
	if target ~= nil and target ~= inst.sg.statemem.target and ShouldButtTarget(inst, target) then
		inst.sg:GoToState("special_atk3", target)
		return true
	end
	return false
end

--------------------------------------------------------------------------

local function SpawnSwipeFX(inst, offset, reverse)
	if inst.swipefx ~= nil then
		--spawn 3 frames early (with 3 leading blank frames) since anim is super short, and tends to get lost with network timing
		inst.sg.statemem.fx = SpawnPrefab(inst.swipefx)
		inst.sg.statemem.fx.entity:SetParent(inst.entity)
		inst.sg.statemem.fx.Transform:SetPosition(offset, 0, 0)
		if reverse then
			inst.sg.statemem.fx:Reverse()
		end
	end
end

local function KillSwipeFX(inst)
	if inst.sg.statemem.fx ~= nil then
		if inst.sg.statemem.fx:IsValid() then
			inst.sg.statemem.fx:Remove()
		end
		inst.sg.statemem.fx = nil
	end
end

local IDLE_FLAGS =
{
	Aggro =		0x01,
	Calm =		0x02,
	NoFaced =	0x04,
}

local events=
{
	CommonHandlers.OnLocomote(true,true),
	CommonHandlers.OnSleep(),
	CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.AddCommonHandlers(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	EventHandler("doattack", function(inst, data) inst.sg:GoToState("attack", data.target) end),
	EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") and not inst.sg:HasStateTag("busy") and inst.israged ~= nil and inst.israged == false then inst.sg:GoToState("hit") end end),
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}

local function ShakeIfClose(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, .7, .02, .3, inst, 40)
	end
end

local function ShakeIfClose_Footstep(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, .35, .02, 1.25, inst, 40)
	end
end

local function DoFootstep(inst)
	inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/step_stomp")
	ShakeIfClose_Footstep(inst)
	--ShakeIfClose(inst)
end

local states=
{

	State{
		name = "idle",
		tags = {"idle"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle_loop", true)
		end,

		timeline=
		{

		},
	},

----------------------COMBAT------------------------

	State{
		name = "targetstolen",
		tags = {"busy", "canrotate"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("taunt")
		end,

		timeline=
		{
			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt") end),
			TimeEvent(9*FRAMES, function(inst) DoFootstep(inst) end),
			TimeEvent(33*FRAMES, function(inst) DoFootstep(inst) end),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},
	
	State{
		name = "special_atk2",
		tags = {"busy", "canrotate"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("walk_pst")
			if inst.israged == true then
				inst.israged = false
			else	
			inst.Physics:Stop()
			inst.israged = true
			inst.sg:GoToState("targetstolen")
			end
		end,

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State{
		name = "hit",
		tags = {"hit", "busy"},

		onenter = function(inst, cb)
			if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			inst.AnimState:PlayAnimation("standing_hit")
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

    State{
		name = "attack",
		tags = { "attack", "busy", "jumping", "weapontoss" },

		onenter = function(inst)
            local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
            if target and target:IsValid() then
				inst.sg.statemem.target = target
			end
            inst.combo_attacks = inst.combo_attacks + 1
			inst.components.locomotor:Stop()
			inst.components.combat:StartAttack()
			inst:SwitchToEightFaced()
			inst.AnimState:PlayAnimation("atk1")
			inst.AnimState:PushAnimation("atk1_pst", false)
			StartTrackingTarget(inst, target)
			inst.sg.statemem.original_target = target --remember for onmissother event
		end,

		onupdate = UpdateTrackingTarget,

		timeline =
		{
			FrameEvent(4, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			FrameEvent(10, StopTrackingTarget),
			FrameEvent(28, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/swhoosh") end),
			FrameEvent(27, function(inst) inst.Physics:SetMotorVelOverride(6, 0, 0) end),
			FrameEvent(28, function(inst) inst.Physics:SetMotorVelOverride(12, 0, 0) end),
			FrameEvent(29, function(inst)
				SpawnSwipeFX(inst, COMBO_ARC_OFFSET)
			end),
			FrameEvent(32, function(inst)
				inst.sg.statemem.targets = {}
				ToggleOffCharacterCollisions(inst)
				DoComboArcAttack(inst, inst.sg.statemem.targets)
			end),
			FrameEvent(33, function(inst)
				DoComboArcAttack(inst, inst.sg.statemem.targets)
				DoComboArcWork(inst)
				ToggleOnCharacterCollisions(inst)
				if next(inst.sg.statemem.targets) == nil then
					inst:PushEvent("onmissother", { target = inst.sg.statemem.original_target }) --for ChaseAndAttack
				end
			end),
			FrameEvent(34, function(inst) inst.Physics:SetMotorVelOverride(6, 0, 0) end),
			FrameEvent(35, function(inst) inst.Physics:SetMotorVelOverride(3, 0, 0) end),
			FrameEvent(36, function(inst) inst.Physics:SetMotorVelOverride(1.5, 0, 0) end),
			FrameEvent(37, function(inst) inst.Physics:SetMotorVelOverride(0.75, 0, 0) end),
			FrameEvent(38, function(inst) inst.Physics:SetMotorVelOverride(0.375, 0, 0) end),
			FrameEvent(39, function(inst)
				inst.Physics:ClearMotorVelOverride()
				inst.Physics:Stop()
				inst.sg:RemoveStateTag("jumping")
			end),
			--
			FrameEvent(41, function(inst)
				if ShouldComboTarget(inst, inst.sg.statemem.target) then
					inst.sg:GoToState("attack_combo2", inst.sg.statemem.target)
				end
			end),
			FrameEvent(47, function(inst)
				if (inst.sg.mem.dostagger and TryStagger(inst)) or
					(inst.canbutt and TryButt(inst))
				then
					return
				end
				inst.sg:AddStateTag("caninterrupt")
			end),
			FrameEvent(54, function(inst)
				if inst.canbutt and TryButt(inst) then
					return
				end
				inst.sg:RemoveStateTag("busy")
			end),
		},

		events =
		{
			EventHandler("animqueueover", function(inst)
				if inst.AnimState:AnimDone() then
					if inst.canbutt and TryButt(inst) then
						return
					end
					inst.sg.statemem.keepfacing = true
					inst.sg:GoToState("idle")
				end
			end),
		},

		onexit = function(inst)
			ToggleOnCharacterCollisions(inst)
			inst.Physics:ClearMotorVelOverride()
			inst.Physics:Stop()
			KillSwipeFX(inst)
			inst:SwitchToFourFaced()
		end,
	},

	State{
		name = "attack_combo2",
		tags = { "attack", "busy", "jumping", "weapontoss" },

		onenter = function(inst, target)
			inst.components.locomotor:Stop()
			inst.components.combat:StartAttack()
			inst:SwitchToEightFaced()
			inst.AnimState:PlayAnimation("atk2")
			inst.AnimState:PushAnimation("atk2_pst", false)
			StartTrackingTarget(inst, target)
            inst.combo_attacks = inst.combo_attacks + 1
			inst.sg.statemem.original_target = target --remember for onmissother event
		end,

		onupdate = UpdateTrackingTarget,

		timeline =
		{
			FrameEvent(0, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			FrameEvent(10, StopTrackingTarget),
			FrameEvent(24, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/swhoosh") end),
			FrameEvent(23, function(inst) inst.Physics:SetMotorVelOverride(6, 0, 0) end),
			FrameEvent(24, function(inst) inst.Physics:SetMotorVelOverride(12, 0, 0) end),
			FrameEvent(25, function(inst)
				SpawnSwipeFX(inst, COMBO_ARC_OFFSET, true)
			end),
			FrameEvent(28, function(inst)
				inst.sg.statemem.targets = {}
				ToggleOffCharacterCollisions(inst)
				DoComboArcAttack(inst, inst.sg.statemem.targets)
			end),
			FrameEvent(29, function(inst)
				DoComboArcAttack(inst, inst.sg.statemem.targets)
				DoComboArcWork(inst)
				ToggleOnCharacterCollisions(inst)
				if next(inst.sg.statemem.targets) == nil then
					inst:PushEvent("onmissother", { target = inst.sg.statemem.original_target }) --for ChaseAndAttack
				end
			end),
			FrameEvent(30, function(inst) inst.Physics:SetMotorVelOverride(6, 0, 0) end),
			FrameEvent(31, function(inst) inst.Physics:SetMotorVelOverride(3, 0, 0) end),
			FrameEvent(32, function(inst) inst.Physics:SetMotorVelOverride(1.5, 0, 0) end),
			FrameEvent(33, function(inst) inst.Physics:SetMotorVelOverride(0.75, 0, 0) end),
			FrameEvent(34, function(inst) inst.Physics:SetMotorVelOverride(0.375, 0, 0) end),
			FrameEvent(35, function(inst)
				inst.Physics:ClearMotorVelOverride()
				inst.Physics:Stop()
				inst.sg:RemoveStateTag("jumping")
			end),
			--
			FrameEvent(37, function(inst)
				if ShouldComboTarget(inst, inst.sg.statemem.target) then
					inst.sg:GoToState("attack_combo1a", inst.sg.statemem.target)
				end
			end),
			FrameEvent(43, function(inst)
				if (inst.sg.mem.dostagger and TryStagger(inst)) or
					(inst.canbutt and TryButt(inst))
				then
					return
				end
				inst.sg:AddStateTag("caninterrupt")
			end),
			FrameEvent(50, function(inst)
				if inst.canbutt and TryButt(inst) then
					return
				end
				inst.sg:RemoveStateTag("busy")
			end),
		},

		events =
		{
			EventHandler("animqueueover", function(inst)
				if inst.AnimState:AnimDone() then
					if inst.canbutt and TryButt(inst) then
						return
					end
					inst.sg:GoToState("idle")
				end
			end),
		},

		onexit = function(inst)
			ToggleOnCharacterCollisions(inst)
			inst.Physics:ClearMotorVelOverride()
			inst.Physics:Stop()
			KillSwipeFX(inst)
			inst:SwitchToFourFaced()
		end,
	},

	State{
		name = "attack_combo1a",
		tags = { "attack", "busy", "jumping", "weapontoss" },

		onenter = function(inst, target)
			inst.components.locomotor:Stop()
			inst.components.combat:StartAttack()
			inst:SwitchToEightFaced()
			inst.AnimState:PlayAnimation("atk1a")
			inst.AnimState:PushAnimation("atk1_pst", false)
			StartTrackingTarget(inst, target)
            inst.combo_attacks = inst.combo_attacks + 1
			inst.sg.statemem.original_target = target --remember for onmissother event
		end,

		onupdate = UpdateTrackingTarget,

		timeline =
		{
			FrameEvent(0, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			FrameEvent(10, StopTrackingTarget),
			FrameEvent(24, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/swhoosh") end),
			FrameEvent(23, function(inst) inst.Physics:SetMotorVelOverride(6, 0, 0) end),
			FrameEvent(24, function(inst) inst.Physics:SetMotorVelOverride(12, 0, 0) end),
			FrameEvent(25, function(inst)
				SpawnSwipeFX(inst, COMBO_ARC_OFFSET)
			end),
			FrameEvent(28, function(inst)
				inst.sg.statemem.targets = {}
				ToggleOffCharacterCollisions(inst)
				DoComboArcAttack(inst, inst.sg.statemem.targets)
			end),
			FrameEvent(29, function(inst)
				DoComboArcAttack(inst, inst.sg.statemem.targets)
				DoComboArcWork(inst)
				ToggleOnCharacterCollisions(inst)
				if next(inst.sg.statemem.targets) == nil then
					inst:PushEvent("onmissother", { target = inst.sg.statemem.original_target }) --for ChaseAndAttack
				end
			end),
			FrameEvent(30, function(inst) inst.Physics:SetMotorVelOverride(6, 0, 0) end),
			FrameEvent(31, function(inst) inst.Physics:SetMotorVelOverride(3, 0, 0) end),
			FrameEvent(32, function(inst) inst.Physics:SetMotorVelOverride(1.5, 0, 0) end),
			FrameEvent(33, function(inst) inst.Physics:SetMotorVelOverride(0.75, 0, 0) end),
			FrameEvent(34, function(inst) inst.Physics:SetMotorVelOverride(0.375, 0, 0) end),
			FrameEvent(35, function(inst)
				inst.Physics:ClearMotorVelOverride()
				inst.Physics:Stop()
				inst.sg:RemoveStateTag("jumping")
			end),
			--
			FrameEvent(37, function(inst)
				if ShouldComboTarget(inst, inst.sg.statemem.target) then
					inst.sg:GoToState("attack_combo2", inst.sg.statemem.target)
				end
			end),
			FrameEvent(43, function(inst)
				if (inst.sg.mem.dostagger and TryStagger(inst)) or
					(inst.canbutt and TryButt(inst))
				then
					return
				end
				inst.sg:AddStateTag("caninterrupt")
			end),
			FrameEvent(50, function(inst)
				if inst.canbutt and TryButt(inst) then
					return
				end
				inst.sg:RemoveStateTag("busy")
			end),
		},

		events =
		{
			EventHandler("animqueueover", function(inst)
				if inst.AnimState:AnimDone() then
					if inst.canbutt and TryButt(inst) then
						return
					end
					inst.sg:GoToState("idle")
				end
			end),
		},

		onexit = function(inst)
			ToggleOnCharacterCollisions(inst)
			inst.Physics:ClearMotorVelOverride()
			inst.Physics:Stop()
			KillSwipeFX(inst)
			inst:SwitchToFourFaced()
		end,
	},
	
	State{
		name = "quick_attack",
		tags = {"attack", "busy", "canrotate"},

		onenter = function(inst)
      
                inst.components.locomotor:StopMoving()                
                inst.components.combat:StartAttack()
                inst.AnimState:PlayAnimation("atk")			
				inst.AnimState:SetTime(15* FRAMES)
        end,

		timeline=
		{
			TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			--TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/swhoosh") end),
			TimeEvent(20*FRAMES, function(inst) inst:PerformBufferedAction() end),
		},


		events=
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

    State{
		name = "special_atk3",
		tags = { "attack", "busy" },

		onenter = function(inst, target)
			inst.components.locomotor:Stop()
			local left
			if target ~= nil and target:IsValid() then
				local x1, y1, z1 = target.Transform:GetWorldPosition()
				local rot = inst.Transform:GetRotation()
				local rot1 = inst:GetAngleToPoint(x1, y1, z1) + 180
				local drot = ReduceAngle(rot1 - rot)
				if drot ~= 0 and math.abs(drot) < TRACKING_ARC then
					left = drot > 0
					inst.sg.statemem.left = left
					inst.sg.statemem.target = target
					inst.sg.statemem.targetpos = target:GetPosition()
				end
			end
			if left == nil then
				left = math.random() < 0.5
			end
			inst.AnimState:PlayAnimation(left and "butt_pre_L" or "butt_pre_R")
		end,

		timeline =
		{
			FrameEvent(0, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			FrameEvent(23, function(inst)
				local p = inst.sg.statemem.targetpos
				if p ~= nil then
					local rot2
					local target = inst.sg.statemem.target
					if target ~= nil and target:IsValid() then
						local x1, y1, z1 = target.Transform:GetWorldPosition()
						local rot = inst.Transform:GetRotation()
						local rot1 = inst:GetAngleToPoint(x1, y1, z1) + 180
						local drot = ReduceAngle(rot1 - rot)
						local left = drot > 0
						if drot ~= 0 and math.abs(drot) < (left == inst.sg.statemem.left and TRACKING_ARC or TRACKING_ARC / 3) then
							rot2 = rot + drot / 2
							p.x, p.y, p.z = x1, y1, z1
						end
					end
					if rot2 == nil then
						rot2 = inst:GetAngleToPoint(p) + 180
					end
					inst.Transform:SetRotation(rot2)
				end
			end),
			FrameEvent(25, function(inst)

			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					local data = inst.sg.statemem.targetpos ~= nil and {
						targetpos = inst.sg.statemem.targetpos,
						target = inst.sg.statemem.target,
					} or nil
					inst.sg:GoToState("butt", data)
				end
			end),
		},
	},

	State{
		name = "running_butt_pre",
		tags = { "attack", "busy" },

		onenter = function(inst, target)
			inst.components.locomotor:Stop()
			local left
			if target ~= nil and target:IsValid() then
				local x1, y1, z1 = target.Transform:GetWorldPosition()
				local rot = inst.Transform:GetRotation()
				local rot1 = inst:GetAngleToPoint(x1, y1, z1) + 180
				local drot = ReduceAngle(rot1 - rot)
				if drot ~= 0 and math.abs(drot) < TRACKING_ARC then
					left = drot > 0
					inst.sg.statemem.left = left
					inst.sg.statemem.target = target
					inst.sg.statemem.targetpos = target:GetPosition()
				end
			end
			if left == nil then
				left = math.random() < 0.5
			end
			inst.AnimState:PlayAnimation(left and "butt_pre_L" or "butt_pre_R")
			inst.AnimState:SetFrame(22)
		end,

		timeline =
		{
			FrameEvent(1, function(inst)
				local p = inst.sg.statemem.targetpos
				if p ~= nil then
					local rot2
					local target = inst.sg.statemem.target
					if target ~= nil and target:IsValid() then
						local x1, y1, z1 = target.Transform:GetWorldPosition()
						local rot = inst.Transform:GetRotation()
						local rot1 = inst:GetAngleToPoint(x1, y1, z1) + 180
						local drot = ReduceAngle(rot1 - rot)
						local left = drot > 0
						if drot ~= 0 and math.abs(drot) < (left == inst.sg.statemem.left and TRACKING_ARC or TRACKING_ARC / 3) then
							rot2 = rot + drot / 2
							p.x, p.y, p.z = x1, y1, z1
						end
					end
					if rot2 == nil then
						rot2 = inst:GetAngleToPoint(p) + 180
					end
					inst.Transform:SetRotation(rot2)
				end
			end),
			FrameEvent(3, function(inst)
				
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					local data = inst.sg.statemem.targetpos ~= nil and {
						targetpos = inst.sg.statemem.targetpos,
						target = inst.sg.statemem.target,
						running = true,
					} or nil
					inst.sg:GoToState("butt", data)
				end
			end),
		},
	},

	State{
		name = "butt",
		tags = { "attack", "busy", "jumping", "nointerrupt" },

		onenter = function(inst, data)
			inst.components.locomotor:Stop()
			inst.components.combat:RestartCooldown()
			inst.AnimState:PlayAnimation("butt")
			local dist
			if data ~= nil then
				if data.running then
					dist = 6
				else
					local x, y, z = inst.Transform:GetWorldPosition()
					if data.target ~= nil and data.target:IsValid() then
						local x1, y1, z1 = data.target.Transform:GetWorldPosition()
						local dx = x1 - x
						local dz = z1 - z
						if dx ~= 0 or dz ~= 0 then
							local rot = inst.Transform:GetRotation() * DEGREES
							local rot1 = math.atan2(-dz, dx) + PI
							local diff = DiffAngleRad(rot, rot1)
							if diff < PI / 4 then
								dist = math.sqrt(dx * dx + dz * dz)
								dist = dist * math.cos(diff)
							end
						end
					end
					if dist == nil and data.targetpos ~= nil then
						local dx = data.targetpos.x - x
						local dz = data.targetpos.z - z
						if dx ~= 0 or dz ~= 0 then
							dist = math.sqrt(dx * dx + dz * dz)
						end
					end
				end
				inst.sg.statemem.original_target = data.target --remember for onmissother event
			end
			dist = math.clamp(dist or 3, 3, 6)
			inst.Physics:SetMotorVelOverride(-dist / inst.AnimState:GetCurrentAnimationLength(), 0, 0)
		end,

		timeline =
		{
			FrameEvent(1, ToggleOffCharacterCollisions),
			FrameEvent(8, function(inst)
				local pt = inst:GetPosition()
				local rot = inst.Transform:GetRotation() * DEGREES
				local dist = -1
				pt.x = pt.x + dist * math.cos(rot)
				pt.y = 0
				pt.z = pt.z - dist * math.sin(rot)

				inst.components.groundpounder:GroundPound(pt)
				inst.SoundEmitter:PlaySound("rifts3/mutated_bearger/buttslam")
			end),
			FrameEvent(9, function(inst)
				local x, y, z = inst.Transform:GetWorldPosition()
				local rot = inst.Transform:GetRotation() * DEGREES
				local dist = -1
				x = x + dist * math.cos(rot)
				z = z - dist * math.sin(rot)

				inst.sg.statemem.targets = {}
				DoAOEAttack(inst, dist, 4, 1.2, 1.5, nil, inst.sg.statemem.targets)
				ToggleOnCharacterCollisions(inst)

				local sinkhole = SpawnPrefab("bearger_sinkhole")
				sinkhole.Transform:SetPosition(x, 0, z)
				sinkhole:PushEvent("docollapse")

				ShakeIfClose_Pound(inst) --override sinkhole shake
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg.statemem.butt = true
					inst.sg:GoToState("butt_pst", {
						target = inst.sg.statemem.original_target,
						targets = inst.sg.statemem.targets,
					})
				end
			end),
		},

		onexit = function(inst)
			ToggleOnCharacterCollisions(inst)
			if not inst.sg.statemem.butt then
				inst.Physics:ClearMotorVelOverride()
				inst.Physics:Stop()
			end
		end,
	},

	State{
		name = "butt_pst",
		tags = { "attack", "busy", "jumping" },

		onenter = function(inst, data)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("butt_pst", false)
            inst.AnimState:PushAnimation("to_bi", false)
			if data ~= nil and data.targets ~= nil then
				DoAOEAttack(inst, -1, 4, 1.2, 1.5, nil, data.targets)
				if next(data.targets) == nil then
					inst:PushEvent("onmissother", { target = data.target }) --for ChaseAndAttack
				end
			end
		end,

		timeline =
		{
			FrameEvent(0, function(inst) inst.Physics:SetMotorVelOverride(-4, 0, 0) end),
			FrameEvent(1, function(inst) inst.Physics:SetMotorVelOverride(-2, 0, 0) end),
			FrameEvent(2, function(inst) inst.Physics:SetMotorVelOverride(-1, 0, 0) end),
			FrameEvent(3, function(inst) inst.Physics:SetMotorVelOverride(-0.5, 0, 0) end),
			FrameEvent(4, function(inst)
				inst.Physics:ClearMotorVelOverride()
				inst.Physics:Stop()
				inst.sg:RemoveStateTag("jumping")
			end),
			FrameEvent(20, function(inst)
				inst.sg.statemem.vulnerable = true
			end),
			FrameEvent(41, function(inst)
				inst.sg.statemem.vulnerable = false
			end),
		},

		events =
		{
			EventHandler("attacked", function(inst, data)
				if inst.sg.statemem.vulnerable and
					not inst.components.health:IsDead() and
					data ~= nil and data.spdamage ~= nil and data.spdamage.planar ~= nil
				then
					inst.sg:GoToState("butt_face_hit")
				end
				return true
			end),
			EventHandler("animqueueover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},

		onexit = function(inst)
			inst.Physics:ClearMotorVelOverride()
			inst.Physics:Stop()
		end,
	},

	State{
		name = "butt_face_hit",
		tags = { "hit", "busy" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("butt_face_hit")
			CommonHandlers.UpdateHitRecoveryDelay(inst)
			inst.sg.statemem.vulnerable = true
		end,

		timeline =
		{
			FrameEvent(8, function(inst)
				if inst.sg.mem.dostagger and TryStagger(inst) then
					return
				end
				inst.sg.statemem.canstagger = true
			end),
			FrameEvent(28, function(inst)
				inst.sg.statemem.vulnerable = false
			end),
		},

		events =
		{
			EventHandler("attacked", function(inst, data)
				if inst.sg.statemem.vulnerable and
					not inst.components.health:IsDead() and
					data ~= nil and data.spdamage ~= nil and data.spdamage.planar ~= nil
				then
					inst.sg.mem.dostagger = true
					if inst.sg.statemem.canstagger then
						TryStagger(inst)
					end
				end
				return true
			end),
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},
	},

	State{
		name = "special_atk1",
		tags = {"attack", "busy"},

		onenter = function(inst)
			if inst.taunt == false then
				inst.sg:GoToState("idle")
			else 
				if inst.components.locomotor then
					inst.components.locomotor:StopMoving()
				end
				inst.AnimState:PlayAnimation("ground_pound")
				inst.AnimState:PushAnimation("taunt_pre", false)
			end	
		end,
		
		onexit = function(inst)
			if inst.components.revivablecorpse then
				inst.components.combat:SetAreaDamage(PP_FORGE.BEARGER.AOE_RANGE, PP_FORGE.BEARGER.AOE_DMGMULT)
			end		
		end,

		timeline=
		{
			TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/swhoosh") end),
			TimeEvent(20*FRAMES, function(inst)
				ShakeIfClose(inst)
				if not inst.components.revivablecorpse then
					local isname = inst:GetDisplayName()
					local selfpos = inst:GetPosition()
					print("LOGGED: "..isname.." used ground pound at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)
				end
				if TheNet:GetServerGameMode() == "lavaarena" then
					inst.components.combat:SetAreaDamage(PP_FORGE.BEARGER.AOE_RANGE, PP_FORGE.BEARGER.GP_DMGMULT)
					inst.components.combat:DoAreaAttack(inst, 6, nil, nil, "strong", GetExcludeTagsp)
				else
					inst.components.groundpounder:GroundPound()
				end				
				inst.cangroundpound = false
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
			end),
		},

		events=
		{
			EventHandler("animqueueover", function(inst)
				inst.taunt = false
				inst:DoTaskInTime(10, function() inst.taunt = true end)
				inst.sg:GoToState("idle")
			end),
		},
	},

	State{
		name = "death",
		tags = {"busy", "pausepredict", "nomorph"},

		onenter = function(inst)
			if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			inst.AnimState:PlayAnimation("death")
			inst.components.inventory:DropEverything(true)
			--inst.Physics:ClearCollisionMask()
			
		end,

		timeline=
		{
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/death") end),
			--TimeEvent(6*FRAMES, function(inst)inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition())) end),
			TimeEvent(50*FRAMES, function(inst)
				ShakeIfClose(inst)
				inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
			end),
		},
		
		 events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

	},

-------------------EATING-------------------------

	State{
		name = "action2",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("action")
			inst.AnimState:PushAnimation("eat_loop", false)
		end,

		onexit = function(inst)

		end,

		timeline=
		{
			TimeEvent(5*FRAMES, function(inst) end),
			TimeEvent(15*FRAMES, function(inst)
				inst:PerformBufferedAction()
				--inst.sg:RemoveStateTag("busy")
				--inst.sg:AddStateTag("wantstoeat")
				--inst.last_eat_time = GetTime()
			end),
			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/gulp") end),
		},

		events =
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("eat_pst") end)
		}
	},

	State{
		name = "eat_loop",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PushAnimation("eat_loop", true)
			local timeout = math.random()+.5
			local ba = inst:GetBufferedAction()
			if ba and ba.target and ba.target:HasTag("edible") then
					timeout = timeout*2
			inst.sg:SetTimeout(timeout)
			end
		end,

		timeline=
		{
			TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/chew") end),
			TimeEvent(10*FRAMES, function(inst) inst:PerformBufferedAction() end),
			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/chew") end),
			TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/chew") end),
		},

		ontimeout = function(inst)
			local ba = inst:GetBufferedAction()
			inst:PerformBufferedAction()
			inst.last_eat_time = GetTime()
			inst.sg:GoToState("eat_pst")
		end,

	},

	State{
		name = "eat_pst",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("eat_pst")
		end,

		timeline=
		{
		},

		events=
		{
			EventHandler("animover", function(inst)
				inst.sg:GoToState("idle")
			end),
		},
	},

	State{
		name = "steal",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("atk", false)
		end,

		timeline=
		{
			TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			--TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/attack") end),
			TimeEvent(28*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/swhoosh") end),
			TimeEvent(35*FRAMES, function(inst) PlayablePets.DoWork(inst, 8) end),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

---------------------------WALKING---------------
-- the tags and names have been switched around to trick the game into making you "walk"

	State{
			name = "run_start",
			tags = {"moving", "running", "canrotate"},

			onenter = function(inst)
			if inst.israged == true then
				inst.components.locomotor:RunForward()
				inst.AnimState:PlayAnimation("charge_pre")
			else
				inst.components.locomotor:WalkForward()
				inst.AnimState:PlayAnimation("walk_pre")
			end
				--local anim = (inst.components.combat.target and not inst.components.combat.target:HasTag("beehive")) and "charge_pre" or "walk_pre"
				--inst.AnimState:PlayAnimation(anim)
			end,

			events =
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},
		},

	State{
			name = "run",
			tags = {"moving", "running", "canrotate"},

			onenter = function(inst)
				if inst.israged == true then
					inst.components.locomotor:RunForward()
					inst.AnimState:PlayAnimation("charge_loop")
				else
					inst.components.locomotor:WalkForward()
					inst.AnimState:PlayAnimation("walk_loop")
				end
				--local anim = (inst.components.combat.target and not inst.components.combat.target:HasTag("beehive")) and "charge_loop" or "walk_loop"
				--inst.AnimState:PlayAnimation(anim)
				--inst.components.locomotor:RunForward()
				if inst.israged == true then
					inst:DoTaskInTime(math.random(13)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/grrrr") end)
				end
			end,

			events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

			timeline=
			{
				TimeEvent(2*FRAMES, function(inst)
					if inst.israged == true then
						DoFootstep(inst)
						ShakeIfClose(inst)
						
					end
				end),
				TimeEvent(18*FRAMES, function(inst)
					if inst.israged == true then
						DoFootstep(inst)
						ShakeIfClose(inst)
					end
				end),
				TimeEvent(4*FRAMES, function(inst)
					if not inst.israged == true then
						DoFootstep(inst)
						ShakeIfClose(inst)
					end
				end),
				TimeEvent(30*FRAMES, function(inst)
					if not inst.israged == true then
						DoFootstep(inst)
						ShakeIfClose(inst)
					end
				end),
			},
		},

	State{
			name = "run_stop",
			tags = {"canrotate"},

			onenter = function(inst)
				inst.components.locomotor:StopMoving()
				--local anim = (inst.components.combat.target and not inst.components.combat.target:HasTag("beehive")) and "charge_pst" or "walk_pst"
				DoFootstep(inst)
				ShakeIfClose(inst)
				if inst.israged == true then
					inst.AnimState:PlayAnimation("charge_pst")
				else
					inst.AnimState:PlayAnimation("walk_pst")
				end
			end,

			events=
			{
				EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
			},
		},

	State{
			name = "walk_start",
			tags = {"moving", "atk_pre", "canrotate"},

			onenter = function(inst)
				inst.components.locomotor:WalkForward()
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt", "taunt")
				inst.AnimState:PlayAnimation("charge_pre")
			end,

			events =
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},
		},

	State{

			name = "walk",
			tags = {"moving", "canrotate"},

			onenter = function(inst)
				--inst.components.locomotor:WalkForward()
				if not inst.SoundEmitter:PlayingSound("taunt") then inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt", "taunt") end
				inst.AnimState:PlayAnimation("charge_roar_loop")
			end,

			timeline=
			{
				TimeEvent(3*FRAMES, function(inst)
					DoFootstep(inst)
				end),
				TimeEvent(11*FRAMES, function(inst)
					DoFootstep(inst)
				end),
			},

			events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},
		},

	State{

			name = "walk_stop",
			tags = {"canrotate"},

			onenter = function(inst)
				inst.components.locomotor:StopMoving()
				--local should_softstop = false
				inst.AnimState:PlayAnimation("charge_pst")
			end,

			events=
			{
				EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
			},
		},
		
		State{

			name = "action",
			tags = {"canrotate"},

			onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst:PerformBufferedAction()
				inst.AnimState:PlayAnimation("charge_pst")
			end,

			events=
			{
				EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
			},
		},

------------------SLEEPING-----------------

		State{
			name = "sleep",
			tags = {"busy", "sleeping"},

			onenter = function(inst)
				
				inst.components.locomotor:StopMoving(true)
				inst.AnimState:PlayAnimation("standing_sleep_pre")
				inst.AnimState:PushAnimation("sleep_pre", false)
			end,

			events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
				EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
			},

			timeline=
			{
				TimeEvent(25*FRAMES, function(inst)
					DoFootstep(inst)
				end),
			},
		},

		State{

			name = "sleeping",
			tags = {"sleeping", "busy"},
			onenter = function(inst)
				if inst.components.locomotor then
					inst.components.locomotor:StopMoving()
				end
				inst.Physics:Stop()
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
				
			end,
			
			timeline =
			{
				TimeEvent(0*FRAMES, function(inst) inst.components.locomotor:StopMoving() end)
			},

			events=
			{
				EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),				
			},
		},

		State{

			name = "wake",
			tags = {"busy", "waking"},

			onenter = function(inst)
				--inst.last_eat_time = GetTime() -- Fake this as eating so he doesn't aggro immediately
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("sleep_pst")
				if inst.components.sleeper and inst.components.sleeper:IsAsleep() then
					inst.components.sleeper:WakeUp()
				end
			end,

			events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
			},

			timeline=
			{
				TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt_short") end),
			},
		},
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/death") end),
			TimeEvent(50*FRAMES, function(inst)
				ShakeIfClose(inst)
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt") end),
			TimeEvent(9*FRAMES, function(inst) DoFootstep(inst) end),
			TimeEvent(33*FRAMES, function(inst) DoFootstep(inst) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)

return StateGraph("bearger_yulep", states, events, "idle", actionhandlers)

