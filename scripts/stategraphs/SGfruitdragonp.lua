require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "fruitdragon"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "fruitdragon"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "fruitdragon"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local function CalcJumpSpeed(inst, target)
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (inst:GetPhysicsRadius(0) + target:GetPhysicsRadius(-1))
        if dist > 0 then
            return math.min(8, dist) / (10 * FRAMES)
        end
    end
    return 0
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if TheNet:GetServerGameMode() == "lavaarena" then
			return "quick_attack"
		else
			if inst._is_ripe and inst.fire_attack then
				return "attack_fire"
			else
				return "attack"
			end
		end	
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{
   State{
        name = "idle",
        tags = {"idle", "canrotate"},
		
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle_loop")
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "do_ripen",
        tags = {"busy", "waking"},

        onenter = function(inst)
 --           inst.AnimState:PlayAnimation("tail_growth")
            inst.AnimState:PlayAnimation("sleep_ripe_pst")
        end,

        timeline=
        {
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },

		onexit = function(inst)
			if not inst._is_ripe then
				inst:MakeRipe()
			end
		end,
    },

    State{
        name = "do_unripen",
        tags = {"busy", "sleeping"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("sleep_ripe_pre")
			if inst._is_ripe then
				inst:MakeUnripe()
			end
        end,

        timeline=
        {
            TimeEvent(36*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.do_unripen)
            end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

	State
    {
        name = "attack",
        tags = { "attack", "busy" },

        onenter = function(inst, target)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("attack")
            inst.components.combat:StartAttack()
            inst.sg.statemem.target = target
        end,

        timeline =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack) end),
			TimeEvent(22*FRAMES, function(inst) PlayablePets.DoWork(inst, 2.5) end),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "quick_attack",
        tags = { "attack", "busy" },

        onenter = function(inst, target)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("attack")
            inst.components.combat:StartAttack()
            inst.sg.statemem.target = target
			inst.AnimState:SetTime(20 * FRAMES)
        end,

        timeline =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack) end),
			TimeEvent(2*FRAMES, function(inst) inst:PerformBufferedAction() end),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	State
    {
        name = "attack_fire",
        tags = { "attack", "busy" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("attack_fire")
            inst.components.combat:StartAttack()
        end,

        timeline =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack_fire) end),
			TimeEvent(16*FRAMES, function(inst) 
				inst.Light:Enable(true)
				inst.DynamicShadow:Enable(false)
			end),
			TimeEvent(20*FRAMES, function(inst) 
				local x, y, z = inst.Transform:GetWorldPosition()
				local ents = TheSim:FindEntities(x, y, z, TUNING.FRUITDRAGON.FIREATTACK_HIT_RANGE + 6, nil, GetExcludeTags(inst), {"_health", "canlight"})
				for _, ent in ipairs(ents) do
					if inst:IsNear(ent, ent:GetPhysicsRadius(0) + TUNING.FRUITDRAGON.FIREATTACK_HIT_RANGE + 2) then
						if ent.components.health ~= nil and not ent.components.health:IsDead() then
							ent.components.health:DoFireDamage(TUNING.FRUITDRAGON.FIREATTACK_DAMAGE, inst, true)
						end
						if ent.components.burnable and MOBFIRE== "Disable" then
							ent.components.burnable:Ignite(true, inst)
						end
					end
				end
			end),

			TimeEvent(37*FRAMES, function(inst) 
				inst.Light:Enable(false)
				inst.DynamicShadow:Enable(true)
				inst.sg:RemoveStateTag("busy")
			end),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },

		onexit = function(inst)
			inst.fire_attack = false
			inst:DoTaskInTime(5, function(inst) inst.fire_attack = true end)
			inst.Light:Enable(false)
			inst.DynamicShadow:Enable(true)
		end
    },
	
	State{
        name = "special_atk1",
        tags = {"busy", "canrotate", "caninterrupt"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("challenge_pre")
			inst.components.locomotor:StopMoving()
        end,

        timeline=
        {
            TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.challenge_pre)
				--inst.components.combat:DoAttack(nil, nil, nil, nil, 0)
            end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("challenge_attack", 1 + math.random(3)) end ),
        },
    },

    State{
        name = "challenge_attack",
        tags = {"busy", "canrotate", "caninterrupt"},

        onenter = function(inst, num_loops)
			inst.sg.statemem.num_loops = (num_loops or 1) - 1
            inst.AnimState:PlayAnimation("challenge_loop")
			inst.components.locomotor:StopMoving()
        end,

        timeline=
        {
            TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.challenge)
            end),
        },

        events=
        {
            EventHandler("animover", function(inst)
				inst.sg:GoToState("challenge_attack_pst")
			end),
        },
    },

    State{
        name = "challenge_attack_pst",
        tags = {"busy", "canrotate", "caninterrupt"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("challenge_pst")
			inst.components.locomotor:StopMoving()
        end,

        timeline=
        {
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.challenge_pst) end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("walk_pst")
			inst.components.locomotor:StopMoving()
			if not inst.shouldwalk or inst.shouldwalk == false then
				inst.shouldwalk = true
			else
				inst.shouldwalk = false
			end
        end,

        timeline=
        {
            TimeEvent(5*FRAMES, function(inst)
                --inst.SoundEmitter:PlaySound("dontstarve/creatures/together/grass_gekko/tail_regrow") 
            end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	 State{
        name = "eat",
        tags = {"busy", "canrotate"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("eat")
			inst.components.locomotor:StopMoving()
        end,

        timeline=
        {
            TimeEvent(5*FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "challenge_lose",
        tags = {"busy", "canrotate"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("hit")
			inst.components.locomotor:StopMoving()
        end,

        timeline=
        {
            TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/grass_gekko/tail_regrow") 
            end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {
			TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
			TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/grass_gekko/body_fall") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
}

CommonStates.AddFrozenStates(states)
CommonStates.AddHitState(states,
{
    TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.onhit) end),
})

CommonStates.AddWalkStates(states,
{
    starttimeline =
    {
    },
    runtimeline =
    {
        TimeEvent(FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(8*FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(15*FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(23*FRAMES, function(inst) PlayFootstep(inst) end),
    },
    endtimeline =
    {
        TimeEvent(FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(2*FRAMES, function(inst) PlayFootstep(inst) end),
    },
}
, nil, nil, true)

CommonStates.AddRunStates(states,
{
    starttimeline =
    {
        TimeEvent(FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(8*FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(15*FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(23*FRAMES, function(inst) PlayFootstep(inst) end),
    },
    runtimeline =
    {
        TimeEvent(FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(8*FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(15*FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(23*FRAMES, function(inst) PlayFootstep(inst) end),
    },
    endtimeline =
    {
        TimeEvent(FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(8*FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(15*FRAMES, function(inst) PlayFootstep(inst) end),
        TimeEvent(23*FRAMES, function(inst) PlayFootstep(inst) end),
    },
})

local TEMP_TRIGGER = 50

CommonStates.AddSleepStates(states,
{
    starttimeline =
    {
        TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.stretch) end)
    },

    sleeptimeline =
    {
		TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.sleep_loop) end),
        TimeEvent(15*FRAMES, function(inst) 
			PlayablePets.SleepHeal(inst)
		end),
		TimeEvent(32*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.sleep_loop) end),
    },
	
	waketimeline =
    {
        TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.stretch) end),
    },
},
{
	onsleep = function(inst)
		if inst.components.temperature.current < TEMP_TRIGGER and inst._is_ripe then
            inst.sg:GoToState("do_unripen")
		end
	end,

	onwake = function(inst)
		if inst.components.temperature.current >= TEMP_TRIGGER and not inst._is_ripe then
            inst.sg:GoToState("do_ripen")
		end
	end,
})
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "challenge_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
			TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/grass_gekko/body_fall") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.challenge_win) 
            end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "challenge_win"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "challenge_win")
CommonStates.AddHopStates(states, false, {pre = "run_pre", loop = "run_loop", pst = "run_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle_loop",
	plank_idle_loop = "idle_loop",
	plank_idle_pst = "idle_loop",
	
	plank_hop_pre = "run_pre",
	plank_hop = "run_loop",
	
	steer_pre = "walk_pst",
	steer_idle = "idle_loop",
	steer_turning = "challenge_win",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "run_pre",
	leap_loop = "run_loop",
	leap_pst = "run_pst",
	
	castspelltime = 10,
})
    
return StateGraph("fruitdragonp", states, events, "idle", actionhandlers)

