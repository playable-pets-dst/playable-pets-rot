require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.CAST_NET, longaction),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	PP_CommonHandlers.OnAttacked(),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),

	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{
   State{
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst, pushanim)            
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle_loop", true)
            inst.sg:SetTimeout(1 + 2*math.random())            
        end,

        ontimeout = function(inst)                         
            local rand = math.random()
            if rand < .5 then
                inst.sg:GoToState("bellow")
            else
                inst.sg:GoToState("shake")
            end
        end,
    },

    State{
        name = "shake",
        tags = {"canrotate"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("shake")
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/shake")

        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "bellow",
        tags = {"canrotate"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("bellow")
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/alert")
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name="graze",
        tags = {"idle", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("graze_loop", true)
            inst.sg:SetTimeout(5+math.random()*5)
            -- inst.SoundEmitter:PlaySound("dontstarve/creatures/koalefant/chew")
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end,
    },

    State{
        name = "alert",
        tags = {"idle", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/alert")
            inst.AnimState:PlayAnimation("alert_pre")
            inst.AnimState:PushAnimation("alert_idle", true)
        end,
    },

    State{
        name = "attack",
        tags = {"attack", "busy"},

        onenter = function(inst)
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/attack")
            inst.components.combat:StartAttack()
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("atk_pre")
            inst.AnimState:PushAnimation("atk", false)
        end,


        timeline=
        {
            TimeEvent(15*FRAMES, function(inst) PlayablePets.DoWork(inst, 4) end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/death")
            inst.AnimState:PlayAnimation("death")
            inst.components.locomotor:StopMoving()
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		events = {
			EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
		}

    },

    State{
        name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst)
			if inst.shed_ready and not inst.components.timer:TimerExists("shed") then
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("shake")
				inst.components.locomotor:StopMoving()
			else
				inst.sg:GoToState("idle")
			end
        end,

        timeline=
        {
            
            TimeEvent(4*FRAMES, function(inst)inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/shake") end),

            TimeEvent(8*FRAMES, function(inst) 
                inst.shed_ready = nil

                if inst.components.timer:TimerExists("shed") then
                    inst.components.timer:StopTimer("shed")
                end

                inst.components.timer:StartTimer("shed", TUNING.GRASSGATOR_SHEDTIME_SET + (math.random()* TUNING.GRASSGATOR_SHEDTIME_VAR))                
                inst.components.lootdropper:SpawnLootPrefab("twigs") 
            end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    }, 

	State{
        name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst)
			PlayablePets.ToggleWalk(inst)
			inst.AnimState:PlayAnimation("run_pst")
        end,

        timeline=
        {
            
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },  

    State{
        name = "fall_pre",
        tags = { "busy" },
        onenter = function(inst)   
            inst.sg:SetTimeout(2)
            inst:Hide()
        end,

        ontimeout = function(inst)            
            inst.sg:GoToState("fall")
        end,

        onexit = function(inst)
            inst:Show()
        end,
    }, 

    State{
        name = "fall",
        tags = { "busy" },
        onenter = function(inst)   
            --inst.AnimState:SetBank("grass_gator_water")         
            --ChangeToCharacterPhysics(inst)
            inst.Physics:SetDamping(0)
            inst.Physics:SetMotorVel(0, math.random() * 10 - 20, 0)
            inst.AnimState:PlayAnimation("fall_loop", true)
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/fall")
        end,

        onupdate = function(inst)
            local x, y, z = inst.Transform:GetWorldPosition()
            if not inst.shadow then
                inst.shadow = SpawnPrefab("warningshadow")
                inst.shadow:ListenForEvent("onremove", OnRemoveDebris, inst)
                inst.shadow.Transform:SetPosition(x, 0, z)
            end
            if inst.UpdateShadowSize then
            inst.UpdateShadowSize(inst.shadow, 35-y)
            end

            if y < 2 then
                inst.Physics:SetMotorVel(0, 0, 0)
                if y <= .1 then
                    inst.Physics:Stop()
                    inst.Physics:SetDamping(5)
                    inst.Physics:Teleport(x, 0, z)
                    inst.sg:GoToState("land")                    
                end
            end
        end,

        onexit = function(inst)
            local x, y, z = inst.Transform:GetWorldPosition()
            inst.Transform:SetPosition(x, 0, z)
            if inst.shadow then inst.shadow:Remove() end
        end,
    },  

    State{
        name = "land",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("fall_pst")
            inst.components.locomotor:StopMoving()
        end,

        timeline=
        {
            TimeEvent(1*FRAMES, function(inst) 
               spawnwaves(inst, 6, 360, 4, nil, nil, 2, nil, true)
            end),
            TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/large") end),
            
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "dive",
        tags = {"canrotate","busy","diving"},

        onenter = function(inst, pushanim)                
            inst.movetoshallow = nil
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("dive")
            
        end,
        
        timeline=
        {
            TimeEvent(1*FRAMES, function(inst) 
                inst.DynamicShadow:Enable(false)
            end),

            TimeEvent(21*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/common/together/water/submerge/medium") end),

            TimeEvent(27*FRAMES, function(inst) 
                spawnwaves(inst, 6, 360, 4, nil, nil, 2, nil, true)
            end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("dive_loop") end),
        },

        onexit = function(inst)
           inst.DynamicShadow:Enable(true)
        end,
    },

    State{
        name = "dive_loop",
        tags = {"canrotate","noattack","busy","diving"},

        onenter = function(inst, pushanim)   
            inst.DynamicShadow:Enable(false)  
        -- TURN OFF PHYSICS?   
       -- inst.Physics:ClearCollisionMask()
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle_loop", true)
            inst:Hide()
            inst.sg:SetTimeout(2 + 2*math.random())
        end,

        onupdate = function(inst)            
            if inst.surfacetime then
                if not inst.surfacelocation then
                    if not inst.searchrange then 
                        inst.searchrange = 15 + (math.random()*5)
                    end
                    inst.surfacelocation = inst.findnewshallowlocation(inst, inst.searchrange)
                    inst.searchrange = inst.searchrange + 8
                end
                if inst.surfacelocation then
                    inst.surfacetime = nil
                    inst.searchrange = nil              
                    inst.Transform:SetPosition(inst.surfacelocation.x, 0, inst.surfacelocation.z)
                    inst.surfacelocation = nil
                    inst.sg:GoToState("surface")
                end
            end
        end,

        ontimeout = function(inst)            
            inst.surfacetime = true
        end,

        onexit = function(inst)
            inst.DynamicShadow:Enable(true)
          
           -- TURN ON PHYSICS
           --MakeCharacterPhysics(inst, 100, .75)

            inst:Show()
        end,
    },

    State{
        name = "surface",
        tags = {"canrotate","busy", "diving"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("emerge")
        end,

        timeline=
        {
            TimeEvent(4*FRAMES, function(inst) 
                spawnwaves(inst, 6, 360, 4, nil, nil, 2, nil, true)
            end),
            TimeEvent(5*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("turnoftides/common/together/water/emerge/medium") 
            end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}

CommonStates.AddWalkStates(
    states,
    {
        walktimeline =
        {
           
            TimeEvent(11*FRAMES, function(inst)
            if inst:HasTag("swimming") then
                inst.SoundEmitter:PlaySound("turnoftides/common/together/water/swim/walk_water_med",nil,.25)
            end
        end),
           TimeEvent(12*FRAMES, function(inst)
                if not inst:HasTag("swimming") then
                    PlayFootstep(inst)
                end
            end),
           TimeEvent(23*FRAMES, function(inst)
                if not inst:HasTag("swimming") then
                    PlayFootstep(inst)
                end
            end),
           TimeEvent(36*FRAMES, function(inst)
                if not inst:HasTag("swimming") then
                   PlayFootstep(inst)
                end
            end),
            TimeEvent(38*FRAMES, function(inst)
                if inst:HasTag("swimming") then
                    inst.SoundEmitter:PlaySound("turnoftides/common/together/water/swim/walk_water_med",nil,.25)
                    end
            end),
           TimeEvent(43*FRAMES, function(inst)
                if not inst:HasTag("swimming") then
                    PlayFootstep(inst)
                end
            end),
        }
    })

CommonStates.AddRunStates(
    states,
    {
        runtimeline =
        {
        TimeEvent(0, function(inst)
            if inst:HasTag("swimming") then
                inst.SoundEmitter:PlaySound("turnoftides/common/together/water/swim/run_water_med")
            else   
            end
        end),
        TimeEvent(1*FRAMES, function(inst)
                if not inst:HasTag("swimming") then
                    PlayFootstep(inst)
                end
        end),
        TimeEvent(2*FRAMES, function(inst)
                if not inst:HasTag("swimming") then
                    PlayFootstep(inst)
                end
        end),
        TimeEvent(7*FRAMES, function(inst)
                if not inst:HasTag("swimming") then
                    PlayFootstep(inst)
                end
        end),
        TimeEvent(8*FRAMES, function(inst)
                if not inst:HasTag("swimming") then
                    PlayFootstep(inst)
                end
        end),
        }
    })

CommonStates.AddSimpleState(states,"hit", "hit", {"hit", "busy"}, nil, 
    {
        TimeEvent(1*FRAMES, function(inst) 
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/sleep_in") 
        end),
    }
)
CommonStates.AddSleepStates(states,
{

    starttimeline =
    {
        TimeEvent(24*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/sleep_out")
        end),
    },

    sleeptimeline =
    {
        TimeEvent(4*FRAMES, function(inst) 
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/sleep_in") 
        end),
        TimeEvent(49*FRAMES, function(inst) 
			PlayablePets.SleepHeal(inst)
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/sleep_out") 
        end),
    },
})
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "bellow", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/death") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/alert") end)
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "bellow"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddHomeState(states, nil, "eat", "idle_loop", true)
CommonStates.AddHopStates(states, false, {pre = "jump", loop = "jump_loop", pst = "jump_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "scared_pre",
	plank_idle_loop = "scared_loop",
	plank_idle_pst = "scared_pst",
	
	plank_hop_pre = "jump",
	plank_hop = "jump_loop",
	
	steer_pre = "run_pst",
	steer_idle = "idle",
	steer_turning = "taunt",
	stop_steering = "run_pst",
	
	row = "run_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "run_pre",
	leap_loop = "run_loop",
	leap_pst = "run_pst",
	
	lunge_pre = "run_pre",
	lunge_loop = "run",
	lunge_pst = "run_pst",
	
	superjump_pre = "run_pre",
	superjump_loop = "run",
	superjump_pst = "run_pst",
	
	castspelltime = 10,
})	
CommonStates.AddAmphibiousCreatureHopStates(states,
{ -- config
    swimming_clear_collision_frame = 9 * FRAMES,
},
{ -- anims
},
{ -- timeline
    hop_pre =
    {
        TimeEvent(0, function(inst)
            if inst:HasTag("swimming") then
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())
            end
        end),
    },
    hop_pst = {
        TimeEvent(4 * FRAMES, function(inst)
            if inst:HasTag("swimming") then
                inst.components.locomotor:Stop()
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())
            end
        end),
        TimeEvent(6 * FRAMES, function(inst)
            if not inst:HasTag("swimming") then
                inst.components.locomotor:StopMoving()
            end
        end),
    }
})

	
return StateGraph("grassgatorp", states, events, "idle", actionhandlers)

