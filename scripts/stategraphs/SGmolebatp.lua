require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "squid"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "squid"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "taunt"
local shortaction = "action"
local workaction = "attack"
local otheraction = "eat_start"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local states=
{
    State
     {
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle_sit", true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState(math.random() < .15 and "sniff_idle" or "idle")
                end
            end),
        },
    },
	
	State
    {
        name = "sniff_idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            inst.AnimState:PlayAnimation("idle_smell")

            inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/sniff")
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "taunt",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt")
			inst:PerformBufferedAction()
        end,

        timeline=
        {
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/taunt") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt")
        end,

        timeline=
        {
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/taunt") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "special_atk2",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            inst.AnimState:PlayAnimation("ally_call")
        end,

        timeline =
        {
            TimeEvent(12*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/summon")
            end),
            TimeEvent(28*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/summon")
            end),
            TimeEvent(44*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/summon")
            end),
            TimeEvent(55*FRAMES, function(inst)
                if inst.SummonAlly ~= nil then
                    inst:SummonAlly()
                end
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "eat_start",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            inst.AnimState:PlayAnimation("eat", false)
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
            TimeEvent(3 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/eat")
            end),
            TimeEvent(36 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/eat_swallow")
            end),

        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "attack",
        tags = {"busy", "attack"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
			inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("attack")
        end,

        timeline =
        {
            TimeEvent(1*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/attack")
			end),
			TimeEvent(10*FRAMES, function(inst)
				PlayablePets.DoWork(inst, 2)
			end),
			TimeEvent(21*FRAMES, PlayFootstep ),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "hit",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("hit")
        end,

        timeline =
        {
            TimeEvent(0, function(inst)
				inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/hit")
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "make_molehill",
        tags = { "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            inst.AnimState:PlayAnimation("eat")

            inst.sg:SetTimeout(32*FRAMES)
        end,

        timeline =
        {
            TimeEvent(3 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/eat")
            end),
            TimeEvent(22 * FRAMES, function(inst)
                inst:PerformBufferedAction()

                inst.SoundEmitter:PlaySound("dontstarve/creatures/spat/spit_playerunstuck")
            end),   
        },

        ontimeout = function(inst) inst.sg:GoToState("idle") end,

        onexit = function(inst)
            inst:ClearBufferedAction()
        end,
    },

    State
    {
        name = "break_molehill",
        tags = { "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            inst.AnimState:PlayAnimation("suck_in")
            inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/suck_up")
        end,

        timeline =
        {
            TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/walk") end),
            TimeEvent(12 * FRAMES, function(inst)
                -- Even if something happened to our action target,
                -- we still want to stop trying if we get this far.
                inst._nest_needs_cleaning = false

                local ba = inst:GetBufferedAction()
                if ba ~= nil and ba.target ~= nil and ba.target:IsValid() then
                    ba.target:PushEvent("suckedup")
                end
            end),
            TimeEvent(32*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/bodyfall",nil,.5) end),
            TimeEvent(46*FRAMES, PlayFootstep ),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },

        onexit = function(inst)
            inst:ClearBufferedAction()
        end,
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
			inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/death")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,
		
		timeline = {
			TimeEvent(2*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/yawn")
			end),
            TimeEvent(24*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/bodyfall")
			end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
			PlayablePets.SleepHeal(inst)
		end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/sleep")
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

		timeline = {
		},
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}
CommonStates.AddWalkStates(states,
{
    starttimeline = 
    {
        TimeEvent(0       , function(inst)
            inst.Physics:Stop()
        end),
    },

    walktimeline =
    {
        TimeEvent(0       , function(inst)
            inst.Physics:Stop()
        end),
        TimeEvent(5*FRAMES, function(inst) 
            inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/walk")
            inst.components.locomotor:WalkForward()
        end ),
        TimeEvent(20*FRAMES, function(inst)
            inst.Physics:Stop()
        end ),
    },
    
    endtimeline =
    {
        TimeEvent(1*FRAMES, PlayFootstep ),
    },

}, nil, true)

local moveanim = "walk"
local idleanim = "idle_sit"
local actionanim = "walk_pst"
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	actionanim, nil, nil, "taunt", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/death") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, "taunt")
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim.."_loop", pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim.."_loop",
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim.."_loop",
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim.."_loop",
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim.."_loop",
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})
	
return StateGraph("molebatp", states, events, "idle", actionhandlers)

