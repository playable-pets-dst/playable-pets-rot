require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.CAST_NET, longaction),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	--CommonHandlers.OnHop(),
	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{
    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.AnimState:PlayAnimation("idle", true)
            if not inst.SoundEmitter:PlayingSound("fly_lp") and not inst._isflying then
                inst.SoundEmitter:PlaySound("monkeyisland/pollyroger/flap_lp", "fly_lp")
            end
        end,
    },
    State{
        name = "take",
        tags = {"busy"},
        onenter = function(inst)
            inst.AnimState:PlayAnimation("take")
        end,

        timeline =
        {
            TimeEvent(7 * FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
        },

        events=
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle") 
                end
            end),
        },
    },

    State{
        name = "give",
        tags = {"busy"},
        onenter = function(inst)
            inst.AnimState:PlayAnimation("give")
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
        },

        events=
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },

    State{
        name = "idle_ground",
        tags = {"idle", "canrotate", "ground"},
        onenter = function(inst, pushanim)
            --PlayablePets.LandFlyingCreature(inst)
            inst.SoundEmitter:KillSound("fly_lp")
            inst:RemoveTag("flying")
            if pushanim then
                if type(pushanim) == "string" then
                    inst.AnimState:PlayAnimation(pushanim)
                end
                inst.AnimState:PushAnimation("idle_ground", true)
            elseif not inst.AnimState:IsCurrentAnimation("idle_ground") then
                inst.AnimState:PlayAnimation("idle_ground", true)
            end
        end,

        onexit = function(inst)
            --PlayablePets.RaiseFlyingCreature(inst)
            inst.SoundEmitter:PlaySound("monkeyisland/pollyroger/flap_lp", "fly_lp")
        end,

        events =
        {
            EventHandler("onlocomotor", function(inst)
                inst.sg:GoToState("takeoff_walk")
            end),
        },
    },

    State{
        name = "takeoff_walk",
        tags = {"busy"},
        onenter = function(inst)
            inst.AnimState:PlayAnimation("takeoff_walk")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst.sg.statemem.stayonground = false
            inst:AddTag("flying")
        end,
    },

    State{
        name = "peck",
        tags = {"idle", "canrotate", "ground"},
        onenter = function(inst)
            inst:RemoveTag("flying")
            inst.AnimState:PlayAnimation("peck")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg.statemem.stayonground = true
                    inst.sg:GoToState("idle_ground")
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.stayonground then
                inst:AddTag("flying")
            end
        end,
    },

    State{
        name = "switch",
        tags = {"idle", "canrotate", "ground"},
        onenter = function(inst)
            inst:RemoveTag("flying")
            inst.Transform:SetRotation(inst.Transform:GetRotation() + 180)
            inst.AnimState:PlayAnimation("switch")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg.statemem.stayonground = true
                    inst.sg:GoToState("idle_ground")
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.stayonground then
                inst:AddTag("flying")
            end
        end,
    },

    State{
        name = "emote",
        tags = {"idle", "canrotate", "ground"},
        onenter = function(inst)
            inst:RemoveTag("flying")
            inst.Transform:SetRotation(inst.Transform:GetRotation() + 180)
            inst.AnimState:PlayAnimation("land_walk")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg.statemem.stayonground = true
                    inst.sg:GoToState("idle_ground")
                end
            end),
        },
    },

    State{
        name = "caw",
        tags = { "idle", "ground" },

        onenter = function(inst)
            inst:RemoveTag("flying")
            if not inst.AnimState:IsCurrentAnimation("caw") then
                inst.AnimState:PlayAnimation("caw", true)
            end
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
            inst.SoundEmitter:PlaySound("monkeyisland/pollyroger/caw")
        end,

        ontimeout = function(inst)
            inst.sg.statemem.stayonground = true
            inst.sg:GoToState("idle_ground")
        end,

        onexit = function(inst)
            if not inst.sg.statemem.stayonground then
                inst:AddTag("flying")
            end
        end,
    },

    State{
        name = "hit",
        tags = { "busy", "ground" },

        onenter = function(inst)
            inst:RemoveTag("flying")
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound("monkeyisland/pollyroger/hit")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if not inst.AnimState:AnimDone() then
                    inst.sg.statemem.stayonground = true
                    inst.sg:GoToState("idle_ground")
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.stayonground then
                inst:AddTag("flying")
            end
        end,
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("death")
            inst.SoundEmitter:PlaySound("monkeyisland/pollyroger/death")
            inst.SoundEmitter:KillSound("fly_lp")
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State{
        name = "land",
        tags = {"busy", "flight", "landing"},
        onenter= function(inst)
			if inst.amphibious or not IsOceanTile(TheWorld.Map:GetTileAtPoint(inst:GetPosition():Get())) then
				inst.components.locomotor:StopMoving()
				inst:Show()
				local pt = Point(inst.Transform:GetWorldPosition())
				pt.y = 25
				inst.Physics:Stop()
				inst.components.locomotor:StopMoving()
				inst.Physics:Teleport(pt.x,pt.y,pt.z)
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("glide", true)
				inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
			
				inst:RemoveTag("notarget")
				inst.components.locomotor:StopMoving()
				inst.components.locomotor.disable = true
				inst.Physics:SetMotorVel(0,-20+math.random()*10,0)
				inst.SoundEmitter:PlaySound("dontstarve/birds/flyin", "flyin")
			else
				--print("DEBUG: failed, going to glide state")
				inst.sg:GoToState("glide")
			end
        end,
		
		onexit = function(inst)
			if inst.amphibious or not IsOceanTile(TheWorld.Map:GetTileAtPoint(inst:GetPosition():Get())) then --this should assume that the state failed, thus not setting the mobs to landform internally
				inst._isflying = false
				inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
				inst.components.health:SetInvincible(false)
				inst.components.locomotor.disable = false
                inst.SoundEmitter:PlaySound("monkeyisland/pollyroger/flap_lp", "fly_lp")
			end
		end,	
		
		 timeline = 
        {
            TimeEvent(5, function(inst) --added as a failsafe. Teleports to the ground if takes too long.
				local pt = Point(inst.Transform:GetWorldPosition())
				pt.y = 0
                inst.components.locomotor:StopMoving()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.AnimState:PlayAnimation("land")
	            inst.DynamicShadow:Enable(true)
				inst.components.health:SetInvincible(false)
                inst.sg:GoToState("idle", true)
			end),
        },
        
        onupdate= function(inst)
            local pt = Point(inst.Transform:GetWorldPosition())
            inst.Physics:SetMotorVel(0,-20+math.random()*10,0)
            if pt.y <= 3 then
                pt.y = 0
                inst.components.locomotor:StopMoving()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.AnimState:PlayAnimation("land")
	            inst.DynamicShadow:Enable(true)
				inst.components.health:SetInvincible(false)
                inst.sg:GoToState("idle", true)
            end
        end,
		

        ontimeout = function(inst)
             inst.SoundEmitter:PlaySound("dontstarve/birds/flyin", "flyin")
             --inst.sg:GoToState("idle")
        end,
    },    
	 
	 State{
        name = "special_atk2",
        tags = {"flight", "busy"}, --sliding happens
        onenter = function(inst)
			if not inst.taunt2 then
				inst.sg:GoToState("idle")
			end
			inst.taunt2 = false
            inst.components.locomotor:StopMoving()
            inst.sg:SetTimeout(.1+math.random()*.2)
            inst.sg.statemem.vert = math.random() > .5
			
	        inst.DynamicShadow:Enable(false)
			inst.components.health:SetInvincible(true)
            inst.SoundEmitter:PlaySound("turnoftides/birds/takeoff_puffin")
            inst.AnimState:PlayAnimation("takeoff_vertical_loop")
        end,
        
        ontimeout= function(inst)
             inst.AnimState:PushAnimation("takeoff_vertical_loop", true)
			 inst.components.locomotor.disable = true
             inst.Physics:SetMotorVel(0,15+math.random()*5,0)
        end,
        
        timeline = 
        {
            TimeEvent(1.5, function(inst) 
				inst:Hide()
				inst._isflying = true
				inst.components.locomotor.disable = false
				if TheNet:GetServerGameMode() == "lavaarena" then
					inst:DoTaskInTime(5, function(inst) if inst._isflying == true and not inst.sg:HasStateTag("landing") then inst.sg:GoToState("land") end end)
				end	
				inst.sg:GoToState("glide") 
			end)
        }
        
    },	
		
	State{
        name = "glide",
        tags = {"flight2", "canrotate"},
        onenter= function(inst)
			
			local pt = Point(inst.Transform:GetWorldPosition())
			if pt.y > .1 then
                pt.y = 0
                inst.Physics:Stop()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
				
			end	
			inst._isflying = true
			--inst.Physics:SetMotorVel(8,0,0)
            inst.AnimState:PlayAnimation("idle", true)
        end,
        
        onupdate= function(inst)
        end,
		

        ontimeout = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/birds/flyin", "flyin")
        end,
    },    
		
	 State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)
            --PlayablePets.RaiseFlyingCreature(inst)
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()
            local pt = Vector3(inst.Transform:GetWorldPosition())
            if pt.y > 1 then
                inst.sg:GoToState("fall")
            end
        end,
        
        events=
        {
            EventHandler("animover", function(inst) 
                    inst.sg:GoToState("idle") 
            end ),
        },        
    },    	
	
	 State{
        name = "fall",
        tags = {"busy"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("fall_loop", true)
        end,
        
        onupdate = function(inst)
            local pt = Vector3(inst.Transform:GetWorldPosition())
            if pt.y <= .2 then
                pt.y = 0
                inst.Physics:Stop()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
	            inst.DynamicShadow:Enable(false)
                inst.sg:GoToState("stunned")
            end
        end,
    },    
		
	State{
        name = "stunned",
        tags = {"busy"},
        
        onenter = function(inst) 
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stunned_loop", true)
            inst.sg:SetTimeout(GetRandomWithVariance(6, 2) )
        end,
        
        ontimeout = function(inst) inst.sg:GoToState("idle") end,
    },
	
	State {
        name = "sleep",
        tags = {"busy", "sleeping" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("land_walk")
            inst.AnimState:PushAnimation("sleep_pre", false)
            --PlayablePets.LandFlyingCreature(inst)
            inst.SoundEmitter:KillSound("fly_lp")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = {"busy", "sleeping" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
			PlayablePets.SleepHeal(inst)
            --PlayablePets.LandFlyingCreature(inst)
		end,

		timeline=
        {

        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            --PlayablePets.RaiseFlyingCreature(inst)
            inst.AnimState:PlayAnimation("sleep_pst")
            inst.AnimState:PushAnimation("takeoff_walk", false)
            inst.SoundEmitter:PlaySound("monkeyisland/pollyroger/flap_lp", "fly_lp")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "run_start",
        tags = {"moving", "canrotate", "hopping"},
        
        onenter = function(inst) 
            inst.AnimState:PlayAnimation("walk_pre")
            inst.components.locomotor:RunForward()
        end,
        
        timeline=
        {
            TimeEvent(8*FRAMES, function(inst) 
				if inst._isflying == false then
					inst.Physics:Stop() 
				end
            end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),
        }
    },
        
    State{            
        name = "run",
        tags = {"moving", "canrotate","running"},
        
        onenter = function(inst) 
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_loop")
        end,          
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),        
        },
        timeline=
        {

        },
    },      
    
    State{            
        name = "run_stop",
        tags = {"canrotate"},
        
        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("walk_pst")            
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    },
	
	State{            
        name = "action2",
        tags = {"busy"},
        
        onenter = function(inst) 
			if inst._isflying == true then
				inst.sg:GoToState("idle")
			else
                inst.components.locomotor:StopMoving()
                inst.AnimState:PlayAnimation("peck")    
                inst:PerformBufferedAction()
			end
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    },
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"take", nil, nil, "walk_loop", "take") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{

		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "takeoff_walk"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk_loop", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "walk_pst",
	plank_idle_loop = "idle",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pst",
	plank_hop = "walk_pst",
	
	steer_pre = "walk_pst",
	steer_idle = "idle",
	steer_turning = "idle",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
    
return StateGraph("pollyp", states, events, "idle", actionhandlers)

