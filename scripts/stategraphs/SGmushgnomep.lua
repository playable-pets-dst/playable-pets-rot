require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "squid"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "squid"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, "attack_pre")
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local PI_BY_6 = PI / 6
local ANGLES = {
    PI_BY_6,
    2 * PI_BY_6,
    3 * PI_BY_6,
    4 * PI_BY_6,
    5 * PI_BY_6,
    PI,
    7 * PI_BY_6,
    8 * PI_BY_6,
    9 * PI_BY_6,
    10 * PI_BY_6,
    11 * PI_BY_6,
    2 * PI,
}

local function spawn_spore(position, angle)
    local radius = math.random() + 1.0
    local offset = Vector3(math.cos(angle), 0, -math.sin(angle)) * radius

    local spore = SpawnPrefab("spore_moon")
    spore.Transform:SetPosition((position + offset):Get())
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
	PP_CommonHandlers.AddCommonHandlers(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local states=
{
    State
     {
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle_loop")
        end,
		
		TimeEvent(7*FRAMES, function(inst)
			inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/idle")
		end),

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State
    {
        name = "attack_pre",
        tags = {"attack", "busy"},

        onenter = function(inst, target)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("atk_pre", false)
        end,

        timeline=
        {
            TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/taunt") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("attack")
            end),
        },
    },

    State
    {
        name = "attack",
        tags = {"attack", "busy"},

        onenter = function(inst, target)
            inst.Physics:Stop()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk_loop", true)

            inst.sg:SetTimeout(2 + math.random() * 0.5)

            inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/attack_LP", "spinning")

            inst.sg.statemem.position = inst:GetPosition()
            inst.sg.statemem.angles = shuffleArray(ANGLES)
        end,

        timeline =
        {
            TimeEvent(23*FRAMES, function(inst)
                local coughout = SpawnPrefab("spore_moon_coughout")
                coughout.Transform:SetPosition(inst.sg.statemem.position:Get())
            end),
            TimeEvent(25*FRAMES, function(inst)
                spawn_spore(inst.sg.statemem.position, inst.sg.statemem.angles[1])
            end),
            TimeEvent(26*FRAMES, function(inst)
                spawn_spore(inst.sg.statemem.position, inst.sg.statemem.angles[2])
            end),
            TimeEvent(27*FRAMES, function(inst)
                spawn_spore(inst.sg.statemem.position, inst.sg.statemem.angles[3])
            end),
            TimeEvent(28*FRAMES, function(inst)
                spawn_spore(inst.sg.statemem.position, inst.sg.statemem.angles[4])
            end),
            TimeEvent(29*FRAMES, function(inst)
                spawn_spore(inst.sg.statemem.position, inst.sg.statemem.angles[5])
            end),
            TimeEvent(30*FRAMES, function(inst)
                spawn_spore(inst.sg.statemem.position, inst.sg.statemem.angles[6])
            end),
            TimeEvent(31*FRAMES, function(inst)
                spawn_spore(inst.sg.statemem.position, inst.sg.statemem.angles[7])
            end),
            TimeEvent(32*FRAMES, function(inst)
                spawn_spore(inst.sg.statemem.position, inst.sg.statemem.angles[8])
            end),
            TimeEvent(33*FRAMES, function(inst)
                spawn_spore(inst.sg.statemem.position, inst.sg.statemem.angles[9])
            end),
            TimeEvent(34*FRAMES, function(inst)
                spawn_spore(inst.sg.statemem.position, inst.sg.statemem.angles[10])
            end),
            TimeEvent(35*FRAMES, function(inst)
                spawn_spore(inst.sg.statemem.position, inst.sg.statemem.angles[11])
            end),
            TimeEvent(36*FRAMES, function(inst)
                spawn_spore(inst.sg.statemem.position, inst.sg.statemem.angles[12])
            end),
        },

        ontimeout = function(inst)
            inst.sg:GoToState("attack_pst")
        end,

        onexit = function(inst)
            inst.SoundEmitter:KillSound("spinning")
        end,
    },

    State
    {
        name = "attack_pst",
        tags = {"attack", "busy"},

        onenter = function(inst, target)
            inst.Physics:Stop()

            inst.AnimState:PlayAnimation("atk_pst", false)
        end,

        timeline=
        {
            TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/taunt") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst)
            --inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt")
        end,

        timeline=
        {
            TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/surpise") end),
            TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/surpise") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	State
    {
        name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst)
            --inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("walk_pst")
			if inst.components.periodicspawner.target_time then
				inst.components.periodicspawner:Stop()
			else
				inst.components.periodicspawner:Start()
			end
        end,

        timeline=
        {
            
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	 State
    {
        name = "hit",
        tags = {"hit", "busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/hit")
        end,

        -- timeline =
        -- {
        --     TimeEvent(5*FRAMES, function(inst)
        --         inst.SoundEmitter:PlaySound("dontstarve/creatures/leif/foley")
        --     end),
        -- },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State
    {
        name = "spawn",
        tags = {"waking", "busy", "noattack"},
        
        onenter = function(inst, start_anim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("spawn")
        end,
        
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },

        timeline=
        {
            TimeEvent(16*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/spawn")
            end),
            TimeEvent(24*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/surpise")
                inst.sg:RemoveStateTag("noattack")
            end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
			inst.components.periodicspawner:Stop()
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
			inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/death")
        end,
		
		timeline =
        {
            TimeEvent(26*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/bodyfall")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,
		
		timeline = {

		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
			PlayablePets.SleepHeal(inst)
		end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/sleep_in")
			end),
			TimeEvent(35*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/sleep_out")
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

		timeline = {
			TimeEvent(7*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/surpise")
			end),
		},
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}
CommonStates.AddWalkStates(states,
{
    starttimeline =
    {
        TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end),
        TimeEvent(11*FRAMES, function(inst) inst.components.locomotor:WalkForward() end),
        TimeEvent(17*FRAMES, function(inst) inst.Physics:Stop() end),
    },
    walktimeline =
    { 
        TimeEvent(0*FRAMES, PlayFootstep ),
        TimeEvent(14*FRAMES, PlayFootstep ),
    },
    endtimeline =
    {
        TimeEvent(0*FRAMES, PlayFootstep ),
    },
})

local moveanim = "walk"
local idleanim = "idle_sit"
local actionanim = "walk_pst"
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	actionanim, nil, nil, "taunt", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/death") end),
            TimeEvent(26*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/bodyfall")
            end),
		},
		
		corpse_taunt =
		{
            TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/surpise") end),
            TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/mushgnome/surpise") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, "taunt")
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim.."_loop", pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim.."_loop",
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim.."_loop",
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim.."_loop",
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim.."_loop",
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})
	
return StateGraph("mushgnomep", states, events, "spawn", actionhandlers)

