require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function DoFaceplantShake(inst)
	ShakeAllCameras(CAMERASHAKE.VERTICAL, .7, .03, .15, inst, 30)
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local function ChooseAttack(inst)
	if target ~= nil and target:IsValid() then
		if inst:IsNear(target, 4) then
			inst.sg:GoToState("slap", target)
		else
			inst.sg:GoToState("jump", target)
		end
		return true
	end
    return false
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local AOE_RANGE_PADDING = 3
local AOE_TARGET_MUSTHAVE_TAGS = { "_combat" }
local AOE_TARGET_CANT_TAGS = { "INLIMBO", "flight", "invisible", "notarget", "noattack", "shadowthrall" }

local function DoAOEAttack(inst, dist, radius, heavymult, mult, forcelanded, targets)
	inst.components.combat.ignorehitrange = true
	local target = inst.components.combat.target
	local targethit = false
	local x, y, z = inst.Transform:GetWorldPosition()
	if dist ~= 0 then
		local rot = inst.Transform:GetRotation() * DEGREES
		x = x + dist * math.cos(rot)
		z = z - dist * math.sin(rot)
	end
	for i, v in ipairs(TheSim:FindEntities(x, y, z, radius + AOE_RANGE_PADDING, AOE_TARGET_MUSTHAVE_TAGS, AOE_TARGET_CANT_TAGS)) do
		if v ~= inst and
			not (targets ~= nil and targets[v]) and
			v:IsValid() and not v:IsInLimbo()
			and not (v.components.health ~= nil and v.components.health:IsDead())
			then
			local range = radius + v:GetPhysicsRadius(0)
			local dsq = v:GetDistanceSqToPoint(x, y, z)
			if dsq < range * range and inst.components.combat:CanTarget(v) then
				if target == v then
					targethit = true
				end
				inst.components.combat:DoAttack(v)
				if mult ~= nil then
					local strengthmult = (v.components.inventory ~= nil and v.components.inventory:ArmorHasTag("heavyarmor") or v:HasTag("heavybody")) and heavymult or mult
					v:PushEvent("knockback", { knocker = inst, radius = radius + dist + 3, strengthmult = strengthmult, forcelanded = forcelanded })
				end
				if targets ~= nil then
					targets[v] = true
				end
			end
		end
	end
	inst.components.combat.ignorehitrange = false
	return targethit
end

local function SetShadowScale(inst, scale)
	inst.DynamicShadow:SetSize(2 * scale, scale)
end

local function SetSpawnShadowScale(inst, scale)
	inst.DynamicShadow:SetSize(1.5 * scale, scale)
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local states=
{
    State{
		name = "idle",
		tags = { "idle", "canrotate" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("idle", true)
		end,
	},

	State{
		name = "spawndelay",
		tags = { "busy", "noattack", "temp_invincible", "invisible" },

		onenter = function(inst, delay)
			inst.components.locomotor:Stop()
			inst.DynamicShadow:Enable(false)
			inst.Physics:SetActive(false)
			inst:Hide()
			inst:AddTag("NOCLICK")
			inst.sg:SetTimeout(delay or 0)
		end,

		ontimeout = function(inst)
			inst.sg.statemem.spawning = true
			inst.sg:GoToState("spawn")
		end,

		onexit = function(inst)
			if not inst.sg.statemem.spawning then
				inst.DynamicShadow:Enable(true)
			end
			inst.Physics:SetActive(true)
			inst:Show()
			inst:RemoveTag("NOCLICK")
		end,
	},

	State{
		name = "spawn",
		tags = { "appearing", "busy", "noattack", "temp_invincible" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("appear")
			inst.SoundEmitter:PlaySound("rifts2/thrall_generic/appear_cloth")
			inst.DynamicShadow:Enable(false)
			ToggleOffCharacterCollisions(inst)
			inst.sg.mem.lastattack = GetTime()
		end,

		timeline =
		{
			FrameEvent(7, function(inst)
				SetSpawnShadowScale(inst, .25)
				inst.DynamicShadow:Enable(true)
			end),
			FrameEvent(8, function(inst) SetSpawnShadowScale(inst, .5) end),
			FrameEvent(9, function(inst) SetSpawnShadowScale(inst, .75) end),
			FrameEvent(10, function(inst) SetSpawnShadowScale(inst, 1) end),
			FrameEvent(40, function(inst) SetSpawnShadowScale(inst, .93) end),
			FrameEvent(42, function(inst) SetSpawnShadowScale(inst, .9) end),
			FrameEvent(44, function(inst) SetShadowScale(inst, .9) end),
			FrameEvent(45, function(inst) inst.SoundEmitter:PlaySound("rifts2/thrall_hands/appear") end),
			FrameEvent(46, function(inst)
				inst.sg:RemoveStateTag("temp_invincible")
				inst.sg:RemoveStateTag("noattack")
				inst.sg:RemoveStateTag("appearing")
				SetShadowScale(inst, .95)
			end),
			FrameEvent(47, ToggleOnCharacterCollisions),
			FrameEvent(48, function(inst) SetShadowScale(inst, 1) end),
			FrameEvent(51, function(inst)
				inst.sg:AddStateTag("caninterrupt")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},

		onexit = function(inst)
			SetShadowScale(inst, 1)
			inst.DynamicShadow:Enable(true)
			ToggleOnCharacterCollisions(inst)
		end,
	},
	
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
			inst.SoundEmitter:PlaySound("rifts2/thrall_generic/vocalization_death")
			inst.SoundEmitter:PlaySound("rifts2/thrall_generic/death_cloth")
            inst.Physics:Stop()		
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        timeline =
		{
			FrameEvent(18, function(inst) inst.DynamicShadow:SetSize(1.7, 1) end),
			FrameEvent(19, RemovePhysicsColliders),
			FrameEvent(20, function(inst) SetSpawnShadowScale(inst, 1) end),
			FrameEvent(46, function(inst) SetSpawnShadowScale(inst, .75) end),
			FrameEvent(47, function(inst) inst.SoundEmitter:PlaySound("rifts2/thrall_generic/death_pop") end),
			FrameEvent(48, function(inst) SetSpawnShadowScale(inst, .5) end),
			FrameEvent(50, function(inst) SetSpawnShadowScale(inst, .25) end),
			FrameEvent(51, function(inst) inst.DynamicShadow:Enable(false) end),
			FrameEvent(53, function(inst)
				local pos = inst:GetPosition()
				pos.y = 3
				inst.components.lootdropper:DropLoot(pos)
                inst.components.inventory:DropEverything(true)
			end),
		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },

    State{
		name = "hit",
		tags = { "hit", "busy" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("rifts2/thrall_generic/vocalization_hit")
		end,

		timeline =
		{
			FrameEvent(12, function(inst)
				if inst.sg.statemem.doattack == nil then
					inst.sg:AddStateTag("caninterrupt")
				end
			end),
			FrameEvent(14, function(inst)
				if inst.sg.statemem.doattack ~= nil then
					if ChooseAttack(inst, inst.sg.statemem.doattack) then
						return
					end
					inst.sg.statemem.doattack = nil
				end
				inst.sg:RemoveStateTag("busy")
			end),
		},

		events =
		{
			EventHandler("doattack", function(inst, data)
				if inst.sg:HasStateTag("busy") then
					inst.sg.statemem.doattack = data
					inst.sg:RemoveStateTag("caninterrupt")
					return true
				end
			end),
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},
	},

	State{
		name = "attack",
		tags = { "attack", "busy"},

		onenter = function(inst)
			inst.components.combat:SetRange(0)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pre")
		end,

		events =
		{
			EventHandler("attacked", function(inst)
				return true
			end),
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg.statemem.running = true
					inst.sg:GoToState("attack_loop", 0)
				end
			end),
		},

		onexit = function(inst)
			if not inst.sg.statemem.running then
				inst.components.combat:SetRange(TUNING.SHADOWTHRALL_HANDS_ATTACK_RANGE)
			end
		end,
	},

	State{
		name = "attack_loop",
		tags = { "attack", "busy"},

		onenter = function(inst, data)
			inst.components.combat:SetRange(0)
			inst.Physics:SetMotorVel(inst.components.locomotor.runspeed, 0, 0)
			inst.AnimState:PlayAnimation("run_loop")
			inst.SoundEmitter:PlaySound("rifts2/thrall_generic/vocalization_big")
			if type(data) == "table" then
				inst.SoundEmitter:PlaySound("rifts2/thrall_hands/footstep", nil, .6)
				if data.stop then
					inst.sg.statemem.stop = data.stop
					inst.sg:RemoveStateTag("canrotate")
				else
					inst.sg.statemem.walk = data.walk
				end
				inst.AnimState:Hide("fx")
				inst.Physics:ClearCollidesWith(COLLISION.OBSTACLES)
				inst.Physics:ClearCollidesWith(COLLISION.SMALLOBSTACLES)
			else
				inst.sg.statemem.loops = data
			end
		end,

		timeline =
		{
			FrameEvent(11, function(inst)
				inst.SoundEmitter:PlaySound("rifts2/thrall_hands/footstep")
				inst.sg.statemem.targets = {}
				if DoAOEAttack(inst, 1.5, 1.2, nil, nil, nil, inst.sg.statemem.targets) then
					inst.sg.statemem.targethit = true
				end
			end),
			FrameEvent(12, function(inst)
				if DoAOEAttack(inst, 1.5, 1.2, nil, nil, nil, inst.sg.statemem.targets) then
					inst.sg.statemem.targethit = true
				end
			end),
			FrameEvent(13, function(inst)
				if DoAOEAttack(inst, .5, 1.8, nil, nil, nil, inst.sg.statemem.targets) then
					inst.sg.statemem.targethit = true
				end
			end),
		},

		events =
		{
			EventHandler("attacked", function(inst)
				return true
			end),
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					if inst.sg.statemem.stop then
						inst.sg:GoToState("attack_stop", inst.sg.statemem.targets ~= nil)
					else
						local loops = (inst.sg.statemem.loops or 0) + 1
						if loops < 4 then
							inst.sg.statemem.running = true
							inst.sg:GoToState("attack_loop", loops)
						else
							inst.sg:GoToState("attack_stop", inst.sg.statemem.targets ~= nil)
						end
					end
				end
			end),
		},

		onexit = function(inst)
			inst.AnimState:Show("fx")
			if not inst.sg.statemem.running then
				inst.components.combat:SetRange(TUNING.SHADOWTHRALL_HANDS_ATTACK_RANGE)
				inst.Physics:CollidesWith(COLLISION.OBSTACLES)
				inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
			end
		end,
	},

	State{
		name = "attack_stop",
		tags = { "busy" },

		onenter = function(inst, attacking)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
			if not attacking then
				inst.AnimState:Hide("fx")
			end
		end,

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},

		onexit = function(inst)
			inst.AnimState:Show("fx")
		end,
	},
}
CommonStates.AddWalkStates(states,
{
	starttimeline =
	{
		FrameEvent(0, function(inst)
			inst.sg.mem.lastfootstep = GetTime()
			inst.SoundEmitter:PlaySound("rifts2/thrall_hands/footstep", nil, .4)
		end),
	},
	walktimeline =
	{
		FrameEvent(0, function(inst)
			inst.SoundEmitter:PlaySound("rifts2/thrall_generic/vocalization_big")
		end),
		FrameEvent(8, function(inst)
			inst.sg.mem.lastfootstep = GetTime()
			inst.SoundEmitter:PlaySound("rifts2/thrall_hands/footstep")
		end),
	},
	endtimeline =
	{
		FrameEvent(0, function(inst)
			if inst.sg.mem.lastfootstep and inst.sg.mem.lastfootstep + 0.2 <= GetTime() then
				inst.SoundEmitter:PlaySound("rifts2/thrall_hands/footstep", nil, .6)
			end
		end),
	},
})
PP_CommonStates.AddHomeState(states, nil, "walk_pst", "walk_pst", true)
PP_CommonStates.AddKnockbackState(states, nil, "hit", nil, {onenter = function(inst)  end}) --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "die")) end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "scream")) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "appear"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk_loop", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle",
	plank_idle_loop = "walk_loop",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk_loop",
	
	steer_pre = "walk_pst",
	steer_idle = "idle",
	steer_turning = "walk_pst",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_pst",
	
	leap_pre = "walk_pre",
	leap_loop = "walk_loop",
	leap_pst = "walk_pst",
	
	castspelltime = 10,
})
    
return StateGraph("shadowthrall_handsp", states, events, "idle", actionhandlers)

