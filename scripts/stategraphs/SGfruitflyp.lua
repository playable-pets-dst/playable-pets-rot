require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "fruitfly"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "fruitfly"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		return inst:HasTag("friendlyfruitfly") and "idle" or "attack"
	end),
	ActionHandler(ACTIONS.REVIVE_CORPSE, function(inst)
		return inst:HasTag("friendlyfruitfly") and "revivecorpse" or "idle"
	end)
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function StartFlap(inst)
	if not inst.SoundEmitter:PlayingSound("flap") then
		inst.SoundEmitter:PlaySound(inst.sounds.flap, "flap")
	end
end

local function StopFlap(inst)
	if inst.SoundEmitter:PlayingSound("flap") then
		inst.SoundEmitter:KillSound("flap")
	end
end

local function SpawnFruitFly(inst)
    local hounded = TheWorld.components.hounded
    if hounded ~= nil then
        local num = inst:NumFruitFliesToSpawn()
        local pt = inst:GetPosition()
		for i = 1, num do
			local x, z = pt.x + GetRandomWithVariance(0, 10), pt.z + GetRandomWithVariance(0, 10)
			local fruitfly = SpawnPrefab("fruitfly")
			fruitfly.Transform:SetPosition(x, 20, z)
			fruitfly.sg:GoToState("land")
            if fruitfly ~= nil and fruitfly.components.follower ~= nil then
                fruitfly.components.follower:SetLeader(inst)
            end
        end
    end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	--CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local states=
{
    State{
		name = "idle",
		tags = {"idle"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle")
			StartFlap(inst)
		end,

		events = 
		{
			EventHandler("animover", function(inst)
				if math.random() < 0.05 then
					inst.sg:GoToState("bored")
				else
					inst.sg:GoToState("idle") 
				end
			end)
		},
    },
	State{
		name = "bored",
		tags = {"idle"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("spin")
			StartFlap(inst)
		end,

		events = 
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end)
		},
    },
    State{
        name = "plant_dance", --friendlyfruitfly only
        tags = {"busy"},

        onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("plant_dance_pre", false)
			inst.AnimState:PushAnimation("plant_dance_loop", true)
			StartFlap(inst)
		end,
	
		timeline =
		{ 
			TimeEvent(77 * FRAMES, function(inst)
                inst:PerformBufferedAction()
				inst.sg:GoToState("plant_dance_pst")
			end),
		},
    },
    State{
        name = "plant_dance_pst", --friendlyfruitfly only
        tags = {"busy"},

        onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("plant_dance_pst", false)
			StartFlap(inst)
        end,

		events = 
		{
            EventHandler("animover", function(inst)
                inst:PerformBufferedAction()
				inst.sg:GoToState("idle")
			end)
		},
    },
    State{
        name = "plant_attack", --fruitfly only
        tags = {"busy"},

        onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("plant_attack_pre", false)
			inst.AnimState:PushAnimation("plant_attack_loop", true)
			StartFlap(inst)
        end,

		timeline =
		{
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spin) end),
			TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.plant_attack) end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.plant_attack) end),
			TimeEvent(37 * FRAMES, function(inst)
				inst:PerformBufferedAction()
				inst.hascausedhavoc = true
				inst.sg:GoToState("plant_attack_pst")
			end),
		},

	},
    State{
        name = "plant_attack_minion", --fruitfly minion only
        tags = {"busy"},

        onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("plant_attack_pre", false)
			inst.AnimState:PushAnimation("plant_attack_loop", true)
			StartFlap(inst)
        end,

		timeline =
		{
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spin) end),
			TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.plant_attack) end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.plant_attack) end),
			TimeEvent(38*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spin) end),
			TimeEvent(42*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.plant_attack) end),
			TimeEvent(46*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.plant_attack) end),
			TimeEvent(63*FRAMES, function(inst)
				inst:PerformBufferedAction()
				inst.hascausedhavoc = true
				inst.sg:GoToState("plant_attack_pst")
			end),
		},

	},
    State{
        name = "plant_attack_pst", --fruitfly only
        tags = {"busy"},

        onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("plant_attack_pst", false)
			StartFlap(inst)
        end,

		events = 
		{
            EventHandler("animover", function(inst)
				inst.sg:GoToState("idle")
			end)
		},
	},
    State{
        name = "land",
        tags = { "flight", "busy" },

        onenter = function(inst)
			inst.AnimState:PlayAnimation("idle", true)
			local sx, sy, sz = inst.Transform:GetScale()
            inst.Physics:SetMotorVelOverride(0, -12/sy, 0)
			StartFlap(inst)
        end,

        onupdate = function(inst)
			local sx, sy, sz = inst.Transform:GetScale()
            inst.Physics:SetMotorVelOverride(0, -12/sy, 0)
            local x, y, z = inst.Transform:GetWorldPosition()
            if y < 0.2 or inst:IsAsleep() then
                inst.Physics:ClearMotorVelOverride()
                inst.Physics:Stop()
                inst.Physics:Teleport(x, 0, z)
                inst.sg:GoToState("idle")
            end
        end,

        onexit = function(inst)
            local x, y, z = inst.Transform:GetWorldPosition()
            if y > 0 then
                inst.Transform:SetPosition(x, 0, z)
            end
            inst.Physics:ClearMotorVelOverride()
        end,
	},
	State{
		name = "buzz",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("plant_dance_pre", false)
			inst.AnimState:PushAnimation("plant_dance_loop", false)
			inst.AnimState:PushAnimation("plant_dance_pst", false)
			StartFlap(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.buzz)
        end,

        timeline =
        {
            TimeEvent(32 * FRAMES, function(inst)
                SpawnFruitFly(inst)
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },

	},
	
	State{
		name = "taunt",
		tags = { "busy" },

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("plant_dance_pre", false)
			inst.AnimState:PushAnimation("plant_dance_loop", false)
			inst.AnimState:PushAnimation("plant_dance_pst", false)
            inst.SoundEmitter:PlaySound(inst.sounds.buzz)
		end,
        
        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	},
	
	State{
		name = "special_atk1",
		tags = {"idle"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("spin")
			StartFlap(inst)
		end,

		events = 
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end)
		},
    },
	
	State{
		name = "special_atk2",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("plant_dance_pre", false)
			inst.AnimState:PushAnimation("plant_dance_loop", false)
			inst.AnimState:PushAnimation("plant_dance_pst", false)
			StartFlap(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.buzz)
        end,

        timeline =
        {
            TimeEvent(32 * FRAMES, function(inst)
				if inst:HasTag("epic") then
					SpawnFruitFly(inst)
				end
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },

	},
	
	State
    {
        name = "attack",
        tags = {"busy", "attack"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
			inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk")
        end,

        timeline =
        {
            TimeEvent(0, function(inst) StartFlap(inst) end),
			TimeEvent(8*FRAMES, function(inst) inst:PerformBufferedAction() end),
			TimeEvent(7*FRAMES, function(inst)	inst.SoundEmitter:PlaySound(inst.sounds.attack) end), --fruitfly only.
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "hit",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("hit")
        end,

        timeline =
        {
            TimeEvent(0, function(inst) StartFlap(inst) end),
			TimeEvent(0, function(inst)	inst.SoundEmitter:PlaySound(inst.sounds.hurt) end)
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

		timeline = {
			TimeEvent(0, function(inst) StartFlap(inst) end),
			TimeEvent(0, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.die) end),
			TimeEvent(10*FRAMES, function(inst) StopFlap(inst) end),
			--TimeEvent(10*FRAMES, LandFlyingCreature),
			TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.die_ground) end)
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,
		
		timeline = {
			TimeEvent(0*FRAMES, function(inst) StartFlap(inst) end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
			PlayablePets.SleepHeal(inst)
			LandFlyingCreature(inst)
		end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) StopFlap(inst) end),
			TimeEvent(35*FRAMES, function(inst) StartFlap(inst) end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.sleep) end)
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
			RaiseFlyingCreature(inst)
        end,

		timeline = {
			TimeEvent(0*FRAMES, function(inst) StartFlap(inst) end),
		},
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}
CommonStates.AddWalkStates(states, 
{	
	starttimeline = {TimeEvent(0, function(inst) StartFlap(inst) end)},
	walktimeline = {TimeEvent(0, function(inst) StartFlap(inst) end)},
	endtimeline = {TimeEvent(0, function(inst) StartFlap(inst) end)},
})

local moveanim = "walk"
local idleanim = "idle"
local actionanim = "walk_pst"
CommonStates.AddFrozenStates(states, LandFlyingCreature, RaiseFlyingCreature)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
			StartFlap(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	actionanim, nil, nil, "idle", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0, function(inst) StartFlap(inst) end),
			TimeEvent(0, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.die) end),
			TimeEvent(10*FRAMES, function(inst) StopFlap(inst) end),
			--TimeEvent(10*FRAMES, LandFlyingCreature),
			TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.die_ground) end)
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) StartFlap(inst) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "takeoff"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, "idle")
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim.."_loop", pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim.."_loop",
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim.."_loop",
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim.."_loop",
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim.."_loop",
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})
	
return StateGraph("fruitflyp", states, events, "idle", actionhandlers)

