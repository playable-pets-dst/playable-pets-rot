require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetTauntCD(inst)
	if TheNet:GetServerGameMode() == "lavaarena" then
		return PPROT_FORGE.CRABKING.FREEZE_CD
	else
		return 8
	end
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "crabking"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "crabking"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local function heal(inst)
    inst.components.health:DoDelta(TUNING.CRABKING_REGEN + math.floor(inst.countgems(inst).orange/2) * TUNING.CRABKING_REGEN_BUFF )

--[[
    local shinefx = SpawnPrefab("crab_king_shine_orange")
    shinefx.entity:AddFollower()
    shinefx.Follower:FollowSymbol(inst.GUID, "rays_placeholder", 0, 0, 0) 
    inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/repair")
]]
end

local function testforlostrock(inst, rightarm)
    if not inst.fixhits then
        inst.fixhits = 0
    end

    if inst.fixhits == math.floor(inst.countgems(inst).orange/3) then
        inst.sg:GoToState("fix_lostrock", rightarm)
        inst.fixhits = 0
    else
        inst.fixhits = inst.fixhits + 1
    end
end

local function spawnwaves(inst, numWaves, totalAngle, waveSpeed, wavePrefab, initialOffset, idleTime, instantActivate, random_angle)
    SpawnAttackWaves(
        inst:GetPosition(),
        (not random_angle and inst.Transform:GetRotation()) or nil,
        initialOffset or (inst.Physics and inst.Physics:GetRadius()) or nil,
        numWaves,
        totalAngle,
        waveSpeed,
        wavePrefab,
        idleTime,
        instantActivate
    )
end

local function throwchunk(inst,prefab)
    local pos = Vector3(inst.Transform:GetWorldPosition())
    local chunk = inst.spawnchunk(inst,prefab,pos)
    chunk.Physics:SetMotorVel(math.random(12,25), math.random(0,10), 0)
end

local function spawnwave(inst, time)
    spawnwaves(inst, 12, 360, 3, nil, 0, 0, nil, true)  --2 1
end

local function GetTransitionState(inst)
    if inst.wantstosummonclaws then
        return "spawnclaws"
    elseif inst.wantstosummonseatacks then
        return "spawnstacks"
    elseif inst.wantstoheal then
        return "fix_pre"
    elseif inst.wantstocast then
        return "cast_pre"
    end              
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		return "cast_pre"
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	--CommonHandlers.OnHop(),
	--[[
	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),]]
	---
	PP_CommonHandlers.OpenGift(),
	--PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

 local states=
{
   State{
        name = "idle",
        tags = { "idle", "canrotate" },

        onenter = function(inst)       
            inst.AnimState:PlayAnimation("idle")
        end,

        timeline=
        {
            TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/vocal",nil,.25) end),
            TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/vocal",nil,.25) end),

        },

        events =
        {
            EventHandler("attacked", function(inst)
                inst.sg:GoToState("hit_light")
            end),
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	 --------------------------------------------------------
    -- INERT states
    --------------------------------------------------------

    State{
        name = "inert",
        tags = { "inert", "canrotate", "noattack"},

        onenter = function(inst, pushanim)
            inst.AnimState:PlayAnimation("inert")
        end,
        
        timeline=
        {
            TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end),
            TimeEvent(21*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end),
            TimeEvent(32*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end),
            TimeEvent(34*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end),
            TimeEvent(41*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end),
            TimeEvent(51*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end), 
        },
        
        events =
        {
            EventHandler("animover", function(inst)
                if math.random() < 0.90 then
                    inst.sg:GoToState("inert")
                else
                    inst.sg:GoToState("inert_blink")
                end
            end),
        },        
    },    

    State{
        name = "reappear",
        tags = { "inert", "canrotate", "noattack","busy"},

        onenter = function(inst, pushanim)
            inst.AnimState:PlayAnimation("reappear")
        end,
        
        timeline=
        {
            TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/common/together/water/submerge/large") end),
        },
        
        events =
        {
            EventHandler("animover", function(inst)
                if math.random() < 0.90 then
                    inst.sg:GoToState("inert")
                else
                    inst.sg:GoToState("inert_blink")
                end
            end),
        },        
    },  

    State{
        name = "inert_blink",
        tags = { "sleeping", "busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("inert_blink")
        end,
        
        timeline=
        {
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/inert_growl") end),
            TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/idle") end),
            TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/idle") end),   
			TimeEvent(17*FRAMES, function(inst) PlayablePets.SleepHeal(inst) end),
            TimeEvent(26*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end),
            TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end), 
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("sleeping")
            end),
        },
    }, 

    State{
        name = "inert_pst",
        tags = { "inert" },

        onenter = function(inst, pushanim)
         --   inst.AnimState:PlayAnimation("red_fx")
          --  inst.AnimState:PushAnimation("inert_pst",false)
            inst.AnimState:PlayAnimation("inert_pst")
            inst.gemshine(inst,"red")
        end,
        
        timeline=
        {
            TimeEvent((15)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/inert_hide") end),
            TimeEvent((25)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/vocal") end),
            TimeEvent((27)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/common/together/water/submerge/large") end),
            TimeEvent((28)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/common/together/water/submerge/large") end),
        },


        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State{
        name = "disappear",
        tags = { "idle", "canrotate", "noattack", "inert"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("disappear")
            inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/disappear")
        end,
        
        events =
        {
            EventHandler("animover", function(inst)
                inst.Physics:SetActive(false)
                inst:Hide()
            end),
        },
    }, 

    State{
        name = "reappear",
        tags = { "idle", "canrotate", "noattack", "inert"},

        onenter = function(inst)
            inst.Physics:SetActive(true)
            inst:Show()
            inst.AnimState:PlayAnimation("reappear")
            inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/appear")
        end,
        
        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("inert")
            end),
        },
    },

    State{
        name = "socket_npc",
        tags = { "idle", "canrotate", "noattack", "inert"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("gem_insert")
        end,
        
        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("inert")
            end),
        },
    },     
	
	--------------------------------------------------------
    -- CAST attack
    --------------------------------------------------------

    State{
        name = "cast_pre",
        tags = { "busy", "canrotate", "casting"},

        onenter = function(inst)
            inst.wantstocast = nil
            if inst.dofreezecast then
                inst.AnimState:PlayAnimation("cast_blue_pre")
            else
                inst.AnimState:PlayAnimation("cast_purple_pre")
            end
            inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/cast_pre")
        end,
        
        timeline=
        {
            TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/together/electricity/light") end),
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/together/electricity/light") end),
            TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/spell") end),
            TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/cast_pre") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("cast_loop")
            end),
        },
    },
    
	State{
        name = "cast_loop",
        tags = { "busy", "canrotate", "casting"},

        onenter = function(inst)
            if not inst.components.timer:TimerExists("casting_timer") then                
                inst.startcastspell(inst,inst.dofreezecast)
				--print(TUNING.CRABKING_CAST_TIME_FREEZE - math.floor(inst.countgems(inst).yellow/2))
                if inst.dofreezecast then
                    inst.isfreezecast = true
                    inst.components.timer:StartTimer("casting_timer",TUNING.CRABKING_CAST_TIME_FREEZE - math.floor(inst.countgems(inst).yellow/2))
                else                    
                    inst.components.timer:StartTimer("casting_timer",TUNING.CRABKING_CAST_TIME - math.floor(inst.countgems(inst).yellow/2))                    
                end
                inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/magic_LP","crabmagic")
            end

            if inst.isfreezecast then
                inst.AnimState:PlayAnimation("cast_blue_loop")
            else
                inst.AnimState:PlayAnimation("cast_purple_loop")
            end

            inst.dofreezecast = nil

            inst.SoundEmitter:SetParameter("crabmagic", "intensity", 0)
        end,

        onupdate = function(inst)
            if inst.components.timer:TimerExists("casting_timer") then
                local totaltime = TUNING.CRABKING_CAST_TIME - math.floor(inst.countgems(inst).yellow/2)
                if inst.isfreezecast then
                    totaltime = TUNING.CRABKING_CAST_TIME_FREEZE - math.floor(inst.countgems(inst).yellow/2)
                end
                local intensity = 1- inst.components.timer:GetTimeLeft("casting_timer")/totaltime                
                inst.SoundEmitter:SetParameter("crabmagic", "intensity", intensity)
            end
            if not inst.components.timer:TimerExists("gem_shine") then
                inst.gemshine(inst,"yellow")
                inst.components.timer:StartTimer("gem_shine",1.5)
            end
        end,

        timeline=
        {       
            TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/together/electricity/light",nil,.25) end),
            TimeEvent(5*FRAMES, function(inst) if math.random() < 0.5 then inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/cast_pre") end end),
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/chatter")  end),
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/together/electricity/light",nil,.25) end),
            TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/together/electricity/light",nil,.25) end),
            TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/together/electricity/light",nil,.25) end),
            TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/together/electricity/light",nil,.25) end),
            TimeEvent(23*FRAMES, function(inst) if math.random() < 0.5 then inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/cast_pre") end end),
            TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/chatter")  end),
            TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/together/electricity/light",nil,.25) end),
            -- TimeEvent(26*FRAMES, function(inst) inst.SoundEmitter:KillSound("crabmagic") end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.keepcast then

                inst:DoTaskInTime(0,function()
                    inst.endcastspell(inst, inst.isfreezecast)
                end)
              
                inst.SoundEmitter:KillSound("crabmagic")
                inst.isfreezecast = nil
            end
        end,
        
        events =
        {
            EventHandler("animover", function(inst)
                inst.sg.statemem.keepcast = true
                inst.sg:GoToState("cast_loop")
            end),
            EventHandler("timerdone", function(inst,data)
                if data.name == "casting_timer" then
                    inst.sg:GoToState("cast_pst", inst.isfreezecast)
                end
            end),
        },
    },   

    State{
        name = "cast_pst",
        tags = { "busy", "canrotate"},

        onenter = function(inst, freezecast)  
            if freezecast then
                inst.AnimState:PlayAnimation("cast_blue_pst")
            else
                inst.AnimState:PlayAnimation("cast_purple_pst")
				inst.components.combat:StartAttack()
            end
        end,
        
        timeline=
        {
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/medium") end),
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/medium") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },   

    --------------------------------------------------------
    -- SPAWN CLAWS
    --------------------------------------------------------

    State{
        name = "special_atk2",
        tags = { "busy", "canrotate", "spawning"},

        onenter = function(inst)
			if inst.arms and #inst.arms > 0 then
				for i, v in ipairs(inst.arms) do
					if v.boat and v.boat.components.debuffable then
						v.boat.components.debuffable:RemoveDebuff("crabkingclaw_hold_debuff")
						v.boat.heldclaws = nil
					end
					v.sg:GoToState("submerge")
				end
				inst.arms = {}
				inst.sg:GoToState("idle")
			elseif inst.cansummonclaws then
				--inst.components.timer:StartTimer("clawsummon_cooldown",TUNING.CRABKING_CLAW_SUMMON_DELAY)
				inst.cansummonclaws = false
				inst:DoTaskInTime(20, function(inst) inst.cansummonclaws = true end)
				inst.AnimState:PlayAnimation("inert_pre")
				inst.AnimState:PushAnimation("inert_pst",false)
			else
				inst.sg:GoToState("idle")
			end
        end,

        timeline = {
            TimeEvent(39*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/inert_hide") end),
            TimeEvent(65*FRAMES, function(inst) 
                inst.spawnarms(inst) 
                inst.gemshine(inst,"green") 
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg.statemem.keepcast = true
                inst.sg:GoToState("idle")
            end),
        },
    },

    --------------------------------------------------------
    -- SPAWN SEASTACKS
    --------------------------------------------------------

    State{
        name = "spawnstacks",
        tags = { "busy", "canrotate", "spawning"},

        onenter = function(inst)        
            inst.components.timer:StartTimer("seastacksummon_cooldown",TUNING.CRABKING_STACK_SUMMON_DELAY)
             
            inst.wantstosummonseatacks = nil
            inst.AnimState:PlayAnimation("inert_pre")
            inst.AnimState:PushAnimation("inert_pst",false)
        end,
        
        timeline = {
            TimeEvent(39*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/inert_hide") end),
            TimeEvent(65*FRAMES, function(inst) inst.spawnstacks(inst) end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },    


    --------------------------------------------------------
    -- HEAL 
    --------------------------------------------------------
	State{
        name = "socket",
        tags = { "busy", "canrotate", "nointerrupt", "inert"},

        onenter = function(inst, gem)
			if gem then
				local symbol = "gems_blue"
				if gem.prefab == "redgem" then
					symbol = "gems_red"
				elseif gem.prefab == "purplegem" then
					symbol = "gems_purple"
				elseif gem.prefab == "orangegem" then
					symbol = "gems_orange"
				elseif gem.prefab == "yellowgem" then
					symbol = "gems_yellow"
				elseif gem.prefab == "greengem" then
					symbol = "gems_green"
				elseif gem.prefab == "opalpreciousgem" then
					symbol = "gems_opal"
				elseif gem.prefab == "hermit_pearl" then
					symbol = "hermit_pearl"
				end
				inst.AnimState:OverrideSymbol("chunks", "crab_king_build", symbol)
				inst.sg.statemem.gem_in_queue = gem
			end
            inst.AnimState:PlayAnimation("fix_pre")
        end,
		
		timeline=
        {   
            TimeEvent(27*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/repair")
				if inst.sg.statemem.gem_in_queue then
					inst.socketitem(inst, inst.sg.statemem.gem_in_queue)
				end
            end),
        },
		
		onexit = function(inst)
			inst.AnimState:OverrideSymbol("chunks", "crab_king_build", "chunks")
		end,
        
        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("socket_pst", "right")
            end),
        },
    },  

	State{
        name = "socket_loop",
        tags = { "busy", "canrotate", "nointerrupt", "inert"},

        onenter = function(inst)
			local arm = math.random() > 0.49 and "right" or "left"
			inst.sg.statemem.arm = arm
            inst.AnimState:PlayAnimation("fix_"..arm.."_loop_"..math.random(1,4))
        end,
		
		timeline=
        {   
            TimeEvent(27*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/repair")
				
            end),
        },
        
        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("socket_pst", inst.sg.statemem.arm)
            end),
        },
    },  
	
	State{
        name = "socket_pst",
        tags = { "busy", "canrotate", "nointerrupt", "inert"},

        onenter = function(inst, arm)
            inst.AnimState:PlayAnimation("fix_"..arm.."_pst")
        end,
		
		timeline=
        {   
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/rock_hit") end),
            TimeEvent(2*FRAMES, function(inst) if math.random() < 0.5 then inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/hit") end end),
        },
        
        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },  
	
    State{
        name = "fix_pre",
        tags = { "canrotate", "fixing"},

        onenter = function(inst)
            inst.sg.statemem.past_interrupt = true
            inst.lastfixloop = nil
            if not inst.components.timer:TimerExists("fix_timer") then
                inst.components.timer:StartTimer("fix_timer",TUNING.CRABKING_FIX_TIME)
            end
            inst.AnimState:PlayAnimation("fix_pre")

            if not inst.components.timer:TimerExists("claw_regen_timer") then
                inst.regenarm(inst)
            end
            inst.gemshine(inst,"orange")
        end,

        timeline=
        {   
            TimeEvent(16*FRAMES, function(inst)
                inst.sg.statemem.past_interrupt = nil
            end),
            TimeEvent(27*FRAMES, function(inst)
                inst.sg.statemem.past_interrupt = true
                inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/repair")
            end),
            TimeEvent(31*FRAMES, function(inst) 
                inst.fixhits = 0
                heal(inst) 
            end),
        },

        events =
        {
            EventHandler("attacked", function(inst)
                if not inst.sg.statemem.past_interrupt then
                    testforlostrock(inst,  not inst.sg.statemem.rightarm)
                end
            end),
            EventHandler("animover", function(inst)
                inst.sg:GoToState("fix_loop")
            end),
        },
    },

    State{
        name = "fix_loop",
        tags = { "canrotate", "fixing", "nointerrupt"},

        onenter = function(inst, rightarm)    
            inst.sg.statemem.past_interrupt = true
            local randomlist = {1,2,3,4}
            if inst.lastfixloop then
                table.remove(randomlist,inst.lastfixloop)
            end
            local randomchoice = randomlist[math.random(1,#randomlist)]
            inst.lastfixloop = randomchoice
            local arm = rightarm and "right" or "left"
            inst.sg.statemem.rightarm = rightarm

            inst.AnimState:PlayAnimation("fix_"..arm.."_loop_"..randomchoice)
            inst.gemshine(inst,"orange")
        end,

        timeline=
        {   
            TimeEvent(4*FRAMES, function(inst) 
                inst.sg:RemoveStateTag("nointerrupt")
                --inst.sg.statemem.past_interrupt = nil
             end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.sg.statemem.rightarm then                    
                    inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/repair")
                    end
             end),
             TimeEvent(16*FRAMES, function(inst)        
                if not inst.sg.statemem.rightarm then                    
                    inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/repair")
                    end
             end),                
            TimeEvent(20*FRAMES, function(inst) inst.sg:AddStateTag("nointerrupt")  end), -- inst.sg.statemem.past_interrupt = true
            TimeEvent(29*FRAMES, function(inst) 
                inst.fixhits = 0 
                heal(inst) 
            end),
        },

        onexit = function(inst)
        
        end,

        events =
        {
            EventHandler("attacked", function(inst)
                if not inst.sg:HasStateTag("nointerrupt") then -- inst.sg.statemem.past_interrupt
                    testforlostrock(inst, inst.sg.statemem.rightarm)
                    --inst.sg:GoToState("fix_lostrock",inst.sg.statemem.rightarm)
                end
            end),
            EventHandler("animover", function(inst)
                if inst.components.health:GetPercent() >= 1 or not inst.wantstoheal then
                    inst.finishfixing(inst)  
                    inst.sg:GoToState("fix_pst", not  inst.sg.statemem.rightarm) 
                else                            
                    
                    inst.sg:GoToState("fix_loop", not inst.sg.statemem.rightarm)
                end
            end),            
        },
    },   

    State{
        name = "fix_lostrock",
        tags = { "canrotate", "fixing"},

        onenter = function(inst, rightarm)
            local arm = "left"
            if rightarm then
                arm = "right"
                inst.sg.statemem.rightarm = rightarm 
            end
            inst.AnimState:PlayAnimation("fix_"..arm.."_lostrock")
            inst.fixhits = 0
        end,
        
        timeline=
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/rock_hit") end),
            TimeEvent(2*FRAMES, function(inst) if math.random() < 0.5 then inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/hit") end end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.components.health:GetPercent() >= 1 or not inst.wantstoheal then
                    inst.finishfixing(inst)  
                    inst.sg:GoToState("fix_pst", not  inst.sg.statemem.rightarm) 
                else                            
                    
                    inst.sg:GoToState("fix_loop", not inst.sg.statemem.rightarm)
                end
            end),
        },
    }, 

    State{
        name = "fix_pst",
        tags = { "canrotate"},

        onenter = function(inst, rightarm)

            local arm = "left"
            if rightarm then            
                arm = "right"            
                inst.sg.statemem.rightarm = rightarm
            end
            inst.AnimState:PlayAnimation("fix_"..arm.."_pst")
            inst.fixhits = 0
        end,
        
        timeline=
        {
            
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },  

    State{
        name = "hit_light",
        tags = { "hit", "busy" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end

            inst.AnimState:PlayAnimation("hit_light")
        end,

        timeline=
        {
            TimeEvent(0 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/hit") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.dofreezecast = true
			inst.sg:GoToState("cast_pre")
        end,

		timeline=
        {
			   
        },
		
		onexit = function(inst)
			inst.taunt = false
			inst:DoTaskInTime(GetTauntCD(inst), function(inst) inst.taunt = true end)			
		end,

        events=
        {
			--EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death2")
            inst.Physics:Stop()
			if inst.arms and #inst.arms > 0 then
				for i, v in ipairs(inst.arms) do
					if v.boat and v.boat.components.debuffable then
						v.boat.components.debuffable:RemoveDebuff("crabkingclaw_hold_debuff")
						v.boat.heldclaws = nil
					end
					v.components.lootdropper:SetLoot({})
					v.components.health:Kill()
				end
			end
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline ={
			TimeEvent(0 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/death2") end),
			TimeEvent(0 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/hit",nil,.5) end),
			TimeEvent(5 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/hit") end),
			TimeEvent(26 * FRAMES, function(inst) 
                inst.dropgems(inst) 
            end),
			TimeEvent(28 * FRAMES, function(inst) 
                throwchunk(inst,"crabking_chip_high")
                throwchunk(inst,"crabking_chip_high")
                throwchunk(inst,"crabking_chip_med")
                throwchunk(inst,"crabking_chip_med")
                throwchunk(inst,"crabking_chip_low")
                throwchunk(inst,"crabking_chip_low")
                throwchunk(inst,"crabking_chip_high")
                throwchunk(inst,"crabking_chip_high")
                throwchunk(inst,"crabking_chip_med")
                throwchunk(inst,"crabking_chip_med")
                throwchunk(inst,"crabking_chip_low")
                throwchunk(inst,"crabking_chip_low")
            end),
			TimeEvent(75 * FRAMES, function(inst) spawnwave(inst) end),

		},
	
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("inert_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.dropgems(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("inert")
		end,
			
		onexit = function(inst)

		end,
		
		timeline=
        {
            TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end),
            TimeEvent(21*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end),
			TimeEvent(17*FRAMES, function(inst)
				PlayablePets.SleepHeal(inst)
			end),
            TimeEvent(32*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end),
            TimeEvent(34*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end),
            TimeEvent(41*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end),
            TimeEvent(51*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble") end), 
        },
        events =
        {
            EventHandler("animover", function(inst) 
				if math.random() > 0.2 then 
					inst.sg:GoToState("sleeping") 
				else 
					inst.sg:GoToState("inert_blink") 
				end 
			end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("inert_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

		timeline = {
			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline/creatures/squid/run") end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline/creatures/squid/run") end),
			TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline/creatures/squid/run") end),
		},
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	-- RUN STATES START HERE
    State{
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)           
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("idle")

            --UpdateRunSpeed(inst)

            --inst.AnimState:SetSortOrder(ANIM_SORT_ORDER_BELOW_GROUND.UNDERWATER)
            --inst.AnimState:SetLayer(LAYER_BELOW_GROUND)            
        end,

        onupdate = function(inst)
            --UpdateRunSpeed(inst)
        end,

        timeline=
        {
            TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/vocal",nil,.25) end),
            TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/vocal",nil,.25) end),

        },     

        onexit = function(inst)           

        end,        

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("run") 
            end),
        },
    },

    State{
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("idle")
        end,  

        timeline=
        {
            TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/vocal",nil,.25) end),
            TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/vocal",nil,.25) end),

        },
		
		events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("run") 
            end),
        },
	},

    State{
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()            
            inst.sg:GoToState("idle")
        end,

        timeline =
        {
		
        },

        events =
        {

        },
    },   
}

CommonStates.AddFrozenStates(states)
--PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"fix_right_pst", nil, nil, "taunt", "fix_right_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/death") end), --TODO: check if this is a valid sound
		},
		
		corpse_taunt =
		{
			TimeEvent(8 * FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.taunt) end),  
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "sleep_pst"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "fix_right_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddHomeState(states, nil, "fix_left_pst", "idle", true)
CommonStates.AddHopStates(states, false, {pre = "idle", loop = "idle", pst = "fix_right_pst"}, nil, "death2")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "fix_right_pst",
	plank_idle_loop = "idle",
	plank_idle_pst = "fix_right_pst",
	
	plank_hop_pre = "idle",
	plank_hop = "fix_right_pst",
	
	steer_pre = "fix_right_pst",
	steer_idle = "idle",
	steer_turning = "idle",
	stop_steering = "fix_right_pst",
	
	row = "fix_right_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "fix_right_pst",
	
	leap_pre = "idle",
	leap_loop = "idle",
	leap_pst = "fix_right_pst",
	
	lunge_pre = "fix_right_pst",
	lunge_loop = "fix_right_pst",
	lunge_pst = "fix_right_pst",
	
	superjump_pre = "catching_pre",
	superjump_loop = "catching_loop",
	superjump_pst = "catching_pst",
	
	castspelltime = 10,
})
    
return StateGraph("crabkingp", states, events, "idle", actionhandlers)

