require("stategraphs/commonstates")
require("stategraphs/ppstates")

local TARGET_CANT_TAGS = { "INLIMBO","ancient_clockwork" }
local TARGET_ONEOF_TAGS = { "character", "monster" }

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "archive_centipede", "ancient_clockwork"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local function doAOEattack(inst)
    local x,y,z = inst.Transform:GetWorldPosition()
    local targets = TheSim:FindEntities(x,y,z, TUNING.ARCHIVE_CENTIPEDE.AOE_RANGE, nil, GetExcludeTags(inst),TARGET_ONEOF_TAGS)
    for i=#targets,1,-1 do
        local target = targets[i]
        if not target.components.health and not target.components.combat then
            table.remove(targets,i)
        end
    end
    inst.components.combat:SetDefaultDamage(TUNING.ARCHIVE_CENTIPEDE.AOE_DAMAGE*3)
    for i,target in ipairs(targets)do
        inst.components.combat:DoAttack(target)
    end
    inst.components.combat:SetDefaultDamage(inst.mob_table.damage)
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.PP_ROLL, "action")
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local states=
{
    State{
        
        name = "idle",
        tags = {"idle", "canrotate","canroll"},
        onenter = function(inst)

            if inst.light_params and  inst._endlight ~= inst.light_params.on then
                inst._endlight = inst.light_params.on
                inst.copyparams(inst._startlight, inst._endlight)
                inst.copyparams(inst._currentlight, inst._endlight)
                inst.pushparams(inst, inst._currentlight)
            end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle", true)
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "taunt",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
			inst:PerformBufferedAction(inst)
            inst.AnimState:PlayAnimation("taunt")
            inst.SoundEmitter:PlaySound("grotto/creatures/centipede/taunt")
        end,
        
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "special_atk2",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            inst.SoundEmitter:PlaySound("grotto/creatures/centipede/taunt")
        end,
        
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "spawn",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("spawn")
            inst.SoundEmitter:PlaySound("grotto/creatures/centipede/spawn")
        end,
        
        timeline = 
        {
            TimeEvent(1*FRAMES, function(inst) 
                inst.copyparams( inst._startlight, inst.light_params.off)
                inst.copyparams( inst._endlight, inst.light_params.on)
                inst.beginfade(inst)
            end ),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },  
    State{  name = "roll_start",
            tags = { "busy", "canrotate", "charge"},
            
            onenter = function(inst, pos)                
                if pos then
                    local x,y,z = pos
                    local angle = inst:GetAngleToPoint(x,y,z)
                    inst.Transform:SetRotation(angle)                    
                end
				inst.SoundEmitter:KillSound("roll")
                inst.Physics:Stop()
                inst.AnimState:PlayAnimation("atk_roll_pre")
            end,
            
            timeline=
            {
                TimeEvent(1*FRAMES,  function(inst) 

                end ),
            },

			onexit = function(inst)
				inst.canroll = false
				inst:DoTaskInTime(20, function(inst) inst.canroll = true end)
			end,
        
            events=
            {
                EventHandler("animover", function(inst) 
                    inst.sg:GoToState("roll") 
                end),
            },
        },

    State{  name = "roll",
            tags = {"busy","charge"},
            
            onenter = function(inst) 
                inst.components.locomotor:Stop()
                inst.components.locomotor:EnableGroundSpeedMultiplier(false)
                inst.Physics:SetMotorVelOverride(15,0,0)
--                inst.components.locomotor:RunForward()
                inst.SoundEmitter:PlaySound("grotto/creatures/centipede/rolling_atk_LP","roll")
                inst.AnimState:PlayAnimation("atk_roll_loop", true)
                inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength()*2)
                --inst.sg:SetTimeout(1)
            end,
            
            timeline=
            {

            },

            onexit = function(inst)
                inst.components.locomotor:Stop()
                inst.components.locomotor:EnableGroundSpeedMultiplier(true)
                inst.Physics:ClearMotorVelOverride()
                inst.SoundEmitter:KillSound("roll")
            end,

            ontimeout = function(inst)
                inst.sg:GoToState("roll_stop")
            end,
        },
    
    State{  name = "roll_stop",
            tags = {"busy","charge"},
            
            onenter = function(inst) 
				inst.SoundEmitter:KillSound("roll")
                inst.components.locomotor:Stop()
                inst.AnimState:PlayAnimation("atk_roll_pst")
            end,

            timeline=
            {
                 TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/centipede/taunt") end),
            },
            
            events=
            {   
                EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
            },
        }, 
    State{  name = "special_atk1",
            tags = {"canrotate", "busy", "atk_pre"},
            
            onenter = function(inst) 
                inst.components.locomotor:Stop()
                inst.AnimState:PlayAnimation("atk_aoe")
                inst.SoundEmitter:PlaySound("grotto/creatures/centipede/aoe")

            end,
			
			onexit = function(inst)
				if not inst.taunt then
					inst:DoTaskInTime(10, function(inst) inst.taunt = true end)
				end
			end,

            timeline=
            {
                TimeEvent(25*FRAMES,  function(inst) 
                    doAOEattack(inst)
					inst.taunt = false
                end),
            },
            
            events=
            {   
                EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
            },
    },
	
	
	State
    {
        name = "attack",
        tags = {"busy", "attack"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
			inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk")
        end,

        timeline =
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/centipede/attack") end),
			TimeEvent(9*FRAMES, function(inst) PlayablePets.DoWork(inst, 3) end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "hit",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("hit")
        end,

        timeline =
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/centipede/hit_react") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline = {
			TimeEvent(0*FRAMES, function(inst) 
				inst.copyparams( inst._endlight, inst.light_params.off)
				inst.beginfade(inst)
			end),

			TimeEvent(0*FRAMES, function (inst) inst.SoundEmitter:PlaySound("grotto/creatures/centipede/death") 
			end), 
        
			TimeEvent(17*FRAMES, function(inst) 
				inst.SoundEmitter:KillSound("active")
			end),
		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					local husk = SpawnPrefab("archive_centipede_huskp")
					local x,y,z = inst.Transform:GetWorldPosition()
					husk.Transform:SetPosition(x,y,z)
					husk.Transform:SetRotation(inst.Transform:GetRotation())
					husk.owner = inst:GetDisplayName()
					husk.components.named:SetName(husk.owner)
					if inst.isshiny ~= 0 then
						husk.AnimState:SetBuild(inst.mob_table.shiny.."_shiny_build_0"..inst.isshiny)
						husk.isshiny = inst.isshiny
					end

					inst.noskeleton = true
				    PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,
		
		timeline = {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/centipede/sleep") end ),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
			PlayablePets.SleepHeal(inst)
		end,
			
		onexit = function(inst)

		end,

		timeline=
        {

        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

		timeline = {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("grotto/creatures/centipede/sleep") end ),
		},
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}
CommonStates.AddWalkStates(states,
{
    starttimeline = 
    {
	    TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
        TimeEvent(1*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/centipede/walk")
            end ),
    },
	walktimeline = {
		    TimeEvent(0*FRAMES, function(inst) inst.components.locomotor:WalkForward() end ),
            TimeEvent(0*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/centipede/walk")
            end ),
            TimeEvent(4*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/centipede/walk")
            end ),
            TimeEvent(8*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/centipede/walk")
            end ),
            TimeEvent(12*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/centipede/walk")
            end ),
            TimeEvent(16*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/centipede/walk")
            end ),
    },
    endtimeline = {
            TimeEvent(4*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/centipede/walk")
            end ),
            TimeEvent(7*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("grotto/creatures/centipede/walk")
            end ),
	},
}, nil,true)

local moveanim = "walk"
local idleanim = "idle"
local actionanim = "walk_pst"
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	actionanim, nil, nil, "taunt", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.copyparams( inst._endlight, inst.light_params.off)
				inst.beginfade(inst)
			end),

			TimeEvent(0*FRAMES, function (inst) inst.SoundEmitter:PlaySound("grotto/creatures/centipede/death") 
			end), 
        
			TimeEvent(17*FRAMES, function(inst) 
				inst.SoundEmitter:KillSound("active")
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(1*FRAMES, function(inst) 
                inst.copyparams( inst._startlight, inst.light_params.off)
                inst.copyparams( inst._endlight, inst.light_params.on)
                inst.beginfade(inst)
            end ),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "spawn"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, "taunt")
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim.."_loop", pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim.."_loop",
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim.."_loop",
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim.."_loop",
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim.."_loop",
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})
	
return StateGraph("archive_centipedep", states, events, "spawn", actionhandlers)

