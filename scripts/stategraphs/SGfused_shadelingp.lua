require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow_aligned"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "shadow_aligned"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "shadow_aligned"}
	end
end

local function OnDisappear(inst)
    inst:AddTag("invisible")
	inst:AddTag("notarget")
	inst.components.health:SetInvincible(true)
	inst.noactions = true
    inst.DynamicShadow:Enable(false)
    inst.MiniMapEntity:SetIcon("")
	inst:RemoveTag("scarytoprey")
    local speed = 2
    inst.components.locomotor:SetExternalSpeedMultiplier(inst, speed, speed)  
end

local function OnAppear(inst)
    inst.specialsleep = true
    inst.noactions = nil
    local speed = 2
    inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, speed)
	inst.components.health:SetInvincible(false)
    inst.DynamicShadow:Enable(true)
	inst:RemoveTag("invisible")
	inst:RemoveTag("notarget")
	inst:AddTag("scarytoprey") 
    inst.MiniMapEntity:SetIcon(inst.mob_table.minimap)
    inst.SoundEmitter:PlaySound(inst.sounds.appear)
end

local function go_to_idle(inst)
    inst.sg:GoToState("idle")
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
    CommonHandlers.OnSink(),
    CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local states=
{
    State{
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("idle")
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst, data)
            inst.AnimState:PlayAnimation("death_pre")
            inst.AnimState:PushAnimation("death", false)
            inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.Physics:Stop()		
			RemovePhysicsColliders(inst)
			inst.components.lootdropper:DropLoot()
            inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end

            inst.sg.statemem.killer = (data and data.afflicter) or nil
        end,

        timeline =
		{
            FrameEvent(12, function(inst)
                inst.persists = false

                local my_position = inst:GetPosition()

                local bomb = SpawnPrefab("fused_shadeling_bomb")
                bomb.Transform:SetPosition(my_position:Get())
                if inst.sg.statemem.killer and inst.sg.statemem.killer:IsValid() then
                    bomb:PushEvent("setexplosiontarget", inst.sg.statemem.killer)
                end

                inst.components.lootdropper:DropLoot(my_position)
            end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },

    State {
        name = "special_atk2", --just jumps forward
        tags = {"busy", "jumping"},

        onenter = function(inst)
            if inst.noactions then
                inst.sg:GoToState("idle")
                return
            end
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("jump_pre")
            inst.SoundEmitter:PlaySound(inst.sounds.jump_pre)
        end,

        timeline =
        {
            FrameEvent(12, function(inst)
                inst.sg.statemem.jumpspeed_set = true

                inst.components.locomotor:Stop()
                inst.components.locomotor:EnableGroundSpeedMultiplier(false)
                inst.Physics:SetMotorVelOverride(TUNING.FUSED_SHADELING_JUMPSPEED, 0, 0)

                inst:_RemoveCharacterPhysics(inst)
            end),
            FrameEvent(14, function(inst)
                inst.sg:AddStateTag("noattack")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("jump")
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.jumpspeed_set then
                inst.Physics:ClearMotorVelOverride()
                inst.components.locomotor:Stop()
                inst.components.locomotor:EnableGroundSpeedMultiplier(true)

                inst:_ResetCharacterPhysics()
            end
        end,
    },

    State {
        name = "jump",
        tags = {"busy", "jumping", "noattack"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)
            inst.Physics:SetMotorVelOverride(TUNING.FUSED_SHADELING_JUMPSPEED, 0, 0)

            inst:_RemoveCharacterPhysics(inst)

            inst.AnimState:PlayAnimation("jump")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("jump_pst")
            end),
        },

        onexit = function(inst)
            inst.Physics:ClearMotorVelOverride()
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)

            inst:_ResetCharacterPhysics()
        end,
    },

    State {
        name = "jump_pst",
        tags = {"busy", "jumping", "noattack"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)

            inst.sg.statemem.speed = TUNING.FUSED_SHADELING_JUMPSPEED
            inst.Physics:SetMotorVelOverride(TUNING.FUSED_SHADELING_JUMPSPEED, 0, 0)

            inst:_RemoveCharacterPhysics(inst)

            inst.AnimState:PlayAnimation("jump_pst")

            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength() + 5*FRAMES)
        end,

        ontimeout = function(inst)
            if not inst.components.combat:TryAttack() then
                go_to_idle(inst)
            end
        end,

        onupdate = function(inst, dt)
            if not inst.sg.statemem.finished_jumping then
                inst.sg.statemem.speed = 0.75 * inst.sg.statemem.speed
                inst.Physics:SetMotorVelOverride(inst.sg.statemem.speed, 0, 0)
            end
        end,

        timeline =
        {
            FrameEvent(2, function(inst)
                inst.sg:RemoveStateTag("noattack")
            end),
            FrameEvent(3, function(inst)
                inst.sg.statemem.finished_jumping = true

                inst.Physics:ClearMotorVelOverride()
                inst.components.locomotor:Stop()
                inst.components.locomotor:EnableGroundSpeedMultiplier(true)

                inst:_ResetCharacterPhysics()
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.finished_jumping then
                inst.Physics:ClearMotorVelOverride()
                inst.components.locomotor:Stop()
                inst.components.locomotor:EnableGroundSpeedMultiplier(true)

                inst:_ResetCharacterPhysics()
            end
        end,
    },

    State {
        name = "spawn_delay",
        tags = { "busy", "noattack", "temp_invincible", "invisible" },

        onenter = function(inst, time)
            inst.components.locomotor:Stop()
            inst.Physics:SetActive(false)
            inst:Hide()
            inst:AddTag("NOCLICK")

            inst.sg:SetTimeout(time or FRAMES)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("appear")
        end,

        onexit = function(inst)
            inst.Physics:SetActive(true)
            inst:Show()
            inst:RemoveTag("NOCLICK")
        end,
    },

    State {
        name = "attack",
        tags = {"attack", "busy"},

        onenter = function(inst)
            if inst.noactions then
                inst.sg:GoToState("idle")
                return
            end
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("attack")

            inst.components.combat:StartAttack()
        end,

        timeline = {
            FrameEvent(6, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.attack)
            end),
            FrameEvent(14, function(inst)
                PlayablePets.DoWork(inst, 3)
            end),
        },

        events = {
            EventHandler("animover", go_to_idle),
        },
    },

    State {
        name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, data)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound(inst.sounds.hit)
        end,

        timeline =
        {
            FrameEvent(13, function(inst)
                if (math.random() > 0.5) then
                    inst.sg:GoToState("teleport_pre")
                end
            end),
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },

    State {
        name = "special_atk1",
        tags = {"taunt", "busy"},

        onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("taunt")
        end,

        timeline =
        {
            FrameEvent(1, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.taunt)
            end),
            FrameEvent(14, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.taunt2)
            end),
            FrameEvent(21, function(inst)
                -- The taunt tag is what prevents us from transitioning to hit.
                -- Remove it early so that the shadeling can get hit out after
                -- its initial howl in the taunt.
                inst.sg:RemoveStateTag("taunt")
            end),
            FrameEvent(29, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.taunt2)
            end),
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },

    State
    {
        name = "special_sleep",
		tags = {"busy", "hiding"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("disappear_pre")
            inst.AnimState:PushAnimation("disappear", false)
            inst.SoundEmitter:PlaySound(inst.sounds.disappear)   
            OnDisappear(inst)     
        end,
		
		events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
					inst:Hide()
					inst.specialsleep = false
					inst.sg:GoToState("idle")					
                end
            end),
        },

        
    },
	
	State
    {
        name = "special_wake",
		tags = {"busy"},
        onenter = function(inst)
			inst:Show()
            inst.AnimState:PlayAnimation("spawn")
            inst.AnimState:PushAnimation("spawn_pst", false)
            OnAppear(inst) 
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
}

CommonStates.AddWalkStates(states, nil, nil, nil, nil, {
    walkonenter = function(inst)
        if not inst.noactions then
            inst.SoundEmitter:PlaySound(inst.sounds.walk, "walkloop")
        end
    end,
    walkonexit = function(inst)
        inst.SoundEmitter:KillSound("walkloop")
    end,
})

local function teleport_test_fn(test_position)
    return (TheWorld.components.miasmamanager:GetMiasmaAtPoint(test_position:Get()) ~= nil)
end

local function do_teleport(inst)
    local iposition = inst:GetPosition()

    local miasmamanager = TheWorld.components.miasmamanager
    local test_miasma = (miasmamanager ~= nil) and miasmamanager:IsMiasmaActive()

    local initial_angle = TWOPI * math.random()
    local teleport_radius = 2 + (7 * math.sqrt(math.random()))
    local walked_teleport_offset
    if test_miasma then
        walked_teleport_offset = FindWalkableOffset(iposition, initial_angle, teleport_radius, nil, true, true, teleport_test_fn, false, false)
    end

    if not walked_teleport_offset then
        walked_teleport_offset = FindWalkableOffset(iposition, initial_angle, teleport_radius, nil, true, true, nil, false, false)
    end

    if walked_teleport_offset then
        inst.Physics:Teleport(iposition.x + walked_teleport_offset.x, 0, iposition.z + walked_teleport_offset.z)
    end
end
CommonStates.AddSimpleState(states, "teleport_pre", "disappear_pre", {"busy", "hit"}, "teleport")
CommonStates.AddSimpleState(states, "teleport", "disappear", {"busy", "hit"}, "appear",
{
    FrameEvent(7, function(inst)
        inst.sg:AddStateTag("noattack")
    end),
    FrameEvent(14, function(inst)
        local ix, iy, iz = inst.Transform:GetWorldPosition()
        local quickfuse_bomb = SpawnPrefab("fused_shadeling_quickfuse_bomb")

        local combat_target = inst.components.combat.target
        local angle = (combat_target and DEGREES * inst:GetAngleToPoint(combat_target.Transform:GetWorldPosition()))
            or (TWOPI * math.random())
        angle = GetRandomWithVariance(angle, PI/6)

        local speed = 2.5 + math.random()
        quickfuse_bomb.Physics:Teleport(ix, 0.1, iz)
        quickfuse_bomb.Physics:SetVel(speed * math.cos(angle), 12, -speed * math.sin(angle))

        quickfuse_bomb.SoundEmitter:PlaySound(inst.sounds.bomb_spawn)
    end),
},
{
    onenter = function(inst)
        inst.SoundEmitter:PlaySound(inst.sounds.disappear)
    end,
    onexit = function(inst)
        do_teleport(inst)
    end,
})

CommonStates.AddSimpleState(states, "appear", "spawn", {"busy", "noattack"}, "appear_pst",
{
    FrameEvent(4, function(inst)
        inst.sg:RemoveStateTag("noattack")
    end),
},
{
    onenter = function(inst)
        SpawnPrefab("fused_shadeling_spawn_fx").Transform:SetPosition(inst.Transform:GetWorldPosition())
        inst.SoundEmitter:PlaySound(inst.sounds.appear)
    end,
})
CommonStates.AddSimpleState(states, "appear_pst", "spawn_pst", {"busy"})

CommonStates.AddSimpleState(states, "despawn_pre", "disappear_pre", {"busy"}, "despawn")
CommonStates.AddSimpleState(states, "despawn", "disappear", {"busy"}, "idle", nil,
{
    onexit = function(inst)
        --inst:Remove()
    end,
})
PP_CommonStates.AddHomeState(states, nil, "walk_pst", "walk_pst", true)
PP_CommonStates.AddKnockbackState(states, nil, "walk_loop", nil, {onenter = function(inst)  end}) --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			
		},
		
		corpse_taunt =
		{
            
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "spawn_pst"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk_loop", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle",
	plank_idle_loop = "walk_loop",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk_loop",
	
	steer_pre = "walk_pst",
	steer_idle = "idle",
	steer_turning = "walk_pst",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_pst",
	
	leap_pre = "walk_pre",
	leap_loop = "walk_loop",
	leap_pst = "walk_pst",
	
	castspelltime = 10,
})
    
return StateGraph("fused_shadelingp", states, events, "idle", actionhandlers)

