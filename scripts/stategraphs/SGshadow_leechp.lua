require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, "jump_pre"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function TryAttach(inst, target)
	if target ~= nil and target:IsValid() and inst:IsNear(target, 1.8) then
		--if target.AttachLeech then
            --target:AttachLeech(inst)
        --else
            inst:PerformBufferedAction()
        --end
	end
end


local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local states=
{
    State{
		name = "idle",
		tags = { "idle", "canrotate" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("idle", true)
		end,
	},

	State{
		name = "spawn_delay",
		tags = { "busy", "noattack", "temp_invincible", "invisible" },

		onenter = function(inst, delay)
			inst.components.locomotor:Stop()
			inst:Hide()
			inst.sg:SetTimeout(delay or math.random())
		end,

		ontimeout = function(inst)
			inst.sg:GoToState("spawn")
		end,

		onexit = function(inst)
			inst:Show()
		end,
	},

	State{
		name = "spawn",
		tags = { "busy", "noattack", "temp_invincible" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("spawn")
		end,

		timeline =
		{
			FrameEvent(35, function(inst)
				inst.sg:RemoveStateTag("noattack")
				inst.sg:RemoveStateTag("temp_invincible")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},
	},

	State{
		name = "hit",
		tags = { "busy", "hit", "temp_invincible" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("disappear")
			inst.SoundEmitter:PlaySound("daywalker/leech/die")
			--inst.SoundEmitter:PlaySound("dontstarve/sanity/death_pop")
		end,

		timeline =
		{
			FrameEvent(12, function(inst)
				inst.sg:AddStateTag("noattack")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if not inst.noactions then
					local max_tries = 4
					for k = 1, max_tries do
						local x, y, z = inst.Transform:GetWorldPosition()
						local offset = 10
						x = x + math.random(2 * offset) - offset          
						z = z + math.random(2 * offset) - offset
						if TheWorld.Map:IsPassableAtPoint(x, y, z) then
							inst.Transform:SetPosition(x, y, z)
							break
						end
					end
					inst.sg:GoToState("appear")
				else
					inst.sg:GoToState("idle")
				end                
			end),
		},
	},

	State{
		name = "appear",
		tags = { "busy", "temp_invincible" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("appear")
		end,

		timeline =
		{
			FrameEvent(17, function(inst)
				inst.sg:RemoveStateTag("temp_invincible")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},
	},

    State
    {
        name = "special_sleep",
		tags = {"busy", "hiding"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("disappear")
			inst:AddTag("invisible")
			inst:AddTag("notarget")
			inst.components.health:SetInvincible(true)
			inst.noactions = true
			inst:RemoveTag("scarytoprey")          
        end,
		
		events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					inst:Hide()
					inst.specialsleep = false
					inst.sg:GoToState("idle")					
                end
            end),
        },

        
    },
	
	State
    {
        name = "special_wake",
		tags = {"busy"},
        onenter = function(inst)
			inst:Show()
			inst.components.health:SetInvincible(false)
            inst.AnimState:PlayAnimation("appear")
			inst.specialsleep = true
			inst.noactions = nil
			inst:RemoveTag("invisible")
			inst:RemoveTag("notarget")
			inst:AddTag("scarytoprey") 
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    }, 

	State{
		name = "jump_pre",
		tags = { "busy" },

		onenter = function(inst, target)
            if inst.noactions then
                inst.sg:GoToState("idle")
            else
                inst.components.locomotor:Stop()
                inst.AnimState:PlayAnimation("jump_pre")
                inst.SoundEmitter:PlaySound("daywalker/leech/leap")
                local buffaction = inst:GetBufferedAction()
                local target = buffaction ~= nil and buffaction.target or nil
                if target then
                    inst.sg.statemem.target = target
                end
            end
		end,

		onupdate = function(inst)
			if inst.sg.statemem.target ~= nil and not inst.noactions then
				if inst.sg.statemem.target:IsValid() then
					local pos = inst.sg.statemem.target:GetPosition()
					pos.x, pos.y, pos.z = inst.sg.statemem.target.Transform:GetWorldPosition()
					inst:ForceFacePoint(pos)
				else
					inst.sg.statemem.target = nil
				end
			end
		end,

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("jump", inst.sg.statemem.target or inst.sg.statemem.targetpos)
				end
			end),
		},
	},

	State{
		name = "jump",
		tags = { "busy", "jumping", "noattack" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("jump")
			inst.SoundEmitter:PlaySound("daywalker/leech/vocalization")
            local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target then
				inst.sg.statemem.target = target
			end
			local dist
			if target == nil then
				dist = 6
				local theta = inst.Transform:GetRotation() * DEGREES
				target = inst:GetPosition()
				target.x = target.x + math.cos(theta) * dist
				target.z = target.z - math.sin(theta) * dist
			elseif EntityScript.is_instance(target) and target:IsValid() then
				inst.sg.statemem.target = target
				target = target:GetPosition()
				dist = math.sqrt(inst:GetDistanceSqToPoint(target))
			end
			inst:ForceFacePoint(target)
			inst.sg.statemem.speed = math.min(16.5, dist / (11 * FRAMES))
			inst.Physics:SetMotorVelOverride(inst.sg.statemem.speed, 0, 0)
		end,

		timeline =
		{
			FrameEvent(11, function(inst)
				TryAttach(inst, inst.sg.statemem.target)
			end),
			FrameEvent(15, function(inst)
				inst.sg:RemoveStateTag("noattack")
				inst.Physics:SetMotorVelOverride(inst.sg.statemem.speed * .35, 0, 0)
				inst.SoundEmitter:PlaySound("daywalker/leech/vocalization")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("flail")
				end
			end),
		},

		onexit = function(inst)
			inst.Physics:ClearMotorVelOverride()
			inst.Physics:Stop()
		end,
	},

	State{
		name = "flail",
		tags = { "busy" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("flail_loop", true)
			inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength() * 3)
		end,

		ontimeout = function(inst)
			inst.sg:GoToState("flail_pst")
		end,
	},

	State{
		name = "flail_pst",
		tags = { "busy" },

		onenter = function(inst)
			inst.AnimState:PlayAnimation("flail_pst")
		end,

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},
	},

	State{
		name = "attached",
		tags = { "busy", "noattack", "temp_invincible" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("attach_loop", true)
			--inst.Physics:SetActive(false)
			inst:AddTag("notarget")
			inst:ToggleBrain(false)
			inst.SoundEmitter:PlaySound("daywalker/leech/suck", "suckloop")
		end,

		onexit = function(inst)
			inst.Follower:StopFollowing()
			--inst.Physics:SetActive(true)
			inst:RemoveTag("notarget")
			inst:ToggleBrain(true)
			inst.SoundEmitter:KillSound("suckloop")
			local daywalker = inst.components.entitytracker:GetEntity("daywalker")
			if daywalker ~= nil then
				daywalker:OnAttachmentInterrupted(inst)
			end
		end,
	},

	State{
		name = "flung",
		tags = { "busy", "jumping", "noattack", "temp_invincible" },

		onenter = function(inst, speedmult)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("toss")
			inst.SoundEmitter:PlaySound("daywalker/leech/fall_off")
			inst.sg.statemem.speed = -10 * (speedmult or 1)
			inst.Physics:SetMotorVelOverride(inst.sg.statemem.speed, 0, 0)
		end,

		timeline =
		{
			FrameEvent(18, function(inst)
				inst.sg:RemoveStateTag("noattack")
				inst.sg:RemoveStateTag("temp_invincible")
				inst.Physics:SetMotorVelOverride(inst.sg.statemem.speed * .35, 0, 0)
				inst.SoundEmitter:PlaySound("daywalker/leech/vocalization")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("flail")
				end
			end),
		},

		onexit = function(inst)
			inst.Physics:ClearMotorVelOverride()
			inst.Physics:Stop()
		end,
	},
	
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("disappear")
			inst.SoundEmitter:PlaySound("daywalker/leech/die")
			inst.SoundEmitter:PlaySound("dontstarve/sanity/death_pop")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))   
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },
}

--CommonStates.AddFrozenStates(states)
CommonStates.AddRunStates(states,
{
	starttimeline =
	{
		FrameEvent(6, function(inst)
			inst.components.locomotor:RunForward()
		end),
	},
},
nil, nil, true--[[delaystart]],
{
	runonenter = function(inst)
        if not inst.noactions then
		    inst.SoundEmitter:PlaySound("daywalker/leech/walk", "walkloop")
        end
	end,
	runonexit = function(inst)
		inst.SoundEmitter:KillSound("walkloop")
	end,
})
PP_CommonStates.AddHomeState(states, nil, "run_pst", "run_pst", true)
PP_CommonStates.AddKnockbackState(states, nil, "toss", nil, {onenter = function(inst) inst.SoundEmitter:PlaySound("daywalker/leech/fall_off") end}) --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"run_pst", nil, nil, "idle", "run_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "die")) end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "scream")) end),
		},
	
	},
	--anims = 
	{
		corpse = "disappear",
		corpse_taunt = "appear"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
CommonStates.AddHopStates(states, false, {pre = "run_pre", loop = "run_loop", pst = "run_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle",
	plank_idle_loop = "run_loop",
	plank_idle_pst = "run_pst",
	
	plank_hop_pre = "run_pre",
	plank_hop = "run_loop",
	
	steer_pre = "run_pst",
	steer_idle = "idle",
	steer_turning = "run_pst",
	stop_steering = "run_pst",
	
	row = "run_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "run_pre",
	leap_loop = "run_loop",
	leap_pst = "run_pst",
	
	castspelltime = 10,
})
    
return StateGraph("shadow_leechp", states, events, "spawn", actionhandlers)

