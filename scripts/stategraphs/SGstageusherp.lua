require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
    ActionHandler(ACTIONS.ATTACK, "attack"),
}

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}


 local states=
{

	State{
        name = "initailstate",
        
        onenter = function(inst)
			inst.sg.is_hiding = false
            ChangeToCharacterPhysics(inst)
            inst.Physics:SetMass(150)
			inst.sg:GoToState("idle")
		end,
	},
	
	State{
        name = "idle",
        onenter = function(inst)
			inst.Physics:Stop()
	        inst.sg.is_hiding = false
            inst.AnimState:PlayAnimation("awake_idle", true)
        end
    },

    State {
        name = "attack",
        tags = {"attack", "busy"},

        onenter = function(inst)
            local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target and target:IsValid() then
				inst.sg.statemem.target = target
				inst:FacePoint(target:GetPosition())
			end
            inst.components.locomotor:StopMoving()

            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("sleep")
        end,

        timeline =
        {
            TimeEvent(15*FRAMES, function(inst)
                local target = inst.sg.statemem.target
                if (target ~= nil and target:IsValid()) and
                        (inst.StartAttackingTarget ~= nil and inst:StartAttackingTarget(target)) then
                    inst.sg.statemem.attack_success = true
                else
                    inst.sg.statemem.attack_success = false
                end
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.sg.statemem.attack_success then
                    inst.sg:GoToState("attack_loop")
                else
                    inst.sg:GoToState("standup")
                end
            end),
        },
    },

    State {
        name = "attack_loop",
        tags = {"attack", "busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            inst.AnimState:PlayAnimation("idle_loop_01", true)

            -- Safety timeout.
            inst.sg:SetTimeout(10)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("standup")
        end,

        events =
        {
            EventHandler("handfinished", function(inst)
                inst.sg:GoToState("standup")
            end),
        },
    },
   
    State{
        name = "idle_standing",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            local animname = "idle"
          
            if inst.sg.is_hiding then
                inst.sg:GoToState("standup")
            else
	            inst.sg.is_hiding = false
                inst.AnimState:PlayAnimation("awake_idle")
            end
            
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
        
    State{
        name = "idle_hiding",
        tags = {"idle", "hiding"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            local animname = "idle"
			
            if not inst.sg.is_hiding then
                inst.sg:GoToState("hide")
            else
	            inst.sg.is_hiding = true
	            
	            local anim = "idle_loop_01"
	            if TheWorld.state.isdusk then
					local chance = math.random()
					if chance < 0.02 then
						anim = "peeking_idle_loop_01"
					elseif chance < 0.04 then
						anim = "peeking_idle_loop_02"
					end
				elseif TheWorld.state.isnight then
					local chance = math.random()
					if chance < 0.2 then
						anim = "peeking_idle_loop_01"
					elseif chance < 0.4 then
						anim = "peeking_idle_loop_02"
					end
	            end
	            inst.AnimState:PlayAnimation(anim)
            end

        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
    
    State{
        name = "hide",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("sleep")
            inst.sg.is_hiding = true
            ChangeToObstaclePhysics(inst)
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },    
    
    State{
        name = "standup",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("awake_pre")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) 
                inst.sg.is_hiding = false
                ChangeToCharacterPhysics(inst)
                inst.Physics:SetMass(150)
                inst.sg:GoToState(inst.components.locomotor:WantsToMoveForward() and "walk_start" or "idle") end
            ),
        },
    },    
	
    State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)
--            inst.SoundEmitter:PlaySound(inst.sounds.hit)
            inst.AnimState:PlayAnimation(inst.sg.is_hiding and "hit" or "awake_hit")
            inst.Physics:Stop()            
        end,
        
		timeline=
        {
            TimeEvent(0*FRAMES, function(inst) if not inst.sg.is_hiding then inst.SoundEmitter:PlaySound(inst.sounds.hit) end end), -- awake hit timing
            TimeEvent(3*FRAMES, function(inst) if inst.sg.is_hiding then     inst.SoundEmitter:PlaySound(inst.sounds.hit) end end), -- hiding hit timing
        },
		
        events=
        {
            EventHandler("animover", function(inst) 
			if inst.sg.is_hiding == true then
				inst.sg:GoToState("sleeping")
			else	
				inst.sg:GoToState("idle") 
			end 
			end ),
        },        
    },    
    
    State{
        name = "extinguish",
        tags = {"busy"},
        
        onenter = function(inst)
			if inst.sg.is_hiding then
	            inst.AnimState:PlayAnimation("extinguish")
		        inst.Physics:Stop()            
		    else
				inst.sg:GoToState("hide")
		    end
        end,
        
        timeline=
        {
	        TimeEvent(20*FRAMES, function(inst) inst.components.burnable:Extinguish() end ),
	    },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },    

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.Physics:Stop()            
			RemovePhysicsColliders(inst)            
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))

			local fx = SpawnPrefab("collapse_small")
			fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
			fx:SetMaterial("pot")
			inst:Hide()
			inst.components.inventory:DropEverything(true)
			
			inst:DoTaskInTime(2, function(inst)
                    inst:Show()
                    PlayablePets.DoDeath(inst)
				end)
			end,

        events =
        {
            
        },

    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_pre")
        end,

		timeline = 
		{
			TimeEvent( 6*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.footstep) end),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
        TimeEvent( 0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.footstep) end),
        TimeEvent( 6*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.footstep) end),
        TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.footstep) end),
        TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.footstep) end),
    },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")            
        end,

		timeline = 
		{
			TimeEvent( 0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.footstep) end),
			TimeEvent( 5*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.footstep) end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep")
			inst.sg.is_hiding = true
            ChangeToObstaclePhysics(inst)
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline=
        {
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.awake_pre) end),
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.hit) end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
			
            local animname = "idle"
			
            if not inst.sg.is_hiding then
                inst.sg:GoToState("hide")
            else
	            inst.sg.is_hiding = true
	            
	            local anim = "idle_loop_01"
	            if TheWorld.state.isdusk then
					local chance = math.random()
					if chance < 0.02 then
						anim = "peeking_idle_loop_01"
					elseif chance < 0.04 then
						anim = "peeking_idle_loop_02"
					end
				elseif TheWorld.state.isnight then
					local chance = math.random()
					if chance < 0.2 then
						anim = "peeking_idle_loop_01"
					elseif chance < 0.4 then
						anim = "peeking_idle_loop_02"
					end
	            end
	            inst.AnimState:PlayAnimation(anim)
            end
				--end
				--inst.components.sanity:DoDelta(1, false)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
		
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("awake_pre")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        timeline = 
		{
			TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.hit) end),
            TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.awake_pre) end),
            TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.footstep) end),
            TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.footstep) end),
		},

        events =
        {
            EventHandler("animover", function(inst) 
			inst.sg.is_hiding = false
            ChangeToCharacterPhysics(inst)
            inst.Physics:SetMass(150)
			inst.sg:GoToState("idle") end),
        },
    },
	
	
}

--CommonStates.AddFrozenStates(states)
local moveanim = "walk"
local idleanim = "idle"
local actionanim = "walk_pst"
--CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            PlayablePets.DoWork(inst, 3)
        end),
	}, 
	actionanim, nil, nil, "idle_loop", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
				local fx = SpawnPrefab("collapse_small")
				fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
				fx:SetMaterial("pot")
			end),
		},
		
		corpse_taunt =
		{
			
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "mating_taunt1",
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, idleanim)
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim.."_loop", pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim.."_loop",
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim.."_loop",
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim.."_loop",
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim.."_loop",
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})

    
return StateGraph("stageusherp", states, events, "idle", actionhandlers)

