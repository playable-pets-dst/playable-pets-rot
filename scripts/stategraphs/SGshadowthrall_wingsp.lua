require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function SetShadowScale(inst, scale)
	inst.DynamicShadow:SetSize(1.7 * scale, .9 * scale)
end

local function SetSpawnShadowScale(inst, scale)
	inst.DynamicShadow:SetSize(1.5 * scale, scale)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
    ActionHandler(ACTIONS.ATTACK, "cast")
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local AOE_RANGE_PADDING = 3
local AOE_TARGET_MUSTHAVE_TAGS = { "_combat" }
local AOE_TARGET_CANT_TAGS = { "INLIMBO", "flight", "invisible", "notarget", "noattack", "shadowthrall" }
local AOE_DEVOUR_RADIUS_SQ = TUNING.SHADOWTHRALL_HORNS_DEVOUR_RADIUS * TUNING.SHADOWTHRALL_HORNS_DEVOUR_RADIUS

local function DoAOEAttack(inst, dist, radius, heavymult, mult, forcelanded, targets, devour)
	inst.components.combat.ignorehitrange = true
	local x, y, z = inst.Transform:GetWorldPosition()
	if dist ~= 0 then
		local rot = inst.Transform:GetRotation() * DEGREES
		x = x + dist * math.cos(rot)
		z = z - dist * math.sin(rot)
	end
	for i, v in ipairs(TheSim:FindEntities(x, y, z, radius + AOE_RANGE_PADDING, AOE_TARGET_MUSTHAVE_TAGS, AOE_TARGET_CANT_TAGS)) do
		if v ~= inst and
			not (targets ~= nil and targets[v]) and
			v:IsValid() and not v:IsInLimbo()
			and not (v.components.health ~= nil and v.components.health:IsDead())
			then
			local range = radius + v:GetPhysicsRadius(0)
			local dsq = v:GetDistanceSqToPoint(x, y, z)
			if dsq < range * range and inst.components.combat:CanTarget(v) then
				inst.components.combat:DoAttack(v)
				if devour == true and v.sg ~= nil and v:HasTag("player") and dsq < AOE_DEVOUR_RADIUS_SQ then
					--Don't buffer, handle immediately
					v.sg:HandleEvent("devoured", { attacker = inst })
					if v.sg:HasStateTag("devoured") and v.sg.statemem.attacker == inst then
						devour = v
					end
				end
				if mult ~= nil and devour ~= v then
					local strengthmult = (v.components.inventory ~= nil and v.components.inventory:ArmorHasTag("heavyarmor") or v:HasTag("heavybody")) and heavymult or mult
					v:PushEvent("knockback", { knocker = inst, radius = radius + dist + 3, strengthmult = strengthmult, forcelanded = forcelanded })
				end
				if targets ~= nil then
					targets[v] = true
				end
			end
		end
	end
	inst.components.combat.ignorehitrange = false
	return EntityScript.is_instance(devour) and devour or nil
end

local SLAP_BEAM_LEN = TUNING.SHADOWTHRALL_HORNS_BISHIBASHI_RANGE
local SLAP_BEAM_WID = TUNING.SHADOWTHRALL_HORNS_BISHIBASHI_WIDTH
local SLAP_AOE_DIST = SLAP_BEAM_LEN / 2
local SLAP_AOE_RADIUS = math.sqrt(SLAP_AOE_DIST * SLAP_AOE_DIST + SLAP_BEAM_WID * SLAP_BEAM_WID / 4)

local function DoAOESlap(inst)
	inst.components.combat.ignorehitrange = true
	local x, y, z = inst.Transform:GetWorldPosition()
	local theta = inst.Transform:GetRotation() * DEGREES
	local vx = math.cos(theta)
	local vz = -math.sin(theta)
	for i, v in ipairs(TheSim:FindEntities(x + SLAP_AOE_DIST * vx, y, z + SLAP_AOE_DIST * vz, SLAP_AOE_RADIUS + AOE_RANGE_PADDING, AOE_TARGET_MUSTHAVE_TAGS, AOE_TARGET_CANT_TAGS)) do
		if v ~= inst and
			v:IsValid() and not v:IsInLimbo()
			and not (v.components.health ~= nil and v.components.health:IsDead())
			then
			local physrad = v:GetPhysicsRadius(0)
			local x1, y1, z1 = v.Transform:GetWorldPosition()
			local dx = x1 - x
			local dz = z1 - z
			local dot = vx * dx + vz * dz
			if dot > 0 and dot < SLAP_BEAM_LEN + physrad and
				--perpendicular vector: (vz, -vx)
				math.abs(vz * dx - vx * dz) < SLAP_BEAM_WID + physrad and
				inst.components.combat:CanTarget(v)
				then
				inst.components.combat:DoAttack(v)
			end
		end
	end
	inst.components.combat.ignorehitrange = false
end

local COLLAPSIBLE_WORK_ACTIONS =
{
	CHOP = true,
	HAMMER = true,
	MINE = true,
	-- no digging
}
local COLLAPSIBLE_TAGS = { "NPC_workable" }
for k, v in pairs(COLLAPSIBLE_WORK_ACTIONS) do
	table.insert(COLLAPSIBLE_TAGS, k.."_workable")
end
local NON_COLLAPSIBLE_TAGS = { "FX", --[["NOCLICK",]] "DECOR", "INLIMBO" }

local function DoAOEWork(inst, dist, radius, targets)
	local x, y, z = inst.Transform:GetWorldPosition()
	if dist ~= 0 then
		local rot = inst.Transform:GetRotation() * DEGREES
		x = x + dist * math.cos(rot)
		z = z - dist * math.sin(rot)
	end
	for i, v in ipairs(TheSim:FindEntities(x, y, z, radius, nil, NON_COLLAPSIBLE_TAGS, COLLAPSIBLE_TAGS)) do
		if not (targets ~= nil and targets[v]) and v:IsValid() and not v:IsInLimbo() and v.components.workable ~= nil then
			local work_action = v.components.workable:GetWorkAction()
			--V2C: nil action for NPC_workable (e.g. campfires)
			if (work_action == nil and v:HasTag("NPC_workable")) or
				(v.components.workable:CanBeWorked() and work_action ~= nil and COLLAPSIBLE_WORK_ACTIONS[work_action.id])
				then
				v.components.workable:Destroy(inst)
				--[[if v:IsValid() and v:HasTag("stump") then
					v:Remove()
				end]]
			end
		end
	end
end

local function PlaySlapSound(inst)
	inst.SoundEmitter:PlaySound("rifts2/thrall_horns/smack")
end

local function IsDevouring(inst, target)
	return target ~= nil
		and target:IsValid()
		and target.sg ~= nil
		and target.sg:HasStateTag("devoured")
		and target.sg.statemem.attacker == inst
end

local function DoChew(inst, target, useimpactsound)
	if not useimpactsound then
		inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_flesh_med_dull")
	end
	if IsDevouring(inst, target) then
		local dmg, spdmg = inst.components.combat:CalcDamage(target)
		local noimpactsound = target.components.combat.noimpactsound
		target.components.combat.noimpactsound = not useimpactsound
		target.components.combat:GetAttacked(inst, dmg, nil, nil, spdmg)
		target.components.combat.noimpactsound = noimpactsound
	end
end

local function DoSpitOut(inst, target)
	if IsDevouring(inst, target) then
		target.sg.currentstate:HandleEvent(target.sg, "spitout", { spitter = inst, radius = inst:GetPhysicsRadius(0) + 3, strengthmult = 1 })
		if not inst.components.health:IsDead() then
			inst.components.combat:SetTarget(target)
		end
	end
end

local function SetShadowScale(inst, scale)
	inst.DynamicShadow:SetSize(5 * scale, 2.5 * scale)
end

local function SetSpawnShadowScale(inst, scale)
	inst.DynamicShadow:SetSize(1.5 * scale, scale)
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local states=
{
    State{
		name = "idle",
		tags = { "idle", "canrotate" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("idle", true)
		end,
	},

	State{
		name = "spawndelay",
		tags = { "busy", "noattack", "temp_invincible", "invisible" },

		onenter = function(inst, delay)
			inst.components.locomotor:Stop()
			inst.DynamicShadow:Enable(false)
			inst.Physics:SetActive(false)
			inst:Hide()
			inst:AddTag("NOCLICK")
			inst.sg:SetTimeout(delay or 0)
		end,

		ontimeout = function(inst)
			inst.sg.statemem.spawning = true
			inst.sg:GoToState("spawn")
		end,

		onexit = function(inst)
			if not inst.sg.statemem.spawning then
				inst.DynamicShadow:Enable(true)
			end
			inst.Physics:SetActive(true)
			inst:Show()
			inst:RemoveTag("NOCLICK")
		end,
	},

	State{
		name = "spawn",
		tags = { "appearing", "busy", "noattack", "temp_invincible" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("appear")
			inst.SoundEmitter:PlaySound("rifts2/thrall_generic/appear_cloth")
			inst.DynamicShadow:Enable(false)
			ToggleOffCharacterCollisions(inst)
			inst.sg.mem.lastattack = GetTime()
		end,

		timeline =
		{
			FrameEvent(7, function(inst)
				SetSpawnShadowScale(inst, .25)
				inst.DynamicShadow:Enable(true)
			end),
			FrameEvent(8, function(inst) SetSpawnShadowScale(inst, .5) end),
			FrameEvent(9, function(inst) SetSpawnShadowScale(inst, .75) end),
			FrameEvent(10, function(inst) SetSpawnShadowScale(inst, 1) end),
			FrameEvent(40, function(inst)
				SetSpawnShadowScale(inst, .93)
				inst.SoundEmitter:PlaySound("rifts2/thrall_wings/appear")
			end),
			FrameEvent(42, function(inst) SetSpawnShadowScale(inst, .9) end),
			FrameEvent(44, function(inst) SetShadowScale(inst, .93) end),
			FrameEvent(45, function(inst)
				inst.sg:RemoveStateTag("temp_invincible")
				inst.sg:RemoveStateTag("noattack")
				inst.sg:RemoveStateTag("appearing")
				SetShadowScale(inst, 1)
				ToggleOnCharacterCollisions(inst)
			end),
			FrameEvent(48, function(inst)
				inst.sg:AddStateTag("caninterrupt")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},

		onexit = function(inst)
			if inst.sg:HasStateTag("noattack") then
				SetShadowScale(inst, 1)
				inst.DynamicShadow:Enable(true)
				ToggleOnCharacterCollisions(inst)
			end
		end,
	},
	
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
			inst.SoundEmitter:PlaySound("rifts2/thrall_generic/vocalization_death")
			inst.SoundEmitter:PlaySound("rifts2/thrall_generic/death_cloth")
            inst.Physics:Stop()		
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        timeline =
		{
			FrameEvent(13, function(inst)
				RemovePhysicsColliders(inst)
				SetSpawnShadowScale(inst, 1)
			end),
			FrameEvent(26, function(inst) inst.SoundEmitter:PlaySound("rifts2/thrall_generic/death_pop") end),
			FrameEvent(30, function(inst) SetSpawnShadowScale(inst, .5) end),
			FrameEvent(31, function(inst) inst.DynamicShadow:Enable(false) end),
			FrameEvent(32, function(inst)
				local pos = inst:GetPosition()
				pos.y = 3
				inst.components.lootdropper:DropLoot(pos)
                inst.components.inventory:DropEverything(true)
			end),
		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },

    State{
		name = "hit",
		tags = { "hit", "busy" },

		onenter = function(inst)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("rifts2/thrall_generic/vocalization_hit")
		end,

		timeline =
		{
			FrameEvent(11, function(inst)
				if inst.sg.statemem.doattack == nil then
					inst.sg:AddStateTag("caninterrupt")
				end
			end),
			FrameEvent(12, function(inst)
				if inst.sg.statemem.doattack ~= nil then
					if ChooseAttack(inst, inst.sg.statemem.doattack) then
						return
					end
					inst.sg.statemem.doattack = nil
				end
				inst.sg:RemoveStateTag("busy")
			end),
		},

		events =
		{
			EventHandler("doattack", function(inst, data)
				if inst.sg:HasStateTag("busy") then
					inst.sg.statemem.doattack = data
					inst.sg:RemoveStateTag("caninterrupt")
					return true
				end
			end),
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},
	},

	State{
		name = "cast",
		tags = { "attack", "busy" },

		onenter = function(inst, target)
			inst.components.locomotor:Stop()
			inst.AnimState:PlayAnimation("cast")
			inst.SoundEmitter:PlaySound("rifts2/thrall_wings/cast_f0")
			inst.SoundEmitter:PlaySound("rifts2/thrall_generic/vocalization_big")
            local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target ~= nil and target:IsValid() then
				inst.sg.statemem.target = target
				inst.sg.statemem.targetpos = target:GetPosition()
				inst:ForceFacePoint(inst.sg.statemem.targetpos)
			end
		end,

		onupdate = function(inst)
			if inst.sg.statemem.target ~= nil then
				if inst.sg.statemem.target:IsValid() then
					local pos = inst.sg.statemem.targetpos
					pos.x, pos.y, pos.z = inst.sg.statemem.target.Transform:GetWorldPosition()
				else
					inst.sg.statemem.target = nil
				end
			end
			inst:ForceFacePoint(inst.sg.statemem.targetpos)
		end,

		timeline =
		{
			FrameEvent(23, function(inst)
				inst.SoundEmitter:PlaySound("rifts2/thrall_wings/cast_f25")

				local x, y, z = inst.Transform:GetWorldPosition()
				local pos = inst.sg.statemem.targetpos
				if inst.sg.statemem.target ~= nil then
					if inst.sg.statemem.target:IsValid() then
						pos.x, pos.y, pos.z = inst.sg.statemem.target.Transform:GetWorldPosition()
					end
					inst.sg.statemem.target = nil
				end
				local dir
				if pos ~= nil then
					inst:ForceFacePoint(pos)
					dir = inst.Transform:GetRotation() * DEGREES
				else
					dir = inst.Transform:GetRotation() * DEGREES
					pos = Vector3(x + 8 * math.cos(dir), 0, z - 8 * math.sin(dir))
				end

				local targets = {} --shared table for the whole patch of particles
				local sfx = {} --shared table so we only play sfx once for the whole batch
				local proj = SpawnPrefab("shadowthrall_projectile_fx")
				proj.Physics:Teleport(x, y, z)
				proj.targets = targets
				proj.sfx = sfx
				proj.components.complexprojectile:Launch(pos, inst)

				dir = dir + PI
				local pos1 = Vector3(0, 0, 0)
				for i = 0, 4 do
					local theta = dir + TWOPI / 5 * i
					pos1.x = pos.x + 2 * math.cos(theta)
					pos1.z = pos.z - 2 * math.sin(theta)
					local proj = SpawnPrefab("shadowthrall_projectile_fx")
					proj.Physics:Teleport(x, y, z)
					proj.targets = targets
					proj.sfx = sfx
					proj.components.complexprojectile:Launch(pos1, inst)
				end
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},
	},
}

CommonStates.AddWalkStates(states,
nil, --timeline
nil, nil, nil,
{
	walkonenter = function(inst)
		inst.SoundEmitter:PlaySound("rifts2/thrall_generic/vocalization_small")
		inst.SoundEmitter:PlaySound("rifts2/thrall_wings/flap_walk")
		inst.sg.mem.lastflap = GetTime()
	end,
	endonenter = function(inst)
		if (inst.sg.mem.lastflap or 0) + 0.5 < GetTime() then
			inst.SoundEmitter:PlaySound("rifts2/thrall_wings/flap_walk", nil, .5)
		end
	end,
})
PP_CommonStates.AddHomeState(states, nil, "walk_pst", "walk_pst", true)
PP_CommonStates.AddKnockbackState(states, nil, "hit", nil, {onenter = function(inst)  end}) --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "die")) end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "scream")) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "appear"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk_loop", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle",
	plank_idle_loop = "walk_loop",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk_loop",
	
	steer_pre = "walk_pst",
	steer_idle = "idle",
	steer_turning = "walk_pst",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_pst",
	
	leap_pre = "walk_pre",
	leap_loop = "walk_loop",
	leap_pst = "walk_pst",
	
	castspelltime = 10,
})
    
return StateGraph("shadowthrall_wingsp", states, events, "idle", actionhandlers)

