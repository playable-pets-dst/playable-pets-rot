require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, "roll_start"),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "walk_pst")
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() and not inst.sg:HasStateTag("shield") then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState(inst.sg:HasStateTag("shield") and "shield_hit" or "hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "brightmare"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "brightmare"}
	end
end

local TARGET_MUSTHAVE_TAGS = { "_health", "_combat" }
local TARGET_CANT_TAGS = { "brightmare", "INLIMBO" }
local TARGET_ONEOF_TAGS = { "character", "monster" }
local function DoAOEAttack(inst, range)
    local x,y,z = inst.Transform:GetWorldPosition()
    local targets = TheSim:FindEntities(
        x, y, z, range,
        TARGET_MUSTHAVE_TAGS, GetExcludeTags(inst), TARGET_ONEOF_TAGS
    )

    inst.components.combat:SetDefaultDamage(TUNING.ALTERGUARDIAN_PHASE1_AOEDAMAGE)
    for _, target in ipairs(targets) do
        if target ~= inst then
            inst.components.combat:DoAttack(target)
        end
    end
    inst.components.combat:SetDefaultDamage(TUNING.ALTERGUARDIAN_PHASE1_ROLLDAMAGE)
end

local function go_to_idle(inst)
    inst.sg:GoToState("idle")
end

local TANTRUM_SS_SPEED = 0.05
local TANTRUM_SS_SCALE = 0.075
local function tantrum_screenshake(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, TANTRUM_SS_SPEED, TANTRUM_SS_SCALE, inst, 60)
end

local ROLL_SS_SPEED = 0.1
local ROLL_SS_SCALE = 0.1
local function roll_screenshake(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, ROLL_SS_SPEED, ROLL_SS_SCALE, inst, 40)
end

local states=
{
    State
    {
        name = "prespawn_idle",
        tags = {"busy", "noaoestun", "noattack", "nofreeze", "nosleep", "nostun" },

        onenter = function(inst)
            inst.AnimState:SetBuild("alterguardian_spawn_death")
            inst.AnimState:SetBankAndPlayAnimation("alterguardian_spawn_death", "fall_idle", true)
            inst.components.health:SetInvincible(true)
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("spawn")
            end),
        },

        onexit = function(inst)
            inst.components.health:SetInvincible(false)
            inst.AnimState:SetBuild("alterguardian_phase1")
            inst.AnimState:SetBankAndPlayAnimation("alterguardian_phase1", "idle")
        end,
    },

    State
    {
        name = "spawn",
        tags = {"busy", "noaoestun", "noattack", "nofreeze", "nosleep", "nostun" },

        onenter = function(inst)
            inst.AnimState:SetBuild("alterguardian_spawn_death")
            inst.AnimState:SetBankAndPlayAnimation("alterguardian_spawn_death", "phase1_spawn")
            inst.components.health:SetInvincible(true)
			inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/spawn")
        end,

        events =
        {
            EventHandler("animover", go_to_idle),
        },

        onexit = function(inst)
            inst.components.health:SetInvincible(false)
            inst.AnimState:SetBuild("alterguardian_phase1")
            inst.AnimState:SetBankAndPlayAnimation("alterguardian_phase1", "idle")

            inst.components.combat:RestartCooldown()
        end,
    },

    State
    {
        name = "idle",
        tags = {"canroll", "canrotate", "idle"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle", true)
        end,

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },

    State
    {
        name = "roll_start",
        tags = {"attack", "busy", "canrotate", "charge", "moving", "running"},

        onenter = function(inst)
			inst.Physics:Stop()			
			
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target and target:IsValid() then
				inst.roll_target = target
				inst:FacePoint(target:GetPosition())
			end
			
			if inst.roll_target and inst.roll_target:IsValid() then
                local tx, ty, tz = inst.roll_target.Transform:GetWorldPosition()
                inst.Transform:SetRotation(inst:GetAngleToPoint(tx, ty, tz))
            end

            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("roll_pre")
        end,

        timeline =
        {
            TimeEvent(0*FRAMES, function(inst)
                inst.components.locomotor:WalkForward() --For some retarded reason, player alterguardians can't move on the first frame of this state.
				--I've tried everything. Its just specifically this state. Why?
            end),
            TimeEvent(12*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
            TimeEvent(17*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
            TimeEvent(29*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
            TimeEvent(39*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst:PushEvent("attackstart")
                inst.sg:GoToState("roll")
            end),
        },

        onexit = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.Physics:ClearMotorVelOverride()
        end,
    },

    State
    {
        name = "roll",
        tags = {"attack", "busy", "charge", "moving", "running"},

        onenter = function(inst)
            inst:EnableRollCollision(true)

            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)
            inst.Physics:SetMotorVelOverride(10, 0, 0)

            inst.AnimState:PlayAnimation("roll_loop", true)

            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())

            if inst.sg.mem._num_rolls == nil then
                inst.sg.mem._num_rolls = TUNING.ALTERGUARDIAN_PHASE1_MINROLLCOUNT + (math.random() * 4)
            else
                inst.sg.mem._num_rolls = inst.sg.mem._num_rolls - 1
            end
        end,

        timeline =
        {
            TimeEvent(1*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/roll")

                roll_screenshake(inst)
            end),
        },

        events =
        {
            EventHandler("rollcollidedwithplayer", function(inst)
                inst.sg.statemem.hitplayer = true
            end),
        },

        onexit = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.Physics:ClearMotorVelOverride()

            inst:EnableRollCollision(false)
        end,

        ontimeout = function(inst)
            if not inst.sg.statemem.hitplayer and inst.sg.mem._num_rolls > 0 then
                local final_rotation = nil
                if inst.roll_target ~= nil and inst.roll_target:IsValid() then
                    -- Retarget, and keep rolling!
                    local tx, ty, tz = inst.roll_target.Transform:GetWorldPosition()
                    local target_facing = inst:GetAngleToPoint(tx, ty, tz)

                    local current_facing = inst:GetRotation()

                    local target_angle_diff = ((target_facing - current_facing + 540) % 360) - 180

                    -- If our rotation is sufficiently "opposite" the direction of our target,
                    -- just straight up turn around.
                    if math.abs(target_angle_diff) > 120 and math.abs(target_angle_diff) < 240 then
                        final_rotation = target_facing + GetRandomWithVariance(0, -10)
                    elseif target_angle_diff < 0 then
                        final_rotation = (current_facing + math.max(target_angle_diff, -20)) % 360
                    else
                        final_rotation = (current_facing + math.min(target_angle_diff, 20)) % 360
                    end
                else
                    final_rotation = 360*math.random()
                end

                inst.Transform:SetRotation(final_rotation)

                inst.sg:GoToState("roll")
            else
                inst.sg.mem._num_rolls = nil
                inst.sg:GoToState("roll_stop")
            end
        end,
    },

    State
    {
        name = "roll_stop",
        tags = {"busy", "canrotate", "charge", "idle"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("roll_pst")

            roll_screenshake(inst)

            inst.components.timer:StartTimer("roll_cooldown", TUNING.ALTERGUARDIAN_PHASE1_ROLLCOOLDOWN)

            inst:EnableRollCollision(true)

            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)
            inst.Physics:SetMotorVelOverride(10, 0, 0)
        end,

        timeline =
        {
            TimeEvent(1*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/roll")

                roll_screenshake(inst)
            end),
            TimeEvent(18*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/roll")
            end),
            TimeEvent(18*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/tantrum")
                tantrum_screenshake(inst)
                DoAOEAttack(inst, TUNING.ALTERGUARDIAN_PHASE1_AOERANGE)
            end),
            TimeEvent(22*FRAMES, function(inst)
                -- Velocity overrides are a stack, so we have to clear our 10 off first.
                inst.Physics:ClearMotorVelOverride()
                inst.Physics:SetMotorVelOverride(3, 0, 0)
            end),
            TimeEvent(35*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")

                inst.components.locomotor:Stop()
                inst.components.locomotor:EnableGroundSpeedMultiplier(true)
                inst.Physics:ClearMotorVelOverride()

                inst:EnableRollCollision(false)

                inst.sg.statemem.roll_finished = true
            end),
            TimeEvent(43*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
            TimeEvent(48*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
            TimeEvent(52*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },

        onexit = function(inst)
            if not inst.sg.statemem.roll_finished then
                inst.components.locomotor:Stop()
                inst.components.locomotor:EnableGroundSpeedMultiplier(true)
                inst.Physics:ClearMotorVelOverride()

                inst:EnableRollCollision(false)
            end
        end,
    },

    State
    {
        name = "special_atk1",
        tags = {"attack", "busy", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("tantrum_pre")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("tantrum")
            end),
        },
    },

    State
    {
        name = "tantrum",
        tags = {"attack", "busy", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("tantrum_loop")

            if inst.sg.mem.aoes_remaining == nil or inst.sg.mem.aoes_remaining == 0 then
                inst.sg.mem.aoes_remaining = RoundBiasedUp(GetRandomMinMax(3, 5))
            end
        end,

        timeline =
        {
            
            TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/tantrum")
            end),
            TimeEvent(7*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/tantrum")

                tantrum_screenshake(inst)
            end),
            
            TimeEvent(8*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/tantrum")

                DoAOEAttack(inst, TUNING.ALTERGUARDIAN_PHASE1_AOERANGE)

                inst.sg.mem.aoes_remaining = inst.sg.mem.aoes_remaining - 1
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState((inst.sg.mem.aoes_remaining > 0 and "tantrum") or "tantrum_pst")
            end),
        },
    },

    State
    {
        name = "tantrum_pst",
        tags = {"busy", "canrotate"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("tantrum_pst")
        end,

        timeline =
        {
            TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/tantrum")
            end),
            TimeEvent(7*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/tantrum")

                tantrum_screenshake(inst)
            end),
            TimeEvent(8*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/tantrum")

                DoAOEAttack(inst, TUNING.ALTERGUARDIAN_PHASE1_AOERANGE)
            end),
            TimeEvent(45*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
            TimeEvent(51*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
            TimeEvent(54*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },

    State
    {
        name = "hide_pre",
        tags = {"busy"},

        onenter = function(inst)
			if not inst.components.timer:TimerExists("summon_cooldown") then
				inst.Physics:Stop()
				inst.AnimState:PlayAnimation("shield_pre")
				
			else
				inst.sg:GoToState("idle")
			end
        end,
        
        timeline =
        {
            TimeEvent(0*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/shield_pre")
            end),
            TimeEvent(15*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/shield")
            end),
            TimeEvent(26*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/shield")
            end),
            TimeEvent(30*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/shield")
            end),
        },
        
        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("shield")
            end),
        },
    },

    State
    {
        name = "shield",
        tags = {"busy", "hiding", "shield"},

        onenter = function(inst)
			inst.Physics:Stop()

			inst.AnimState:PlayAnimation("shield", true)
			if not inst.components.timer:TimerExists("summon_cooldown") then
				inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/summon")
			end

			inst:EnterShield()
        end,

        onexit = function(inst)
            inst:ExitShield()
        end,
    },

    State
    {
        name = "shield_hit",
        tags = {"busy", "hit", "shield"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            inst.AnimState:PlayAnimation("shield_hit")

            inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/hit")

            inst:EnterShield()
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("shield")
            end),
        },

        onexit = function(inst)
            inst:ExitShield()
        end,
    },

    State
    {
        name = "hide_pst",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("shield_pst")
        end,
        
        timeline =
        {
            TimeEvent(20*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
            TimeEvent(29*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
            TimeEvent(34*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
        },
        
        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			--inst.current_phase = 2 --forces you into the next phase if you leave/reload in the midst of this
            inst.Physics:Stop()
            --RemovePhysicsColliders(inst)     
			--inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.AnimState:SetBuild("alterguardian_spawn_death")
            inst.AnimState:SetBankAndPlayAnimation("alterguardian_spawn_death", "phase1_death")
            inst.AnimState:PushAnimation("phase1_death_idle", true)
			
			inst.sg:SetTimeout(7)
        end,
		
		timeline =
        {
			TimeEvent(0*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/death")
            end),
            TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:KillSound("idle_LP")
            end),
            TimeEvent(26*FRAMES, function(inst)
                ShakeAllCameras(CAMERASHAKE.FULL, 0.5, 0.1, 0.6, inst, 60)
            end),
        },

        events =
        {
			--[[
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					if MOBGHOST== "Enable" then
						inst:PushEvent(inst.ghostenabled and "makeplayerghost" or "playerdied", { skeleton = false })
					else
						TheWorld:PushEvent("ms_playerdespawnanddelete", inst)
					end
                end
            end),]]
        },
		
		ontimeout = function(inst)
            if inst.prefab == "alterguardianp" then
				inst:SetPhase(2)
			else
                inst.noskeleton = true
				PlayablePets.DoDeath(inst)
			end
        end,

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("shield_pre")
        end,
		
		timeline = {
			TimeEvent(0*FRAMES, function(inst)
                --inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/shield_pre")
            end),
            TimeEvent(15*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/shield")
            end),
            TimeEvent(26*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/shield")
            end),
            TimeEvent(30*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/shield")
            end),
		},
		
		onexit = function(inst)
            inst:ExitShield()
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy"},
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("shield", true)
			PlayablePets.SleepHeal(inst)
		end,
			
		onexit = function(inst)

		end,

		timeline=
        {

        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			inst.AnimState:PlayAnimation("shield_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

		timeline =
        {
            TimeEvent(20*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
            TimeEvent(29*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
            TimeEvent(34*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
            end),
        },
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "shield_hit",
        tags = {"busy", "hit", "shield"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            inst.AnimState:PlayAnimation("shield_hit")

            inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/hit")

            inst:EnterShield()
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("shield")
            end),
        },

        onexit = function(inst)
            inst:ExitShield()
        end,
    },
}

local function play_foley(inst)
    inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/foley")
end

local function play_step(inst)
    inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step")
end

CommonStates.AddWalkStates(states,
{
    walktimeline = {
        TimeEvent(3*FRAMES, play_foley),
        TimeEvent(6*FRAMES, play_step),
        TimeEvent(6*FRAMES, PlayFootstep),
        TimeEvent(10*FRAMES, play_foley),
        TimeEvent(16*FRAMES, play_step),
        TimeEvent(16*FRAMES, PlayFootstep),
        TimeEvent(22*FRAMES, play_foley),
        TimeEvent(27*FRAMES, play_step),
        TimeEvent(27*FRAMES, PlayFootstep),
    },
    endtimeline = {
        TimeEvent(3*FRAMES, play_foley),
        TimeEvent(5*FRAMES, play_foley),
        TimeEvent(7*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step",nil,.50)
        end),
        TimeEvent(7*FRAMES, PlayFootstep),
        TimeEvent(8*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/step",nil,.25)
        end),
        TimeEvent(8*FRAMES, PlayFootstep),
    },
})

CommonStates.AddHitState(states)
local moveanim = "walk"
local idleanim = "idle"
local actionanim = "walk_pst"
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	actionanim, nil, nil, "idle", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:KillSound("idle_LP")
            end),
            TimeEvent(26*FRAMES, function(inst)
                ShakeAllCameras(CAMERASHAKE.FULL, 0.5, 0.1, 0.6, inst, 60)
            end),
		},
		
		corpse_taunt =
		{
			
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "walk_pst"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, "idle")
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim.."_loop", pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim.."_loop",
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim.."_loop",
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim.."_loop",
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim.."_loop",
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})
	
return StateGraph("alterguardian1p", states, events, "spawn", actionhandlers)

