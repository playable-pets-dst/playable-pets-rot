require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.PICKUP, workaction),
	ActionHandler(ACTIONS.PICK, workaction),
	ActionHandler(ACTIONS.HARVEST, workaction),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnDeath(),
	EventHandler("respawnfromghost", function(inst)  
		if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:Enable(true)
        end

        inst.components.health:SetInvincible(false)
        inst:ShowHUD(true)
        inst:SetCameraDistance()

        SerializeUserSession(inst) 
	end),	
}


local states=
{
	State {
        name = "idle",
        tags = { "idle", "canrotate" },
        onenter = function(inst, playanim)
			inst.components.locomotor:StopMoving()

            local next_anim = (math.random() > 0.2 and "idle1") or "idle2"
            inst.AnimState:PlayAnimation(next_anim)
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State {
        name = "attack",
        tags = { "attack", "busy", "jumping" },

        onenter = function(inst, target)
            if target then
                inst.sg.statemem.target = target
                inst:ForceFacePoint(target:GetPosition())
            end
            inst.components.locomotor:StopMoving()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("attack")
        end,

        timeline =
        {
            FrameEvent(5, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.attack_munch)
            end),
            FrameEvent(17, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.attack_hit)
            end),
            FrameEvent(18, function(inst)
                PlayablePets.DoWork(inst, 2)
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State {
        name = "special_atk1",
        tags = { "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt_pre")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("taunt_loop")
            end),
        },
    },

    State {
        name = "taunt_loop",
        tags = {"idle"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt_loop")
            --inst.sg.mem.taunt_loops = (inst.sg.mem.taunt_loops or (2 + math.random(2))) - 1

            inst.SoundEmitter:PlaySound(inst.sounds.taunt)
        end,

        timeline =
        {
           SoundFrameEvent(8, "rifts4/mimic/mimic_chest/taunt_step"),
           SoundFrameEvent(18, "rifts4/mimic/mimic_chest/taunt_step"),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("taunt_loop")
            end),
			EventHandler("locomote", function(inst)
                inst.sg:GoToState("taunt_pst")
            end),
        },
    },

    State {
        name = "taunt_pst",
        tags = { "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt_pst")
            inst.SoundEmitter:PlaySound("rifts4/mimic/mimic_chest/taunt_pst")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

	State {
        name = "eat_loop",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()

            if not inst.sg.mem.chew_loops or inst.sg.mem.chew_loops == 0 then
                inst.SoundEmitter:PlaySound(inst.sounds.chew, "chew_loop")
                inst.sg.mem.chew_loops = math.random(4, 6)
            end

            inst:PerformBufferedAction()

            inst.sg.mem.chew_loops = inst.sg.mem.chew_loops - 1

            inst.AnimState:PlayAnimation("eat_loop")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                local next_state
                if inst.sg.mem.chew_loops > 0 then
                    next_state = "eat_loop"
                    inst.sg.statemem.keep_sound = true
                else
                    next_state = "eat_pst"
                end

                inst.sg:GoToState(next_state)
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.keep_sound then
                inst.SoundEmitter:KillSound("chew_loop")
            end
        end,
    },

	State {
        name = "spawn",
        tags = {"busy"},

        onenter = function(inst, isopen)
            inst.components.locomotor:StopMoving()

            inst.components.combat:StartAttack()

            inst.Transform:SetNoFaced()

			inst.SoundEmitter:PlaySound(inst.sounds.open)
            inst.AnimState:PlayAnimation("spawn")
        end,

        timeline =
        {
            FrameEvent(0, function(inst)
                if not inst.sg.statemem.played_open then
                    inst.SoundEmitter:PlaySound(inst.sounds.spawn)
                end
            end),
            FrameEvent(4, function(inst)
                if inst.sg.statemem.played_open then
                    inst.SoundEmitter:PlaySound(inst.sounds.spawn)
                end
            end),
            FrameEvent(38, function(inst)
                if not inst.sg.statemem.played_open then
                    inst.SoundEmitter:PlaySound(inst.sounds.attack_hit)
                    inst.components.combat:DoAttack()
                end
            end),
            FrameEvent(42, function(inst)
                if inst.sg.statemem.played_open then
                    inst.SoundEmitter:PlaySound(inst.sounds.attack_hit)
                    inst.components.combat:DoAttack()
                end
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },

        onexit = function(inst)
            inst.Transform:SetSixFaced()
        end,
    },

	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", PlayablePets.DoDeath),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("open")
			if inst.DisguiseSelf then
				inst:DisguiseSelf()
			end
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = {"sleeping", "busy"},
		
		onenter = function(inst)
			inst:Hide()
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("idle1")
		end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst)
				PlayablePets.SleepHeal(inst)
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			if inst.RevealSelf then
				inst:RevealSelf()
			end
			inst.Transform:SetNoFaced()

            inst.AnimState:PlayAnimation("spawn")
			inst.SoundEmitter:PlaySound(inst.sounds.open)
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

		timeline =
        {
            FrameEvent(0, function(inst)
                if not inst.sg.statemem.played_open then
                    inst.SoundEmitter:PlaySound(inst.sounds.spawn)
                end
            end),
            FrameEvent(4, function(inst)
                if inst.sg.statemem.played_open then
                    inst.SoundEmitter:PlaySound(inst.sounds.spawn)
                end
            end),
            FrameEvent(38, function(inst)
                if not inst.sg.statemem.played_open then
                    inst.SoundEmitter:PlaySound(inst.sounds.attack_hit)
					inst.components.combat:DoAreaAttack(inst, 2, nil, nil, "strong", PlayablePets.GetExcludeTags(inst, {"chestmonster"}))
                end
            end),
            FrameEvent(42, function(inst)
                if inst.sg.statemem.played_open then
                    inst.SoundEmitter:PlaySound(inst.sounds.attack_hit)
                    inst.components.combat:DoAreaAttack(inst, 2, nil, nil, "strong", PlayablePets.GetExcludeTags(inst, {"chestmonster"}))
                end
            end),
        },

		onexit = function(inst)
			inst.Transform:SetSixFaced()
		end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}

CommonStates.AddFrozenStates(states)
CommonStates.AddWalkStates(states, {
    walktimeline =
    {
        SoundTimeEvent(0, "rifts4/mimic/mimic_chest/walk"),
        FrameEvent(10, function(inst)
            inst.components.locomotor:WalkForward()
        end),
        FrameEvent(28, function(inst)
            PlayFootstep(inst)
            inst.Physics:Stop()
        end),
    },
}, { walk = "walk" }, true)
local common_act = "walk_pst"
local move_act = "walk"
CommonStates.AddSimpleState(states, "hit", "hit", {"busy", "hit"}, "idle")
CommonStates.AddSimpleActionState(states, "eat", "eat_loop", nil, {"busy"}, "eat_loop", {
    FrameEvent(0, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.pickup) end),
    FrameEvent(0, function(inst) inst:PerformBufferedAction() end),
})
CommonStates.AddSimpleState(states, "eat_pst", "eat_pst", {"busy"}, nil, {
    FrameEvent(0, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.eat_pst) end),
    FrameEvent(4, function(inst) inst.sg:RemoveStateTag("busy") end),
})

PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, "taunt_loop", "taunt_loop", "taunt_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
		},
		
		corpse_taunt =
		{
			
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "spawn"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "taunt")
PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "taunt_pre",
	plank_idle_loop = "taunt_loop",
	plank_idle_pst = "taunt_pst",
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk",
	
	steer_pre = common_act,
	steer_idle = "idle1",
	steer_turning = common_act,
	stop_steering = common_act,
	
	row = common_act,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_pst",
	
	leap_pre = "walk_pre",
	leap_loop = "walk_loop",
	leap_pst = "walk_pst",
	
	lunge_pre = "walk_pre",
	lunge_loop = "walk",
	lunge_pst = "walk_pst",
	
	superjump_pre = "walk_pre",
	superjump_loop = "walk",
	superjump_pst = "walk_pst",
	
	castspelltime = 10,
})
	
return StateGraph("chest_mimicp", states, events, "spawn", actionhandlers)

