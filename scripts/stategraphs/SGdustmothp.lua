require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "squid"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "squid"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.DUSTMOTH_DUST, "dustoff_pre"),
	ActionHandler(ACTIONS.ATTACK, "idle")
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function PlaySoundDustoff(inst)
    inst.SoundEmitter:PlaySound(inst._sounds.dustoff)
end

local function PlaySoundClean(inst)
    inst.SoundEmitter:PlaySound(inst._sounds.clean)
end

local DUSTOFF_LOOP_AUDIO_OFFSET = 10*FRAMES
local function PlayDustoffLoopSound(inst)
    if inst.sg:HasStateTag("dusting") then
        PlaySoundDustoff(inst)
    end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local states=
{
    State
     {
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle", true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "special_atk2",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("sneeze")
        end,

        timeline = 
        {
            TimeEvent(36*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.sneeze)
            end),
        },

        events = 
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end)
        },
    },
	
	 State{
        name = "dustoff_pre",
        tags = { "busy", "dusting" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("clean_pre")
        end,

        events =
        {
            EventHandler("animover", function (inst)
                inst.sg:GoToState("dustoff_loop")
            end),
        },
    },

    State{
        name = "dustoff_loop",
        tags = { "busy", "dusting" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("clean_loop")

            inst.sg.statemem.loop_count = 4 + math.random(3)

            inst:DoTaskInTime(DUSTOFF_LOOP_AUDIO_OFFSET, PlayDustoffLoopSound)
        end,

        timeline=
        {
            TimeEvent(9*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.dustoff)
            end),
            TimeEvent(16*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.clean)
            end),

            TimeEvent(22*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.dustoff)
            end),
        },

        events =
        {
            EventHandler("animover", function (inst)
                inst.sg.statemem.loop_count = inst.sg.statemem.loop_count - 1

                if inst.sg.statemem.loop_count <= 0 then
                    inst.sg:GoToState("dustoff_pst")
                else
                    inst.AnimState:PlayAnimation("clean_loop")
                    inst:DoTaskInTime(DUSTOFF_LOOP_AUDIO_OFFSET, PlayDustoffLoopSound)
                end
            end),
        },
    },

    State{
        name = "dustoff_pst",
        tags = { "busy", "dusting" },

        onenter = function(inst)
			local ba = inst:GetBufferedAction()
            if ba ~= nil and ba.target ~= nil and ba.target:IsValid() then
				inst.DustOther(inst, ba.target)
				inst.sg.statemem.target = nil
			end
			inst.Physics:Stop()
            inst.AnimState:PlayAnimation("clean_pst")
        end,

        events =
        {
            EventHandler("animover", function (inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	State{
        name = "repair_den_pre",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            
            inst.AnimState:PlayAnimation("clean_pre")

            inst.sg.statemem.startpos = inst:GetPosition()
        end,
        
        timeline=
        {
            TimeEvent(10*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.clean)
            end),
        },

        events =
        {
            EventHandler("animover", function (inst)
                local ba = inst:GetBufferedAction()

                if ba ~= nil and ba.target ~= nil and ba.target:IsValid() then
                    inst:ClearBufferedAction()
                    inst.sg:GoToState("repair_den_loop", { target = ba.target, startpos = inst.sg.statemem.startpos })
                else
                    inst.sg:GoToState("repair_den_pst")
                end
            end),
        },
    },

    State{
        name = "repair_den_loop",
        tags = { "busy" },

        onenter = function(inst, data)
            inst.Physics:Stop()
            
            inst.AnimState:Hide("dust")
            inst.AnimState:PlayAnimation("clean_loop")

            local target = data.target

            if target ~= nil and target:IsValid() and target._start_repairing_fn ~= nil then
                target:_start_repairing_fn(inst)
            end

            inst.sg.statemem.startpos = data.startpos

            inst.sg.statemem.target = target
            inst.sg.statemem.dust_anim_loops = math.random(5, 9)

            inst.sg.statemem.sound_task1 = inst:DoTaskInTime(9*FRAMES, PlaySoundDustoff)
            inst.sg.statemem.sound_task2 = inst:DoTaskInTime(16*FRAMES, PlaySoundClean)
            inst.sg.statemem.sound_task3 = inst:DoTaskInTime(22*FRAMES, PlaySoundDustoff)
        end,

        onupdate = function(inst)
            if inst.sg.statemem.target ~= nil and inst.sg.statemem.target:IsValid() then
                local delta = inst.sg.statemem.startpos - inst:GetPosition()
                if VecUtil_LengthSq(delta.x, delta.z) > 2.25 then
                    inst.sg:GoToState("repair_den_pst")
                end
            end
        end,

        onexit = function(inst)
            inst.AnimState:Show("dust")
            
            if inst.sg.statemem.target ~= nil and inst.sg.statemem.target:IsValid() and inst.sg.statemem.target._pause_repairing_fn ~= nil then
                inst.sg.statemem.target:_pause_repairing_fn()
            end

            if inst.sg.statemem.sound_task1 ~= nil then
                inst.sg.statemem.sound_task1:Cancel()
            end
            if inst.sg.statemem.sound_task2 ~= nil then
                inst.sg.statemem.sound_task2:Cancel()
            end
            if inst.sg.statemem.sound_task3 ~= nil then
                inst.sg.statemem.sound_task3:Cancel()
            end
        end,
        
        timeline =
        {
            TimeEvent(9*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.dustoff)
            end),
            TimeEvent(16*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.clean)
            end),
            TimeEvent(22*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.dustoff)
            end),
        },

        events = 
        {
            EventHandler("animqueueover", function(inst)
                if inst.sg.statemem.dust_anim_loops > 0 then
                    inst.AnimState:PlayAnimation("clean_loop")
                    inst.sg.statemem.dust_anim_loops = inst.sg.statemem.dust_anim_loops - 1
                    
                    inst.sg.statemem.sound_task1 = inst:DoTaskInTime(9*FRAMES, PlaySoundDustoff)
                    inst.sg.statemem.sound_task2 = inst:DoTaskInTime(16*FRAMES, PlaySoundClean)
                    inst.sg.statemem.sound_task3 = inst:DoTaskInTime(22*FRAMES, PlaySoundDustoff)
                else
                    inst.AnimState:PlayAnimation("clean_pst")
                    inst.AnimState:PushAnimation("clean_pre", false)
                    inst.sg.statemem.dust_anim_loops = math.random(5, 9)
                end
            end),
            EventHandler("dustmothden_repaired", function(inst)
                inst.sg:GoToState("repair_den_pst")
            end),
        },
    },

    State{
        name = "repair_den_pst",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            
            inst.AnimState:PlayAnimation("clean_pst")
        end,

        timeline=
        {
            TimeEvent(10*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.mumble)
            end),
        },


        events =
        {
            EventHandler("animover", function (inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	State{
        name = "pickup",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("pickup")
        end,

        timeline = 
        {
            TimeEvent(10*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.eat_slide)
            end),
            TimeEvent(16*FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
        },

        events =
        {
            EventHandler("animover", function (inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State{
        name = "eat",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat")
        end,

        timeline = 
        {
            TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.eat)
            end),
        },

        events = 
        {
            EventHandler("animover", function(inst)
                inst._time_spent_stuck = 0
                
                inst:PerformBufferedAction()
                inst.sg:GoToState("idle")
            end)
        },
    },

    State{
        name = "special_atk1",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle2")
        end,

        timeline = 
        {
            TimeEvent(9*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.mumble)
            end),
            TimeEvent(29*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.mumble)
            end),
        },

        events = 
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end)
        },
    },

    State{
        name = "refuseitem",
        tags = { "busy" },

        onenter = function(inst, giver)
            inst.Physics:Stop()

            if giver ~= nil and giver:IsValid() then
                inst.Transform:SetRotation(inst:GetAngleToPoint(giver:GetPosition()))
            end

            inst.AnimState:PlayAnimation("idle2")
        end,

        timeline = 
        {
            TimeEvent(9*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.mumble)
            end),
            TimeEvent(29*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.mumble)
            end),
        },

        events = 
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end)
        },
    },
	
	State
    {
        name = "hit",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("hit")
        end,

        TimeEvent(1*FRAMES, function(inst)  
            inst.SoundEmitter:PlaySound(inst._sounds.hit)
        end),

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline = {
			TimeEvent(7*FRAMES, function(inst)  
				inst.SoundEmitter:PlaySound(inst._sounds.death)
			end),
			TimeEvent(20*FRAMES, function(inst)  
				inst.SoundEmitter:PlaySound(inst._sounds.death)
			end),
			TimeEvent(24*FRAMES, function(inst)  
				inst.SoundEmitter:PlaySound(inst._sounds.fall)
			end),
		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,
		
		timeline = {
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
			PlayablePets.SleepHeal(inst)
		end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

		timeline = {
			
		},
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}
CommonStates.AddWalkStates(states,
{
    walktimeline =
    {
        TimeEvent(0*FRAMES, function(inst)
            inst.Physics:Stop()
            inst.SoundEmitter:PlaySound(inst._sounds.slide_out)
        end),

        TimeEvent(9*FRAMES, function(inst) if math.random() < 0.3 then inst.SoundEmitter:PlaySound(inst._sounds.mumble) end end),

        TimeEvent(21*FRAMES, function(inst)
            inst.components.locomotor:WalkForward()
            inst.SoundEmitter:PlaySound(inst._sounds.slide_in)
        end),
        TimeEvent(34*FRAMES, function(inst)
            inst.Physics:Stop()
        end),
	},
},
{
    walk = "walk",
}, true)
local moveanim = "walk"
local idleanim = "idle_sit"
local actionanim = "walk_pst"
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            PlayablePets.DoWork(inst, 2)
        end),
	}, 
	actionanim, nil, nil, "idle", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(7*FRAMES, function(inst)  
				inst.SoundEmitter:PlaySound(inst._sounds.death)
			end),
			TimeEvent(20*FRAMES, function(inst)  
				inst.SoundEmitter:PlaySound(inst._sounds.death)
			end),
			TimeEvent(24*FRAMES, function(inst)  
				inst.SoundEmitter:PlaySound(inst._sounds.fall)
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(9*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.mumble)
            end),
            TimeEvent(29*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst._sounds.mumble)
            end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "sneeze"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, "idle2")
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim, pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim,
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim,
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim,
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim,
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})
	
return StateGraph("dustmothp", states, events, "idle", actionhandlers)

