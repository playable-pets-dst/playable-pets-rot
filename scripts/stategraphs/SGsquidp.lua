require("stategraphs/commonstates")
require("stategraphs/ppstates")

local MOB_TUNING = TUNING.SQUIDP

local longaction = "taunt"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	--PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


local function dimLight(inst,dim,instant,zero,time)
    local frames = time or 5 * FRAMES
    inst.eyeglow.components.fader:StopAll()
    if dim then
        if instant then
            inst.eyeglow.Light:SetRadius(TUNING.SQUID_LIGHT_DOWN_RADIUS)
            inst.eyeglow.Light:SetIntensity(TUNING.SQUID_LIGHT_DOWN_INTENSITY)    
            inst.eyeglow.Light:SetFalloff(TUNING.SQUID_LIGHT_DOWN_FALLOFF)   
        else            
            inst.eyeglow.components.fader:Fade(1, 0, frames, function(v)            
                inst.eyeglow.Light:SetIntensity(Remap(v, 1, 0, TUNING.SQUID_LIGHT_UP_INTENSITY, TUNING.SQUID_LIGHT_DOWN_INTENSITY))            
                inst.eyeglow.Light:SetFalloff(Remap(v, 1, 0, TUNING.SQUID_LIGHT_UP_FALLOFF, TUNING.SQUID_LIGHT_DOWN_FALLOFF))            
                inst.eyeglow.Light:SetRadius(Remap(v, 1, 0, TUNING.SQUID_LIGHT_UP_RADIUS, TUNING.SQUID_LIGHT_DOWN_RADIUS))           
            end)        
        end
    else
        if instant then
            inst.eyeglow.Light:SetRadius(TUNING.SQUID_LIGHT_UP_RADIUS)
            inst.eyeglow.Light:SetIntensity(TUNING.SQUID_LIGHT_UP_INTENSITY)    
            inst.eyeglow.Light:SetFalloff(TUNING.SQUID_LIGHT_UP_FALLOFF)               
        else
            if zero then
                inst.eyeglow.components.fader:Fade(0, 1, frames, function(v) 
                    inst.eyeglow.Light:SetIntensity(Remap(v, 0, 1, TUNING.SQUID_LIGHT_DOWN_INTENSITY, TUNING.SQUID_LIGHT_UP_INTENSITY))        
                    
                    inst.eyeglow.Light:SetFalloff(Remap(v, 0, 1, TUNING.SQUID_LIGHT_DOWN_FALLOFF, TUNING.SQUID_LIGHT_UP_FALLOFF))
                    
                    inst.eyeglow.Light:SetRadius(Remap(v, 0, 1, 0.01, TUNING.SQUID_LIGHT_UP_RADIUS))
                end)
            else
                inst.eyeglow.components.fader:Fade(0, 1, frames, function(v) 
                    inst.eyeglow.Light:SetIntensity(Remap(v, 0, 1, TUNING.SQUID_LIGHT_DOWN_INTENSITY, TUNING.SQUID_LIGHT_UP_INTENSITY))        
                    
                    inst.eyeglow.Light:SetFalloff(Remap(v, 0, 1, TUNING.SQUID_LIGHT_DOWN_FALLOFF, TUNING.SQUID_LIGHT_UP_FALLOFF))
                    
                    inst.eyeglow.Light:SetRadius(Remap(v, 0, 1, TUNING.SQUID_LIGHT_DOWN_RADIUS, TUNING.SQUID_LIGHT_UP_RADIUS))            
                end)
            end
        end
    end
end

local function testExtinguish(inst)
    if inst:HasTag("swimming") then 
        if inst.components.burnable:IsBurning() then inst.components.burnable:Extinguish() end
    end
end

local function UpdateRunSpeed(inst)
    local rod = inst.components.oceanfishable ~= nil and inst.components.oceanfishable:GetRod() or nil
    local check_tension = rod ~= nil and math.abs(anglediff(inst.Transform:GetRotation(), inst:GetAngleToPoint(rod.Transform:GetWorldPosition()))) > 90
    local tension_mod = check_tension and (1 - math.min(0.8, rod.components.oceanfishingrod:GetTensionRating())) or 1

    inst.components.locomotor.runspeed = MOB_TUNING.RUNSPEED * tension_mod
end


local function setdivelayering(inst,under)
    local dive = false
    if inst:HasTag("swimming") and under then
        dive = true
    end

    if dive and not inst.under then
        inst.AnimState:SetSortOrder(ANIM_SORT_ORDER_BELOW_GROUND.UNDERWATER)
        inst.AnimState:SetLayer(LAYER_BELOW_GROUND)        
        inst.under = true
    else
        inst.AnimState:SetSortOrder(0)
        inst.AnimState:SetLayer(LAYER_WORLD)
        inst.under = nil
    end
end

local function RestoreCollidesWith(inst)
    inst.Physics:CollidesWith(COLLISION.WORLD 
                        + COLLISION.OBSTACLES
                        + COLLISION.SMALLOBSTACLES
                        + COLLISION.CHARACTERS
                        + COLLISION.GIANTS)
end

local states=
{
   State{
        name = "idle",
        tags = { "idle", "canrotate" },
        onenter = function(inst, playanim)
            setdivelayering(inst, false)
            
            inst.Physics:Stop()
            local anim = "idle"
			if inst.noactions then
				anim = "idle"
            elseif math.random()<0.6 then
                if math.random()<0.5 then
                    anim = "idle2"
                else
                    anim = "idle3"
                end                                
            end
            inst.AnimState:PlayAnimation(anim, true)
            inst.sg:SetTimeout(2*math.random()+.5)
        end,
        
        timeline =
        {
            
            TimeEvent(8*FRAMES, function(inst)
               if inst.AnimState:IsCurrentAnimation("idle3") then
                    inst.SoundEmitter:PlaySound("hookline/creatures/squid/eye")
               end
            end),             
            TimeEvent(10*FRAMES, function(inst)
               if inst.AnimState:IsCurrentAnimation("idle3") and inst.eyeglow then
                    inst.eyeglow.Light:Enable(false)
               end
            end),
            TimeEvent(20*FRAMES, function(inst)
               if inst.AnimState:IsCurrentAnimation("idle3") and inst.eyeglow then
                    inst.eyeglow.Light:Enable(true)
               end
            end),
            TimeEvent(21*FRAMES, function(inst)
               if inst.AnimState:IsCurrentAnimation("idle3") then
                    inst.SoundEmitter:PlaySound("hookline/creatures/squid/eye")
               end
            end),
        },
        onexit = function(inst)
			if inst.eyeglow and not inst.noactions then
				inst.eyeglow.Light:Enable(true)
			elseif inst.eyeglow and inst.noactions then
				inst.eyeglow.Light:Enable(false)
			end
        end,       

        events =
        {            
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },                 
    },
	
	State{
        name = "spawn",
        tags = { "busy" },

        onenter = function(inst)
			inst:Show()
            dimLight(inst,false,false,true,20*FRAMES)            
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("spawn", false)
			inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
        end,
		
		onexit = function(inst)
			inst.components.locomotor:SetAllowPlatformHopping(true)
			inst:Show()
			inst.noactions = nil
			inst.taunt2 = false
			inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
			inst.components.health:SetInvincible(false)
			inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
		end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "despawn",
        tags = { "busy" },

        onenter = function(inst)
            dimLight(inst,true)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("despawn", false)
        end,

        events =
        {
            EventHandler("animover", function(inst) inst:Remove() end),
        },
    },    
	
	State{
        name = "attack",
        tags = { "attack", "busy" },

        onenter = function(inst, target)
            inst.sg.statemem.target = target
            inst.Physics:Stop()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("attack")
        end,

        onexit = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.Physics:ClearMotorVelOverride()
        end,

        timeline =
        {
        
            TimeEvent(10*FRAMES, function(inst)
                inst.components.combat:DoAreaAttack(inst, inst.components.combat.hitrange, nil, nil, nil, PlayablePets.GetExcludeTags(inst)) 
                inst.SoundEmitter:PlaySound(inst.sounds.attack)
                inst.components.locomotor:EnableGroundSpeedMultiplier(false)            
                inst.Physics:SetMotorVelOverride(3,0,0)
            end),

            TimeEvent(18*FRAMES, function(inst)
                inst.components.combat:DoAreaAttack(inst, inst.components.combat.hitrange, nil, nil, nil, PlayablePets.GetExcludeTags(inst)) 
            end),

            TimeEvent(26*FRAMES, function(inst)
                inst.components.combat:DoAreaAttack(inst, inst.components.combat.hitrange, nil, nil, nil, PlayablePets.GetExcludeTags(inst)) 
                inst.components.locomotor:Stop()
            end),

        },

        events =
        {
            
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "special_atk3",
        tags = { "attack", "busy" },

        onenter = function(inst, targetpos)
            if inst.noactions then
                inst.sg:GoToState("idle")
            end
			if targetpos then
                inst.sg.statemem.targetpos = targetpos
                inst:ForceFacePoint(targetpos)
            end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("flee")
        end,

        timeline =
        {   
            TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spit) end),
            TimeEvent(15*FRAMES, function(inst)
                if inst.sg.statemem.targetpos then
                    inst:ForceFacePoint(inst.sg.statemem.targetpos)
                    inst.sg.statemem.targetpos = PlayablePets.GetClampedPosition(inst, inst.sg.statemem.targetpos, MOB_TUNING.SPECIAL_MAX_DIST)       
                    inst:LaunchProjectile(inst.sg.statemem.targetpos)
                end
            end),
        },
		
		onexit = function(inst)
			inst.components.playablepet:CooldownAbility(PP_ABILITY_SLOTS.SKILL3, MOB_TUNING.SPECIAL_CD)
		end,

        events =
        {
            EventHandler("animover", function(inst)  inst.sg:GoToState("idle") end),
        },
    },    

    State{
        name = "gobble",
        tags = { "busy" },

        onenter = function(inst, ent)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("gobble_pre")
            setdivelayering(inst,true)
			if ent then
				inst.sg.statemem.fish_to_eat = ent
			end
        end,      


        timeline =
        {
            TimeEvent(12*FRAMES, function(inst) 
                inst.components.locomotor:Stop()
                inst.components.locomotor:EnableGroundSpeedMultiplier(false)
                inst.Physics:SetMotorVelOverride(20,0,0)
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)             
                inst.SoundEmitter:PlaySound(inst.sounds.bite) 
                local success = math.random()
                if success >= 0.7 then
                    inst.sg:GoToState("gobble_fail")
                else
                    inst.sg:GoToState("gobble_success", inst.sg.statemem.fish_to_eat)
                end
            end),
        },       
		
        onexit = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.Physics:ClearMotorVelOverride()
			inst.sg.statemem.fish_to_eat = nil
            setdivelayering(inst,false)
        end,         
    },

    State{
        name = "gobble_success",
        tags = { "busy" },

        onenter = function(inst, fish)
            setdivelayering(inst,true)            
            inst.foodtarget = fish or nil			
			
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)            
            inst.Physics:SetMotorVelOverride(10,0,0)
            inst.AnimState:PlayAnimation("gobble_success")
            inst.SoundEmitter:PlaySound(inst.sounds.gobble)
        end,

        timeline =
        {
            TimeEvent(6*FRAMES, function(inst) 
                inst.components.locomotor:Stop()
                inst.components.locomotor:EnableGroundSpeedMultiplier(true)
                inst.Physics:ClearMotorVelOverride()
				if inst.foodtarget then
					if inst.foodtarget:HasTag("player") then
						inst.foodtarget.components.health:Kill()
						inst.components.hunger:DoDelta(30, false)
						inst.components.health:DoDelta(12, false)
					else
						inst.components.hunger:DoDelta(20, false)
						inst.components.health:DoDelta(2, false)
						inst.foodtarget:Remove()
					end
				end
            end),

            TimeEvent(10*FRAMES, function(inst)
                setdivelayering(inst,false)
            end),
            
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
        
        onexit = function(inst)
			inst.foodtarget = nil
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.Physics:ClearMotorVelOverride()
            setdivelayering(inst,false)
        end,        
    },

    State{
        name = "gobble_fail",
        tags = { "busy" },

        onenter = function(inst)
            setdivelayering(inst,true)
            inst.Physics:Stop()    
            inst.AnimState:PlayAnimation("gobble_fail")

            -- If the squid misses, give up this target and choose another from it's school 
            -- so that it doesn't chase it off into no mans land away from the rest of the squid
            if inst.foodtarget then
                local herd = inst.foodtarget.components.herdmember:GetHerd()
                local list = {}
                if herd then
                    for k,v in pairs(herd.components.herd.members)do
                        if k ~= inst.foodtarget then
                            table.insert(list,k)
                        end
                    end
                end
                if #list > 0 then
                    inst.foodtarget = list[math.random(1,#list)]
                end
            end
        end,

        timeline =
        {
            TimeEvent(7*FRAMES, function(inst)
                setdivelayering(inst,false)
            end),
        },        

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
        
        onexit = function(inst)
            setdivelayering(inst,false)
        end,        
    },
	
	State{
        name = "eat",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk_pre")
            inst.AnimState:PushAnimation("atk", false)
			inst.AnimState:PushAnimation("atk_pst", false)
			
        end,

		timeline=
        {
			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.bite) end),
			TimeEvent(14*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
		},
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "taunt",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {
			TimeEvent(8 * FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.taunt) inst:PerformBufferedAction() end),   
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {
			TimeEvent(8 * FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.taunt) end),   
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst, cb)
			if (inst.components.amphibiouscreature and inst.components.amphibiouscreature.in_water) and not inst.noactions then
				inst.Physics:Stop()
				inst.noactions = true
				inst.components.health:SetInvincible(true)
				inst.AnimState:PlayAnimation("despawn")
			elseif inst.noactions then
				inst.sg:GoToState("spawn")
			else
				inst.sg:GoToState("idle")
			end            
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) 
				if (inst.components.amphibiouscreature and inst.components.amphibiouscreature.in_water) then
					inst:Hide()
					inst.components.locomotor:SetAllowPlatformHopping(false)
					inst.Physics:CollidesWith(COLLISION.LIMITS) 
					inst.sg:GoToState("idle") 
				end
			end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.noactions = nil
			inst.SoundEmitter:PlaySound(inst.sounds.death)
			inst.eyeglow.Light:Enable(false)
            inst.AnimState:PlayAnimation("dead")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.sleep)
				PlayablePets.SleepHeal(inst)
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

		timeline = {
			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline/creatures/squid/run") end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline/creatures/squid/run") end),
			TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("hookline/creatures/squid/run") end),
		},
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	-- RUN STATES START HERE
    State{
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)           
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")

            --UpdateRunSpeed(inst)

            --inst.AnimState:SetSortOrder(ANIM_SORT_ORDER_BELOW_GROUND.UNDERWATER)
            --inst.AnimState:SetLayer(LAYER_BELOW_GROUND)            
        end,

        onupdate = function(inst)
            --UpdateRunSpeed(inst)
        end,

        timeline = 
        {
            TimeEvent(1 * FRAMES, function(inst)  
                if inst:HasTag("swimming") then
                    dimLight(inst,true)
                end
            end), 
            TimeEvent(3 * FRAMES, function(inst) 
                testExtinguish(inst) 
                setdivelayering(inst,true)
            end),                
        },        

        onexit = function(inst)           
            if inst.staydim then
                inst.staydim = nil                
            else
                if inst:HasTag("swimming") then
                    dimLight(inst,false)
                    --inst.AnimState:SetSortOrder(ANIM_SORT_ORDER_BELOW_GROUND.UNDERWATER)
                    --inst.AnimState:SetLayer(LAYER_WORLD)                     
                end
            end
            setdivelayering(inst,false)
            --RestorRunSpeed(inst)
        end,        

        events =
        {
            EventHandler("animover", function(inst)
                inst.staydim = true 
                inst.sg:GoToState("run") 
            end),
        },
    },

    State{
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            setdivelayering(inst,true)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_loop")
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())

            if inst:HasTag("swimming") and not inst.noactions then
                inst.waketask = inst:DoPeriodicTask(0.3,function()
                    local wake = SpawnPrefab("wake_small")
                    local theta = inst.Transform:GetRotation() * DEGREES
                    local dist = 1
                    local offset = Vector3(dist * math.cos( theta ), 0, -dist * math.sin( theta ))
                    local pos = Vector3(inst.Transform:GetWorldPosition()) + offset
                    wake.Transform:SetPosition(pos.x,pos.y,pos.z)
                    wake.Transform:SetRotation(inst.Transform:GetRotation()-90)
                end)
            end
            UpdateRunSpeed(inst)
        end,

        onupdate = function(inst)
			if inst:HasTag("swimming") and not inst.noactions then
				local ent = FindEntity(inst, 1, nil, {"oceanfish"})
				if ent then
					inst.sg:GoToState("gobble", ent)
				end
			end
            --UpdateRunSpeed(inst)
        end,        

        timeline =
        {
            
            TimeEvent(0, function(inst)         
				if not inst.noactions then
					if inst:HasTag("swimming") then 
						inst.Physics:Stop()  
					else
						PlayFootstep(inst,0.2)
					end     
				end
            end),
            TimeEvent(2*FRAMES, function(inst) 
				if not inst.noactions then
					if not inst:HasTag("swimming") then
						inst.SoundEmitter:PlaySound("hookline/creatures/squid/run") 
					end 
				end	
            end),

            TimeEvent(4 * FRAMES, function(inst)
				if not inst.noactions then
					if inst:HasTag("swimming") then 
						inst.SoundEmitter:PlaySound(inst.sounds.swim)
					else
						PlayFootstep(inst,0.2)                    
					end
				end
            end),
            TimeEvent(6*FRAMES, function(inst) 
				if not inst.noactions then
					if not inst:HasTag("swimming") then
						inst.SoundEmitter:PlaySound("hookline/creatures/squid/run") 
					end 
				end	
            end),
            TimeEvent(7 * FRAMES, function(inst) 
				if not inst.noactions then
					if inst:HasTag("swimming") then 
						inst.components.locomotor:RunForward() 
					end 
				end	
            end),
            TimeEvent(8*FRAMES, function(inst) 
				if not inst.noactions then
					if not inst:HasTag("swimming") then
						inst.SoundEmitter:PlaySound("hookline/creatures/squid/run") 
					end 
				end	
            end),
            TimeEvent(10*FRAMES, function(inst) 
				if not inst.noactions then
					if not inst:HasTag("swimming") then
						inst.SoundEmitter:PlaySound("hookline/creatures/squid/run") 
					end 
				end	
            end),

        },
             
        onexit = function(inst)     
            if inst.waketask then
                inst.waketask:Cancel()
                inst.waketask = nil
            end
            if inst.staydim then
                inst.staydim = nil                
            else
                if inst:HasTag("swimming") then
                    dimLight(inst,false)                 
                end
            end
            setdivelayering(inst,false)
        end,

        ontimeout = function(inst)
            inst.staydim = true 
            inst.sg:GoToState("run") 
        end,
    },

    State{
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst)
            setdivelayering(inst,true)
            inst.components.locomotor:StopMoving()            
            inst.AnimState:PlayAnimation("run_pst")        
        end,

        timeline =
        {
            TimeEvent(7 * FRAMES, function(inst)
                setdivelayering(inst,false)
            end),
        },

        onexit = function(inst)
            setdivelayering(inst,false)            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },   
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"run_pst", nil, nil, "taunt", "run_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) inst.eyeglow.Light:Enable(false) end),
		},
		
		corpse_taunt =
		{
			TimeEvent(8 * FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.taunt) end),  
		},
	
	},
	--anims = 
	{
		corpse = "dead",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "taunt")
PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = "jump", loop = "jump_loop", pst = "jump_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "run_pst",
	plank_idle_loop = "idle",
	plank_idle_pst = "run_pst",
	
	plank_hop_pre = "jump",
	plank_hop = "jump_loop",
	
	steer_pre = "run_pst",
	steer_idle = "idle",
	steer_turning = "taunt",
	stop_steering = "run_pst",
	
	row = "run_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "jump",
	leap_loop = "jump_loop",
	leap_pst = "jump_pst",
	
	lunge_pre = "run_pst",
	lunge_loop = "run_pst",
	lunge_pst = "run_pst",
	
	superjump_pre = "catching_pre",
	superjump_loop = "catching_loop",
	superjump_pst = "catching_pst",
	
	castspelltime = 10,
})
    
CommonStates.AddAmphibiousCreatureHopStates(states, 
{ -- config
    swimming_clear_collision_frame = 9 * FRAMES,
},
{ -- anims
},
{ -- timeline
    hop_pre =
    {
        TimeEvent(0, function(inst) 
            if inst:HasTag("swimming") then 
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition()) 
            end
        end),
    },
    hop_pst = {
        TimeEvent(4 * FRAMES, function(inst) 
            if inst:HasTag("swimming") then 
                inst.components.locomotor:Stop()
                SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition()) 
                testExtinguish(inst)
                
            end
        end),
        TimeEvent(6 * FRAMES, function(inst) 
            if not inst:HasTag("swimming") then 
                inst.components.locomotor:StopMoving()
            end
        end),
        TimeEvent(9 * FRAMES, function(inst) 
            setdivelayering(inst,true)
        end),        
        TimeEvent(17 * FRAMES, function(inst)
            setdivelayering(inst)
        end),      
    }
})	
	
return StateGraph("squidp", states, events, "idle", actionhandlers)

