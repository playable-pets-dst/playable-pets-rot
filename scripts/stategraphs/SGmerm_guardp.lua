require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function tool_or_work(inst)    
    local hand_item = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    return (hand_item ~= nil and hand_item.components.tool ~= nil and "use_tool")
        or "work"
end

local function tool_or_attack(inst)
    local hand_item = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    return (hand_item ~= nil and "use_tool")
        or "attack"   
end

local PICK_PROP_RANGE = 1 + .1 --with error
local PICK_GOLD_RANGE = 1
local GOLD_DIVE_RANGE_CLAMPED = 4.5
local GOLD_DIVE_RANGE = GOLD_DIVE_RANGE_CLAMPED + .1 --with error
local POSING_MASS = 5000
local DEFAULT_MASS = 50


local function IsMinigameItem(inst)
    return inst:HasTag("minigameitem")
end

local longaction = "pickup"
local shortaction = "pickup"
local workaction = tool_or_work
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction, true)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.SLEEPIN,  function(inst, action)
            if action.invobject ~= nil then
                if action.invobject.onuse ~= nil then
                    --action.invobject.onuse(inst)
                end
                return "bedroll"
            else
                return "tent"
            end
        end),
    ActionHandler(ACTIONS.ROW, "row2"), --why is this here??
    ActionHandler(ACTIONS.ATTACK, tool_or_attack)
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function SetSleeperAwakeState(inst, forcetaunt)
    if inst.components.grue ~= nil then
        inst.components.grue:RemoveImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:StopIgnoringAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Enable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(true)
        inst.components.playercontroller:Enable(true)
    end
    inst:OnWakeUp()
    inst.components.inventory:Show()
    inst:ShowActions(true)	
	end
	
local function SetSleeperSleepState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:AddImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:IgnoreAll("sleeping")
    end
	
	if inst.components.shedder ~= nil then
	inst.components.shedder:StopShedding()
	end
	
    if inst.components.firebug ~= nil then
        inst.components.firebug:Disable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(false)
        inst.components.playercontroller:Enable(false)
    end
    inst:OnSleepIn()
    inst.components.inventory:Hide()
    inst:PushEvent("ms_closepopups")
    inst:ShowActions(false)
end

local function DoAOEAttack(inst, dist, radius)
    local hit = false
    --inst.components.combat.ignorehitrange = true
    local x0, y0, z0 = inst.Transform:GetWorldPosition()
    local angle = (inst.Transform:GetRotation() + 90) * DEGREES
    local sinangle = math.sin(angle)
    local cosangle = math.cos(angle)
    local x = x0 + dist * sinangle
    local z = z0 + dist * cosangle
    for i, v in ipairs(TheSim:FindEntities(x, y0, z, radius * 5, { "_combat" }, { "flying", "shadow", "ghost", "FX", "NOCLICK", "DECOR", "INLIMBO", "playerghost" })) do
        if v:IsValid() and not v:IsInLimbo() and
            not (v.components.health ~= nil and v.components.health:IsDead()) then
            local range = radius + v:GetPhysicsRadius(.5)
            if v:GetDistanceSqToPoint(x, y0, z) < range * range and inst.components.combat:CanTarget(v) then
                --dummy redirected so that players don't get red blood flash
                v:PushEvent("attacked", { attacker = inst, damage = 0, redirected = v })
                v:PushEvent("knockback", { knocker = inst, radius = radius + dist, propsmashed = true })
                hit = true
            end
        end
    end
    --inst.components.combat.ignorehitrange = false
    if hit then
        local prop = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        if prop ~= nil then
            dist = dist + radius - .5
            return { prop = prop, pos = Vector3(x0 + dist * sinangle, y0, z0 + dist * cosangle) }
        end
    end
end

local events=
{
    CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
    PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.OnLocomoteAdvanced(),
	EventHandler("transformnormal", function(inst) if not inst.components.health:IsDead() then inst.sg:GoToState("transformNormal") end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
    CommonHandlers.OnAttack(),
    CommonHandlers.OnAttacked(),
	PP_CommonHandlers.AddCommonHandlers(),
	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}

local function DoAOEAttack(inst, dist, radius)
    local hit = false
    --inst.components.combat.ignorehitrange = true
    local x0, y0, z0 = inst.Transform:GetWorldPosition()
    local angle = (inst.Transform:GetRotation() + 90) * DEGREES
    local sinangle = math.sin(angle)
    local cosangle = math.cos(angle)
    local x = x0 + dist * sinangle
    local z = z0 + dist * cosangle
    for i, v in ipairs(TheSim:FindEntities(x, y0, z, radius * 5, { "_combat" }, { "flying", "shadow", "ghost", "FX", "NOCLICK", "DECOR", "INLIMBO", "playerghost" })) do
        if v:IsValid() and not v:IsInLimbo() and
            not (v.components.health ~= nil and v.components.health:IsDead()) then
            local range = radius + v:GetPhysicsRadius(.5)
            if v:GetDistanceSqToPoint(x, y0, z) < range * range and inst.components.combat:CanTarget(v) then
                --dummy redirected so that players don't get red blood flash
                v:PushEvent("attacked", { attacker = inst, damage = 0, redirected = v })
                v:PushEvent("knockback", { knocker = inst, radius = radius + dist, propsmashed = true })
                hit = true
            end
        end
    end

    if hit then
        local prop = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        if prop ~= nil then
            dist = dist + radius - .5
            return { prop = prop, pos = Vector3(x0 + dist * sinangle, y0, z0 + dist * cosangle) }
        end
    end
end

local function go_to_idle(inst)
    inst.sg:GoToState("idle")
end


local states=
{
	
	State{
        name = "bedroll",
        tags = { "bedroll", "busy", "nomorph" },

        onenter = function(inst)
            inst.components.locomotor:Stop()

            local failreason =
                (TheWorld.state.isday and
                    (TheWorld:HasTag("cave") and "ANNOUNCE_NODAYSLEEP_CAVE" or "ANNOUNCE_NODAYSLEEP")
                )
                --or (IsNearDanger(inst) and "ANNOUNCE_NODANGERSLEEP")
                -- you can still sleep if your hunger will bottom out, but not absolutely
                or (inst.components.hunger.current < TUNING.CALORIES_MED and "ANNOUNCE_NOHUNGERSLEEP")
                or (inst.components.beaverness ~= nil and inst.components.beaverness:IsStarving() and "ANNOUNCE_NOHUNGERSLEEP")
                or nil

            if failreason ~= nil then
                inst:PushEvent("performaction", { action = inst.bufferedaction })
                inst:ClearBufferedAction()
                inst.sg:GoToState("idle")
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, failreason))
                end
                return
            end

            inst.AnimState:PlayAnimation("sleep_pre")
            inst.AnimState:PushAnimation("sleep_loop", false)

            SetSleeperSleepState(inst)
        end,

        timeline =
        {
            TimeEvent(20 * FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve/wilson/use_bedroll")
            end),
        },

        events =
        {
            EventHandler("firedamage", function(inst)
                if inst.sg:HasStateTag("sleeping") then
                    inst.sg.statemem.iswaking = true
                    inst.sg:GoToState("wakeup")
                end
            end),
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    if TheWorld.state.isday or
                        (inst.components.health ~= nil and inst.components.health.takingfiredamage) or
                        (inst.components.burnable ~= nil and inst.components.burnable:IsBurning()) then
                        inst:PushEvent("performaction", { action = inst.bufferedaction })
                        inst:ClearBufferedAction()
                        inst.sg.statemem.iswaking = true
                        inst.sg:GoToState("wakeup")
                    elseif inst:GetBufferedAction() then
                        inst:PerformBufferedAction() 
                        if inst.components.playercontroller ~= nil then
                            inst.components.playercontroller:Enable(true)
                        end
                        inst.sg:AddStateTag("sleeping")
                        inst.sg:AddStateTag("silentmorph")
                        inst.sg:RemoveStateTag("nomorph")
                        inst.sg:RemoveStateTag("busy")
                        inst.AnimState:PlayAnimation("sleep_loop", true)
                    else
                        inst.sg.statemem.iswaking = true
                        inst.sg:GoToState("wakeup")
                    end
                end
            end),
        },

        onexit = function(inst)
            if inst.sleepingbag ~= nil then
                --Interrupted while we are "sleeping"
                inst.sleepingbag.components.sleepingbag:DoWakeUp(true)
                inst.sleepingbag = nil
                SetSleeperAwakeState(inst)
            elseif not inst.sg.statemem.iswaking then
                --Interrupted before we are "sleeping"
                SetSleeperAwakeState(inst)
            end
        end,
    },

    State{
        name = "use_tool",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("work")
        end,

        timeline =
        {
            TimeEvent(14 * FRAMES, function(inst)
                local act = inst:GetBufferedAction()
                local target = act.target
                local tool = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) 
                if target and target:IsValid() and target.components.workable and tool and tool.components.tool then
                    target.components.workable:WorkedBy(inst, tool.components.tool:GetEffectiveness(act.action))
                    tool:OnUsedAsItem(act.action, inst, target)
                end

                if target ~= nil and act.action == ACTIONS.MINE then
                    PlayMiningFX(inst, target)
                end

                if target ~= nil and  target:HasTag("farm_debris") and act.action == ACTIONS.DIG then
                    inst.SoundEmitter:PlaySound("dontstarve/wilson/dig")
                end

                if act.action == ACTIONS.TILL then
                    inst.SoundEmitter:PlaySound("dontstarve/wilson/dig")
                end

                if target ~= nil and target:HasTag("stump") and act.action == ACTIONS.DIG then
                    inst.SoundEmitter:PlaySound("dontstarve/wilson/use_axe_tree")
                end                
            
                PlayablePets.DoWork(inst, 2)
            end),
        },

        events =
        {
            EventHandler("animover", go_to_idle),
        },
    },

    State{

        name = "row2",
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("row_pre",false)
            inst.AnimState:PushAnimation("row_loop",false)
            inst.AnimState:PushAnimation("row_pst",false)
           -- inst.sg:SetTimeout(4)
        end,

        timeline =
        {

            TimeEvent(8*FRAMES, function(inst)
               inst:PerformBufferedAction()
               inst.SoundEmitter:PlaySound(inst.sounds.row or inst.sounds.attack)
            end),
            TimeEvent(13*FRAMES, function(inst)
               inst.SoundEmitter:PlaySound(inst.sounds.row or inst.sounds.attack)
            end),     
        },

        ontimeout = function(inst)
        
            inst:PerformBufferedAction()
            inst.sg:GoToState("idle")
        end,

        events=
        {
            EventHandler("animqueueover", function (inst)
                inst.sg:GoToState("idle")
            end),
        }
    },
	
	State{
        name = "tent",
        tags = { "tent", "busy", "silentmorph" },

        onenter = function(inst)
            inst.components.locomotor:Stop()

            local target = inst:GetBufferedAction().target
            local siesta = target:HasTag("siestahut")
            local failreason =
                (siesta ~= TheWorld.state.isday and
                    (siesta
                    and (TheWorld:HasTag("cave") and "ANNOUNCE_NONIGHTSIESTA_CAVE" or "ANNOUNCE_NONIGHTSIESTA")
                    or (TheWorld:HasTag("cave") and "ANNOUNCE_NODAYSLEEP_CAVE" or "ANNOUNCE_NODAYSLEEP"))
                )
                or (target.components.burnable ~= nil and
                    target.components.burnable:IsBurning() and
                    "ANNOUNCE_NOSLEEPONFIRE")
                --or (IsNearDanger(inst) and "ANNOUNCE_NODANGERSLEEP")
                -- you can still sleep if your hunger will bottom out, but not absolutely
                or (inst.components.hunger.current < TUNING.CALORIES_MED and "ANNOUNCE_NOHUNGERSLEEP")
                or (inst.components.beaverness ~= nil and inst.components.beaverness:IsStarving() and "ANNOUNCE_NOHUNGERSLEEP")
                or nil

            if failreason ~= nil then
                inst:PushEvent("performaction", { action = inst.bufferedaction })
                inst:ClearBufferedAction()
                inst.sg:GoToState("idle")
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, failreason))
                end
                return
            end

            inst.AnimState:PlayAnimation("pig_pickup")
            inst.sg:SetTimeout(11 * FRAMES)

            SetSleeperSleepState(inst)
        end,

        ontimeout = function(inst)
            local bufferedaction = inst:GetBufferedAction()
            if bufferedaction == nil then
                inst.AnimState:PlayAnimation("pig_pickup")
                inst.sg:GoToState("idle", true)
                return
            end
            local tent = bufferedaction.target
            if tent == nil or
                not tent:HasTag("tent") or
                tent:HasTag("hassleeper") or
                tent:HasTag("siestahut") ~= TheWorld.state.isday or
                (tent.components.burnable ~= nil and tent.components.burnable:IsBurning()) then
                --Edge cases, don't bother with fail dialogue
                --Also, think I will let smolderig pass this one
                inst:PushEvent("performaction", { action = inst.bufferedaction })
                inst:ClearBufferedAction()
                inst.AnimState:PlayAnimation("pig_pickup")
                inst.sg:GoToState("idle", true)
            else
                inst:PerformBufferedAction()
                inst.components.health:SetInvincible(true)
                inst:Hide()
                if inst.Physics ~= nil then
                    inst.Physics:Teleport(inst.Transform:GetWorldPosition())
                end
                if inst.DynamicShadow ~= nil then
                    inst.DynamicShadow:Enable(false)
                end
                inst.sg:AddStateTag("sleeping")
                inst.sg:RemoveStateTag("busy")
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:Enable(true)
                end
            end
        end,

        onexit = function(inst)
            inst.components.health:SetInvincible(false)
            inst:Show()
            if inst.DynamicShadow ~= nil then
                inst.DynamicShadow:Enable(true)
            end
            if inst.sleepingbag ~= nil then
                --Interrupted while we are "sleeping"
                inst.sleepingbag.components.sleepingbag:DoWakeUp(true)
                inst.sleepingbag = nil
                SetSleeperAwakeState(inst)
            elseif not inst.sg.statemem.iswaking then
                --Interrupted before we are "sleeping"
                SetSleeperAwakeState(inst)
            end
        end,
    },
	
	State{
        name = "wakeup",
        tags = { "busy", "waking", "nomorph" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(false)
            end
            if inst.AnimState:IsCurrentAnimation("sleep_loop") or
                inst.AnimState:IsCurrentAnimation("bedroll_sleep_loop") then
                inst.AnimState:PushAnimation("sleep_pst")
            elseif not (inst.AnimState:IsCurrentAnimation("bedroll_wakeup") or
                        inst.AnimState:IsCurrentAnimation("wakeup")) then
                inst.AnimState:PlayAnimation("sleep_pst")
            end
            if not inst:IsHUDVisible() then
                --Touch stone rez
                inst.sg.statemem.isresurrection = true
                inst.sg:AddStateTag("nopredict")
                inst.sg:AddStateTag("silentmorph")
                inst.sg:RemoveStateTag("nomorph")
                inst.components.health:SetInvincible(false)
                inst:ShowHUD(false)
                inst:SetCameraDistance(12)
            end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            SetSleeperAwakeState(inst)
            if inst.sg.statemem.isresurrection then
                --Touch stone rez
                inst:ShowHUD(true)
                inst:SetCameraDistance()
                SerializeUserSession(inst)
            end
        end,
    },
	
	State
	{	
		name = "death",
        tags = {"busy", "pausepredict", "nomorph"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()

            inst.AnimState:PlayAnimation("death")
			inst.SoundEmitter:PlaySound(inst.sounds.death)
			
			inst.components.inventory:DropEverything(true)
			inst.components.lootdropper:DropLoot(inst:GetPosition())

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
	},	
	
    State{
        name= "funnyidle",
        tags = {"idle"},
        
        onenter = function(inst)
			inst.Physics:Stop()
            local daytime = not TheWorld.state.isnight
			inst.SoundEmitter:PlaySound(inst.sounds.attack)            
            if inst.components.follower.leader and inst.components.follower:GetLoyaltyPercent() < 0.05 then
                inst.AnimState:PlayAnimation("hungry")
                inst.SoundEmitter:PlaySound("dontstarve/wilson/hungry")
            elseif inst:HasTag("guard") then
                inst.AnimState:PlayAnimation("idle_angry")
            elseif daytime then
                if inst.components.combat.target then
                    inst.AnimState:PlayAnimation("idle_angry")
                elseif inst.components.follower.leader and inst.components.follower:GetLoyaltyPercent() > 0.3 then
                    inst.AnimState:PlayAnimation("idle_happy")
                else
                    inst.AnimState:PlayAnimation("idle_creepy")
                end
            else
                inst.AnimState:PlayAnimation("idle_scared")
            end
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
	
	State{
        name= "special_atk1",
        tags = {"idle", "busy"},
        
        onenter = function(inst)
			inst.Physics:Stop()
            local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			if hands then
				inst.AnimState:Show("ARM_normal")
                inst.AnimState:Hide("ARM_carry_up")
			end
            if inst:HasTag("monkey") then
                inst.components.talker:Say(STRINGS["MONKEY_COMMAND"][math.random(1,#STRINGS["MONKEY_COMMAND"])])
                inst.SoundEmitter:PlaySound(inst.sounds.taunt)
            else
			    inst.SoundEmitter:PlaySound(inst.sounds.attack)
            end
            inst.AnimState:PlayAnimation("idle_angry")
        end,

        onexit = function(inst)
			local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			if hands and not hands:HasTag("book") then
				inst.AnimState:Hide("ARM_normal")
                inst.AnimState:Show("ARM_carry_up")
			end
		end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
	
	State{
        name= "special_atk2",
        tags = {"idle", "busy"},
        
        onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("walk_pst")
			if inst.shouldwalk then
				inst.shouldwalk = false
			elseif not inst.shouldwalk then
				inst.shouldwalk = true
			end
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },

    State{
        name = "dive",
        tags = {"busy", "nomorph", },

        onenter = function(inst)
            local platform = inst:GetCurrentPlatform()
            if platform then
                local pt = Vector3(inst.Transform:GetWorldPosition())
                local angle = platform:GetAngleToPoint(pt)
                inst.Transform:SetRotation(angle)
            end

            inst.AnimState:PlayAnimation("dive")
            inst.Physics:Stop()
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)
        end,

        timeline =
        {
            TimeEvent(10*FRAMES, function(inst)

                inst.sg.statemem.collisionmask = inst.Physics:GetCollisionMask()
                inst.Physics:SetCollisionMask(COLLISION.GROUND)
                if not TheWorld.ismastersim then
                    inst.Physics:SetLocalCollisionMask(COLLISION.GROUND)
                end

                inst.Physics:SetMotorVelOverride(5,0,0)
            end),

            TimeEvent(30*FRAMES, function(inst)
                
            end),
        },

        onexit = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.Physics:ClearMotorVelOverride()
            
            inst.Physics:ClearLocalCollisionMask()
            if inst.sg.statemem.collisionmask ~= nil then
                inst.Physics:SetCollisionMask(inst.sg.statemem.collisionmask)
            end   
        end,

        events=
        {
            EventHandler("animover", function(inst)
                if TheWorld.Map:IsVisualGroundAtPoint(inst.Transform:GetWorldPosition()) or inst:GetCurrentPlatform() then
                    inst.sg:GoToState("dive_pst_land")
                else
                    SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())

                    if TheWorld.components.piratespawner then
                        TheWorld.components.piratespawner:StashLoot(inst)
                    end
                    inst:Remove()
                end
            end),
        },
    },

    State{
        name = "dive_pst_land",
        tags = {"busy"},

        onenter = function(inst)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("dive_pst_land")
            PlayFootstep(inst)
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name= "emote",
        tags = {"idle"},
        
        onenter = function(inst, data)
			inst.Physics:Stop()
			if data.anim and data.anim == "emoteXL_angry" then
				inst.AnimState:PlayAnimation("idle_angry")
				inst.SoundEmitter:PlaySound(inst.sounds.taunt or inst.sounds.attack)
			elseif data.anim and data.anim == "emoteXL_happycheer" then
				inst.AnimState:PlayAnimation("idle_happy")
				inst.SoundEmitter:PlaySound(inst.sounds.attack)
			elseif data.anim and data.anim == "emoteXL_annoyed" then
				inst.AnimState:PlayAnimation("abandon") 
				inst.SoundEmitter:PlaySound(inst.sounds.attack)
			elseif data.anim and data.anim == "emoteXL_sad" then 
				inst.AnimState:PlayAnimation("idle_scared")
				inst.SoundEmitter:PlaySound(inst.sounds.attack)
			--elseif data.anim and type(data.anim) == "table" then 
				--inst.sg:GoToState("sit")
			elseif data.anim and data.anim == "emoteXL_strikepose" then 
				inst.AnimState:PlayAnimation("idle_creepy")
				inst.SoundEmitter:PlaySound(inst.sounds.attack)
			else
				inst.sg:GoToState("idle")
			end	
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
	
	State{
        name= "castaoe",
        tags = {"busy"},
        
        onenter = function(inst, data)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("buff")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
	
	State{
        name= "sit",
        tags = {"idle"},
        
        onenter = function(inst, data)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("sit")
			inst.AnimState:PushAnimation("sit_idle", true)
        end,     
    },

    State{
        name = "attack",
        tags = {"attack", "busy", "autopredict"},
         
        onenter = function(inst, target)
            inst.SoundEmitter:PlaySound(inst.sounds.attack)
            inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_whoosh")
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.components.combat:SetTarget(target)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            
            local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
            inst.AnimState:PlayAnimation(hands and "work" or "atk")
            inst.AnimState:SetTime(hands and 0*FRAMES or 9* FRAMES)
            if target ~= nil then
                if target:IsValid() then
                    inst:FacePoint(target:GetPosition())
                    inst.sg.statemem.target = target
                end
            end
        end,
         
        onexit = function(inst)
            
        end,
         
        timeline =
        {
            TimeEvent(3 * FRAMES, function(inst)
                    local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
                    if hands and hands.prefab == "propsign" then
                        inst.sg.statemem.smashed = DoAOEAttack(inst, 2, 1.7)
                    end
                end),
                TimeEvent(4 * FRAMES, function(inst)
                    if inst.sg.statemem.smashed ~= nil then
                        local smashed = inst.sg.statemem.smashed
                        inst.sg.statemem.smashed = nil
                        smashed.prop:PushEvent("propsmashed", smashed.pos)
                    else
                        inst:PerformBufferedAction()
                    end
                end),
                TimeEvent(10 * FRAMES, function(inst)
                    inst.sg:RemoveStateTag("attack")
                    inst.sg:RemoveStateTag("busy")
                    inst:RemoveTag("busy")
                    inst.sg:AddStateTag("idle")
                end),
        },
         
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
    State{
        name = "work",
        tags = {"attack", "busy"},
        
        onenter = function(inst, target)
            inst.SoundEmitter:PlaySound(inst.sounds.attack)
            inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_whoosh")
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.components.combat:SetTarget(target)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            
            local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
            inst.AnimState:PlayAnimation("atk")
            
            if target ~= nil then
                if target:IsValid() then
                    inst:FacePoint(target:GetPosition())
                    inst.sg.statemem.attacktarget = target
                end
            end
        end,
        
        onexit = function(inst)
            inst.components.combat:SetTarget(nil)
        end,
        
        timeline =
        {
            TimeEvent(4*FRAMES, function(inst) PlayablePets.DoWork(inst, 1.5) end), --13 frames
            TimeEvent(1*FRAMES, function(inst) inst.sg:RemoveStateTag("attack") inst.sg:RemoveStateTag("busy") end),
        },
        
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
	},
	
    State{
        name = "chop",
        tags = {"chopping"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			inst.AnimState:PlayAnimation(hands and "work" or "atk")
			inst.AnimState:SetTime(hands and 0*FRAMES or 9* FRAMES)
        end,
        
        timeline=
        {
            
            TimeEvent(13*FRAMES, function(inst) inst:PerformBufferedAction() end ),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
    State{
        name = "eat",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()            
            inst.AnimState:PlayAnimation("eat")
            if inst.sounds.eat then
                inst.SoundEmitter:PlaySound(inst.sounds.eat)
            end
        end,
        
        timeline=
        {
            TimeEvent(10*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
    State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.hit)
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()            
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },    
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        timeline = {
            TimeEvent(1*FRAMES, function(inst)
                if inst.sounds.sleep_pre then
                    inst.SoundEmitter:PlaySound(inst.sounds.sleep_pre)
                end
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

		timeline=
        {
            TimeEvent(1*FRAMES, function(inst)
                if inst.sounds.sleep_lp then
                    inst.SoundEmitter:PlaySound(inst.sounds.sleep_lp, "sleep_lp")
                end
            end),
			TimeEvent(35*FRAMES, function(inst) 
				if inst:HasTag("pig") then
					inst.SoundEmitter:PlaySound("dontstarve/pig/sleep") 
                elseif inst:HasTag("merm") then
					inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/sleep")
                end
			end ),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        timeline = {
            TimeEvent(1*FRAMES, function(inst)
                if inst.sounds.sleep_pst then
                    inst.SoundEmitter:PlaySound(inst.sounds.sleep_pst)
                end
            end),
        },

        onexit = function(inst)
            inst.SoundEmitter:KillSound("sleep_lp")
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "knockback",
        tags = { "knockback", "busy", "nosleep", "nofreeze", "jumping" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("smacked")
			
			local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			if hands then
				inst.AnimState:Show("ARM_normal")
			end

            if data ~= nil then
                if data.propsmashed then
                    local item = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
                    local pos
                    if item ~= nil then
                        pos = inst:GetPosition()
                        pos.y = TUNING.KNOCKBACK_DROP_ITEM_HEIGHT_HIGH
                        local dropped = inst.components.inventory:DropItem(item, true, true, pos)
                        if dropped ~= nil then
                            dropped:PushEvent("knockbackdropped", { owner = inst, knocker = data.knocker, delayinteraction = TUNING.KNOCKBACK_DELAY_INTERACTION_HIGH, delayplayerinteraction = TUNING.KNOCKBACK_DELAY_PLAYER_INTERACTION_HIGH })
                        end
                    end
                    if item == nil or not item:HasTag("propweapon") then
                        item = inst.components.inventory:FindItem(IsMinigameItem)
                        if item ~= nil then
                            pos = pos or inst:GetPosition()
                            pos.y = TUNING.KNOCKBACK_DROP_ITEM_HEIGHT_LOW
                            item = inst.components.inventory:DropItem(item, false, true, pos)
                            if item ~= nil then
                                item:PushEvent("knockbackdropped", { owner = inst, knocker = data.knocker, delayinteraction = TUNING.KNOCKBACK_DELAY_INTERACTION_LOW, delayplayerinteraction = TUNING.KNOCKBACK_DELAY_PLAYER_INTERACTION_LOW })
                            end
                        end
                    end
                end
                if data.radius ~= nil and data.knocker ~= nil and data.knocker:IsValid() then
                    local x, y, z = data.knocker.Transform:GetWorldPosition()
                    local distsq = inst:GetDistanceSqToPoint(x, y, z)
                    local rangesq = data.radius * data.radius
                    local rot = inst.Transform:GetRotation()
                    local rot1 = distsq > 0 and inst:GetAngleToPoint(x, y, z) or data.knocker.Transform:GetRotation() + 180
                    local drot = math.abs(rot - rot1)
                    while drot > 180 do
                        drot = math.abs(drot - 360)
                    end
                    local k = distsq < rangesq and .3 * distsq / rangesq - 1 or -.7
                    inst.sg.statemem.speed = (data.strengthmult or 1) * 10 * k
                    inst.sg.statemem.dspeed = 0
                    if drot > 90 then
                        inst.sg.statemem.reverse = true
                        inst.Transform:SetRotation(rot1 + 180)
                        inst.Physics:SetMotorVel(-inst.sg.statemem.speed, 0, 0)
                    else
                        inst.Transform:SetRotation(rot1)
                        inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
                    end
                end
            end
        end,

        onupdate = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.sg.statemem.speed = inst.sg.statemem.speed + inst.sg.statemem.dspeed
                if inst.sg.statemem.speed < 0 then
                    inst.sg.statemem.dspeed = inst.sg.statemem.dspeed + .075
                    inst.Physics:SetMotorVel(inst.sg.statemem.reverse and -inst.sg.statemem.speed or inst.sg.statemem.speed, 0, 0)
                else
                    inst.sg.statemem.speed = nil
                    inst.sg.statemem.dspeed = nil
                    inst.Physics:Stop()
                end
            end
        end,

        timeline =
        {
            TimeEvent(3 * FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack) end),
            TimeEvent(12 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/movement/bodyfall_dirt") end),
            TimeEvent(14 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("nofreeze")
            end),
            TimeEvent(32 * FRAMES, function(inst)
                if inst.components.sleeper then
					inst.components.sleeper:WakeUp()
				end	
                inst.sg:RemoveStateTag("nosleep")
            end),
            TimeEvent(35 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },

        onexit = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.Physics:Stop()
            end
			local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			if hands then
				inst.AnimState:Hide("ARM_normal")
			end
        end,
    },
	
	State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
			local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			inst.AnimState:PlayAnimation("idle_loop", true)
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			inst.AnimState:PlayAnimation("run_pre")			
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline =
        {
            TimeEvent(0*FRAMES, PlayFootstep ),
			TimeEvent(12*FRAMES, PlayFootstep ),
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			inst.AnimState:PlayAnimation("run_pst")
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "walkning", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:WalkForward()
            local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			inst.AnimState:PlayAnimation("walk_pre")			
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "walkning", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:WalkForward()
            local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline =
        {
            TimeEvent(0*FRAMES, PlayFootstep ),
			TimeEvent(10*FRAMES, PlayFootstep ),
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			inst.AnimState:PlayAnimation("walk_pst")
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}

CommonStates.AddSimpleActionState(states, "eat", "eat", 10*FRAMES, {"busy"})
CommonStates.AddFrozenStates(states)

CommonStates.AddSimpleActionState(states,"pickup", "pig_pickup", 10*FRAMES, {"busy"})

CommonStates.AddSimpleActionState(states, "gohome", "pig_pickup", 4*FRAMES, {"busy"})

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound(inst.sounds.death)
			end)
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound(inst.sounds.attack)
			end)
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "idle_angry"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "run_pst"
local simpleidle = "idle_loop"
local simplemove = "run"
CommonStates.AddHopStates(states, false, {pre = "boat_jump_pre", loop = "boat_jump_loop", pst = "boat_jump_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
})

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "idle_angry",
	
	leap_pre = "boat_jump_pre",
	leap_loop = "boat_jump_loop",
	leap_pst = "boat_jump_pst",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	castspelltime = 10,
})


    
return StateGraph("merm_guardp", states, events, "idle", actionhandlers)

