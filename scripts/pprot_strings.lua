
local prefab = ""
------------------------------------------------------------------
-- Recipe strings
------------------------------------------------------------------
STRINGS.RECIPE_DESC.MOONSPIDERDENP = "Perfect for your ugly friends."
STRINGS.RECIPE_DESC.DUSTMOTHDENP = "Do what you do best lil' moth."

------------------------------------------------------------------
-- Character select screen strings
------------------------------------------------------------------
prefab = "worm_bossp"
------------------------------------------------------------------
STRINGS.CHARACTER_TITLES[prefab] = "Great Depths Worm"
STRINGS.CHARACTER_NAMES[prefab] = STRINGS.CHARACTER_TITLES[prefab]
STRINGS.CHARACTER_DESCRIPTIONS[prefab] = "*TODO."
STRINGS.CHARACTER_QUOTES[prefab] = "Finally, a new pugalisk."
STRINGS.CHARACTER_SURVIVABILITY[prefab] = "Boss-like"
------------------------------------------------------------------
prefab = "chest_mimicp"
------------------------------------------------------------------
STRINGS.CHARACTER_TITLES[prefab] = "Ornery Chest"
STRINGS.CHARACTER_NAMES[prefab] = STRINGS.CHARACTER_TITLES[prefab]
STRINGS.CHARACTER_DESCRIPTIONS[prefab] = "*Is shadow aligned.\n*Can disguise self as a chest.\n*Immune to poison and acid."
STRINGS.CHARACTER_QUOTES[prefab] = "It hungers for more than just loot."
STRINGS.CHARACTER_SURVIVABILITY[prefab] = "Pretty good"
------------------------------------------------------------------

STRINGS.CHARACTER_TITLES.crabking_mob_knightp = "Crab Knight"
STRINGS.CHARACTER_NAMES.crabking_mob_knightp = "Crab Knight"
STRINGS.CHARACTER_DESCRIPTIONS.crabking_mob_knightp = "*Is lunar aligned.\n*Can dive into water.\n*Has a spin ability."
STRINGS.CHARACTER_QUOTES.crabking_mob_knightp = "They did it! They made a good crabking update!"
STRINGS.CHARACTER_SURVIVABILITY.crabking_mob_knightp = "Very good"

STRINGS.CHARACTER_TITLES.crabking_mobp = "Crab Guard"
STRINGS.CHARACTER_NAMES.crabking_mobp = "Crab Guard"
STRINGS.CHARACTER_DESCRIPTIONS.crabking_mobp = "*Is lunar aligned.\n*Can dive into water."
STRINGS.CHARACTER_QUOTES.crabking_mobp = "They did it! They made a good crabking update!"
STRINGS.CHARACTER_SURVIVABILITY.crabking_mobp = "Pretty good"

STRINGS.CHARACTER_TITLES.otterp = "Marotter"
STRINGS.CHARACTER_NAMES.otterp = "Marotter"
STRINGS.CHARACTER_DESCRIPTIONS.otterp = "*Is amphibious.\n*Can only eat meat.\n*Can knock fish out of the water."
STRINGS.CHARACTER_QUOTES.otterp = "Cute and deadly at the same time!"
STRINGS.CHARACTER_SURVIVABILITY.otterp = "Pretty good"

STRINGS.CHARACTER_TITLES.pollyp = "Polly Rogers"
STRINGS.CHARACTER_NAMES.pollyp = STRINGS.CHARACTER_TITLES.pollyp
STRINGS.CHARACTER_DESCRIPTIONS.pollyp = "*Is a bird.\n*Is a uh... bird.\n*...its pink?"
STRINGS.CHARACTER_QUOTES.pollyp = "Is only playable because I lost a bet."

STRINGS.CHARACTER_TITLES.fused_shadelingp = "Fused Shadeling"
STRINGS.CHARACTER_NAMES.fused_shadelingp = STRINGS.CHARACTER_TITLES.fused_shadelingp
STRINGS.CHARACTER_DESCRIPTIONS.fused_shadelingp = "*Is shadow aligned.\n*Is a shadow.\n*Drops a bomb on death."
STRINGS.CHARACTER_QUOTES.fused_shadelingp = "Cousin to the Parasitic Shadeling."

STRINGS.CHARACTER_TITLES.mutateddeerclopsp = "Crystal Deerclops"
STRINGS.CHARACTER_NAMES.mutateddeerclopsp = STRINGS.CHARACTER_TITLES.mutateddeerclopsp
STRINGS.CHARACTER_DESCRIPTIONS.mutateddeerclopsp = "*Is lunar aligned.\n*Can throw ice lances.\n*Has a freezing aura."
STRINGS.CHARACTER_QUOTES.mutateddeerclopsp = "I guess you could say hes COOLER now."

STRINGS.CHARACTER_TITLES.mutatedbeargerp = "Armored Bearger"
STRINGS.CHARACTER_NAMES.mutatedbeargerp = STRINGS.CHARACTER_TITLES.mutatedbeargerp
STRINGS.CHARACTER_DESCRIPTIONS.mutatedbeargerp = "*Is lunar aligned.\n*Can combo slash.\n*Can ground pound."
STRINGS.CHARACTER_QUOTES.mutatedbeargerp = "Took the swineclops self-defense class."

STRINGS.CHARACTER_TITLES.mutatedvargp = "Possessed Varg"
STRINGS.CHARACTER_NAMES.mutatedvargp = STRINGS.CHARACTER_TITLES.mutatedvargp
STRINGS.CHARACTER_DESCRIPTIONS.mutatedvargp = "*Is lunar aligned.\n*Can summon hounds.\n*Can spew cold flames."
STRINGS.CHARACTER_QUOTES.mutatedvargp = "Looks like the hounds got a new alpha..."

STRINGS.CHARACTER_TITLES.froglunarp = "Bright-Eyed Frog"
STRINGS.CHARACTER_NAMES.froglunarp = STRINGS.CHARACTER_TITLES.froglunarp
STRINGS.CHARACTER_DESCRIPTIONS.froglunarp = "*Is lunar aligned.\n*Is a possessed frog.\n*Its really just a stronger frog."
STRINGS.CHARACTER_QUOTES.froglunarp = "Who asked for this..."

STRINGS.CHARACTER_TITLES.gestaltp = "Gestalt"
STRINGS.CHARACTER_NAMES.gestaltp = STRINGS.CHARACTER_TITLES.gestaltp
STRINGS.CHARACTER_DESCRIPTIONS.gestaltp = "*Is lunar aligned.\n*Deals drowziness on hit.\n*Gives sanity to nearby players."
STRINGS.CHARACTER_QUOTES.gestaltp = "Ironically lives in his big brother's shadow."

STRINGS.CHARACTER_TITLES.gestaltguardp = "Greater Gestalt"
STRINGS.CHARACTER_NAMES.gestaltguardp = STRINGS.CHARACTER_TITLES.gestaltguardp
STRINGS.CHARACTER_DESCRIPTIONS.gestaltguardp = "*Is lunar aligned.\n*Deals alot of damage.\n*Gives sanity to nearby players."
STRINGS.CHARACTER_QUOTES.gestaltguardp = "Ironically overshadows his lil brother."

STRINGS.CHARACTER_TITLES.lunar_grazerp = "Grazer"
STRINGS.CHARACTER_NAMES.lunar_grazerp = STRINGS.CHARACTER_TITLES.lunar_grazerp
STRINGS.CHARACTER_DESCRIPTIONS.lunar_grazerp = "*Is lunar aligned.\n*Emits drowsy clouds and cannot die.\n*Deals lunar damage."
STRINGS.CHARACTER_QUOTES.lunar_grazerp = "Grazer? More like GLAZER! Ammirite guys? Hahaha."

STRINGS.CHARACTER_TITLES.shadowthrall_hornsp = "Rasp"
STRINGS.CHARACTER_NAMES.shadowthrall_hornsp = STRINGS.CHARACTER_TITLES.shadowthrall_hornsp
STRINGS.CHARACTER_DESCRIPTIONS.shadowthrall_hornsp = "*Is shadow aligned.\n*Can chew players up.\n*Deals shadow damage."
STRINGS.CHARACTER_QUOTES.shadowthrall_hornsp = "He's not hungry... he's starving."

STRINGS.CHARACTER_TITLES.shadowthrall_handsp = "Jitters"
STRINGS.CHARACTER_NAMES.shadowthrall_handsp = STRINGS.CHARACTER_TITLES.shadowthrall_handsp
STRINGS.CHARACTER_DESCRIPTIONS.shadowthrall_handsp = "*Is shadow aligned.\n*Has a handy attack.\n*Deals shadow damage."
STRINGS.CHARACTER_QUOTES.shadowthrall_handsp = "Even CatDog joined the dark side."

STRINGS.CHARACTER_TITLES.shadowthrall_wingsp = "Shriek"
STRINGS.CHARACTER_NAMES.shadowthrall_wingsp = STRINGS.CHARACTER_TITLES.shadowthrall_wingsp
STRINGS.CHARACTER_DESCRIPTIONS.shadowthrall_wingsp = "*Is shadow aligned.\n*Shoots projectiles.\n*Deals shadow damage."
STRINGS.CHARACTER_QUOTES.shadowthrall_wingsp = "The shotgun character we all needed."

STRINGS.CHARACTER_TITLES.daywalkerp = STRINGS.NAMES.DAYWALKER
STRINGS.CHARACTER_NAMES.daywalkerp = STRINGS.CHARACTER_TITLES.daywalkerp
STRINGS.CHARACTER_DESCRIPTIONS.daywalkerp = "*Has gotten into places where he doesn't belong.\n*Pretty jacked.\n*Very destructive."
STRINGS.CHARACTER_QUOTES.daywalkerp = "THIS IS YOUR PIG ON DRUGS!"

STRINGS.CHARACTER_TITLES.shadow_leechp = STRINGS.NAMES.SHADOW_LEECH
STRINGS.CHARACTER_NAMES.shadow_leechp = STRINGS.CHARACTER_TITLES.shadow_leechp
STRINGS.CHARACTER_DESCRIPTIONS.shadow_leechp = "*Is a nightmare\n*Pretty fast on its feet.\n*Leaps."
STRINGS.CHARACTER_QUOTES.shadow_leechp = "Cutest most vile thing ever!"

STRINGS.CHARACTER_TITLES.stageusherp = "Stage Usher"
STRINGS.CHARACTER_NAMES.stageusherp = STRINGS.CHARACTER_TITLES.stageusherp
STRINGS.CHARACTER_DESCRIPTIONS.stageusherp = "*Is a fancier stagehand\n*Has more bulk, but is slower\n*Can attack"
STRINGS.CHARACTER_QUOTES.stageusherp = "Stagehand's big brother"

STRINGS.CHARACTER_TITLES.hound_hedgep = "Briar Hound"
STRINGS.CHARACTER_NAMES.hound_hedgep = "Briar Hound"
STRINGS.CHARACTER_DESCRIPTIONS.hound_hedgep = "*Is a hound\n*Has slightly higher damage\n*Is very frail and weak to fire"
STRINGS.CHARACTER_QUOTES.hound_hedgep = "Acts as lawn decor AND defense!"

STRINGS.CHARACTER_TITLES.lightcrabp = "Crustashine"
STRINGS.CHARACTER_NAMES.lightcrabp = "Crustashine"
STRINGS.CHARACTER_DESCRIPTIONS.lightcrabp = "*Is a crab\n*Gives off light\n*Is fast but weak"
STRINGS.CHARACTER_QUOTES.lightcrabp = "Pebblecrab+"

STRINGS.CHARACTER_TITLES.primematep = "Prime Mate"
STRINGS.CHARACTER_NAMES.primematep = "Prime Mate"
STRINGS.CHARACTER_DESCRIPTIONS.primematep = "*Is a monkey\n*Is a pirate\n*Can only eat veggies"
STRINGS.CHARACTER_QUOTES.primematep = "From monkey island no doubt!"

STRINGS.CHARACTER_TITLES.powdermonkeyp = "Powder Monkey"
STRINGS.CHARACTER_NAMES.powdermonkeyp = "Powder Monkey"
STRINGS.CHARACTER_DESCRIPTIONS.powdermonkeyp = "*Is a monkey\n*Is a pirate\n*Can only eat veggies"
STRINGS.CHARACTER_QUOTES.powdermonkeyp = "MONKEY!"

STRINGS.CHARACTER_TITLES.vargletp = "Varglet"
STRINGS.CHARACTER_NAMES.vargletp = "Varglet"
STRINGS.CHARACTER_DESCRIPTIONS.vargletp = "*Is a hound\n*Can summon fellow hounds\n*Can only eat meat"
STRINGS.CHARACTER_QUOTES.vargletp = "Ugh, teen hounds"

STRINGS.CHARACTER_TITLES.striderp = "Water Strider"
STRINGS.CHARACTER_NAMES.striderp = "Water Strider"
STRINGS.CHARACTER_DESCRIPTIONS.striderp = "*Is a spider\n*Can walk across water\n*Can only eat meat"
STRINGS.CHARACTER_QUOTES.striderp = "Water skating is so much fun!"

STRINGS.CHARACTER_TITLES.grassgatorp = "Grass Gator"
STRINGS.CHARACTER_NAMES.grassgatorp = "Grass Gator"
STRINGS.CHARACTER_DESCRIPTIONS.grassgatorp = "*Is bulky\n*Can swim\n*Can only eat veggies"
STRINGS.CHARACTER_QUOTES.grassgatorp = "'I think so' - Rei"

STRINGS.CHARACTER_TITLES.spider_wolfp = "Nurse Spider"
STRINGS.CHARACTER_NAMES.spider_wolfp = "Nurse Spider"
STRINGS.CHARACTER_DESCRIPTIONS.spider_wolfp = "*Is a spider\n*Is bulky for a spider\n*Can heal with v"
STRINGS.CHARACTER_QUOTES.spider_wolfp = "The medic no one asked for"

STRINGS.CHARACTER_TITLES.robin_mutantp = "Misshapen Bird"
STRINGS.CHARACTER_NAMES.robin_mutantp = "Misshapen Bird"
STRINGS.CHARACTER_DESCRIPTIONS.robin_mutantp = "*Is a bird\n*Spits bile at foes\n*Immune to storms"
STRINGS.CHARACTER_QUOTES.robin_mutantp = "Don't do drugs kids"

STRINGS.CHARACTER_TITLES.crow_mutantp = "Moonblind Crow"
STRINGS.CHARACTER_NAMES.crow_mutantp = "Moonblind Crow"
STRINGS.CHARACTER_DESCRIPTIONS.crow_mutantp = "*Is a bird\n*More vicious than normal birds\n*Immune to storms"
STRINGS.CHARACTER_QUOTES.crow_mutantp = "Just when you thought crows couldn't get any worse..."

STRINGS.CHARACTER_TITLES.alterguardianp = "Celestial Champion"
STRINGS.CHARACTER_NAMES.alterguardianp = "Celestial Champion"
STRINGS.CHARACTER_DESCRIPTIONS.alterguardianp = "*Is a Giant\n*Can revive multiple times\n*Each revive starts a new phase"
STRINGS.CHARACTER_QUOTES.alterguardianp = "Its not just a boulder!... its a rock!"

STRINGS.CHARACTER_TITLES.fruitflylordp = "Lord of the Fruit Flies"
STRINGS.CHARACTER_NAMES.fruitflylordp = "Lord of the Fruit Flies"
STRINGS.CHARACTER_DESCRIPTIONS.fruitflylordp = "*Is a... Giant?\n*Loves rotting food\n*Can spawn minions"
STRINGS.CHARACTER_QUOTES.fruitflylordp = "Not really an impressive title is it?"

STRINGS.CHARACTER_TITLES.fruitflyp = "Fruit Fly"
STRINGS.CHARACTER_NAMES.fruitflyp = "Fruit Fly"
STRINGS.CHARACTER_DESCRIPTIONS.fruitflyp = "*Is a flying bug\n*Loves rotting food\n*Is very weak..."
STRINGS.CHARACTER_QUOTES.fruitflyp = "Who would've thought fruit with wings could be so vile?"

STRINGS.CHARACTER_TITLES.fruitfly_goodp = "Friendly Fruit Fly"
STRINGS.CHARACTER_NAMES.fruitfly_goodp = "Friendly Fruit Fly"
STRINGS.CHARACTER_DESCRIPTIONS.fruitfly_goodp = "*Is a flying bug\n*Loves rotting food\n*Can't attack..."
STRINGS.CHARACTER_QUOTES.fruitfly_goodp = "Who would've thought fruit with wings could be so cute?"

STRINGS.CHARACTER_TITLES.gestalt_guardp = "Bulbous Lightbug"
STRINGS.CHARACTER_NAMES.gestalt_guardp = "Bulbous Lightbug"
STRINGS.CHARACTER_DESCRIPTIONS.gestalt_guardp = "*Is a gestalt\n*Deals very high damage\n*Hates shadows"
STRINGS.CHARACTER_QUOTES.gestalt_guardp = "Hes a very bright boy"

STRINGS.CHARACTER_TITLES.lightflierp = "Bulbous Lightbug"
STRINGS.CHARACTER_NAMES.lightflierp = "Bulbous Lightbug"
STRINGS.CHARACTER_DESCRIPTIONS.lightflierp = "*Is a flying bug\n*Can't attack...\n*Can only eat veggies"
STRINGS.CHARACTER_QUOTES.lightflierp = "Hes a very bright boy"

STRINGS.CHARACTER_TITLES.mushgnomep = "Mush Gnome"
STRINGS.CHARACTER_NAMES.mushgnomep = "Mush Gnome"
STRINGS.CHARACTER_DESCRIPTIONS.mushgnomep = "*Is a mushgnome\n*Leaves behind explosive spores\n*Isn't very fast..."
STRINGS.CHARACTER_QUOTES.mushgnomep = "Just strolling around town!"

STRINGS.CHARACTER_TITLES.dustmothp = "Dust Moth"
STRINGS.CHARACTER_NAMES.dustmothp = "Dust Moth"
STRINGS.CHARACTER_DESCRIPTIONS.dustmothp = "*Can only eat elemental foods...\n*Just wants to clean\n*Can't attack..."
STRINGS.CHARACTER_QUOTES.dustmothp = "The best and cutest butler there is!"

STRINGS.CHARACTER_TITLES.archive_centipedep = "Ancient Sentrypede"
STRINGS.CHARACTER_NAMES.archive_centipedep = "Ancient Sentrypede"
STRINGS.CHARACTER_DESCRIPTIONS.archive_centipedep = "*Is a robot\n*Is pretty bulky\n*Can roll"
STRINGS.CHARACTER_QUOTES.archive_centipedep = "Ancients combined security guards and animatronics, genius!"

STRINGS.CHARACTER_TITLES.molebatp = "Naked Mole Bat"
STRINGS.CHARACTER_NAMES.molebatp = "Naked Mole Bat"
STRINGS.CHARACTER_DESCRIPTIONS.molebatp = "*Is a mole bat\n*Is pretty frail\n*Can call for allies in caves"
STRINGS.CHARACTER_QUOTES.molebatp = "Moles but cooler"

STRINGS.CHARACTER_TITLES.sharkp = "Rockjaw"
STRINGS.CHARACTER_NAMES.sharkp = "Rockjaw"
STRINGS.CHARACTER_DESCRIPTIONS.sharkp = "*Is pretty bulky\n*Can swim\n*Can only eat meat"
STRINGS.CHARACTER_QUOTES.sharkp = "Sea Hound, but better"

STRINGS.CHARACTER_TITLES.wobsterp = "Wobster"
STRINGS.CHARACTER_NAMES.wobsterp = "Wobster"
STRINGS.CHARACTER_DESCRIPTIONS.wobsterp = "*Is very frail\n*Can swim\n*Can't attack..."
STRINGS.CHARACTER_QUOTES.wobsterp = "Don't let your memes be dreams"

STRINGS.CHARACTER_TITLES.hermit_crabp = "Crabler"
STRINGS.CHARACTER_NAMES.hermit_crabp = "Crabler"
STRINGS.CHARACTER_DESCRIPTIONS.hermit_crabp = "*Is a humanoid\n*Adapted to the environment\n*Serves the Crab King"
STRINGS.CHARACTER_QUOTES.hermit_crabp = "Crab Dance to assert dominance"

STRINGS.CHARACTER_TITLES.crabkingp = "Crab King"
STRINGS.CHARACTER_NAMES.crabkingp = "Crab King"
STRINGS.CHARACTER_DESCRIPTIONS.crabkingp = "*Is a Giant\n*Can socket gems for power\n*Can't go on land"
STRINGS.CHARACTER_QUOTES.crabkingp = "He's pretty crabby for a King"

STRINGS.CHARACTER_TITLES.squidp = "Skitter Squid"
STRINGS.CHARACTER_NAMES.squidp = "Skitter Squid"
STRINGS.CHARACTER_DESCRIPTIONS.squidp = "*Is a squid\n*Can swim\n*Can spit ink"
STRINGS.CHARACTER_QUOTES.squidp = "Better than Quacken"

STRINGS.CHARACTER_TITLES.gnarwailp = "Gnarwail"
STRINGS.CHARACTER_NAMES.gnarwailp = "Gnarwail"
STRINGS.CHARACTER_DESCRIPTIONS.gnarwailp = "*Lives in the ocean\n*Can attack boats\n*Is bulky"
STRINGS.CHARACTER_QUOTES.gnarwailp = "They got a kickass facial horn"

STRINGS.CHARACTER_TITLES.merm_guardp = "Loyal Merm Guard"
STRINGS.CHARACTER_NAMES.merm_guardp = "Loyal Merm Guard"
STRINGS.CHARACTER_DESCRIPTIONS.merm_guardp = "*Is a merm\n*Is strong\n*Runs fast"
STRINGS.CHARACTER_QUOTES.merm_guardp = "Don't ask about what they're wearing"

STRINGS.CHARACTER_TITLES.spider_moonp = "Shattered Spider"
STRINGS.CHARACTER_NAMES.spider_moonp = "Shattered Spider"
STRINGS.CHARACTER_DESCRIPTIONS.spider_moonp = "*Is a spider\n*Has a ranged attack\n*Is bulky"
STRINGS.CHARACTER_QUOTES.spider_moonp = "They're isolated for a reason"

STRINGS.CHARACTER_TITLES.hound_mutantp = "Horror Hound"
STRINGS.CHARACTER_NAMES.hound_mutantp = "Horror Hound"
STRINGS.CHARACTER_DESCRIPTIONS.hound_mutantp = "*Is a hound\n*Is a corpse\n*Can swim"
STRINGS.CHARACTER_QUOTES.hound_mutantp = "You didn't need to see how this thing spawns"

STRINGS.CHARACTER_TITLES.penguin_mutantp = "Moonrock Pengull"
STRINGS.CHARACTER_NAMES.penguin_mutantp = "Moonrock Pengull"
STRINGS.CHARACTER_DESCRIPTIONS.penguin_mutantp = "*Is a pengull\n*Is a corpse\n*Immune to Freezing"
STRINGS.CHARACTER_QUOTES.penguin_mutantp = "Spine chilling"

STRINGS.CHARACTER_TITLES.fruitdragonp = STRINGS.NAMES.FRUITDRAGON
STRINGS.CHARACTER_NAMES.fruitdragonp = STRINGS.NAMES.FRUITDRAGON
STRINGS.CHARACTER_DESCRIPTIONS.fruitdragonp = "*Is small, but bulky\n*Loves the heat\n*Needs to sleep to transform"
STRINGS.CHARACTER_QUOTES.fruitdragonp = "Don't underestimate it"

STRINGS.CHARACTER_TITLES.butterfly_moonp = STRINGS.NAMES.MOONBUTTERFLY
STRINGS.CHARACTER_NAMES.butterfly_moonp = STRINGS.NAMES.MOONBUTTERFLY
STRINGS.CHARACTER_DESCRIPTIONS.butterfly_moonp = "*Can't attack\n*Is flying\n*Provides light"
STRINGS.CHARACTER_QUOTES.butterfly_moonp = "Alien butterflies best butterflies"

STRINGS.CHARACTER_TITLES.carratp = STRINGS.NAMES.CARRAT
STRINGS.CHARACTER_NAMES.carratp = STRINGS.NAMES.CARRAT
STRINGS.CHARACTER_DESCRIPTIONS.carratp = "*Is a carrot?\n*Is very weak\n*Can hide"
STRINGS.CHARACTER_QUOTES.carratp = "The moon does... some pretty weird things"

STRINGS.CHARACTER_TITLES.carrat_shadowp = "Shadow Carrat"
STRINGS.CHARACTER_NAMES.carrat_shadowp = "Shadow Carrat"
STRINGS.CHARACTER_DESCRIPTIONS.carrat_shadowp = "*Is a carrot?\n*Is a shadow\n*Can hide"
STRINGS.CHARACTER_QUOTES.carrat_shadowp = "Charlie does... some pretty weird things"

STRINGS.CHARACTER_TITLES.moonmothp = "Mothling"
STRINGS.CHARACTER_NAMES.moonmothp = "Mothling"
STRINGS.CHARACTER_DESCRIPTIONS.moonmothp = "*Can't attack\n*Is flying\n*Provides light"
STRINGS.CHARACTER_QUOTES.moonmothp = "As cute as the moon is round"

STRINGS.CHARACTER_TITLES.puffinp = "Puffin"
STRINGS.CHARACTER_NAMES.puffinp = "Puffin"
STRINGS.CHARACTER_DESCRIPTIONS.puffinp = "*Is a bird\n*Can fly\nCan swim"
STRINGS.CHARACTER_QUOTES.puffinp = "The bird you need a boat to kill"

STRINGS.CHARACTER_TITLES.cookiecutterp = "Cookie Cutter"
STRINGS.CHARACTER_NAMES.cookiecutterp = "Cookie Cutter"
STRINGS.CHARACTER_DESCRIPTIONS.cookiecutterp = "*Only swims in ocean\n*Eats wood"
STRINGS.CHARACTER_QUOTES.cookiecutterp = "A distant cousin of Weevole?"

STRINGS.CHARACTER_TITLES.malbatrossp = "Malbatross"
STRINGS.CHARACTER_NAMES.malbatrossp = "Malbatross"
STRINGS.CHARACTER_DESCRIPTIONS.malbatrossp = "*Is a giant\n*Can fly\n*Can dive"
STRINGS.CHARACTER_QUOTES.malbatrossp = "Just don't eat any trash"
------------------------------------------------------------------
-- The prefab names as they appear in-game
------------------------------------------------------------------
STRINGS.NAMES.MOONSPIDERDENP = STRINGS.NAMES.MOONSPIDERDEN
STRINGS.NAMES.DUSTMOTHDENP = STRINGS.NAMES.DUSTMOTHDEN

------------------------------------------------------------------
--Reforged Strings
------------------------------------------------------------------
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.fruitflylordp = "*Is a... Giant?\n*Can be revived twice as fast\n*Buffs ally fruit flies\nExpertise:\nDarts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.fruitflyp = "*Is pretty fragile\n*Can be revived twice as fast\n*Buffs Fruit Fly Lords\nExpertise:\nDarts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.fruitfly_goodp = "*Is pretty fragile\n*Can revive others 3x as fast\n*Can't attack...\nExpertise:\nDarts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.sharkp = "*Has AOE\n*Is pretty bulky\n*Is fast\nExpertise:\nMelee"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.hermit_crabp = "*WIP\n\nExpertise:\nMelee, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.crabkingp = "*Aoe attack\n*Ink slows enemies down by 50%\n*Is fast but weak\n\nExpertise:\nMelee, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.squidp = "*Aoe attack\n*Ink slows enemies down by 50%\n*Is fast but weak\n\nExpertise:\nMelee, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.gnarwailp = "*Is Bulky\n*Perks coming soon!\n\n\nExpertise:\nMelee, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.merm_guardp = "*Is a merm\n*50% increased attack damage\n*Is fast\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.hound_mutantp = "*Is a corpse\n*Infects on kill\n*Is fast\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.spider_moonp = "*Is a spider\n*Has natural armor\n*+10% cooldown\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.penguin_mutantp = "*Is a corpse\n*Infects on kill\n*Immune to cold\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.fruitdragonp = "*Is bulky\n*Transforms when weak\n*10% more fire damage\n\nExpertise:\nMelee, Darts, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.butterfly_moonp = "*Is very weak\n*Has a heal aura\n*Can be revived 3x faster\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.moonmothp = "*Can't attack\n*+10% cooldown\n*Can revive others twice as fast\n\nExpertise:\nStaves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.puffinp = "*Is very weak\n*Can fly with C\n*Can be revived 3x faster\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.carratp = "*Is very weak\n*Has more aggro\n*Is fast\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.carrat_shadowp = "*Is very weak\n*Is immune to buffs and debuffs\n*Is fast\n\nExpertise:\nMelee, Darts, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.cookiecutterp = "*Is slow\n*Can't really do much else\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.malbatrossp = "*Is a Giant\n*Can swoop\n*Can flip snortoises\n\nExpertise:\nMelee, Darts"
------------------------------------------------------------------
-- Mob strings
------------------------------------------------------------------

------------------------------------------------------------------
-- The default responses of characters examining the prefabs
------------------------------------------------------------------

--STRINGS.CHARACTERS.GENERIC.DESCRIBE.CITY_LAMPP = 
--{
--	ON = "Nice and cozy",
--	GENERIC = "It turns on at night",
--}


------------------------------------------------------------------
-- NPC strings
------------------------------------------------------------------

------------------------------------------------------------------
-- Shadow skin strings
------------------------------------------------------------------

--[[
STRINGS.SKIN_QUOTES.houndplayer_shadow = "The Rare Varg Hound"
STRINGS.SKIN_NAMES.houndplayer_shadow = "Shadow Hound"
--]]

return STRINGS