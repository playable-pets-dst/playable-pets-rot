local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------
local prefabname = "mutateddeerclopsp"

local assets = 
{
	Asset("ANIM", "anim/deerclops_basic.zip"),
    Asset("ANIM", "anim/deerclops_actions.zip"),
    Asset("ANIM", "anim/deerclops_mutated_actions.zip"),
    Asset("ANIM", "anim/deerclops_mutated.zip"),
	Asset("ANIM", "anim/lunar_flame.zip"),
    Asset("SOUND", "sound/deerclops.fsb"),
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = BOSS_STATS and TUNING.MUTATED_DEERCLOPS_HEALTH or TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 4,
	walkspeed = 4,
	attackperiod = TUNING.MUTATED_DEERCLOPS_ATTACK_PERIOD,
	damage = BOSS_STATS and TUNING.MUTATED_DEERCLOPS_DAMAGE or 120*2,
	range = TUNING.MUTATED_DEERCLOPS_ATTACK_RANGE,
	bank = "deerclops",
	build = "deerclops_mutated",
	scale = 1.65,
	--build2 = "alternate build here",
	stategraph = "SGmutateddeerclopsp",
	minimap = prefabname..".tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
    { "spoiled_food",			        	 1.0  },
	{ "spoiled_food",			        	 1.0  },
	{ "spoiled_food",			        	 1.0  },
	{ "spoiled_food",			        	 0.5  },
	{ "purebrilliance",				         1.0  },
	{ "purebrilliance",				         0.75 },
	{ "ice",						         1.0  },
	{ "ice",						         0.75 },
})

------------------------------------------------------
local mutated_sounds =
{
	step = "dontstarve/creatures/deerclops/step",
	taunt_grrr = "rifts3/mutated_deerclops/taunt_grrr",
	taunt_howl = "rifts3/mutated_deerclops/taunt_howl",
	hurt = "rifts3/mutated_deerclops/hurt",
	death = "rifts3/mutated_deerclops/death",
	attack = "rifts3/mutated_deerclops/attack",
	swipe = "dontstarve/creatures/deerclops/swipe",
	charge = "dontstarve/creatures/deerclops/charge",
	walk = "rifts3/mutated_deerclops/walk", --this is vocalization not footstep
}
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local function Mutated_Disengage(inst)
	inst._disengagetask = nil
	if inst.sg.mem.circle ~= nil then
		inst.sg.mem.circle:KillFX()
		inst.sg.mem.circle = nil
	end
end

local function Mutated_OnDroppedTarget(inst)
	if inst._disengagetask == nil then
		inst._disengagetask = inst:DoTaskInTime(6, Mutated_Disengage)
	end
end

local function Mutated_OnDead(inst)
    if TheWorld ~= nil and TheWorld.components.lunarriftmutationsmanager ~= nil then
        TheWorld.components.lunarriftmutationsmanager:SetMutationDefeated(inst)
    end
end

local function Mutated_OnIgnite(inst, source, doer)
	if inst.components.burnable.burntime ~= 0 and inst.spikefire == nil and not (inst.sg.mem.noice == 1 and inst.sg.mem.noeyeice) then
		inst.spikefire = SpawnPrefab("deerclops_spikefire_fx")
		inst.spikefire.Follower:FollowSymbol(inst.GUID,
			(inst.sg.mem.noice == 1 and "swap_fire_2") or
			(inst.sg.mem.noice == 0 and "swap_fire_1") or
			"swap_fire_0",
			0, 0, 0, true)
	end
end

local function Mutated_OnExtinguish(inst)
	if inst.spikefire ~= nil then
		inst.spikefire:KillFX()
		inst.spikefire = nil
	end
	if inst._staggertask ~= nil then
		inst._staggertask:Cancel()
		inst._staggertask = nil
	end
end

local function Mutated_OnTemp8Faced(inst)
	if inst.temp8faced:value() then
		inst.gestalt.Transform:SetEightFaced()
	else
		inst.gestalt.Transform:SetFourFaced()
	end
end

local function Mutated_SwitchToEightFaced(inst)
	if not inst.temp8faced:value() then
		inst.temp8faced:set(true)
		if not TheNet:IsDedicated() then
			Mutated_OnTemp8Faced(inst)
		end
		inst.Transform:SetEightFaced()
	end
end

local function Mutated_SwitchToFourFaced(inst)
	if inst.temp8faced:value() then
		inst.temp8faced:set(false)
		if not TheNet:IsDedicated() then
			Mutated_OnTemp8Faced(inst)
		end
		inst.Transform:SetFourFaced()
	end
end

local function Mutated_CreateGestaltFlame()
	local inst = CreateEntity()

	inst:AddTag("FX")
	--[[Non-networked entity]]
	--inst.entity:SetCanSleep(false) --commented out; follow parent sleep instead
	inst.persists = false

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddFollower()

	inst.Transform:SetFourFaced()

	inst.AnimState:SetBank("lunar_flame")
	inst.AnimState:SetBuild("lunar_flame")
	inst.AnimState:PlayAnimation("gestalt_eye", true)
	inst.AnimState:SetMultColour(1, 1, 1, 0.6)
	inst.AnimState:SetLightOverride(0.1)
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
	inst.AnimState:UsePointFiltering(true)

	return inst
end

local function Mutated_SetFrenzied(inst, frenzied)
	if frenzied then
		if not inst.frenzied then
			inst.frenzied = true
			inst.frenzy_starttime = GetTime()
			inst.frenzy_starthp = inst.components.health:GetPercent()
		end
	elseif inst.frenzied then
		inst.frenzied = nil
		inst.frenzy_starttime = nil
		inst.frenzy_starthp = nil
	end
end

local function Mutated_ShouldStayFrenzied(inst)
	--frenzy ends after losing enough hp
	--frenzy must last minimum duration (even if hp requirement is met)
	return true
end

local function NoHoles(pt)
    return not TheWorld.Map:IsGroundTargetBlocked(pt) 
end

local function ReticuleTargetFn(inst)
    local rotation = inst.Transform:GetRotation() * DEGREES
    local pos = inst:GetPosition()
    pos.y = 0
    for r = 13, 4, -.5 do
        local offset = FindWalkableOffset(pos, rotation, r, 1, false, true, NoHoles)
        if offset ~= nil then
            pos.x = pos.x + offset.x
            pos.z = pos.z + offset.z
            return pos
        end
    end
    for r = 13.5, 16, .5 do
        local offset = FindWalkableOffset(pos, rotation, r, 1, false, true, NoHoles)
        if offset ~= nil then
            pos.x = pos.x + offset.x
            pos.z = pos.z + offset.z
            return pos
        end
    end
    pos.x = pos.x + math.cos(rotation) * 13
    pos.z = pos.z - math.sin(rotation) * 13
    return pos
end

local function GetPointSpecialActions(inst, pos, useitem, right)
    if right and useitem == nil then
        local rider = inst.replica.rider
        if rider == nil or not rider:IsRiding() then
            return { ACTIONS.PP_RANGEDSPECIAL}
        end
    end
    return {}
end

local function OnSetOwner(inst)
    if inst.components.playeractionpicker ~= nil and TheNet:GetServerGameMode() ~= "lavaarena" then
        inst.components.playeractionpicker.pointspecialactionsfn = GetPointSpecialActions
    end
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)

	  inst:AddTag("lunar_aligned")

	  inst.AnimState:Hide("gestalt_eye")

	  inst:ListenForEvent("setowner", OnSetOwner)
	--if inst.components.playeractionpicker ~= nil then
        --inst.components.playeractionpicker.pointspecialactionsfn = GetPointSpecialActions
   -- end
	
	inst:AddComponent("reticule")
    inst.components.reticule.targetfn = ReticuleTargetFn
    inst.components.reticule.ease = true
  
	  inst.temp8faced = net_bool(inst.GUID, "mutatedbearger.temp8faced", "temp8faceddirty")
  
	  --Dedicated server does not need to trigger music
	  --Dedicated server does not need to spawn the local fx
	  if not TheNet:IsDedicated() then  
		  inst.gestalt = Mutated_CreateGestaltFlame()
		  inst.gestalt.entity:SetParent(inst.entity)
		  inst.gestalt.Follower:FollowSymbol(inst.GUID, "swap_gestalt_flame", 0, 0, 0, true)
		  local frames = inst.gestalt.AnimState:GetCurrentAnimationNumFrames()
		  local rnd = math.random(frames) - 1
		  inst.gestalt.AnimState:SetFrame(rnd)
	  end

	if not TheWorld.ismastersim then
		inst:ListenForEvent("temp8faceddirty", Mutated_OnTemp8Faced)
	end
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function oncollapse(inst, other)
	if other and other.components.workable ~= nil and other.components.workable.workleft > 0 then
		SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.workable:Destroy(inst)
	end
end

local function oncollide(inst, other)
	if other == nil or not other:HasTag("tree") then
		return
	end
	local v1 = Vector3(inst.Physics:GetVelocity())
	if v1:LengthSq() < 1 then
		return
	end
	inst:DoTaskInTime(2*FRAMES, oncollapse, other)
end

local function OnNewState(inst, data)
    if not (inst.sg:HasStateTag("sleeping") or inst.sg:HasStateTag("waking")) then
        inst.Light:SetIntensity(.6)
        inst.Light:SetRadius(8)
        inst.Light:SetFalloff(3)
        inst.Light:SetColour(1, 0, 0)
    end
end

local function OnHitOther(inst, data)
	local other = data.target
	if other and other.components.freezable and TheNet:GetServerGameMode() ~= "lavaarena" then
		other.components.freezable:AddColdness(2)
		other.components.freezable:SpawnShatterFX()
	end
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.DEERCLOPS)
	
	inst.mobsleep = false
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local master_postinit = function(inst) 
	inst.sounds = mutated_sounds
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	--Attack is done through ice fx now.
    --inst.components.combat:SetAreaDamage(TUNING.DEERCLOPS_AOE_RANGE, TUNING.DEERCLOPS_AOE_SCALE)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable(prefabname)
	----------------------------------
	--Tags--
	
	inst:AddTag("epic")
    inst:AddTag("monster")
    inst:AddTag("deerclops")
    inst:AddTag("largecreature")
	inst:AddTag("lunar_aligned")

	inst.AnimState:Hide("head_neutral")
	
	inst.altattack = true
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	inst.hasiceaura = true
	inst.hasknockback = true
	inst.hasicelance = true
	inst.hasfrenzy = true
	inst.freezepower = 3
	inst.ignorebase = true
	inst.icespike_pool = {}
	
	inst.isshiny = 0
	
	inst.recentlycharged = {}
	inst.Physics:SetCollisionCallback(oncollide)
	
	local body_symbol = "deerclops_body"
	inst.poisonsymbol = body_symbol
	MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)
	PlayablePets.SetCommonStatResistances(inst, nil, nil, nil, 10) --fire, acid, poison, freeze (flat value, not a multiplier)

	inst.components.burnable.fxdata[1].prefab = "character_fire_flicker"
	inst.components.burnable.nocharring = true
	inst.components.burnable:SetOnIgniteFn(Mutated_OnIgnite)
	inst.components.burnable:SetOnExtinguishFn(Mutated_OnExtinguish)

	inst.SwitchToEightFaced = Mutated_SwitchToEightFaced
	inst.SwitchToFourFaced = Mutated_SwitchToFourFaced

	inst.SetFrenzied = Mutated_SetFrenzied
	inst.ShouldStayFrenzied = Mutated_ShouldStayFrenzied

	inst:AddComponent("planarentity")
	inst:AddComponent("planardamage")
	inst.components.planardamage:SetBaseDamage(TUNING.MUTATED_DEERCLOPS_PLANAR_DAMAGE)
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(2,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1000, .5)
    inst.DynamicShadow:SetSize(6, 3.5)
    inst.Transform:SetFourFaced()
	
	inst.Physics:SetCollisionCallback(oncollide)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("newstate", OnNewState)
	inst:ListenForEvent("onhitother", OnHitOther)
	inst:ListenForEvent("onremove", function(inst)  
		if inst.sg.mem.circle then
			inst.sg.mem.circle:KillFX()
		end
	end)
	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
