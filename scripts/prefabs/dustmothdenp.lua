require("worldsettingsutil")

local assets =
{
    Asset("ANIM", "anim/dustmothden.zip"),
}

local prefabs =
{
    "dustmoth",
}

SetSharedLootTable('dustmothden',
{
    {'thulecite_pieces',  1.0},
    {'thulecite_pieces',  1.0},
    {'thulecite_pieces',  1.0},
    {'thulecite_pieces',  0.75},
    {'thulecite_pieces',  0.5},
    {'thulecite_pieces',  0.25},
    {'thulecite_pieces',  0.25},
    {'thulecite',  0.01},
})

local function StartRepairing(inst, repairer)
    -- Usually called from SGdustmoth
    inst.components.trader:Disable()
    if inst.components.timer:TimerExists("repair") then
        if inst.components.timer:IsPaused("repair") then
            inst.components.timer:ResumeTimer("repair")
        end
    else
        inst.components.timer:StartTimer("repair", TUNING.DUSTMOTHDEN_REPAIR_TIME/2)
    end

    inst.AnimState:PlayAnimation("repair", true)
end

local function PauseRepairing(inst)
    inst.components.timer:PauseTimer("repair")

    if not inst.components.workable.workable then
        inst.AnimState:PlayAnimation("idle")
    end

    inst.components.entitytracker:ForgetEntity("repairer")
end

local function MakeWhole(inst, play_growth_anim)
    if play_growth_anim then
        inst.AnimState:PlayAnimation("growth")
        inst.AnimState:PushAnimation("idle_thulecite", false)
    else
        inst.AnimState:PlayAnimation("idle_thulecite")
    end

    inst.components.workable.workleft = inst.components.workable.workleft <= 0 and inst.components.workable.maxwork or inst.components.workable.workleft
    inst.components.workable.workable = true
    inst.components.trader:Disable()
end

local function OnTimerDone(inst, data)
    if data ~= nil and data.name == "repair" then
        MakeWhole(inst, true)
    end
end

local goods = {
    {'thulecite_pieces',  1.0},
    {'thulecite_pieces',  1.0},
    {'thulecite_pieces',  1.0},
    {'thulecite_pieces',  0.75},
    {'thulecite_pieces',  0.5},
    {'thulecite_pieces',  0.25},
    {'thulecite_pieces',  0.25},
    {'thulecite',  0.01},
}

local function DropTheGoods(inst)
    for i, v in ipairs(goods) do
        if math.random() <= v[2] then
            local dust = SpawnPrefab(v[1])
            dust.Transform:SetPosition(inst:GetPosition():Get())
            Launch(dust, inst, 1)
        end
    end
end

local function OnFinishWork(inst, worker)
    DropTheGoods(inst)
    inst.components.workable.workable = false

    inst.AnimState:PlayAnimation("idle")

    inst.SoundEmitter:PlaySoundWithParams("dontstarve/creatures/together/antlion/sfx/ground_break", { size = 1 })
    inst.components.trader:Enable()
end

local function OnLoadPostPass(inst, ents, data)
    if inst.components.workable.workleft <= 0 then
        inst.components.workable.workable = false
        inst.AnimState:PlayAnimation("idle")
        inst.components.trader:Enable()
    else
        MakeWhole(inst, false)
    end

    if inst.components.timer:TimerExists("repair") then
        PauseRepairing(inst)
    end
end

local function onsleep(inst, sleeper)

end

local function OnGetItemFromPlayer(inst, giver, item)
    if giver:HasTag("dustmoth") and item and item.prefab == "dustmeringue" and not inst.components.timer:TimerExists("repair")  then
        item:Remove()
        StartRepairing(inst, giver)
    else
        local dust = SpawnPrefab("dustmeringue")
		dust.Transform:SetPosition(inst:GetPosition():Get())
		Launch(dust, inst, 1)
    end
end

local function ShouldAcceptItem(inst, item)
	return item and item.prefab == "dustmeringue" and not inst.components.timer:TimerExists("repair")
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeSmallObstaclePhysics(inst, .5)

    inst.MiniMapEntity:SetIcon("dustmothden.png")

    inst.AnimState:SetBank("dustmothden")
    inst.AnimState:SetBuild("dustmothden")
    inst.AnimState:PlayAnimation("idle")

    inst:SetPrefabNameOverride("dustmothden")

    inst:AddTag("structure")
    inst:AddTag("dustmothdenp")

    MakeSnowCoveredPristine(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst._start_repairing_fn = StartRepairing
    inst._pause_repairing_fn = PauseRepairing

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.MINE)
    inst.components.workable:SetMaxWork(TUNING.DUSTMOTHDEN_MAXWORK)
    inst.components.workable:SetWorkLeft(TUNING.DUSTMOTHDEN_MAXWORK)
    inst.components.workable:SetOnFinishCallback(OnFinishWork)
    inst.components.workable.savestate = true

    inst.dust_collected = 0

    --[[
    inst:AddComponent("multisleepingbag")
	inst.components.multisleepingbag:MustHaveTag("dustmoth")
	inst.components.multisleepingbag.onsleep = onsleep
    inst.components.multisleepingbag.onwake = onwake
    --convert wetness delta to drying rate
    inst.components.multisleepingbag.dryingrate = math.max(0, -TUNING.SLEEP_WETNESS_PER_TICK / TUNING.SLEEP_TICK_PERIOD)
	inst.hunger_tick = TUNING.SLEEP_HUNGER_PER_TICK / 6
	inst.sleeptask = inst:DoPeriodicTask(TUNING.SLEEP_TICK_PERIOD, onsleeptick, nil, inst.components.multisleepingbag.sleepers)]]

    inst.components.workable.workleft = 0
    inst.components.workable.workable = false

    inst:AddComponent("timer")
    inst:AddComponent("entitytracker")

    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('dustmothden')

    inst:AddComponent("trader")
    inst.components.trader:SetAcceptTest(ShouldAcceptItem)
    inst.components.trader.onaccept = OnGetItemFromPlayer
    inst.components.trader.onrefuse = function() end

    inst:AddComponent("inspectable")

    inst:ListenForEvent("timerdone", OnTimerDone)

    MakeSnowCovered(inst)

    MakeHauntableWork(inst)

    inst.OnLoadPostPass = OnLoadPostPass

    return inst
end

return Prefab("dustmothdenp", fn, assets, prefabs),
MakePlacer("dustmothdenp_placer", "dustmothden", "dustmothden", "idle")
