local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "cookiecutterp"

local assets = 
{
	Asset("ANIM", "anim/cookiecutter_build.zip"),
	Asset("ANIM", "anim/cookiecutter.zip"),
	Asset("ANIM", "anim/cookiecutter_water.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.COOKIECUTTER.HEALTH,
	hunger = 125,
	hungerrate = 0.025, 
	sanity = 50,
	
	runspeed = TUNING.COOKIECUTTER.RUN_SPEED,
	walkspeed = TUNING.COOKIECUTTER.WANDER_SPEED,
	
	attackperiod = 2,
	damage = 30,--TUNING.COOKIECUTTER.DAMAGE,
	range = 1,
	hit_range = 1,
	
	bank = "cookiecutter",
	build = "cookiecutter_build",
	shiny = "cookiecutter",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('cookiecutterp',
{
    {"monstermeat",			1.0},
    {"cookiecuttershell",	1.0},
})

local sounds =
{
    attack = "saltydog/creatures/cookiecutter/attack",
	eat = "saltydog/creatures/cookiecutter/eat_LP",
	eat_item = "saltydog/creatures/cookiecutter/bite",
	eat_finish = "saltydog/creatures/cookiecutter/attack",
	jump = "turnoftides/common/together/water/splash/jump_small",
	splash = "turnoftides/common/together/water/splash/small",
	land = "turnoftides/common/together/boat/damage_small",
	hit = "saltydog/creatures/cookiecutter/hit",
    death = "saltydog/creatures/cookiecutter/death",
}
--==============================================
--					Mob Functions
--==============================================
local function SetSortOrderIsInWater(inst, inwater)
	if inwater and TheNet:GetServerGameMode() ~= "lavaarena"then
		inst.AnimState:SetSortOrder(ANIM_SORT_ORDER_BELOW_GROUND.BOAT_LIP)
		inst.AnimState:SetLayer(LAYER_BELOW_GROUND)
	else
		inst.AnimState:SetSortOrder(0)
		inst.AnimState:SetLayer(LAYER_WORLD)
	end
end

local function OnSink(inst)
	inst:ClearBufferedAction()
	inst.Physics:SetActive(true)
	inst.Physics:CollidesWith(COLLISION.CHARACTERS)
	inst.should_drill = false
	if inst.SoundEmitter:PlayingSound("eat_LP") then inst.SoundEmitter:KillSound("eat_LP") end
	inst.sg:GoToState("idle")
end

local function OnTeleported(inst)
	inst.components.amphibiouscreature:OnExitOcean()
end

local function OnSubmerge(inst)
	inst.should_drill = false
	inst.should_start_drilling = false
	inst:AddTag("submerged")
	inst:DoTaskInTime(TUNING.COOKIECUTTER.RESURFACE_DELAY, function()
		inst:PushEvent("onresurface")
	end)
	inst.sg:GoToState("idle")
	inst:RemoveFromScene()
end

local function OnResurface(inst)
	local resurfacepoint = inst.components.knownlocations:GetLocation("resurfacepoint")
		or inst.components.knownlocations:GetLocation("home")
		or inst:GetPosition()
	local rand = math.random()
	local offset = FindSwimmableOffset(resurfacepoint, math.random() * PI * 2, (1 - rand * rand) * TUNING.COOKIECUTTER.MAX_RESURFACE_RADIUS + inst:GetPhysicsRadius(0), 8, false, true)

	if offset ~= nil then
		inst.is_fleeing = false

		inst.should_drill = false
		inst.should_start_drilling = false

		inst:RemoveTag("submerged")

		inst.Transform:SetPosition(resurfacepoint.x + offset.x, 0, resurfacepoint.z + offset.z)
		inst.Physics:CollidesWith(COLLISION.CHARACTERS)
		inst:ReturnToScene()

		inst:setsortorderisinwaterfn(true)
		inst.sg:GoToState("surface")
	else
		-- Try again with delay
		inst:DoTaskInTime(1, function() OnResurface(inst) end)
	end
end
--==============================================
--				Custom Common Functions
--==============================================
local function OnEat(inst, food)
	if food and food.components.edible and food.components.edible.foodtype == FOODTYPE.WOOD then
		inst.components.hunger:DoDelta(food.prefab == "boards" and 40 or 20, false)
	end
end
--==============================================
--					Loading/Saving
--==============================================

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst) --Will ocean only mobs be compatible for FF?
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.COOKIECUTTER)
	
	inst.mobsleep = false	
	inst.shouldwalk = true
	
	inst:AddTag("moreaggro") --TODO remove this when FF rework releases
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("animal")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt2 = true
	inst.shouldwalk = false
	
	inst.sounds = sounds
	
	inst.getskins = getskins
	
	local body_symbol = "carrat_body"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeSmallFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(2,2,2) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater:SetDiet({ FOODTYPE.WOOD }, { FOODTYPE.WOOD })
	inst.components.eater.oneatfn = OnEat
	inst:AddComponent("cookiecutterdrill")
	inst.components.cookiecutterdrill.drill_duration = TUNING.COOKIECUTTER.DRILL_TIME
	inst.components.cookiecutterdrill.drill_damage = -TUNING.COOKIECUTTER.DRILL_DAMAGE
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .5)
	inst:SetPhysicsRadiusOverride(.5)
	inst.Transform:SetSixFaced()
	
	inst.DynamicShadow:SetSize(1, 0.75)
	inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Amphibious--
	inst:AddComponent("embarker")
	inst.components.embarker.embark_speed = 5.8
	inst.components.embarker.antic = true
	inst.components.embarker.embarker_min_dist = TUNING.COOKIECUTTER.BOARDING_DISTANCE + math.random() * TUNING.COOKIECUTTER.BOARDING_DISTANCE_VARIANCE
	inst.components.locomotor.hop_distance = 2
	PlayablePets.SetAmphibious(inst, "cookiecutter", "cookiecutter", false, true)
	if inst.components.amphibiouscreature then
		inst.components.amphibiouscreature:SetEnterWaterFn(
        function(inst)
			if not inst:HasTag("playerghost") then
				inst.components.locomotor.runspeed = TUNING.COOKIECUTTER.RUN_SPEED
				inst.should_drill = false
				inst.components.embarker.embarker_min_dist = TUNING.COOKIECUTTER.BOARDING_DISTANCE + math.random() * TUNING.COOKIECUTTER.BOARDING_DISTANCE_VARIANCE
			end	
        end)
		inst.components.amphibiouscreature:SetExitWaterFn(
			function(inst)
				if not inst:HasTag("playerghost") then
					local x, y, z = inst.Transform:GetWorldPosition()
					if TheWorld.Map:GetPlatformAtPoint(x, z) ~= nil then
						inst.should_drill = true
						inst.should_start_drilling = true
					else
						if inst.components.health then
							inst.components.health:Kill()
						end
					end
				end
			end)
	end	
	inst.setsortorderisinwaterfn = SetSortOrderIsInWater
	inst:setsortorderisinwaterfn(true)
	inst.components.locomotor:SetAllowPlatformHopping(true)
	--inst.components.locomotor.pathcaps = { allowocean = true, ignoreLand = true }	
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) inst.components.locomotor:SetAllowPlatformHopping(true) end)
    end)
	
	inst.OnSave = PlayablePets.OnSaveCommon
    inst.OnLoad = PlayablePets.OnLoadCommon
	
    return inst	
end


return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
