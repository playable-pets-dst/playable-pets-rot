local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "squidp"

local assets = 
{
    Asset("ANIM", "anim/squid.zip"),
    Asset("ANIM", "anim/squid_water.zip"),
    Asset("ANIM", "anim/squid_build.zip"),
	
	Asset("ANIM", "anim/squid_watershoot.zip"),
}

local prefabs =
{
    "lightbulb",
    "squidherd",
    "wake_small",
    "squideyelight",
    "inksplat",
    "squid_ink_player_fx",
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local MOB_TUNING = TUNING[string.upper(prefabname)]
local mob = 
{
	health       = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger       = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate   = TUNING.WILSON_HUNGER_RATE, 
	sanity       = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed     = MOB_TUNING.RUNSPEED,
	walkspeed    = MOB_TUNING.WALKSPEED,
	damage       = MOB_TUNING.DAMAGE,
	range        = MOB_TUNING.RANGE, 
	hit_range    = MOB_TUNING.HIT_RANGE,
	attackperiod = MOB_TUNING.ATTACK_PERIOD,
	scale 		 = MOB_TUNING.SCALE,
	
	bank = "squiderp",
	build = "squid_build",
	shiny = "squid",
	
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('squidp',
{
    {'monstermeat', 1.000},
    {'lightbulb', 0.33},  
})

local easing = require("easing")

local sounds =
{
    attack = "hookline/creatures/squid/attack",
    bite = "hookline/creatures/squid/gobble",
    taunt = "hookline/creatures/squid/taunt",
    death = "hookline/creatures/squid/death",
    sleep = "hookline/creatures/squid/sleep",
    hurt = "hookline/creatures/squid/hit",
    gobble = "hookline/creatures/squid/gobble",
    spit = "hookline/creatures/squid/spit",
    swim = "turnoftides/common/together/water/swim/medium",
}

--==============================================
--					Mob Functions
--==============================================
local WAKE_TO_FOLLOW_DISTANCE = 8
local SLEEP_NEAR_HOME_DISTANCE = 10
local SHARE_TARGET_DIST = 30
local HOME_TELEPORT_DIST = 30

local NO_TAGS = { "FX", "NOCLICK", "DECOR", "INLIMBO" }

--Called from stategraph
local function LaunchProjectile(inst, targetpos)
    local x, y, z = inst.Transform:GetWorldPosition()

    local projectile = SpawnPrefab("inksplat")
    projectile.Transform:SetPosition(x, y, z)
	projectile.SetSource = PlayablePets.SetProjectileSource

    --V2C: scale the launch speed based on distance
    --     because 15 does not reach our max range.
    local dx = targetpos.x - x
    local dz = targetpos.z - z
    local rangesq = dx * dx + dz * dz
    local maxrange = TUNING.FIRE_DETECTOR_RANGE
    --local speed = easing.linear(rangesq, 15, 3, maxrange * maxrange)
    local speed = easing.linear(rangesq, 15, 1, maxrange * maxrange)
    projectile.components.complexprojectile:SetHorizontalSpeed(speed)
    projectile.components.complexprojectile:SetGravity(-35)
    projectile.components.complexprojectile:Launch(targetpos, inst, inst)
end

local function geteatchance(inst,target)
    return 0.3
end
--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.SQUID)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("squid")
	inst:AddTag("scarytooceanprey")
    inst:AddTag("monster")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.shouldwalk = false
	
	inst.sounds = sounds	
	--inst:RemoveComponent("inkable")
	
	local body_symbol = "squid_body"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.LaunchProjectile = LaunchProjectile
    inst.geteatchance = geteatchance
	----------------------------------
    --Playable Pets
    inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SLEEP, PP_ABILITIES.PP_SLEEP)
    inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SKILL1, PP_ABILITIES.SKILL1)
	inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SKILL2, PP_ABILITIES.SKILL2)
	inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SKILL3, PP_ABILITIES.SKILL3)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(2,2,2) --This multiplies food stats.
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, 0.5)
	inst.Transform:SetSixFaced()
	inst.DynamicShadow:SetSize(2.5, 1.5)
	--inst.DynamicShadow:Enable(false)  
	PlayablePets.SetAmphibious(inst, "squiderp", "squiderp_water")
	if inst.components.amphibiouscreature then
		inst.components.amphibiouscreature:SetEnterWaterFn(
			function(inst)
				inst.hop_distance = inst.components.locomotor.hop_distance
				inst.components.locomotor.hop_distance = 4
				inst.DynamicShadow:Enable(false)
			end)            
		inst.components.amphibiouscreature:SetExitWaterFn(
			function(inst)
				if inst.hop_distance then
					inst.components.locomotor.hop_distance = inst.hop_distance
				end
				inst.DynamicShadow:Enable(true)
			end)
	end		
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
		if inst.eyeglow then
			inst.eyeglow.Light:Enable(true)
		end
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	if not inst.eyeglow then
		inst.eyeglow = SpawnPrefab("squideyelight")
		inst.eyeglow.entity:SetParent(inst.entity) --prevent 1st frame sleep on clients
		inst.eyeglow.entity:AddFollower()
		inst.eyeglow.Follower:FollowSymbol(inst.GUID, "glow", 0, 0, 0)    
	end
	
    return inst	
end

local function skin_fn()
	inst.entity:AddTransform()
    inst.entity:AddAnimState()
	
	inst.AnimState:SetBank(mob.bank)
	inst.AnimState:SetBuild(mob.build)
	inst.AnimState:PlayAnimation("idle")
	
	
	inst.Transform:SetSixFaced()
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end  

	return inst
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
