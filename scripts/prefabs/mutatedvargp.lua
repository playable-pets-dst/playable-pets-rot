local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "mutatedvargp"

local assets = 
{
	
	Asset("ANIM", "anim/warg_actions.zip"),
	Asset("ANIM", "anim/warg_mutated_actions.zip"),
    Asset("ANIM", "anim/lunar_flame.zip"),
    Asset("SOUND", "sound/vargr.fsb"),
}

local prefabs = {
	"hound"
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {"1"}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	
	runspeed = TUNING.WARG_RUNSPEED,
	walkspeed = TUNING.WARG_RUNSPEED,
	
	attackperiod = 3,
	damage = 50*2,
	range = TUNING.WARG_ATTACKRANGE,
	hit_range = TUNING.WARG_ATTACKRANGE,
	
	bank = "warg",
	build = "warg_mutated_actions",
	shiny = "warg",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
{
    { "spoiled_food",				  1.0  },
	{ "spoiled_food",				  1.0  },
	{ "spoiled_food",				  0.5  },
	{ "purebrilliance",				  1.0  },
	{ "purebrilliance",				  0.75 },
})

local sounds =
{
	idle = "rifts3/mutated_varg/idle",
	howl = "rifts3/mutated_varg/howl",
	hit = "rifts3/mutated_varg/hit",
	attack = "rifts3/mutated_varg/attack",
	death = "rifts3/mutated_varg/death",
	sleep = "rifts3/mutated_varg/sleep",
}

local FORGE_STATS = PPROT_FORGE.VARG
--==============================================
--					Mob Functions
--==============================================

local function CanShareTarget(dude)
    return dude:HasTag("hound")
        and not dude:IsInLimbo()
        and not (dude.components.health:IsDead() or dude:HasTag("player"))
end

local function OnAttacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, CanShareTarget, 5)
end

local TARGETS_MUST_TAGS = {"player"}
local TARGETS_CANT_TAGS = {"playerghost"}
local function NumHoundsToSpawn(inst)
    local numHounds = inst.base_hound_num

    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, TUNING.WARG_NEARBY_PLAYERS_DIST, TARGETS_MUST_TAGS, TARGETS_CANT_TAGS)
    for i, player in ipairs(ents) do
        local playerAge = player.components.age:GetAgeInDays()
        local addHounds = math.clamp(Lerp(1, 4, playerAge/100), 1, 4)
        if inst.spawn_fewer_hounds then
            addHounds = math.ceil(addHounds/2)
        end
        numHounds = numHounds + addHounds
    end
	local numFollowers = inst.components.leader:CountFollowers() + inst.numfollowercorpses
    local num = math.min(numFollowers+numHounds/2, numHounds) -- only spawn half the hounds per howl
    num = (math.log(num)/0.4)+1 -- 0.4 is approx log(1.5)

    num = RoundToNearest(num, 1)

    if inst.max_hound_spawns then
        num = math.min(num,inst.max_hound_spawns)
    end

    return num - numFollowers
end

local function NoHoundsToSpawn(inst)
    return 0
end

local function SpawnHounds(inst, radius_override)
    local hounds = nil
    local hounded = TheWorld.components.hounded
    if hounded == nil then
        return hounds
    end

    local num = inst:NumHoundsToSpawn()
    if inst.max_hound_spawns then
        num = math.min(num,inst.max_hound_spawns)
        inst.max_hound_spawns = inst.max_hound_spawns - num
    end

	local forcemutate = inst:HasTag("lunar_aligned") or nil
    local pt = inst:GetPosition()
    for i = 1, num do
        local hound = hounded:SummonSpawn(pt, radius_override)
        if hound ~= nil then
            if hound.components.follower ~= nil then
                hound.components.follower:SetLeader(inst)
            end
            if hounds == nil then
                hounds = {}
            end
            hound.components.lootdropper:SetChanceLootTable({})
            table.insert(hounds, hound)
        end
    end
    return hounds
end

local function SpawnHound(inst)
	if not TheWorld:HasTag("cave") and not inst.components.revivablecorpse then
		local hounded = TheWorld.components.hounded
		if hounded ~= nil then
			local num = NumHoundsToSpawn(inst)
			local pt = inst:GetPosition()
			for i = 1, num do
				local hound = hounded:SummonSpawn(pt)
				if hound ~= nil then
					hound.components.follower:SetLeader(inst)
				end
			end
		end
	elseif inst.components.revivablecorpse then --Forge
		for i = 1, PP_FORGE.VARG.AMOUNT or 1 do
			local theta = math.random() * 2 * _G.PI
			local pt = inst:GetPosition()
			local radius = math.random(1, 2)
			local offset = _G.FindWalkableOffset(pt, theta, radius, 1.5, true, true)
				if offset ~= nil then
					pt.x = pt.x + offset.x
					pt.z = pt.z + offset.z
				end
			local fx = SpawnPrefab("lavaarena_portal_player_fx").Transform:SetPosition(pt.x, 0, pt.z)
			local pet = SpawnPrefab("hound")
			pet.Transform:SetPosition(pt.x, 0, pt.z)
			pet:Hide()
			pet:DoTaskInTime(0.3, function(inst) inst:Show() end)
			pet:AddTag("companion")
			pet.daddy = inst
			pet.components.health.redirect = NoDmgFromPlayers
			
			inst.components.leader:AddFollower(pet)
			pet.components.follower:KeepLeaderOnAttacked()
			pet.components.follower.keepleader = true
			
			pet.components.combat:SetDamageType(1)	
			pet.components.combat:SetDefaultDamage(15)
			pet.components.combat:SetRetargetFunction(1, RetargetForgeFn)
			pet.components.combat:SetKeepTargetFunction(KeepForgeTarget)
			
			pet:AddComponent("debuffable")
			pet.components.debuffable:IsEnabled(true)
			pet:AddComponent("colouradder")
			pet.components.lootdropper:SetChanceLootTable({})
			
			pet:RemoveEventCallback("onattacked", OnAttacked)
			pet:ListenForEvent("onattacked", OnForgeAttacked)
			--if inst.components.stat_tracker then inst.components.stat_tracker:InitializePetStats(pet) end
		end
		
	end	
end

local function OnCorpseRemoved(corpse)
	local inst = corpse._warg
	inst.followercorpses[corpse] = nil
	inst.numfollowercorpses = inst.numfollowercorpses - 1
end

local function RememberFollowerCorpse(inst, corpse)
	if inst.followercorpses[corpse] == nil then
		corpse._warg = inst
		inst.followercorpses[corpse] = true
		inst.numfollowercorpses = inst.numfollowercorpses + 1
		inst:ListenForEvent("onremove", OnCorpseRemoved, corpse)
	end
end

local function ForgetFollowerCorpse(inst, corpse)
	if inst.followercorpses[corpse] ~= nil then
		inst:RemoveEventCallback("onremove", OnCorpseRemoved, corpse)
		OnCorpseRemoved(corpse)
		corpse._warg = nil
	end
end
--==============================================
--				Custom Common Functions
--==============================================	

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local function OnForgeHitOther(inst, other)
    local followers = inst.components.leader.followers
	if other and other:IsValid() then
		inst.components.combat:ShareTarget(other, 10, CanShareTarget, 20)
		for i, v in ipairs(followers) do
			v.components.combat:SetTarget(other)
		end
	end
end

local function DoAttackBuff(target)
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.defbuffed and target.components.debuffable then
		target.components.debuffable:AddDebuff("hound_buff", "hound_buff")	
	end
end

local function DoHoundAura(inst)
	--OnPulse(inst)
	if inst.components.health and not inst.components.health:IsDead() then
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 30, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO" }, {"player"} )
    for i, v in ipairs(ents) do
		if v ~= inst and v:HasTag("hound") then
			DoAttackBuff(v)
		end
    end
	end
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false
	inst.taunt2 = false
	
	inst.acidmult = 1.5
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	inst.components.combat.onhitotherfn = OnForgeHitOther
	inst.components.revivablecorpse.revivespeedmult = 1.5
	
	inst.DoAura = inst:DoPeriodicTask(TUNING.FORGE.BATTLESTANDARD.PULSE_TICK, DoHoundAura)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local function Mutated_OnRemove(inst)
	if inst.flame_pool ~= nil then
		for i, v in ipairs(inst.flame_pool) do
			v:Remove()
		end
		inst.flame_pool = nil
	end
	if inst.ember_pool ~= nil then
		for i, v in ipairs(inst.ember_pool) do
			v:Remove()
		end
		inst.ember_pool = nil
	end
end

local function Mutated_OnTemp8Faced(inst)
	if inst.temp8faced:value() then
		inst.gestalt.Transform:SetEightFaced()
		inst.eyeL.Transform:SetEightFaced()
		inst.eyeR.Transform:SetEightFaced()
		inst.mouthL.Transform:SetEightFaced()
		inst.mouthR.Transform:SetEightFaced()
	else
		inst.gestalt.Transform:SetSixFaced()
		inst.eyeL.Transform:SetSixFaced()
		inst.eyeR.Transform:SetSixFaced()
		inst.mouthL.Transform:SetSixFaced()
		inst.mouthR.Transform:SetSixFaced()
	end
end

local function Mutated_SwitchToEightFaced(inst)
	if not inst.temp8faced:value() then
		inst.temp8faced:set(true)
		if not TheNet:IsDedicated() then
			Mutated_OnTemp8Faced(inst)
		end
		inst.Transform:SetEightFaced()
	end
end

local function Mutated_SwitchToSixFaced(inst)
	if inst.temp8faced:value() then
		inst.temp8faced:set(false)
		if not TheNet:IsDedicated() then
			Mutated_OnTemp8Faced(inst)
		end
		inst.Transform:SetSixFaced()
	end
end

local function Mutated_CreateGestaltFlame()
	local inst = CreateEntity()

	inst:AddTag("FX")
	--[[Non-networked entity]]
	--inst.entity:SetCanSleep(false) --commented out; follow parent sleep instead
	inst.persists = false

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddFollower()

	inst.Transform:SetSixFaced()

	inst.AnimState:SetBank("lunar_flame")
	inst.AnimState:SetBuild("lunar_flame")
	inst.AnimState:PlayAnimation("gestalt_eye", true)
	inst.AnimState:SetMultColour(1, 1, 1, 0.6)
	inst.AnimState:SetLightOverride(0.1)
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
	inst.AnimState:UsePointFiltering(true)

	return inst
end

local function Mutated_CreateEyeFlame()
	local inst = CreateEntity()

	inst:AddTag("FX")
	--[[Non-networked entity]]
	--inst.entity:SetCanSleep(false) --commented out; follow parent sleep instead
	inst.persists = false

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddFollower()

	inst.Transform:SetSixFaced()

	inst.AnimState:SetBank("lunar_flame")
	inst.AnimState:SetBuild("lunar_flame")
	inst.AnimState:PlayAnimation("flameanim", true)
	inst.AnimState:SetMultColour(1, 1, 1, 0.6)
	inst.AnimState:SetLightOverride(0.1)
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

	return inst
end

local function Mutated_CreateMouthFlame()
	local inst = CreateEntity()

	inst:AddTag("FX")
	--[[Non-networked entity]]
	--inst.entity:SetCanSleep(false) --commented out; follow parent sleep instead
	inst.persists = false

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddFollower()

	inst.Transform:SetSixFaced()

	inst.AnimState:SetBank("lunar_flame")
	inst.AnimState:SetBuild("lunar_flame")
	inst.AnimState:PlayAnimation("mouthflameanim", true)
	inst.AnimState:SetMultColour(1, 1, 1, 0.6)
	inst.AnimState:SetLightOverride(0.1)
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

	return inst
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("monster")
    inst:AddTag("warg")
    inst:AddTag("houndfriend")
    inst:AddTag("lunar_aligned")
    inst:AddTag("epic")
	----------------------------------

    inst.temp8faced = net_bool(inst.GUID, prefabname..".temp8faced", "temp8faceddirty")
    inst.AnimState:SetSymbolBloom("breath_02")
	inst.AnimState:SetSymbolBrightness("breath_02", 1.5)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)

    if not TheNet:IsDedicated() then
        inst.gestalt = Mutated_CreateGestaltFlame()
        inst.gestalt.entity:SetParent(inst.entity)
        inst.gestalt.Follower:FollowSymbol(inst.GUID, "swap_gestalt_flame", 0, 0, 0, true)
        local frames = inst.gestalt.AnimState:GetCurrentAnimationNumFrames()
        local rnd = math.random(frames) - 1
        inst.gestalt.AnimState:SetFrame(rnd)

        inst.eyeL = Mutated_CreateEyeFlame()
        inst.eyeL.entity:SetParent(inst.entity)
        inst.eyeL.Follower:FollowSymbol(inst.GUID, "flameL", 0, 0, 0, true)
        frames = inst.eyeL.AnimState:GetCurrentAnimationNumFrames()
        rnd = math.random(frames) - 1
        inst.eyeL.AnimState:SetFrame(rnd)

        inst.eyeR = Mutated_CreateEyeFlame()
        inst.eyeR.entity:SetParent(inst.entity)
        inst.eyeR.Follower:FollowSymbol(inst.GUID, "flameR", 0, 0, 0, true)
        rnd = (rnd + math.floor((0.35 + math.random() * 0.35) * frames)) % frames
        inst.eyeR.AnimState:SetFrame(rnd)

        inst.mouthL = Mutated_CreateMouthFlame()
        inst.mouthL.entity:SetParent(inst.entity)
        inst.mouthL.Follower:FollowSymbol(inst.GUID, "mouthflameL", 0, 0, 0, true)
        frames = inst.mouthL.AnimState:GetCurrentAnimationNumFrames()
        rnd = math.random(frames) - 1
        inst.mouthL.AnimState:SetFrame(rnd)

        inst.mouthR = Mutated_CreateMouthFlame()
        inst.mouthR.entity:SetParent(inst.entity)
        inst.mouthR.Follower:FollowSymbol(inst.GUID, "mouthflameR", 0, 0, 0, true)
        rnd = (rnd + math.floor((0.35 + math.random() * 0.35) * frames)) % frames
        inst.mouthR.AnimState:SetFrame(rnd)

        if not TheWorld.ismastersim then
            inst:ListenForEvent("temp8faceddirty", Mutated_OnTemp8Faced)
        end
    end
end



local master_postinit = function(inst)
    inst.sounds = sounds
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	inst.components.combat:SetHurtSound("dontstarve_DLC001/creatures/vargr/hit")
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
    inst.base_hound_num = TUNING.WARG_BASE_HOUND_AMOUNT
    inst.flame_pool = {}
	inst.ember_pool = {}
	inst.canflamethrower = true
	inst.hit_recovery = 2
    inst.numfollowercorpses = 0
	inst.followercorpses = {}
	inst.RememberFollowerCorpse = RememberFollowerCorpse
	inst.ForgetFollowerCorpse = ForgetFollowerCorpse
    inst.SpawnHounds = SpawnHounds
    inst.SpawnHound = SpawnHound
    inst.NumHoundsToSpawn = NumHoundsToSpawn
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	inst:AddComponent("planarentity")
	inst:AddComponent("planardamage")
	inst.components.planardamage:SetBaseDamage(TUNING.MUTATED_WARG_PLANAR_DAMAGE)
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT })
    inst.components.eater:SetAbsorptionModifiers(1,1,1)
	inst.components.eater:SetCanEatHorrible()
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1000, 1)
    inst.DynamicShadow:SetSize(2.5, 1.5)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", OnAttacked)
	--inst.OnRemoveEntity = Mutated_OnRemove

	inst.SwitchToEightFaced = Mutated_SwitchToEightFaced
    inst.SwitchToSixFaced = Mutated_SwitchToSixFaced
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)