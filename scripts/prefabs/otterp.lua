local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "otterp"

local assets = 
{
	Asset("ANIM", "anim/otter_basic.zip"),
    Asset("ANIM", "anim/otter_basic_water.zip"),
    Asset("ANIM", "anim/otter_build.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local MOB_TUNING = TUNING[string.upper(prefabname)]
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed     = MOB_TUNING.RUNSPEED,
	walkspeed    = MOB_TUNING.WALKSPEED,
	damage       = MOB_TUNING.DAMAGE,
	range        = MOB_TUNING.RANGE, 
	hit_range    = MOB_TUNING.HIT_RANGE, --range is 1.8 in stategraph.
	attackperiod = MOB_TUNING.ATTACK_PERIOD,
	
	bank = "otter_basics",
	build = "otter_build",
	shiny = "otter",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

SetSharedLootTable('otterp',
{
	{ "meat",            1.00 },
    { "smallmeat",       1.00 },
    { "messagebottle",   0.05 },
})

local sounds =
{
	attack = "pp_sounds/creatures/otter/attack",
	attack_vo = "pp_sounds/creatures/otter/attack_vo",
	taunt = "pp_sounds/creatures/otter/taunt",
	taunt_vo = "pp_sounds/creatures/otter/taunt_vo",
	bite = "pp_sounds/creatures/otter/bite",
	sleep_pre = "pp_sounds/creatures/otter/sleep_pre",
	sleep_in = "pp_sounds/creatures/otter/sleep_in",
	sleep_out = "pp_sounds/creatures/otter/sleep_out",
	impact = "pp_sounds/creatures/otter/impact",
	whoosh = "pp_sounds/creatures/otter/whoosh",
	death = "pp_sounds/creatures/otter/death",
	eat_pre = "meta4/otter/eat_chomp_f17",
	eat_pst = "pp_sounds/creatures/otter/eat_pst",
	step = "pp_sounds/creatures/otter/step",
}

--==============================================
--					Mob Functions
--==============================================
local function TossFish(inst, item)
    if not item:HasTag("oceanfishable_creature") then
        return false
    end

    local item_loots = (item.fish_def and item.fish_def.loot) or nil
    if not item_loots then
        return false
    end

    local ix, iy, iz = item.Transform:GetWorldPosition()
    local owner = item
    for _, loot_prefab in pairs(item_loots) do
        local loot = SpawnPrefab(loot_prefab)
        loot.Transform:SetPosition(ix, iy, iz)
        Launch2(loot, inst, 2, 0, 2, 0)
    end
    owner:Remove()

    return true
end

local function EnterWater(inst)
    inst.hop_distance = inst.components.locomotor.hop_distance
    inst.components.locomotor.hop_distance = TUNING.OTTER_WATER_HOP_DISTANCE
    inst.DynamicShadow:Enable(false)
end

local function ExitWater(inst)
    inst.components.locomotor.hop_distance = inst.hop_distance or TUNING.DEFAULT_LOCOMOTOR_HOP_DISTANCE
    inst.hop_distance = nil
    inst.DynamicShadow:Enable(true)
end

local function OnStoleItem(inst, victim_object, item)
	--???
end

local function OnHitOther(inst, other, damage)
    if other.components.inventory ~= nil and inst.components.theif then
		inst.components.thief:StealItem(other)
	end
end
--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.CARRAT)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("animal")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	inst.sounds = sounds
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = false
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeSmallFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	inst.components.combat:SetHurtSound("meta4/otter/vo_hit_f0")
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	inst:AddComponent("thief")
	inst.components.thief:SetOnStolenFn(OnStoleItem)
	----------------------------------
	--Eater--
    --inst.components.eater:SetAbsorptionModifiers(2,2,2) --This multiplies food stats.
	inst.components.eater:SetDiet({FOODTYPE.MEAT}, {FOODTYPE.MEAT})
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater:SetStrongStomach(true) -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, 0.4)
	inst.Transform:SetSixFaced()	
	inst.DynamicShadow:SetSize(4.0, 2.5) 
	
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank.."_water")
	inst.components.embarker.embark_speed = TUNING.OTTER_RUNSPEED
	if inst.components.amphibiouscreature then
		inst.components.amphibiouscreature:SetEnterWaterFn(EnterWater)
		inst.components.amphibiouscreature:SetExitWaterFn(ExitWater)
	end	
	---------------------------------
	--Listeners--
	inst.TossFish = TossFish

	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) inst.components.locomotor:SetAllowPlatformHopping(false) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) inst.components.locomotor:SetAllowPlatformHopping(false) end) --TODO properfix for this
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)