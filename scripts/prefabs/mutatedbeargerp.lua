local MakePlayerCharacter = require "prefabs/player_common"


local assets =
{
    Asset("ANIM", "anim/bearger_build.zip"),
    Asset("ANIM", "anim/bearger_basic.zip"),
    Asset("ANIM", "anim/bearger_actions.zip"),
    Asset("ANIM", "anim/bearger_yule.zip"),
    Asset("SOUND", "sound/bearger.fsb"),
}

local prefabname = "mutatedbeargerp"

local prefabs =
{
    "groundpound_fx",
    "groundpoundring_fx",
    "bearger_fur",
    "furtuft",
    "meat",
    "chesspiece_bearger_sketch",
    "collapse_small",
}
	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = BOSS_STATS and TUNING.DAYWALKER_HEALTH or TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 6,
	walkspeed = TUNING.BEARGER_CALM_WALK_SPEED,
	damage = TUNING.MUTATED_BEARGER_DAMAGE,
	attackperiod = TUNING.MUTATED_BEARGER_ATTACK_PERIOD,
	range = TUNING.MUTATED_BEARGER_ATTACK_RANGE,
	bank = "bearger",
	build = "bearger_mutated",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGmutatedbeargerp",
	minimap = prefabname..".tex",	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
{
    { "spoiled_food",				1.0  },
	{ "spoiled_food",				1.0  },
	{ "spoiled_food",				1.0  },
	{ "spoiled_food",				0.5  },
	{ "purebrilliance",				1.0  },
	{ "purebrilliance",				0.75 },
})

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end


local function Mutated_OnTemp8Faced(inst)
	if inst.temp8faced:value() then
		inst.gestalt.Transform:SetEightFaced()
		inst.eyeL.Transform:SetEightFaced()
		inst.eyeR.Transform:SetEightFaced()
	else
		inst.gestalt.Transform:SetFourFaced()
		inst.eyeL.Transform:SetFourFaced()
		inst.eyeR.Transform:SetFourFaced()
	end
end

local function Mutated_SwitchToEightFaced(inst)
	if not inst.temp8faced:value() then
		inst.temp8faced:set(true)
		if not TheNet:IsDedicated() then
			Mutated_OnTemp8Faced(inst)
		end
		inst.Transform:SetEightFaced()
	end
end

local function Mutated_SwitchToFourFaced(inst)
	if inst.temp8faced:value() then
		inst.temp8faced:set(false)
		if not TheNet:IsDedicated() then
			Mutated_OnTemp8Faced(inst)
		end
		inst.Transform:SetFourFaced()
	end
end

local function Mutated_CreateGestaltFlame()
	local inst = CreateEntity()

	inst:AddTag("FX")
	--[[Non-networked entity]]
	--inst.entity:SetCanSleep(false) --commented out; follow parent sleep instead
	inst.persists = false

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddFollower()

	inst.Transform:SetFourFaced()

	inst.AnimState:SetBank("lunar_flame")
	inst.AnimState:SetBuild("lunar_flame")
	inst.AnimState:PlayAnimation("gestalt_eye", true)
	inst.AnimState:SetMultColour(1, 1, 1, 0.6)
	inst.AnimState:SetLightOverride(0.1)
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
	inst.AnimState:UsePointFiltering(true)

	return inst
end

local function Mutated_CreateEyeFlame()
	local inst = CreateEntity()

	inst:AddTag("FX")
	--[[Non-networked entity]]
	--inst.entity:SetCanSleep(false) --commented out; follow parent sleep instead
	inst.persists = false

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddFollower()

	inst.Transform:SetFourFaced()

	inst.AnimState:SetBank("lunar_flame")
	inst.AnimState:SetBuild("lunar_flame")
	inst.AnimState:PlayAnimation("flameanim", true)
	inst.AnimState:SetMultColour(1, 1, 1, 0.6)
	inst.AnimState:SetLightOverride(0.1)
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

	return inst
end

local function Mutated_OnRecoveryHealthDelta(inst, data)
	local hp = data ~= nil and data.newpercent or inst.components.health:GetPercent()
	if inst.recovery_starthp - hp > TUNING.MUTATED_BEARGER_RECOVERY_HP then
		inst.recovery_starthp = nil
		inst:RemoveEventCallback("healthdelta", Mutated_OnRecoveryHealthDelta)
		inst.canrunningbutt = not inst.recovery_norunningbutt
		inst.recovery_norunningbutt = nil
	end
end

local function Mutated_StartButtRecovery(inst, norunningbutt)
	if inst.recovery_starthp == nil then
		inst.recovery_starthp = inst.components.health:GetPercent()
		inst:ListenForEvent("healthdelta", Mutated_OnRecoveryHealthDelta)
		inst.canrunningbutt = false
	end
	inst.recovery_norunningbutt = norunningbutt
end

local function Mutated_IsButtRecovering(inst)
	return inst.recovery_starthp ~= nil
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.

    inst.temp8faced = net_bool(inst.GUID, "mutatedbearger.temp8faced", "temp8faceddirty")

    if not TheNet:IsDedicated() then
		inst.gestalt = Mutated_CreateGestaltFlame()
		inst.gestalt.entity:SetParent(inst.entity)
		inst.gestalt.Follower:FollowSymbol(inst.GUID, "swap_gestalt_flame", 0, 0, 0, true)
		local frames = inst.gestalt.AnimState:GetCurrentAnimationNumFrames()
		local rnd = math.random(frames) - 1
		inst.gestalt.AnimState:SetFrame(rnd)

		inst.eyeL = Mutated_CreateEyeFlame()
		inst.eyeL.entity:SetParent(inst.entity)
		inst.eyeL.Follower:FollowSymbol(inst.GUID, "flameL", 0, 0, 0, true)
		frames = inst.eyeL.AnimState:GetCurrentAnimationNumFrames()
		rnd = math.random(frames) - 1
		inst.eyeL.AnimState:SetFrame(rnd)

		inst.eyeR = Mutated_CreateEyeFlame()
		inst.eyeR.entity:SetParent(inst.entity)
		inst.eyeR.Follower:FollowSymbol(inst.GUID, "flameR", 0, 0, 0, true)
		rnd = (rnd + math.floor((0.35 + math.random() * 0.35) * frames)) % frames
		inst.eyeR.AnimState:SetFrame(rnd)
	end
	
end

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function oncollapse(inst, other)
	if other and other.components.workable ~= nil and other.components.workable.workleft > 0 then
		SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.workable:Destroy(inst)
	end
end

local function OnCollide(inst, other)
	if other == nil or not other:HasTag("tree") then
		return
	end
	local v1 = Vector3(inst.Physics:GetVelocity())
	if v1:LengthSq() < 1 then
		return
	end
	inst:DoTaskInTime(2*FRAMES, oncollapse, other)
end

local function OnGroundPound(inst)
	if inst.components.shedder then
		if math.random() < .2 then 
			inst.components.shedder:DoMultiShed(3, false) -- can't drop too many, or it'll be really easy to farm for thick furs
		end
	end
end

local function OnHitOther(inst, data)
	local other = data.target
	if other and other.components.freezable then
		other.components.freezable:AddColdness(2)
		other.components.freezable:SpawnShatterFX()
	end
end

--==============================================
--					Forged Forge
--==============================================
local ex_fns = require "prefabs/player_common_extensions"

local function OnHitOther_Forge(inst, target)
	if inst.sg:HasStateTag("groundpound") and target then 
		target:PushEvent("flipped", {flipper = inst})
	end
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.BEARGER)
	
	inst.mobsleep = false	
	
	inst.israged = true
	
	inst.acidmult = 2.5
	inst.healmult = 2
	
	inst:RemoveComponent("shedder")
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.groundpounder.damageRings = 0
	
	inst.components.combat:EnableAreaDamage(false)
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
----------------------------------------------

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	inst.components.combat:SetHurtSound("dontstarve_DLC001/creatures/bearger/hurt")
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('beargerp')
	----------------------------------
	--Tags--
	
	inst:AddTag("epic")
	inst:AddTag("monster")
	inst:AddTag("bearger")
    inst:AddTag("lunar_aligned")
	
	inst.israged = false
	inst.altattack = true
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
    inst.taunt3 = true
	inst.cancombo = true
	inst.canbutt = true
	inst.canrunningbutt = false
    inst.combo_attacks = 0
    inst.max_combo = 5
	inst.swipefx = "mutatedbearger_swipe_fx"

	inst.isshiny = 0
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)	

	inst.SwitchToEightFaced = Mutated_SwitchToEightFaced
	inst.SwitchToFourFaced = Mutated_SwitchToFourFaced

	inst.StartButtRecovery = Mutated_StartButtRecovery
	inst.IsButtRecovering = Mutated_IsButtRecovering

    inst:StartButtRecovery(true) --norunningbutt = true for initial recovery
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	---------------------------------
	--Physics and Scale--
	
	inst:SetPhysicsRadiusOverride(1.5)
	MakeGiantCharacterPhysics(inst, 1000, inst.physicsradiusoverride)
    inst.DynamicShadow:SetSize(6, 3.5)
    inst.Transform:SetFourFaced()

	inst.recentlycharged = {}
    inst.Physics:SetCollisionCallback(OnCollide)	
	---------------------------------
	--Ground Pound--
	inst:AddComponent("groundpounder")
	inst.components.groundpounder.destroyer = true
	inst.components.groundpounder.groundpounddamagemult = 1
	inst.components.groundpounder.damageRings = TheNet:GetServerGameMode() == "lavaarena" and 0 or 3
	inst.components.groundpounder.destructionRings = MOBFIRE == "Enable" and 0 or 3
	inst.components.groundpounder.platformPushingRings = 3
	inst.components.groundpounder.numRings = 3
	inst.components.groundpounder.radiusStepDistance = 2
	inst.components.groundpounder.ringWidth = 1.5
    inst.components.groundpounder.groundpoundFn = OnGroundPound
	
	inst.components.combat.onhitotherfn = OnHitOther_Forge
	---------------------------------
	--Shedder--
	inst:AddComponent("shedder")
	inst.components.shedder.shedItemPrefab = "furtuft"
	inst.components.shedder.shedHeight = 6.5
	inst.components.shedder:StartShedding(TUNING.BEARGER_SHED_INTERVAL)	

    inst:AddComponent("planarentity")
	inst:AddComponent("planardamage")
	inst.components.planardamage:SetBaseDamage(TUNING.MUTATED_BEARGER_PLANAR_DAMAGE)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end
return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
