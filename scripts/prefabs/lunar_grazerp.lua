local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--TODO Grazer doesn't properly delete fx on despawn.
---------------------------
local prefabname = "lunar_grazerp"

local assets = 
{
    Asset("ANIM", "anim/lunar_grazer.zip"),
}

local prefabs = 
{	
	
}
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 2.5,
	walkspeed = 2.5,
	damage = TUNING.LUNAR_GRAZER_DAMAGE,
	range = TUNING.LUNAR_GRAZER_ATTACK_RANGE, 
	hit_range = TUNING.LUNAR_GRAZER_HIT_RANGE,
	attackperiod = TUNING.LUNAR_GRAZER_ATTACK_PERIOD,
	bank = "lunar_grazer",
	build = "lunar_grazer",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{

})

local sounds =
{
    
}

local FORGE_STATS = PPROT_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local NUM_TRAIL_VARIATIONS = 7
local TRAIL_POOL = {}
local TRACKED_ENTS = {}

local function RecycleTrail(inst, fx)
	if next(TRACKED_ENTS) ~= nil then
		fx:RemoveFromScene()
		table.insert(TRAIL_POOL, fx)
	else
		fx:Remove()
	end
	if inst.last_trail == fx then
		inst.last_trail = nil
	end
end

local function SpawnTrail(inst, scale, duration, pos)
	local variation = table.remove(inst.trails, math.random(3))
	table.insert(inst.trails, variation)

	local fx
	if #TRAIL_POOL > 0 then
		fx = table.remove(TRAIL_POOL)
		fx:ReturnToScene()
	else
		fx = SpawnPrefab("lunar_goop_trail_fx")
		fx.onfinished = inst._ontrailfinished
	end
	if pos ~= nil then
		fx.Transform:SetPosition(pos.x, 0, pos.z)
	else
		local x, y, z = inst.Transform:GetWorldPosition()
		fx.Transform:SetPosition(x, 0, z)
	end
	fx:SetVariation(variation, scale, duration)

	inst.last_trail = fx
end

local function StartTracking(inst)
	TRACKED_ENTS[inst] = true
end

local function StopTracking(inst)
	TRACKED_ENTS[inst] = nil
	if next(TRACKED_ENTS) == nil then
		for i = 1, #TRAIL_POOL do
			TRAIL_POOL[i]:Remove()
			TRAIL_POOL[i] = nil
		end
	end
end

local NUM_DEBRIS = 6

local function HideDebris(inst)
	if inst.debrisshown then
		inst.debrisshown = false
		inst.debrisscattered = false
		for i, v in ipairs(inst.debris) do
			v:RemoveFromScene()
			v.entity:SetParent(inst.entity)
			v.Transform:SetPosition(0, 0, 0)
			v:Remove()
		end
	end
end

local function ShowDebris(inst)
	if not inst.debrisshown then
		inst.debrisshown = true
		inst.debris = {}
		for i = 1, NUM_DEBRIS do
			table.insert(inst.debris, SpawnPrefab("lunar_grazer_debris"))
		end
	end
end

local function ScatterDebris(inst)
	if not inst.debrisscattered and inst.debrisshown then
		inst.debrisscattered = true
		local x, y, z = inst.Transform:GetWorldPosition()
		local theta0 = math.random() * TWOPI
		local delta = TWOPI / #inst.debris
		for i, v in ipairs(inst.debris) do
			local r = 1 + math.random() * 2
			local theta = theta0 + (i + math.random() * 0.5) * delta
			v.Physics:Stop()
			v.Physics:Teleport(x + math.cos(theta) * r, 0, z - math.sin(theta) * r)
		end
	end
end

local function TossDebris(inst)
	if inst.debrisshown then
		inst.debrisscattered = true
		local x, y, z = inst.Transform:GetWorldPosition()
		local theta0 = math.random() * TWOPI
		local delta = TWOPI / #inst.debris
		local radius = 1
		for i, v in ipairs(inst.debris) do
			local theta = theta0 + (i + math.random() * 0.5) * delta
			local cos_theta = math.cos(theta)
			local sin_theta = math.sin(theta)
			local speed = 2 + math.random() * 2
			v.Physics:Teleport(x + cos_theta * radius, 1, z - sin_theta * radius)
			v.Physics:SetVel(speed * cos_theta, 2 + math.random() * 2, -speed * sin_theta)
			v.AnimState:PlayAnimation("rock_float_0"..tostring(v.variation))
		end
	end
end

local function DropDebris(inst)
	if inst.debrisshown then
		inst.debrisscattered = true
		local x, y, z = inst.Transform:GetWorldPosition()
		local theta0 = math.random() * TWOPI
		local delta = TWOPI / #inst.debris
		local radius = 1
		for i, v in ipairs(inst.debris) do
			local theta = theta0 + (i + math.random() * 0.5) * delta
			local cos_theta = math.cos(theta)
			local sin_theta = math.sin(theta)
			local speed = 2 + math.random() * 2
			v.Physics:Teleport(x + cos_theta * radius, 0, z - sin_theta * radius)
			v.Physics:SetVel(speed * cos_theta, 0, -speed * sin_theta)
			v.AnimState:PlayAnimation("rock_float_0"..tostring(v.variation))
		end
	end
end

local function OnRemoveEntity(inst)
	if inst.debrisshown then
		for i, v in ipairs(inst.debris) do
			v:Remove()
		end
	end
	StopTracking(inst)
end

local function OnNewState(inst)
	if not inst.sg:HasStateTag("debris") then
		inst:HideDebris()
	end
end

local CLOUD_RADIUS = 2.5
local PHYSICS_PADDING = 3
local SLEEPER_TAGS = { "player", "sleeper" }
local SLEEPER_NO_TAGS = { "playerghost", "epic", "lunar_aligned", "INLIMBO" }

local function OnClearCloudProtection(ent)
	ent._lunargrazercloudprot = nil
end

local function SetCloudProtection(inst, ent, duration)
	if ent:IsValid() then
		if ent._lunargrazercloudprot ~= nil then
			ent._lunargrazercloudprot:Cancel()
		end
		ent._lunargrazercloudprot = ent:DoTaskInTime(duration, OnClearCloudProtection)
	end
end

local function DoCloudTask(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
	for i, v in ipairs(TheSim:FindEntities(x, y, z, CLOUD_RADIUS + PHYSICS_PADDING, nil, SLEEPER_NO_TAGS, SLEEPER_TAGS)) do
		if v._lunargrazercloudprot == nil and
			v:IsValid() and v.entity:IsVisible() and
			not (v.components.health ~= nil and v.components.health:IsDead()) and
			not (v.sg ~= nil and v.sg:HasStateTag("waking"))
			then
			local range = v:GetPhysicsRadius(0) + CLOUD_RADIUS
			if v:GetDistanceSqToPoint(x, y, z) < range * range then
				if v.components.grogginess ~= nil then
					if not (v.sg ~= nil and v.sg:HasStateTag("knockout")) then
						v.components.grogginess:AddGrogginess(TUNING.LUNAR_GRAZER_GROGGINESS, TUNING.LUNAR_GRAZER_KNOCKOUTTIME)
						inst:SetCloudProtection(v, .5)
					end
				elseif v.components.sleeper ~= nil then
					if not (v.sg ~= nil and v.sg:HasStateTag("sleeping")) then
						v.components.sleeper:AddSleepiness(TUNING.LUNAR_GRAZER_GROGGINESS, TUNING.LUNAR_GRAZER_KNOCKOUTTIME)
						inst:SetCloudProtection(v, .5)
					end
				end
			end
		end
	end
end

local function StartCloudTask(inst)
	if inst.cloudtask == nil then
		inst.cloudtask = inst:DoPeriodicTask(1, DoCloudTask, math.random())
	end
end

local function StopCloudTask(inst)
	if inst.cloudtask ~= nil then
		inst.cloudtask:Cancel()
		inst.cloudtask = nil
	end
end

local function EnableCloud(inst, enable)
	enable = enable ~= false
	if enable ~= inst._cloudenabled:value() then
		inst._cloudenabled:set(enable)
		if not enable then
			StopCloudTask(inst)
		elseif not inst:IsAsleep() then
			StartCloudTask(inst)
		end
	end
end

local function IsCloudEnabled(inst)
	return inst._cloudenabled:value()
end

local function OnRemoved(inst)
	if inst.debris then
		for i, v in ipairs(inst.debris) do
			v:Remove()
		end
	end
end
--==============================================
--				Custom Common Functions
--==============================================	
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
	end
    if data ~= nil and data.debris then
		inst.sg:GoToState("dissipated")
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	if inst.debrisshown or inst.sg:HasStateTag("invisible") then
		data.debris = true
	end
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
    inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("monster")
	inst:AddTag("hostile")
	inst:AddTag("notraptrigger")
	inst:AddTag("lunar_aligned")
	----------------------------------
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
	inst.AnimState:UsePointFiltering(true)
    inst.AnimState:SetLightOverride(0.1)
	inst.AnimState:SetSymbolAddColour("moon_rocks", 0, 0, 0, 1)
    inst.AnimState:SetMultColour(1, 1, 1, .4)

    inst._cloudenabled = net_bool(inst.GUID, "lunar_grazer._cloudenabled")
	inst._cloudenabled:set(true)
	inst.IsCloudEnabled = IsCloudEnabled

	inst:WatchWorldState( "issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)
end

local master_postinit = function(inst)  
	--Stats--
	inst.HideDebris = HideDebris
	inst.ShowDebris = ShowDebris
	inst.ScatterDebris = ScatterDebris
	inst.TossDebris = TossDebris
	inst.DropDebris = DropDebris
	inst.SetCloudProtection = SetCloudProtection
	inst.EnableCloud = EnableCloud
    --These have to be before we set the stategraph, since these functions get called in the initial state.
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
    inst.components.locomotor.softstop = true
    inst.components.health:SetMinHealth(1)
	----------------------------------
	--Variables		
	inst.shouldwalk = true
	inst.mobplayer = true
	
	inst.ghostbuild = "ghost_monster_build"
	inst.debuffimmune = true
    inst.debris = nil
	inst.debrisshown = false
	inst.debrisscattered = false

    inst.cloud = SpawnPrefab("lunar_goop_cloud_fx")
	inst.cloud.entity:SetParent(inst.entity)

	inst.core = SpawnPrefab("lunar_grazer_core_fx")
	inst.core.entity:SetParent(inst.entity)
	inst.core.Follower:FollowSymbol(inst.GUID, "rock_cycle_follow", nil, nil, nil, true)
    ----------------
    --Trails--
    inst.trails = {}
	for i = 1, NUM_TRAIL_VARIATIONS do
		inst.trails[i] = i
	end
	--shuffle veriations
	for i = 1, NUM_TRAIL_VARIATIONS do
		local rnd = math.random(i, NUM_TRAIL_VARIATIONS)
		local v = inst.trails[i]
		inst.trails[i] = inst.trails[rnd]
		inst.trails[rnd] = v
	end
	inst.SpawnTrail = SpawnTrail
	inst._ontrailfinished = function(fx) RecycleTrail(inst, fx) end
    ------------------
	
	local body_symbol = "blob_body"
	inst.poisonsymbol = body_symbol
	--MakeLargeBurnableCharacter(inst, body_symbol)
    --MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetStormImmunity(inst)
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride

    inst:AddComponent("planarentity")
	inst:AddComponent("planardamage")
	inst.components.planardamage:SetBaseDamage(TUNING.LUNAR_GRAZER_PLANAR_DAMAGE)

	inst:AddComponent("damagetyperesist")
	inst.components.damagetyperesist:AddResist("explosive", inst, 99999, "weaktoexplosives")

    --inst:AddComponent("entitytracker") --might not need this but eh.
	----------------------------------
	--Eater--
	inst.components.hunger:Pause()
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	--Physics and Scale--
	MakeCharacterPhysics(inst, 10, .5)
	inst.DynamicShadow:SetSize(5, 2.5)
    inst.DynamicShadow:Enable(false)
	inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
    inst:ListenForEvent("newstate", OnNewState)
	inst:ListenForEvent("onremove", OnRemoved)
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) 
        inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
        --inst.AnimState:UsePointFiltering(true)
        inst.AnimState:SetLightOverride(0.1)
        inst.AnimState:SetSymbolAddColour("moon_rocks", 0, 0, 0, 1)
        inst.AnimState:SetMultColour(1, 1, 1, .4)
    end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true) 
            inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
            --inst.AnimState:UsePointFiltering(true)
            inst.AnimState:SetLightOverride(0.1)
            inst.AnimState:SetSymbolAddColour("moon_rocks", 0, 0, 0, 1)
            inst.AnimState:SetMultColour(1, 1, 1, .4)
		end)
    end)
	
	inst.OnSave = OnSave
	inst.OnLoad = OnLoad
    --inst.OnRemoveEntity = OnRemoveEntity
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
