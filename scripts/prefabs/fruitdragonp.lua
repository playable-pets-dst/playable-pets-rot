local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "fruitdragonp"

local assets = 
{
	Asset("ANIM", "anim/fruit_dragon.zip"),
    Asset("ANIM", "anim/fruit_dragon_build.zip"),
    Asset("ANIM", "anim/fruit_dragon_ripe_build.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.FRUITDRAGON.HEALTH,
	hunger = 125,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 50,
	
	runspeed = TUNING.FRUITDRAGON.RUN_SPEED * 1.5,
	walkspeed = TUNING.FRUITDRAGON.WALK_SPEED * 1.5,
	
	attackperiod = 0,
	damage = TUNING.FRUITDRAGON.UNRIPE_DAMAGE ,
	range = TUNING.FRUITDRAGON.ATTACK_RANGE,
	hit_range = TUNING.FRUITDRAGON.HIT_ATTACK_RANGE,
	
	bank = "fruit_dragon",
	build = "fruit_dragon_build",
	shiny = "fruit_dragon",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('fruit_dragonp',
{
    {'plantmeat',        1.00},
})

SetSharedLootTable('fruit_dragon_ripep',
{
    {'dragonfruit',      1.00},
})


--==============================================
--					Mob Functions
--==============================================
local fruit_dragon_sounds =
{
    idle = "turnoftides/creatures/together/fruit_dragon/idle",
    death = "turnoftides/creatures/together/fruit_dragon/death",
    eat = "turnoftides/creatures/together/fruit_dragon/eat",
    onhit = "turnoftides/creatures/together/fruit_dragon/hit",
    sleep_loop = "turnoftides/creatures/together/fruit_dragon/sleep",
    stretch = "turnoftides/creatures/together/fruit_dragon/stretch",
    --do_ripen = "turnoftides/creatures/together/fruit_dragon/do_ripen",
    do_unripen = "turnoftides/creatures/together/fruit_dragon/stretch",
    attack = "turnoftides/creatures/together/fruit_dragon/attack",
    attack_fire = "turnoftides/creatures/together/fruit_dragon/attack_fire",
    attack_whoosh = "dontstarve/wilson/attack_whoosh",  -- TODO @stevenm remove this when the actual attack sounds are in
    challenge_pre = "turnoftides/creatures/together/fruit_dragon/challenge_pre",
    challenge = "turnoftides/creatures/together/fruit_dragon/challenge",
    challenge_pst = "turnoftides/creatures/together/fruit_dragon/eat",
    challenge_win = "turnoftides/creatures/together/fruit_dragon/eat",
    challenge_lose = "turnoftides/creatures/together/fruit_dragon/eat",
}


local function QueueRipen(inst)
	inst._ripen_pending = not inst._is_ripe
	inst._unripen_pending = false
end

local function MakeRipe(inst, force)
	inst._ripen_pending = false
	inst._is_ripe = true

	inst.components.lootdropper:SetChanceLootTable('fruit_dragon_ripep')
	inst.components.combat:SetDefaultDamage(TUNING.FRUITDRAGON.RIPE_DAMAGE)

	if inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("fruit_dragon_ripe_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild("fruit_dragon_ripe_build")
	end
end

local function QueueUnripe(inst)
	inst._ripen_pending = false
	inst._unripen_pending = inst._is_ripe
end

local function MakeUnripe(inst, force)
	inst._is_ripe = false

	inst.components.lootdropper:SetChanceLootTable('fruit_dragonp')
	inst.components.combat:SetDefaultDamage(TUNING.FRUITDRAGON.UNRIPE_DAMAGE)

	if inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("fruit_dragon_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild("fruit_dragon_build")
	end
end
--==============================================
--				Custom Common Functions
--==============================================
local FORGE_HP_TRIGGER = 0.25
local FORGE_STATS = PPROT_FORGE.FRUITDRAGON

local function SetNormal(inst)	
	inst.transformed = nil
	inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and FORGE_STATS.DAMAGE or mob.damage)
	inst.AnimState:SetBuild(inst.isshiny ~= 0 and "fruit_dragon_shiny_build_0"..inst.isshiny or mob.build)
end

local function SetRipe(inst)
	inst.transformed = true
	
	inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and FORGE_STATS.DAMAGE * 1.5 or mob.damage * 1.5)
	
	inst.AnimState:SetBuild(inst.isshiny ~= 0 and "fruit_dragon_ripe_shiny_build_0"..inst.isshiny or "fruit_dragon_ripe_build")
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		if data._is_ripe then
			inst:MakeRipe(true)
		end
	end
end

local function OnSave(inst, data)
	data._is_ripe = inst._is_ripe
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.FRUITDRAGON)
	
	inst.mobsleep = false	
	
	inst.components.combat:AddDamageBuff("passive_explosive", {buff = 1.1, stimuli = "explosive"})
	inst.components.combat:AddDamageBuff("passive_fire", {buff = 1.1, stimuli = "fire"})
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books"})
	end)
	
	inst:ListenForEvent("healthdelta", function(inst)
		local health = inst.components.health:GetPercent()
		if health > FORGE_HP_TRIGGER and inst.transformed then
			SetNormal(inst)
		elseif health <= FORGE_HP_TRIGGER and not inst.transformed then
			SetRipe(inst)
		end
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
    inst:AddTag("fruitdragon")
	inst:AddTag("animal")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	inst.Light:Enable(false)
    inst.Light:SetRadius(1.25)
    inst.Light:SetFalloff(.98)
    inst.Light:SetIntensity(0.5)
    inst.Light:SetColour(235 / 255, 121 / 255, 12 / 255)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0) --fire, acid, poison
	inst.components.combat:SetHurtSound("turnoftides/creatures/together/fruit_dragon/hit")
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = false
	inst.fire_attack = true
	
	inst.sounds = fruit_dragon_sounds

	inst.MakeRipe = MakeRipe
	inst.MakeUnripe = MakeUnripe
	
	local body_symbol = "gecko_torso_middle"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeSmallFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	inst:AddComponent("timer")
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1, 0.5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(2, 0.75)
	--inst.DynamicShadow:Enable(false)  
	
	PlayablePets.SetStormImmunity(inst)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)