local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "daywalker2p"

local assets = 
{
    Asset("ANIM", "anim/daywalker_build.zip"),
	Asset("ANIM", "anim/daywalker_buried.zip"),
	Asset("ANIM", "anim/daywalker_phase2.zip"),
	Asset("ANIM", "anim/daywalker_phase3.zip"),
	Asset("ANIM", "anim/daywalker_defeat.zip"),
	Asset("ANIM", "anim/scrapball.zip"),
}

local prefabs = 
{	
	"daywalker2_buried_fx",
	"daywalker2_swipe_fx",
	"daywalker2_object_break_fx",
	"daywalker2_spike_break_fx",
	"daywalker2_spike_loot_fx",
	"daywalker2_cannon_break_fx",
	"daywalker2_armor1_break_fx",
	"daywalker2_armor2_break_fx",
	"daywalker2_cloth_break_fx",
	"junkball_fx",
	"junk_break_fx",
	"alterguardian_laser",
	"alterguardian_laserempty",
	"alterguardian_laserhit",
	"scrap_monoclehat",
	"wagpunk_bits",
	"gears",
    "wagpunkhat_blueprint",
    "armorwagpunk_blueprint",
    "chestupgrade_stacksize_blueprint",
    "wagpunkbits_kit_blueprint",
    "wagpunkbits_kit",
}
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = BOSS_STATS and TUNING.DAYWALKER_HEALTH or TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = TUNING.DAYWALKER_RUNSPEED,
	walkspeed = TUNING.DAYWALKER_WALKSPEED,
	damage = TUNING.DAYWALKER_DAMAGE,
	range = TUNING.DAYWALKER_ATTACK_RANGE,
	hit_range = TUNING.DAYWALKER_ATTACK_RANGE,
	attackperiod = 3,
	bank = "daywalker",
	build = "daywalker_build",
	shiny = "daywalker",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGdaywalkerp",
	minimap = "daywalkerp.tex",	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
    { "gears",				0.5 },
	{ "wagpunk_bits",		1 },
	{ "wagpunk_bits",		1 },
	{ "wagpunk_bits",		1 },
	{ "wagpunk_bits",		1 },
	{ "wagpunk_bits",		0.5 },
})

local sounds =
{
    hit              = "dontstarve/creatures/together/stagehand/hit",
	awake_pre        = "dontstarve/creatures/together/stagehand/awake_pre",
	footstep         = "dontstarve/creatures/together/stagehand/footstep",
}

local FORGE_STATS = PPROT_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================

local MASS = 1000

local function ChangeToGiantPhysics(inst)
	inst.Physics:SetCollisionGroup(COLLISION.GIANTS)
	inst.Physics:ClearCollisionMask()
	inst.Physics:CollidesWith(COLLISION.WORLD)
	inst.Physics:CollidesWith(COLLISION.OBSTACLES)
	inst.Physics:CollidesWith(COLLISION.CHARACTERS)
	inst.Physics:CollidesWith(COLLISION.GIANTS)
	inst.Physics:SetMass(MASS)
end

--For clients
local function OnFacingModelDirty(inst)
	local numfacings = inst._facingmodel:value()
	if numfacings == 4 then
		inst.eye.Transform:SetFourFaced()
	elseif numfacings == 6 then
		inst.eye.Transform:SetSixFaced()
	elseif numfacings == 0 then
		inst.eye.Transform:SetNoFaced()
	end
end

local function SwitchToFacingModel(inst, numfacings)
	if numfacings == 0 then
		inst.Transform:SetNoFaced()
		if inst.eye ~= nil then
			inst.eye.Transform:SetNoFaced()
		end
	elseif numfacings == 4 then
		inst.Transform:SetFourFaced()
		if inst.eye ~= nil then
			inst.eye.Transform:SetFourFaced()
		end
	elseif numfacings == 6 then
		inst.Transform:SetSixFaced()
		if inst.eye ~= nil then
			inst.eye.Transform:SetSixFaced()
		end
	else--unsupported
		--assert(false)
		return
	end
	inst._facingmodel:set(numfacings)
end

local ATTACH_POS =
{
	"left",
	"right",
	"top",
}

local function CreateEyeFlame()
	local inst = CreateEntity()

	inst:AddTag("FX")
	--[[Non-networked entity]]
	if not TheWorld.ismastersim then
		inst.entity:SetCanSleep(false)
	end
	inst.persists = false

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddFollower()

	inst.Transform:SetFourFaced()

	inst.AnimState:SetBank("daywalker")
	inst.AnimState:SetBuild("daywalker_build")
	inst.AnimState:PlayAnimation("flame_loop", true)
	inst.AnimState:SetLightOverride(1)

	return inst
end

--------------------------------------------------------------------------

local BLINDSPOT = 15

local function UpdateHead(inst)
	if inst.stalking == nil then
		return
	elseif not inst.stalking:IsValid() then
		inst.stalking = nil
		inst.lastfacing = nil
		inst.lastdir1 = nil
		inst.Transform:SetRotation(0)
		inst.Transform:SetFourFaced()
		inst.eye.Transform:SetFourFaced()
		return
	end

	local parent = inst.entity:GetParent()
	parent.AnimState:MakeFacingDirty()
	local dir1 = parent:GetAngleToPoint(inst.stalking.Transform:GetWorldPosition())
	local camdir = TheCamera:GetHeading()
	local facing = parent.AnimState:GetCurrentFacing()

	dir1 = ReduceAngle(dir1 + camdir)

	if facing == FACING_UP then
		if dir1 > -135 and dir1 < 135 then
			local diff = ReduceAngle(dir1 - 2)
			if math.abs(diff) < BLINDSPOT and facing == inst.lastfacing then
				dir1 = inst.lastdir1
			else
				dir1 = diff > 0 and 135 or -135
			end
		end
	elseif facing == FACING_DOWN then
		if dir1 < -45 or dir1 > 90 then
			local diff = ReduceAngle(dir1 + 178)
			if math.abs(diff) < BLINDSPOT and facing == inst.lastfacing then
				dir1 = inst.lastdir1
			else
				dir1 = diff < 0 and 90 or -45
			end
		end
	elseif facing == FACING_LEFT then
		if dir1 < -45 or dir1 > 135 then
			local diff = ReduceAngle(dir1 + 160)
			if math.abs(diff) < BLINDSPOT and facing == inst.lastfacing then
				dir1 = inst.lastdir1
			else
				dir1 = diff < 0 and 135 or -45
			end
		end
	elseif facing == FACING_RIGHT then
		if dir1 < -135 or dir1 > 45 then
			local diff = ReduceAngle(dir1 - 160)
			if math.abs(diff) < BLINDSPOT and facing == inst.lastfacing then
				dir1 = inst.lastdir1
			else
				dir1 = diff < 0 and 45 or -135
			end
		end
	end

	inst.lastfacing = facing
	inst.lastdir1 = dir1

	inst.Transform:SetRotation(dir1 - camdir - parent.Transform:GetRotation())
	inst.AnimState:MakeFacingDirty()
	local facing1 = inst.AnimState:GetCurrentFacing()
	if facing1 == FACING_UPRIGHT or facing1 == FACING_UPLEFT then
		if facing == FACING_UP then
			inst.AnimState:Hide("side_ear")
			inst.AnimState:Show("back_ear")
		else
			inst.AnimState:Hide("back_ear")
			inst.AnimState:Show("side_ear")
		end
	end
end

local function CreateHead()
	local inst = CreateEntity()

	inst:AddTag("FX")
	--[[Non-networked entity]]
	if not TheWorld.ismastersim then
		inst.entity:SetCanSleep(false)
	end
	inst.persists = false

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddFollower()

	inst.Transform:SetFourFaced()

	inst.AnimState:SetBank("daywalker")
	inst.AnimState:SetBuild("daywalker_build")
	inst.AnimState:PlayAnimation("head", true)

	inst:AddComponent("updatelooper")

	inst.isupdating = false
	inst.stalking = nil
	inst.lastfacing = nil
	inst.lastdir1 = nil

	inst.eye = CreateEyeFlame()
	inst.eye.entity:SetParent(inst.entity)
	inst.eye.Follower:FollowSymbol(inst.GUID, "follow_eye", nil, nil, nil, true)

	return inst
end

local function OnStalkingDirty(inst)
	inst.head.stalking = inst._stalking:value() --available to clients
	if inst.head.stalking ~= nil then
		if not inst.head.isupdating then
			inst.head.isupdating = true
			inst.head.components.updatelooper:AddPostUpdateFn(UpdateHead)
		end
		inst.head.Transform:SetEightFaced()
		inst.head.eye.Transform:SetEightFaced()
	elseif inst.head.isupdating then
		inst.head.isupdating = false
		inst.head.lastfacing = nil
		inst.head.lastdir1 = nil
		inst.head.components.updatelooper:RemovePostUpdateFn(UpdateHead)
		inst.head.Transform:SetRotation(0)
		inst.head.Transform:SetFourFaced()
		inst.head.eye.Transform:SetFourFaced()
	end
end

local function OnHeadTrackingDirty(inst)
	if inst._headtracking:value() then
		if inst.head == nil then
			inst.head = CreateHead()
			inst.head.entity:SetParent(inst.entity)
			inst.head.Follower:FollowSymbol(inst.GUID, "HEAD_follow", nil, nil, nil, true, true)
			inst.highlightchildren = { inst.head }
			inst.head:ListenForEvent("stalkingdirty", OnStalkingDirty, inst)
			OnStalkingDirty(inst)
		end
	elseif inst.head ~= nil then
		inst.head:Remove()
		inst.highlightchildren = nil
	end
end

local function SetHeadTracking(inst, track)
	track = track ~= false
	if inst._headtracking:value() ~= track then
		inst._headtracking:set(track)

		--Dedicated server does not need to spawn the local fx
		if not TheNet:IsDedicated() then
			OnHeadTrackingDirty(inst)
		end
	end
end

local function OnStalkingNewState(inst)
	if inst.sg:HasStateTag("stalking") then
		inst.components.health:StartRegen(TUNING.DAYWALKER_COMBAT_STALKING_HEALTH_REGEN, TUNING.DAYWALKER_COMBAT_HEALTH_REGEN_PERIOD, false)
	else
		inst.components.health:StopRegen()
	end
end

local function SetStalking(inst, stalking)
	if stalking ~= nil and not (inst.hostile and stalking:HasTag("player")) then
		stalking = nil
	end
	if stalking ~= inst._stalking:value() then
		if inst._stalking:value() ~= nil then
			inst:RemoveEventCallback("onremove", inst._onremovestalking, inst._stalking:value())
			if stalking == nil then
				inst:RemoveEventCallback("newstate", OnStalkingNewState)
				if inst.engaged then
					inst.components.health:StopRegen()
				end
			end
		elseif stalking ~= nil then
			inst:ListenForEvent("newstate", OnStalkingNewState)
		end
		inst._stalking:set(stalking)
		if stalking then
			inst:ListenForEvent("onremove", inst._onremovestalking, stalking)
			if not inst.nostalkcd then
				inst.components.timer:StopTimer("stalk_cd")
				inst.components.timer:StartTimer("stalk_cd", TUNING.DAYWALKER_STALK_CD)
			end
		end
	end
end

local function GetStalking(inst)
	return inst._stalking:value()
end

local function IsStalking(inst)
	return inst.shouldwalk == true or inst._stalking:value() ~= nil
end

local function SetEngaged(inst, engaged)
	if inst.engaged ~= engaged and (engaged ~= nil) == inst.hostile then
		inst.engaged = engaged
		if engaged then
			inst.components.health:StopRegen()
			inst:StartAttackCooldown()
			if not inst.components.timer:TimerExists("roar_cd") then
				inst:PushEvent("roar", { target = inst.components.combat.target })
			end
		else
			inst:SetStalking(nil)
			if engaged == false then
				inst.components.health:StartRegen(TUNING.DAYWALKER_HEALTH_REGEN, 1)
			else--if engaged == nil then
				inst.components.health:StopRegen()
			end
			inst.components.combat:ResetCooldown()
			inst.components.combat:DropTarget()
		end
	end
end

local function OnAttacked(inst, data)

end

local function StartAttackCooldown(inst)
	inst.components.combat:SetAttackPeriod(GetRandomMinMax(TUNING.DAYWALKER_ATTACK_PERIOD.min, TUNING.DAYWALKER_ATTACK_PERIOD.max))
	inst.components.combat:RestartCooldown()
end

local function OnMinHealth(inst)
	if not POPULATING then
		--inst:MakeDefeated()
	end
end

local function OnDespawnTimer(inst, data)
	if data ~= nil and data.name == "despawn" then
		if inst:IsAsleep() then
			inst:Remove()
		else
			inst.components.talker:IgnoreAll("despawn")
			inst.components.despawnfader:FadeOut()
			inst.DynamicShadow:Enable(false)
		end
	end
end

local function RegenFatigue(inst)
	inst.fatigue = inst.fatigue - TUNING.DAYWALKER_FATIGUE_REGEN
	if inst.fatigue <= 0 then
		inst.fatigue = 0
		inst._fatiguetask:Cancel()
		inst._fatiguetask = nil
	end
end

local function DeltaFatigue(inst, fatigue)
	if inst.canfatigue then
		inst.fatigue = math.max(0, inst.fatigue + fatigue)
		if inst._fatiguetask ~= nil then
			inst._fatiguetask:Cancel()
		end
		inst._fatiguetask = inst.fatigue > 0 and inst:DoPeriodicTask(TUNING.DAYWALKER_FATIGUE_REGEN_PERIOD, RegenFatigue, fatigue >= 0 and TUNING.DAYWALKER_FATIGUE_REGEN_START_PERIOD or nil) or nil
	end
end

local function ResetFatigue(inst)
	inst.fatigue = 0
	if inst._fatiguetask ~= nil then
		inst._fatiguetask:Cancel()
		inst._fatiguetask = nil
	end
end

local function IsFatigued(inst)
	return inst.fatigue >= TUNING.DAYWALKER_FATIGUE_TIRED
end
--==============================================
--				Custom Common Functions
--==============================================	
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
    inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("epic")
	inst:AddTag("monster")
	inst:AddTag("hostile")
	inst:AddTag("largecreature")
	----------------------------------

	inst:WatchWorldState( "issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)

	inst._enablechains = net_bool(inst.GUID, "daywalker._enablechains", "chainsdirty")
	inst._facingmodel = net_tinybyte(inst.GUID, "daywalker._facingmodel", "facingmodeldirty")
	inst._headtracking = net_bool(inst.GUID, "daywalker._headtracking", "headtrackingdirty")
	inst._stalking = net_entity(inst.GUID, "daywalker._stalking", "stalkingdirty")

	if not TheNet:IsDedicated() then
		inst.eye = CreateEyeFlame()
		inst.eye.entity:SetParent(inst.entity)
		inst.eye.Follower:FollowSymbol(inst.GUID, "follow_eye", nil, nil, nil, true)
	end
	if not TheWorld.ismastersim then
		inst:ListenForEvent("facingmodeldirty", OnFacingModelDirty)
		inst:ListenForEvent("headtrackingdirty", OnHeadTrackingDirty)
	end
end

local master_postinit = function(inst)  
	--Stats--
	inst.SwitchToFacingModel = SwitchToFacingModel
	inst.StartAttackCooldown = StartAttackCooldown
	inst.SetHeadTracking = SetHeadTracking
	inst.SetStalking = SetStalking
	inst.GetStalking = GetStalking
	inst.IsStalking = IsStalking
	inst.DeltaFatigue = DeltaFatigue
	inst.ResetFatigue = ResetFatigue
	inst.IsFatigued = IsFatigued
	inst.HasLeechAttached = HasLeechAttached
	inst.HasLeechTracked = HasLeechTracked
	inst.AttachLeech = AttachLeech
	inst.DetachLeech = DetachLeech
	inst.OnAttachmentInterrupted = OnAttachmentInterrupted
	inst.StartTrackingLeech = StartTrackingLeech
	inst.SpawnLeeches = SpawnLeeches

	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 1, 1, 1, 9999) --fire, acid, poison

    inst.AnimState:OverrideSymbol("dark_spew", "stagehand", "dark_spew")
    inst.AnimState:OverrideSymbol("fx", "stagehand", "fx")
    inst.AnimState:OverrideSymbol("stagehand_fingers", "stagehand", "stagehand_fingers")
	----------------------------------
	--Variables		
	inst.mobsleep = true
	inst.shouldwalk = false
	inst.mobplayer = true
	inst.taunt = true
	inst.taunt2 = true
	inst.taunt3 = true
	
	inst.ghostbuild = "ghost_monster_build"
	
	inst.sounds = sounds

	inst.hit_recovery = TUNING.DAYWALKER_HIT_RECOVERY

	inst._busy_attach_pos = {}
	inst._incoming_jumps = {}
	inst._leeches = {}
	inst._onremoveleech = function(leech) inst._leeches[leech] = nil end

	inst:ListenForEvent("incoming_jump", OnIncomingJump)

	inst.chained = false
	inst.hostile = true
	inst.engaged = nil
	inst.defeated = false
	inst.looted = false
	inst.fatigue = 0
	inst._fatiguetask = nil

	--ability unlocks
	inst.canfatigue = false
	inst.nostalkcd = true
	inst.canstalk = true
	inst.canslam = false
	inst.canwakeuphit = false

	inst._onremovestalking = function(stalking) inst._stalking:set(nil) end
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    --MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	inst:AddComponent("epicscare")
	inst.components.epicscare:SetRange(TUNING.DAYWALKER_EPICSCARE_RANGE)
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5) --This might multiply food stats.
	---------------------------------
	--Physics and Shadows--
	--Physics and Scale--
	inst.Transform:SetFourFaced()
	--inst.Transform:SetSixFaced() --V2C: TwoFaced has a built in rot offset hack for stationary objects
	MakeGiantCharacterPhysics(inst, MASS, 1.3)

    inst.DynamicShadow:SetSize(3.5, 1.5)
	inst.DynamicShadow:Enable(true) --Disables shadows.
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
		inst:SwitchToFacingModel(4)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
