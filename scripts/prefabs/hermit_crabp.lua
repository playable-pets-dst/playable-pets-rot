local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {"1"}

local prefabname = "hermit_crabp"

local assets = 
{
	Asset("ANIM", "anim/player_hermitcrab_idle.zip"),
    Asset("ANIM", "anim/player_hermitcrab_walk.zip"),
    Asset("ANIM", "anim/player_hermitcrab_look.zip"),
    

    Asset("ANIM", "anim/hermitcrab_build.zip"),
	Asset("ANIM", "anim/hermitcrab_generic_build.zip"),
	
	Asset("ANIM", "anim/hermitcrab_shiny_build_01.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 200,
	hunger = 125,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 125,
	
	runspeed = 6,
	walkspeed = 3,
	
	attackperiod = 0,
	damage = 10,
	range = 2,
	hit_range = 2,
	
	bank = "wilson",
	build = "hermitcrab_generic_build",
	shiny = "hermitcrab",
	
	scale = 1,
	stategraph = "SGwilson",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('hermit_crabp',
{
    {"meat",       1.00},
})

--==============================================
--					Mob Functions
--==============================================
local function ontalk(inst, script) 
    inst.SoundEmitter:PlaySound("hookline_2/characters/hermit/talk")
end
--==============================================
--				Custom Common Functions
--==============================================
local function OverrideDance(inst, data)
	if not inst.sg:HasStateTag("busy") and not inst.components.health:IsDead() then
		if data.anim and data.anim[1] and data.anim[1] == "emoteXL_pre_dance0" then
			inst:DoTaskInTime(0, function(inst)
				inst.sg:GoToState("funnyidle_clack_pre")
			end)
		elseif data.anim and data.anim[1] and data.anim[1] == "emoteXL_pre_dance7" then
			inst:DoTaskInTime(0, function(inst)
				inst.sg:GoToState("funnyidle_tango_pre")
			end)
		end
	end
end

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.CRABLER)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("crab")
	----------------------------------
	
	--inst.components.talker.font = TALKINGFONT_HERMIT
	inst.talksoundoverride = "hookline_2/characters/hermit/talk"
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5) --fire, acid, poison
	----------------------------------
	--Variables	
	
	inst.AnimState:Hide("ARM_carry")
    inst.AnimState:Hide("HAT")
    inst.AnimState:Hide("HAIR_HAT")
    inst.AnimState:Show("HAIR_NOHAT")
    inst.AnimState:Show("HAIR")
    inst.AnimState:Show("HEAD")
    inst.AnimState:Hide("HEAD_HAT")
	
	inst.AnimState:OverrideSymbol("fx_wipe", "wilson_fx", "fx_wipe")
    inst.AnimState:OverrideSymbol("fx_liquid", "wilson_fx", "fx_liquid")
    inst.AnimState:OverrideSymbol("shadow_hands", "shadow_hands", "shadow_hands")
    inst.AnimState:OverrideSymbol("snap_fx", "player_actions_fishing_ocean_new", "snap_fx")
	
	inst.AnimState:AddOverrideBuild("player_hit_darkness")
    inst.AnimState:AddOverrideBuild("player_receive_gift")
    inst.AnimState:AddOverrideBuild("player_actions_uniqueitem")
    inst.AnimState:AddOverrideBuild("player_wrap_bundle")
    inst.AnimState:AddOverrideBuild("player_lunge")
    inst.AnimState:AddOverrideBuild("player_attack_leap")
    inst.AnimState:AddOverrideBuild("player_superjump")
    inst.AnimState:AddOverrideBuild("player_multithrust")
    inst.AnimState:AddOverrideBuild("player_parryblock")
    inst.AnimState:AddOverrideBuild("player_emote_extra")
    inst.AnimState:AddOverrideBuild("player_boat")
    inst.AnimState:AddOverrideBuild("player_boat_plank")
    inst.AnimState:AddOverrideBuild("player_boat_net")        
    inst.AnimState:AddOverrideBuild("player_boat_sink")
    inst.AnimState:AddOverrideBuild("player_oar")
    inst.AnimState:AddOverrideBuild("player_peruse")
    inst.AnimState:AddOverrideBuild("player_boat_channel")

    inst.AnimState:AddOverrideBuild("player_actions_fishing_ocean")
    inst.AnimState:AddOverrideBuild("player_actions_fishing_ocean_new")
	inst.AnimState:AddOverrideBuild("player_actions_feast_eat")
	
	inst.getskins = getskins
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible()
    inst.components.eater:SetCanEatRaw()
    inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 50, .5)
	inst.DynamicShadow:SetSize(1.5, .75)
	inst.Transform:SetFourFaced()
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", function(inst) inst.AnimState:Show("HAIR") end) --Shows head when hats make heads disappear.
	inst:ListenForEvent("emote", OverrideDance)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) inst:AddComponent("cursable") end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv),
CreatePrefabSkin(prefabname.."_none",
{
	base_prefab = prefabname,
	type = "base",
	assets = assets,
	build_name = "hermitcrab_generic_build",
	rarity = "Common",
	skin_tags = { "BASE", "CHARACTER", "HERMIT_CRABP", },
	skins = { ghost_skin = "ghost_monster_build", ghost_werebeaver_skin = "ghost_werebeaver_build", normal_skin = "hermitcrab_generic_build", werebeaver_skin = "werebeaver_build", },
	feet_cuff_size = {},
	release_group = 999,
})