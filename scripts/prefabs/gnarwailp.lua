local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "gnarwailp"

local assets = 
{
	Asset("ANIM", "anim/gnarwail.zip"),
    Asset("ANIM", "anim/gnarwail_build.zip"),
	Asset("ANIM", "anim/gnarwail_water_shadow.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.GNARWAIL.HEALTH,
	hunger = 250,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 130,
	
	runspeed = TUNING.GNARWAIL.RUN_SPEED,
	walkspeed = TUNING.GNARWAIL.WALK_SPEED ,
	
	attackperiod = 1,
	damage = 60*2,
	range = 15,
	hit_range = 3.5,
	
	bank = "gnarwail",
	build = "gnarwail_build",
	shiny = "gnarwail",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('gnarwailp',
{
    {"fishmeat",       1.00},
	{"fishmeat",       1.00},
	{"fishmeat",       1.00},
	{"fishmeat",       1.00},
})
--horn drops on death if it exists

--==============================================
--					Mob Functions
--==============================================

local function WantsToTossItem(inst, item)
    -- NOTE: We assume that everything we're asked about CAN be tossed; we're just checking whether we want to.
    local leader = inst.components.follower:GetLeader()
    if leader then
        return item.components.health == nil
    else
        -- Without a leader, we only toss fish that we want to eat.
        return item:HasTag("ediblefish_meat")
    end
end

local function TossItem(inst, item, target_to_toss_to)
    if item:HasTag("oceanfish") then
        local item_loots = (item.fish_def and item.fish_def.loot) or nil
        if item_loots ~= nil then
            local ix, iy, iz = item.Transform:GetWorldPosition()
            local owner = item
            for _, loot_prefab in pairs(item_loots) do
                local loot = SpawnPrefab(loot_prefab)
                loot.Transform:SetPosition(ix, iy, iz)
                item = loot
            end
            owner:Remove()
        end
    end

    if target_to_toss_to and target_to_toss_to:IsValid() then
        LaunchAt(item, inst, target_to_toss_to, 5, 2, nil, 0)
    else
        Launch2(item, inst, 2, 0, 2, 0)
    end

    if item.components.inventoryitem then
        item.components.inventoryitem:SetLanded(false, true)
    end

    inst.ready_to_toss = false
end
-- We try to leverage FindSwimmableOffset to identify a resurface location that is in the ocean and not under a boat.
local function TryGnarwailResurface(gnarwail_instance, horn_position)
    local resurface_radius = TUNING.MAX_WALKABLE_PLATFORM_RADIUS + TUNING.MAX_WALKABLE_PLATFORM_RADIUS + gnarwail_instance:GetPhysicsRadius(0)
    local emerge_offset = FindSwimmableOffset(Vector3(unpack(horn_position)), math.random() * PI * 2, resurface_radius)
    if emerge_offset then
        gnarwail_instance:ReturnToScene()
        gnarwail_instance.Transform:SetPosition(horn_position[1] + emerge_offset.x, horn_position[2] + emerge_offset.y, horn_position[3] + emerge_offset.z)
        gnarwail_instance.sg:GoToState("emerge")
    else
        gnarwail_instance:DoTaskInTime(1, TryGnarwailResurface, horn_position)
    end
end

local function horn_retreat(horn_inst, horn_broken, leak_size)
    -- Try to spawn a boat leak at our location.
    local horn_x, horn_y, horn_z = horn_inst.Transform:GetWorldPosition()
    local boat = TheWorld.Map:GetPlatformAtPoint(horn_x, horn_z)
    if boat then
        local leak = SpawnPrefab("boat_leak")
        leak.Transform:SetPosition(horn_x, horn_y, horn_z)
        leak.components.boatleak.isdynamic = true
        leak.components.boatleak:SetBoat(boat)
        leak.components.boatleak:SetState(leak_size, true)

        table.insert(boat.components.hullhealth.leak_indicators_dynamic, leak)
    end

    -- If this was spawned legitimately, it should have a gnarwail it was sourced from. So, we need to try to resurface it.
    if horn_inst.gnarwail_record then
        local gnarwail = SpawnPrefab(horn_inst.gnarwail_record.prefab)
        gnarwail:SetPersistData(horn_inst.gnarwail_record.data)
        gnarwail.Transform:SetPosition(horn_inst.gnarwail_record.x, 0, horn_inst.gnarwail_record.z)
        if horn_broken then
            gnarwail:SetHornBroken(true)
        end
        gnarwail:DoTaskInTime(TUNING.GNARWAIL.HORN_RETREAT_TIME, TryGnarwailResurface, {horn_x, horn_y, horn_z})
        gnarwail:RemoveFromScene()
    end

    horn_inst:Remove()
end

local function OnHornHit(horn_inst)
    if not horn_inst.components.health:IsDead() then
        horn_inst.AnimState:PlayAnimation("pierce_hit")
    end
end

local function HornAttack_AnimOver(horn_inst)
    horn_retreat(horn_inst, false, "med_leak")
end

local function EndHornAttack(horn_inst)
    horn_inst.AnimState:PlayAnimation("attack_stage_2_pre", false)
    horn_inst.AnimState:PushAnimation("attack_stage_2_loop", false)
    horn_inst.AnimState:PushAnimation("attack_stage_2_retreat", false)
    horn_inst:ListenForEvent("animqueueover", HornAttack_AnimOver)

    horn_inst:DoTaskInTime(7*FRAMES, function(i) i.SoundEmitter:PlaySound("turnoftides/common/together/boat/thunk") end)
    horn_inst:DoTaskInTime(14*FRAMES, function(i) i.SoundEmitter:PlaySound("turnoftides/common/together/boat/damage") end)
    horn_inst:DoTaskInTime(16*FRAMES, function(i) i.SoundEmitter:PlaySound("turnoftides/common/together/boat/thunk") end)
    horn_inst:DoTaskInTime(19*FRAMES, function(i) i.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/jump_small") end)
    horn_inst:DoTaskInTime(25*FRAMES, function(i) i.SoundEmitter:PlaySound("turnoftides/common/together/boat/thunk") end)

    horn_inst._horn_attack_ending = true
    horn_inst:RemoveEventCallback("attacked", OnHornHit)
end

local function HornBroken_AnimOver(horn_inst)
    horn_retreat(horn_inst, true, "small_leak")
end

local function HornBrokenWide_AnimOver(horn_inst)
    horn_retreat(horn_inst, true, "med_leak")
end


local function PlayAnimation(inst, anim_name, loop)
    inst.AnimState:PlayAnimation(anim_name, loop or false)
    if inst._water_shadow ~= nil then
        inst._water_shadow.AnimState:PlayAnimation(anim_name, loop or false)
    end
end

local function PushAnimation(inst, anim_name, loop)
    inst.AnimState:PushAnimation(anim_name, loop or false)
    if inst._water_shadow ~= nil then
        inst._water_shadow.AnimState:PushAnimation(anim_name, loop or false)
    end
end

local function RemoveShadowOnDeath(inst, data)
    if inst._water_shadow and inst._water_shadow:IsValid() then
        inst._water_shadow:DoTaskInTime(inst.components.health.destroytime or 2, ErodeAway)
    end
end

local function UpdateShadowRotation(inst)
    if inst._water_shadow then
        inst._water_shadow.Transform:SetRotation(inst.Transform:GetRotation())
    end
end
--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		
		if data.horn_broken ~= nil then
            inst:SetHornBroken(data.horn_broken)
        end

        if data.ready_to_toss == false then
            -- Our reset timer should have been saved via the timer component,
            -- so just set ourselves back to false.
            inst.ready_to_toss = false
        end

        if data.formation_angle ~= nil then
            inst._formation_angle = data.formation_angle
        end
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.horn_broken = inst._horn_broken
	if inst._formation_angle ~= nil then
        data.formation_angle = inst._formation_angle
    end
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.GNARWAIL)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("scarytocookiecutters")
	inst:AddTag("gnarwail")
	----------------------------------
	AddDefaultRippleSymbols(inst, true, false)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = false
	
	inst.has_horn = true
	
	inst.getskins = getskins
	
	local body_symbol = "gn_head"
	inst.poisonsymbol = body_symbol
	--MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.PlayAnimation = PlayAnimation
    inst.PushAnimation = PushAnimation
	inst.WantsToToss = WantsToTossItem
    inst.TossItem = TossItem
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })
    inst.components.eater:SetAbsorptionModifiers(2,2,2) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1, 1.25)
	inst.Transform:SetSixFaced()
	
	inst.DynamicShadow:SetSize(1, 0.75)
	inst.DynamicShadow:Enable(false)  
	PlayablePets.SetAmphibious(inst, "gnarwail", "gnarwail", false, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)