local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "hound_hedgep"

local assets = 
{
	Asset("ANIM", "anim/hound_hedge.zip"),
    Asset("ANIM", "anim/hound_hedge_action.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

local getskins = {"1"}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 200,
	hunger = 125,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 50,
	
	runspeed = TUNING.HOUND_SPEED,
	walkspeed = TUNING.HOUND_SPEED/2,
	
	attackperiod = 0,
	damage = 30,
	range = 3,
	hit_range = 3,
	
	bank = "hound",
	build = "hound_mutated",
	shiny = "hound_mutated",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'hound_mutantp',
{
    {'monstermeat',  1},
	{'houndstooth',  1},
})

local FORGE_STATS = PPROT_FORGE.HOUND_MUTANT
--==============================================
--					Mob Functions
--==============================================
local sounds =
{
    pant = "turnoftides/creatures/together/mutated_hound/pant",
    attack = "turnoftides/creatures/together/mutated_hound/attack",
    bite = "turnoftides/creatures/together/mutated_hound/bite",
    bark = "turnoftides/creatures/together/mutated_hound/bark",
    --barkbark = "turnoftides/creatures/together/mutated_hound/barkbark", TODO @stevenm is this a thing???
    death = "turnoftides/creatures/together/mutated_hound/death",
    sleep = "dontstarve/creatures/hound/sleep",
    growl = "turnoftides/creatures/together/mutated_hound/growl",
    howl = "dontstarve/creatures/together/clayhound/howl",
    hurt = "turnoftides/creatures/together/mutated_hound/hurt",
}
--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================
local function RetargetForgeFn(inst)
	return FindEntity(inst, 30, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() end, nil, { "structure", "player", "companion", "_isinheals" })
end

local function KeepForgeTarget(inst, target)
    return target:IsValid() and 
		target.sg ~= nil and not 
		((target.sg:HasStateTag("sleeping") or target:HasTag("_isinheals")) or target.sg:HasStateTag("fossilized")) and not target:HasTag("player")
end

local function NoDmgFromPlayers(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
    return afflicter ~= nil and (afflicter:HasTag("player") or afflicter:HasTag("companion"))
end

local function OnForgeAttacked(inst, data)
	if not (data.attacker:HasTag("player") or data.attacker:HasTag("companion") or data.attacker:HasTag("hound")) then
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST,
        function(dude)
            return not (dude.components.health ~= nil and dude.components.health:IsDead())
                and (dude:HasTag("hound") or dude:HasTag("houndfriend"))
                and data.attacker ~= (dude.components.follower ~= nil and dude.components.follower.leader or nil)
        end, 5)
	end	
end

--old
local function OnAttacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST,
        function(dude)
            return not (dude.components.health ~= nil and dude.components.health:IsDead())
                and (dude:HasTag("hound") or dude:HasTag("houndfriend"))
                and data.attacker ~= (dude.components.follower ~= nil and dude.components.follower.leader or nil)
        end, 5)
end

local function Infect(inst)
			local fx = SpawnPrefab("lavaarena_portal_player_fx").Transform:SetPosition(inst:GetPosition():Get())
			local pet = SpawnPrefab("mutatedhound")
			if inst.isshiny and inst.isshiny ~= 0 then
				pet.AnimState:SetBuild("hound_mutated_shiny_build_0"..inst.isshiny)
			end
			pet.Transform:SetPosition(inst:GetPosition():Get())
			pet:Hide()
			pet:DoTaskInTime(0.3, function(inst) inst:Show() end)
			pet:AddTag("companion")
			pet.components.health.redirect = NoDmgFromPlayers
			pet.components.health:SetMaxHealth(300)
			pet.components.combat:SetDamageType(1)	
			pet.components.combat:SetDefaultDamage(30)
			pet.components.combat:SetRetargetFunction(0.25, RetargetForgeFn)
			pet.components.combat:SetKeepTargetFunction(KeepForgeTarget)
			
			PlayablePets.SetCommonStatResistances(pet, 3, 2, 1) --fire, acid, poison
			
			pet:AddComponent("debuffable")
			pet.components.debuffable:IsEnabled(true)
			pet:AddComponent("colouradder")
			pet.components.lootdropper:SetChanceLootTable({})
			pet:RemoveEventCallback("onattacked", OnAttacked)
			pet:ListenForEvent("onattacked", OnForgeAttacked)
			--if inst.components.stat_tracker then inst.components.stat_tracker:InitializePetStats(pet) end
end


local function OnHitOtherForge(inst, other)
	if other.infect_task then
		other.infect_task:Cancel()
		other.infect_task = nil
		other:RemoveEventCallback("death", Infect)
	end
	if not other:HasTag("structure") and not other:HasTag("corpse") then
	other:ListenForEvent("death", Infect)
	other.infect_task = other:DoTaskInTime(5, function(inst) inst:RemoveEventCallback("death", Infect) end)
	end
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.HOUND_MUTANT)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	inst.components.combat.onhitotherfn = OnHitOtherForge
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("monster")
	inst:AddTag("hound")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 3, 2, 1) --fire, acid, poison
	inst.components.combat:SetHurtSound(sounds.hurt)	
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	inst.sounds = sounds
	
	inst.getskins = getskins
	
	local body_symbol = "hound_body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(2.5, 1.5)
	--inst.DynamicShadow:Enable(false)  
	
	PlayablePets.SetAmphibious(inst, "hound", "hound_water")
	
	PlayablePets.SetStormImmunity(inst)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)