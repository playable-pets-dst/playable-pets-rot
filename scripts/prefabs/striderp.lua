local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "striderp"

local assets = 
{
	Asset("ANIM", "anim/spider_water.zip"),
    Asset("ANIM", "anim/spider_water_water.zip"),
	Asset("ANIM", "anim/spider_fluffy.zip"),
    Asset("ANIM", "anim/spider_fluffy_water.zip"),
    Asset("SOUND", "sound/spider.fsb"),
}

local prefabs = 
{	
	"spider_mutate_fx",
    "spider_heal_fx",
    "spider_heal_target_fx",
    "spider_heal_ground_fx"
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	
	runspeed = TUNING.SPIDER_WATER_RUNSPEED,
	walkspeed = TUNING.SPIDER_WATER_WALKSPEED,
	
	attackperiod = 0,
	damage = 20*2,
	range = TUNING.SPIDER_WATER_HIT_RANGE,
	hit_range = TUNING.SPIDER_WATER_HIT_RANGE,
	
	bank = "spider_water",
	build = "spider_water",
	shiny = "spider_water",
	
	scale = 1,
	stategraph = "SGspider_waterp",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'spider_waterp',
{
    {'monstermeat',  1},
})

local FORGE_STATS = PPROT_FORGE.SPIDER_WATER
--==============================================
--					Mob Functions
--==============================================
local function WaterSpider_SetHappyFace(inst, is_happy)
    if is_happy then
        inst.AnimState:OverrideSymbol("waterforest_eyes", inst.build, "happy_face")
        inst.AnimState:OverrideSymbol("fangs", inst.build, "happy_fangs")
    else
        inst.AnimState:ClearOverrideSymbol("fangs")
        inst.AnimState:ClearOverrideSymbol("waterforest_eyes")
    end
end

local function OnEnterWater(inst)
    inst.hop_distance = inst.components.locomotor.hop_distance
    inst.components.locomotor.hop_distance = 4

    inst.components.ppskin_manager:LoadSkin(mob, true)
end

local function OnExitWater(inst)
    if inst.hop_distance then
        inst.components.locomotor.hop_distance = inst.hop_distance
    end

    inst.components.ppskin_manager:LoadSkin(mob, true)
end
--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.SPIDER_MOON)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "armors"})
	end)
	
	inst.components.health:SetAbsorptionAmount(0.8)
	
	inst:AddComponent("buffable")
	inst.components.buffable:AddBuff("winona_passive", {{name = "cooldown", val = -0.1, type = "add"}})
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("spiderwhisperer")
	inst:AddTag("spider")
	inst:AddTag("monster")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local function SetSkinDefault(inst, data)
	if data and data.build then
		if inst:HasTag("swimming") then
			inst.AnimState:SetBuild(data.build2)
		else
			inst.AnimState:SetBuild(data.build)
		end
	else
		if inst:HasTag("swimming") then
			inst.AnimState:SetBuild(mob.build2)
		else
			inst.AnimState:SetBuild(mob.build)
		end	
	end
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, nil, true) --mob table, ishuman, ignorepvpmultiplier, ignoreweb
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeTinyFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .5)
	inst.Transform:SetFourFaced()
	inst.DynamicShadow:SetSize(1.5, .5)
	PlayablePets.SetAmphibious(inst, "spider_water", "spider_water_water")
	inst.components.amphibiouscreature:SetEnterWaterFn(OnEnterWater)
    inst.components.amphibiouscreature:SetExitWaterFn(OnExitWater)
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", function(inst, data) PlayablePets.GetAllyHelp(inst, data.attacker, "spider") end)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)