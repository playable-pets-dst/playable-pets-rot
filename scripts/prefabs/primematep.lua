local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "primematep"

local assets = 
{
    Asset("ANIM", "anim/monkey_small.zip"),
	Asset("ANIM", "anim/monkey_small_sw.zip"),
    --for water fx build overrides
    Asset("ANIM", "anim/slide_puff.zip"),
    Asset("ANIM", "anim/splash_water_rot.zip"),

    Asset("SOUND", "sound/monkey.fsb"),
}

local prefabs = 
{	
    "poop",
    "monkeyprojectile",
    "smallmeat",
    "cave_banana",
    "cutless",
    "cursed_monkey_token",
    "oar_monkey",
    "monkey_smallhat",
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

local getskins = {}
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	
	runspeed = TUNING.MONKEY_MOVE_SPEED,
	walkspeed = TUNING.MONKEY_MOVE_SPEED/2,
	
	attackperiod = 0,
	damage = TUNING.PRIME_MATE_DAMAGE,
	range = TUNING.MONKEY_MELEE_RANGE,
	hit_range = TUNING.MONKEY_MELEE_RANGE,
	
	bank = "pigman",
	build = "monkeymen_build",
	
	scale = 1.2,
	stategraph = "SGmerm_guardp",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
{
    {'meat',     1.0},
    {'bananajuice',  0.2},
})

local sounds = {
	idle = "monkeyisland/primemate/idle",
	row = "monkeyisland/primemate/row",
	eat = "monkeyisland/primemate/eat",
    attack = "monkeyisland/primemate/taunt",
    hit = "monkeyisland/primemate/hit",
    death = "monkeyisland/primemate/death",
    --talk = "dontstarve/characters/wurt/merm/warrior/talk",
    buff = "monkeyisland/primemate/taunt",
	taunt = "monkeyisland/primemate/taunt",
	sleep_pre = "monkeyisland/powdermonkey/sleep_pre",
	sleep_lp = "monkeyisland/powdermonkey/sleep_lp",
	sleep_pst = "monkeyisland/powdermonkey/sleep_pst",
} 

local function ontalk(inst, script)
    inst.SoundEmitter:PlaySound(inst.sounds.taunt)
end

local FORGE_STATS = PPROT_FORGE[string.upper(prefabname)]
--==============================================
--					Mob Functions
--==============================================
local function IsPoop(item)
    return item.prefab == "poop"
end

local function oneat(inst)
    --Monkey ate some food. Give him some poop!
	local ch = math.random()
    if inst.components.inventory ~= nil and ch >= 0.75 then
        local maxpoop = 3
        local poopstack = inst.components.inventory:FindItem(IsPoop)
        if poopstack == nil or poopstack.components.stackable.stacksize < maxpoop then
            inst.components.inventory:GiveItem(SpawnPrefab("poop"))
        end
    end
end

local function speech_override_fn(inst, speech)
    if not ThePlayer or (ThePlayer:HasTag("wonkey") or ThePlayer:HasTag("monkey")) then
        return speech
    else
        return CraftMonkeySpeech()
    end 
end

local function OnUnequip(inst)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if not hands then
		inst.AnimState:OverrideSymbol("swap_object", inst.AnimState:GetBuild(), "swap_object")
		inst.AnimState:Hide("ARM_carry_up")
	end
end
--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("monkey")
	inst:AddTag("pirate")
	----------------------------------
    inst.components.talker.fontsize = 35
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.offset = Vector3(0, -400, 0)
    inst.components.talker:MakeChatter()

    inst.speech_override_fn = speech_override_fn
end

local master_postinit = function(inst)
	--Stats--
	inst.sounds = sounds
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	----------------------------------
	--Variables	
	inst.shouldwalk = false
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true

    inst.AnimState:OverrideSymbol("fx_slidepuff01", "slide_puff", "fx_slidepuff01")
    inst.AnimState:OverrideSymbol("splash_water_rot", "splash_water_rot", "splash_water_rot")
    inst.AnimState:OverrideSymbol("fx_water_spot", "splash_water_rot", "fx_water_spot")
    inst.AnimState:OverrideSymbol("fx_splash_wide", "splash_water_rot", "fx_splash_wide")
    inst.AnimState:OverrideSymbol("fx_water_spray", "splash_water_rot", "fx_water_spray")
	inst.AnimState:Hide("ARM_carry_up")
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison

	PlayablePets.SetSkeleton(inst, "pp_skeleton_pig")
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
    inst:AddComponent("thief")
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.VEGGIE }, { FOODTYPE.VEGGIE })
    --inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
    inst.components.eater:SetOnEatFn(oneat)
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, 0.25)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(2, 1.25)
	--inst.DynamicShadow:Enable(false)  
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("unequip", OnUnequip)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	inst.components.talker.ontalk = ontalk
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)