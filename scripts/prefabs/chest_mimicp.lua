local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "chest_mimicp"

local assets = 
{
	Asset("ANIM", "anim/pandoras_chest.zip"),
	Asset("ANIM", "anim/chest_mimic.zip"),
    Asset("ANIM", "anim/slurper_basic.zip"),
}

local prefabs = 
{	
	"chest_mimic_revealed",
    "chest_mimic_ruinsspawn_tracker",
    "shadowheart_infused",
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local MOB_TUNING = TUNING[string.upper(prefabname)]
local mob = 
{
	health       = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger       = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate   = TUNING.WILSON_HUNGER_RATE, 
	sanity       = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed     = MOB_TUNING.RUNSPEED,
	walkspeed    = MOB_TUNING.WALKSPEED,
	damage       = MOB_TUNING.DAMAGE,
	range        = MOB_TUNING.RANGE, 
	hit_range    = MOB_TUNING.HIT_RANGE, --range is 1.8 in stategraph.
	attackperiod = MOB_TUNING.ATTACK_PERIOD,
    scale        = 1,
	
	bank         = "chest_mimic",
	build        = "chest_mimic",
	shiny        = "chest_mimic", --deprecated.
	
	stategraph   = "SG"..prefabname,
	minimap      = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
{
    {"meat",  1.00},
    {"meat",  0.25},

    {"rocks", 1.00},
    {"rocks", 0.25},

    {"flint", 0.50},
    {"flint", 0.15},
})

local sounds = {
    open  = "dontstarve/wilson/chest_open",
    close = "dontstarve/wilson/chest_close",
    built = "dontstarve/common/chest_craft",

    spawn = "rifts4/mimic/mimic_chest/spawn",
    walk_lp = "rifts4/mimic/mimic_chest/walk_lp",
    attack_munch = "rifts4/mimic/mimic_chest/attack",
    attack_hit = "dontstarve/wilson/chest_close",
    death = "rifts4/mimic/mimic_chest/death",
    taunt = "rifts4/mimic/mimic_chest/taunt",
    pickup = "rifts4/mimic/mimic_chest/eat_pre",
    chew = "rifts4/mimic/mimic_chest/eating_lp",
    eat_pst = "rifts4/mimic/mimic_chest/eat_pst",
}

local FORGE_STATS = PPROT_FORGE[string.upper(prefabname)]
--==============================================
--					Mob Functions
--==============================================
local function transfer_item_to_monster_inventory(item, monster, owner)
    local item_removed_from_container = owner.components.container:RemoveItem(item, true)
    if item_removed_from_container then
        monster.components.inventory:GiveItem(item_removed_from_container)
    end
end

local function do_transform(inst, data)
    local ix, iy, iz = inst.Transform:GetWorldPosition()
    local open = inst.components.container:IsOpen()

    local chest_monster = SpawnPrefab("chest_mimic_revealed")
    chest_monster.Transform:SetPosition(ix, iy, iz)

    local ruinsspawn_tracker = inst.components.entitytracker:GetEntity("ruinsspawn_tracker")
    chest_monster.components.entitytracker:TrackEntity("ruinsspawn_tracker", ruinsspawn_tracker)

    inst.components.container:ForEachItem(transfer_item_to_monster_inventory, chest_monster, inst)

    if data then
        if data.doer then
            chest_monster.components.combat:SetTarget(data.doer)
        end
    end

    chest_monster.sg:GoToState("spawn", open)

    inst:Remove()
end

local function initiate_transform(inst, data)
    if not inst._transform_task then
        inst._transform_task = inst:DoTaskInTime(2.5, do_transform, data)
    end
end

-- Container
local function onopen(inst, data)
    inst.AnimState:PlayAnimation("open")
    inst.SoundEmitter:PlaySound(inst.sounds.open)

    initiate_transform(inst, data)
end

local function onclose(inst, doer)
    inst.AnimState:PlayAnimation("close")
    inst.AnimState:PushAnimation("closed", false)
    inst.SoundEmitter:PlaySound(inst.sounds.close)

    initiate_transform(inst, {doer = doer})
end

--
local function create_tracker_at_my_feet(inst)
    -- We might already have a tracker if we're a mimic that transformed back.
    if not inst.components.entitytracker:GetEntity("ruinsspawn_tracker") then
        local tracker = SpawnPrefab("chest_mimic_ruinsspawn_tracker")
        tracker.Transform:SetPosition(inst.Transform:GetWorldPosition())

        inst.components.entitytracker:TrackEntity("ruinsspawn_tracker", tracker)
    end
end

local function OnHitOther(inst, other, damage, stimuli, weapon, damageresolved, spdamage, damageredirecttarget)
    if not damageredirecttarget then
        inst.components.thief:StealItem(other)
    end
end

--
--==============================================
--				Custom Common Functions
--==============================================

local function RevealSelf(inst)
    inst.MiniMapEntity:SetIcon(mob.minimap)
    inst:RemoveTag("noplayerindicator")
    SpawnPrefab("explode_reskin").Transform:SetPosition(inst:GetPosition():Get())
    if inst._disguise then
        inst._disguise:Remove()
    end
    inst:Show()
    inst.DynamicShadow:Enable(true)
end

local function RevealOwner(inst)
    if inst._owner and inst._owner:IsValid() then
        inst._owner.sg:GoToState("wake")
    end
end

local function DisguiseSelf(inst)
    inst:AddTag("noplayerindicator")
    inst.MiniMapEntity:SetIcon("andnoonewilleverknow")
    --grab the closest chest and its skin.
    local targetchest = FindEntity(inst, 10,
        function(guy)
            return not guy:HasTag("burnt")
        end,
        {"chest"}
    )
    local reveal_function = RevealOwner
    SpawnPrefab("explode_reskin").Transform:SetPosition(inst:GetPosition():Get())
    local fakechest = SpawnPrefab(targetchest and targetchest.prefab or "treasurechest")
    fakechest._owner = inst
    fakechest.Transform:SetPosition(inst:GetPosition():Get())
    fakechest.persists = false

    fakechest.components.container.onopenfn = RevealOwner
    fakechest.components.workable:SetOnWorkCallback(reveal_function)
    fakechest.components.workable:SetOnFinishCallback(reveal_function)

    if targetchest then
        TheSim:ReskinEntity(fakechest.GUID, fakechest.skinname, targetchest.skinname, targetchest.skin_id)
    end
    --make changes to the chest to ensure that any interactivity
    --is essentially prevented and forces attack.

    inst._disguise = fakechest
    inst:Hide()
    inst.DynamicShadow:Enable(true)
end

local function OnAttacked(inst, data)
    --reveal self if attacked.
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.SPIDER_MOON)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "armors"})
	end)
	
	inst.components.health:SetAbsorptionAmount(0.8)
	
	inst:AddComponent("buffable")
	inst.components.buffable:AddBuff("winona_passive", {{name = "cooldown", val = -0.1, type = "add"}})
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("chessfriend")
    inst:AddTag("chestmonster")
    inst:AddTag("monster")
    inst:AddTag("shadow_aligned")
    inst:AddTag("wooden")
	----------------------------------
    inst:ListenForEvent("phasechanged", PlayablePets.SetNightVision)
	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst)
	--Stats--
    inst.sounds = sounds
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier, ignoreweb
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	----------------------------------
	--Variables	
    inst.DisguiseSelf = DisguiseSelf
    inst.RevealSelf = RevealSelf

    inst.shouldwalk = true
    ----------------------------------
    --Playable Pets
    inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SLEEP, PP_ABILITIES.PP_SLEEP)
    inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SKILL1, PP_ABILITIES.SKILL1)
	----------------------------------
	--Debuff Symbol
	local body_symbol = "mimicchest"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
    PlayablePets.SetCommonStatResistances(inst, nil, 0, 0) --fire, acid, poison
    ----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride

    inst:AddComponent("planardamage")
    inst.components.planardamage:SetBaseDamage(TUNING.CHEST_MIMIC_PLANAR_DAMAGE)

    inst:AddComponent("planarentity")

    inst:AddComponent("thief")

    inst.components.combat.onhitotherfn = OnHitOther
	----------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible()
    inst.components.eater:SetStrongStomach(true) -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .1)
    inst.DynamicShadow:SetSize(2.5, 1.5)
	inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", OnAttacked)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function()
        PlayablePets.RevRestore(inst, mob) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)

    PlayablePets.SetCommonSaveAndLoad(inst, OnSave, OnLoad)
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)