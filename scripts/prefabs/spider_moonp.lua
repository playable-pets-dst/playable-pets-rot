local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "spider_moonp"

local assets = 
{
	Asset("ANIM", "anim/ds_spider_basic.zip"),
    Asset("ANIM", "anim/ds_spider_moon.zip"),
    Asset("SOUND", "sound/spider.fsb"),
}

local prefabs = 
{	
	"moonspider_spikep",
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 300,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 50,
	
	runspeed = 6,
	walkspeed = 6,
	
	attackperiod = 3,
	damage = 30,
	range = 15,
	hit_range = 3,
	
	bank = "spider_moon",
	build = "DS_spider_moon",
	shiny = "spider_moon",
	
	scale = 1.25,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'spider_moonp',
{
    {'monstermeat',  1},
	{'houndstooth',  1},
})

local FORGE_STATS = PPROT_FORGE.HOUND_MUTANT
--==============================================
--					Mob Functions
--==============================================
local variations = {1, 2, 3, 4, 5}

local function DoSpikeAttack(inst, pt)
	local x, y, z = pt:Get()
	local inital_r = 1
	x = GetRandomWithVariance(x, inital_r)
	z = GetRandomWithVariance(z, inital_r)

	shuffleArray(variations)

	local num = math.random(2, 4)
    local dtheta = PI * 2 / num
    local thetaoffset = math.random() * PI * 2
    local delaytoggle = 0
	if TheWorld.Map:IsVisualGroundAtPoint(x, 0, z) and not TheWorld.Map:IsPointNearHole(Vector3(x, 0, z)) then
        local spike = SpawnPrefab("moonspider_spikep")
        spike.Transform:SetPosition(x, 0, z)
		spike:SetOwner(inst)
    end
	for i = 1, num do
		local r = 1.1 + math.random() * 1.75
		local theta = i * dtheta + math.random() * dtheta * 0.8 + dtheta * 0.2
        local x1 = x + r * math.cos(theta)
        local z1 = z + r * math.sin(theta)
        if TheWorld.Map:IsVisualGroundAtPoint(x1, 0, z1) and not TheWorld.Map:IsPointNearHole(Vector3(x1, 0, z1)) then
            local spike = SpawnPrefab("moonspider_spikep")
            spike.Transform:SetPosition(x1, 0, z1)
			spike:SetOwner(inst)
			if variations[i + 1] ~= 1 then
				spike.AnimState:OverrideSymbol("spike01", "spider_spike", "spike0"..tostring(variations[i + 1]))
			end
        end
	end
end	
    
--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.SPIDER_MOON)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "armors"})
	end)
	
	inst.components.health:SetAbsorptionAmount(0.8)
	
	inst:AddComponent("buffable")
	inst.components.buffable:AddBuff("winona_passive", {{name = "cooldown", val = -0.1, type = "add"}})
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("spiderwhisperer")
	inst:AddTag("spider")
	inst:AddTag("monster")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true, true) --mob table, ishuman, ignorepvpmultiplier, ignoreweb
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 1, 0.5, 0.5) --fire, acid, poison
	inst.components.combat:SetHurtSound("turnoftides/creatures/together/spider_moon/hit_response")
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeTinyFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.DoSpikeAttack = DoSpikeAttack	
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(1.5, .5)
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", function(inst, data) PlayablePets.GetAllyHelp(inst, data.attacker, "spider") end)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)