local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "molebatp"

local assets = 
{
    Asset("ANIM", "anim/molebat.zip"),
}

local prefabs =
{
    "batnose",
    "molebathill",
    "monstermeat",
}
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.MOLEBAT.HEALTH,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	
	runspeed = TUNING.MOLEBAT.WALK_SPEED,
	walkspeed = TUNING.MOLEBAT.WALK_SPEED,
	
	attackperiod = 1,
	damage = TUNING.MOLEBAT.DAMAGE,
	range = 2,
	hit_range = 2.5,
	
	bank = "molebat",
	build = "molebat",
	shiny = "molebat",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable("molebatp",
{
    {'monstermeat',     1.0},
	{"batnose",		1.0},
	{'guano',      0.15},
})
--==============================================
--					Mob Functions
--==============================================
local MAX_ALLIES = 4
local ALLY_CD = 10

local function should_summon_allies(inst)
    return inst._can_summon_allies and inst.allies and #inst.allies < MAX_ALLIES
end

local function RemoveAlly(new_bat)
	if new_bat.owner and new_bat.owner.allies and new_bat.owner.allies[new_bat] then
		new_bat.owner.allies[new_bat] = nil
	end	
end

local function RemoveAllies(inst)
	if inst.allies then
		for i, v in ipairs(inst.allies) do
			if v.components.health then
				v.components.lootdropper:SetLoot({})
				v.components.health:Kill()
			end
		end
		inst.allies = nil
	end
end

local function summon_ally(inst)
	if (TheNet:GetServerGameMode() == "lavaarena" or TheWorld:HasTag("cave")) and should_summon_allies(inst) then
    -- If we fail to spawn, it's ok. Due to not setting entitytracker,
    -- we'll succeed again once our timer finishes.
		local i_pos = inst:GetPosition()
		local offset = FindWalkableOffset(i_pos, math.random()*2*PI, 3, 8, false, true)
		if offset ~= nil then
			local new_bat = SpawnPrefab("molebat")
			local spawn_point = i_pos + offset
			new_bat.Transform:SetPosition(spawn_point.x, spawn_point.y, spawn_point.z)
			new_bat.sg:GoToState("fall")
			new_bat.owner = inst
			inst:ListenForEvent("onremove", RemoveAlly)
			new_bat:ListenForEvent("death", RemoveAlly)
		
			table.insert(inst.allies, new_bat)
			inst._can_summon_allies = false
			inst:DoTaskInTime(TheNet:GetServerGameMode() == "lavaarena" and PPROT_FORGE.MOLEBAT.SPECIAL_CD or ALLY_CD, function(inst) inst._can_summon_allies = true end)
		end
	end
end
--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.MOLEBAT)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("bat")
    inst:AddTag("cavedweller")
    inst:AddTag("hostile")
    inst:AddTag("monster")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = true
	inst.allies = {}
	
	inst.SummonAlly = summon_ally
	
	inst._can_summon_allies = true
	
	inst.getskins = getskins
	--inst:RemoveComponent("inkable")
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1.3,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("onremove", RemoveAllies)
	inst:ListenForEvent("attacked", function(inst, data) PlayablePets.GetAllyHelp(inst, data.attacker, "bat") end)
	--inst:ListenForEvent("death", RemoveAllies) 
	--disabled because it would just force other players to focus the player instead of the allies
	--plus I don't think it'd be exploited much.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
