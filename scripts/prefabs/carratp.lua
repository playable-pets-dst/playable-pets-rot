local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {"1"}

local prefabname = "carratp"

local assets = 
{
	Asset("ANIM", "anim/carrat_basic.zip"),
	Asset("ANIM", "anim/carrat_build.zip"),
	Asset("ANIM", "anim/carrat_shiny_build_01.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 50,
	hunger = 100,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 75,
	
	runspeed = TUNING.CARRAT.RUN_SPEED,
	walkspeed = TUNING.CARRAT.WALK_SPEED,
	
	attackperiod = 1,
	damage = 10,
	range = 1,
	hit_range = 1,
	
	bank = "carrat",
	build = "carrat_build",
	shiny = "carrat",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('carratp',
{
    {"plantmeat",       1.00},
    {"carrot_seeds",    0.33},
})

local carratsounds =
{
    idle = "turnoftides/creatures/together/carrat/idle",
    hit = "turnoftides/creatures/together/carrat/hit",
    sleep = "turnoftides/creatures/together/carrat/sleep",
    death = "turnoftides/creatures/together/carrat/death",
    emerge = "turnoftides/creatures/together/carrat/emerge",
    submerge = "turnoftides/creatures/together/carrat/submerge",
    eat = "turnoftides/creatures/together/carrat/eat",
    stunned = "turnoftides/creatures/together/carrat/stunned",
	reaction = "turnoftides/creatures/together/carrat/reaction",
	
	step = "dontstarve/creatures/mandrake/footstep",
}

--==============================================
--					Mob Functions
--==============================================
local available_colors = -- Used to get a random color on eating unknown veggie seeds
{
	"black", -- Black color swap exists but is never used as the color is reserved for shadow racers
	"blue",
	"brown",
	"green",
	"pink",
	"purple",
	"white",
	"yellow",

	"NEUTRAL",
}

local function common_setcolor(inst, color)
	color = color == "RANDOM" and available_colors[math.random(#available_colors)] or color
	color = color ~= "NEUTRAL" and color or nil

	if inst.prefab == "carrat_planted" then
		if color == nil then
			inst.AnimState:ClearOverrideSymbol("carrot_parts")
		else
			inst.AnimState:OverrideSymbol("carrot_parts", "yotc_carrat_colour_swaps", color.."_carrot_parts")
		end
	else
		if color == nil then
			inst.AnimState:ClearOverrideSymbol("carrat_tail")
			inst.AnimState:ClearOverrideSymbol("carrat_ear")
			inst.AnimState:ClearOverrideSymbol("carrot_parts")
		else
			inst.AnimState:OverrideSymbol("carrat_tail", "yotc_carrat_colour_swaps", color.."_carrat_tail")
			inst.AnimState:OverrideSymbol("carrat_ear", "yotc_carrat_colour_swaps", color.."_carrat_ear")
			inst.AnimState:OverrideSymbol("carrot_parts", "yotc_carrat_colour_swaps", color.."_carrot_parts")
		end
	end

	inst._color = color
end

local food_colors =
{
    watermelon_seeds = "blue",
    
    onion_seeds = "brown",
    potato_seeds = "brown",

	asparagus_seeds = "green",
    durian_seeds = "green",

	dragonfruit_seeds = "pink",
	pomegranate_seeds = "pink",
	tomato_seeds = "pink",
	pepper_seeds = "pink",

    eggplant_seeds = "purple",
    
	garlic_seeds = "white",

	corn_seeds = "yellow",
	pumpkin_seeds = "yellow",

	carrot_seeds = "NEUTRAL",
	seeds = "RANDOM",
}

local function GetColorFromFood(inst, data)
	local food_prefab = data ~= nil and data.food ~= nil and data.food.prefab or nil
	return food_prefab ~= nil and food_colors[food_prefab] or nil
end

local function yotc_oneatfn(inst, data)
	local color = GetColorFromFood(inst, data)
	if color ~= nil then
		common_setcolor(inst, color)
	end
end
--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		if data.color ~= nil then
            common_setcolor(inst, data.color)
        end
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.colour = inst.colour
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.CARRAT)
	
	inst.mobsleep = false	
	
	inst:AddTag("moreaggro") --TODO remove this when FF rework releases
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("animal")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 2) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt_multistate = true
	inst.taunt2 = true
	inst.shouldwalk = false
	
	inst.sounds = carratsounds
	
	inst.getskins = getskins
	
	local body_symbol = "carrat_body"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeSmallFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst._setcolorfn = common_setcolor
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(2,2,2) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1, 0.5)
	inst.Transform:SetSixFaced()
	
	inst.DynamicShadow:SetSize(1, 0.75)
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("oneat", yotc_oneatfn)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)