local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "mushgnomep"

local assets = 
{
    Asset("ANIM", "anim/grotto_mushgnome.zip"),
    Asset("SOUND", "sound/leif.fsb"),
}

local prefabs =
{

}
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.MUSHGNOME_HEALTH,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	
	runspeed = 2,
	walkspeed = 2,
	
	attackperiod = TUNING.MUSHGNOME_ATTACK_PERIOD,
	damage = TUNING.LEIF_DAMAGE,
	range = 3,
	hit_range = 3,
	
	bank = "grotto_mushgnome",
	build = "grotto_mushgnome",
	shiny = "mushgnome",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable("mushgnomep",
{
    {"livinglog",   1.0},
    {"livinglog",   0.5},
    {"spore_moon",  1.0},
    {"spore_moon",  1.0},
    {"spore_moon",  1.0},
    {"spore_moon",  0.5},
    {"spore_moon",  0.5},
    {"moon_cap",   1.0},
    {"moon_cap",   1.0},
})
--==============================================
--					Mob Functions
--==============================================
local function onspawnfn(inst, spawn)
    inst.SoundEmitter:PlaySound("dontstarve/cave/mushtree_tall_spore_fart")

    local pos = inst:GetPosition()

    local offset = FindWalkableOffset(
        pos,
        math.random() * 2 * PI,
        spawn:GetPhysicsRadius(0) + inst:GetPhysicsRadius(0),
        8
    )
    local off_x = (offset and offset.x) or 0
    local off_z = (offset and offset.z) or 0
    spawn.Transform:SetPosition(pos.x + off_x, 0, pos.z + off_z)
end
--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.MUSHGNOME)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local COLOUR_R, COLOUR_G, COLOUR_B = 227/255, 227/255, 227/255

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("leif")
    inst:AddTag("monster")
    inst:AddTag("tree")
	----------------------------------
	
	inst.Light:SetColour(COLOUR_R, COLOUR_G, COLOUR_B)
    inst.Light:SetIntensity(0.75)
    inst.Light:SetFalloff(0.5)
    inst.Light:SetRadius(0.6)
	inst.Light:Enable(true)
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, nil, 0) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = true
	
	inst.getskins = getskins
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	local color = .5 + math.random() * .5
    inst.AnimState:SetMultColour(color, color, color, 1)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	inst:AddComponent("periodicspawner")
    inst.components.periodicspawner:SetPrefab("spore_moon")
    inst.components.periodicspawner:SetOnSpawnFn(onspawnfn)
    inst.components.periodicspawner:SetDensityInRange(TUNING.MUSHSPORE_MAX_DENSITY_RAD, TUNING.MUSHSPORE_MAX_DENSITY)
    inst.components.periodicspawner:SetRandomTimes(TUNING.MUSHGNOME_SPORESPAWN_MIN, TUNING.MUSHGNOME_SPORESPAWN_MAX)
    inst.components.periodicspawner:Stop()
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1000, .5)
    inst.DynamicShadow:SetSize(4, 1.5)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) 
		inst.Light:SetColour(COLOUR_R, COLOUR_G, COLOUR_B)
		inst.Light:SetIntensity(0.75)
		inst.Light:SetFalloff(0.5)
		inst.Light:SetRadius(0.6)
		inst.Light:Enable(true)
		end)
		
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
