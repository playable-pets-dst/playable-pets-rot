local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "gestaltguardp"

local assets = 
{
    Asset("ANIM", "anim/brightmare_gestalt_evolved.zip"),
}

local prefabs = 
{	
	"gestalt_guard_head",
	"gestalt_trail",
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	
	runspeed = TUNING.GESTALTGUARD_WALK_SPEED,
	walkspeed = TUNING.GESTALTGUARD_WALK_SPEED,
	
	attackperiod = 0,
	damage = 180*2,
	range = TUNING.GESTALTGUARD_ATTACK_RANGE,
	hit_range = 2,
	
	bank = "brightmare_gestalt_evolved",
	build = "brightmare_gestalt_evolved",
	shiny = "gestalt",
	
	scale = 0.8,
	stategraph = "SGgestaltp",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'gestaltp',
{

})

local FORGE_STATS = PPROT_FORGE.GESTALTGUARDP
--==============================================
--					Mob Functions
--==============================================
local function SetHeadAlpha(inst, a)
	if inst.blobhead then
		inst.blobhead.AnimState:OverrideMultColour(1, 1, 1, a)
	end
end
--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(DAMAGETYPES.MAGIC)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("brightmare")
	inst:AddTag("brightmare_gestalt")
	inst:AddTag("lunar_aligned")
    inst:AddTag("soulless")
	inst:AddTag("brightmare_guard")
	----------------------------------
    inst._level = net_tinybyte(inst.GUID, "gestalt.level", "leveldirty")
    inst._level:set(1)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)

	if not TheNet:IsDedicated() then
		inst.blobhead = SpawnPrefab("gestalt_guard_head")
		inst.blobhead.entity:SetParent(inst.entity) --prevent 1st frame sleep on clients
		inst.blobhead.Follower:FollowSymbol(inst.GUID, "brightmare_gestalt_head_evolved", 0, 0, 0)
        inst.blobhead.AnimState:SetMultColour(1, 1, 1, 0.6)

		inst.blobhead.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
		inst.blobhead.persists = false

	    inst.highlightchildren = { inst.blobhead }
	end

	inst.AnimState:SetMultColour(1, 1, 1, 0.6)
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, false) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
	----------------------------------
	--Variables	
	inst.shouldwalk = true
    inst.isguard = true
	inst._notrail = true
	inst.specialsleep = true
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	--MakeLargeBurnableCharacter(inst, body_symbol)
    --MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	inst:AddComponent("sanityaura")
	inst.components.sanityaura.aura = TUNING.SANITYAURA_MED
	----------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignorespoilage = true
	---------------------------------
	--Physics and Shadows--
	MakeGhostPhysics(inst, .5, .5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(2.5, 1.5)
	inst.DynamicShadow:Enable(false)  
	
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) inst.AnimState:SetMultColour(1, 1, 1, 0.6) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) 
			inst.AnimState:SetMultColour(1, 1, 1, 0.6)
		end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)