local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "fused_shadelingp"

local assets = 
{
	Asset("ANIM", "anim/fused_shadeling.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local MOB_TUNING = TUNING[string.upper(prefabname)]
local mob = 
{
	health       = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger       = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate   = TUNING.WILSON_HUNGER_RATE, 
	sanity       = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed     = MOB_TUNING.RUNSPEED,
	walkspeed    = MOB_TUNING.WALKSPEED,
	damage       = MOB_TUNING.DAMAGE,
	range        = MOB_TUNING.RANGE, 
	hit_range    = MOB_TUNING.HIT_RANGE,
	attackperiod = MOB_TUNING.ATTACK_PERIOD,
	scale        = MOB_TUNING.SCALE,
	
	bank = "fused_shadeling",
	build = "fused_shadeling",
	shiny = "fused_shadeling",

	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
{
    {"horrorfuel", 1.00},
    {"horrorfuel", 1.00},
    {"horrorfuel", 0.75},
    {"horrorfuel", 0.50},
})

local sounds =
{
    death = "daywalker/leech/die",
    attack = "daywalker/leech/vocalization",
    taunt = "daywalker/leech/vocalization",
    taunt2 = "daywalker/leech/fall_off",
    appear = "daywalker/leech/fall_off",
    disappear = "daywalker/leech/fall_off",
    hit = "daywalker/leech/die",
    jump_pre = "daywalker/leech/leap",
    walk = "daywalker/leech/walk",
    bomb_spawn = "wes/common/foley/balloon_vest",
}

--==============================================
--					Mob Functions
--==============================================
local function remove_character_physics(inst)
    local physics = inst.Physics
    physics:ClearCollisionMask()
    physics:CollidesWith(COLLISION.WORLD)
    physics:CollidesWith(COLLISION.GIANTS)
end

local function reset_character_physics(inst)
    local physics = inst.Physics
    physics:ClearCollisionMask()
    physics:CollidesWith(COLLISION.WORLD)
    physics:CollidesWith(COLLISION.OBSTACLES)
    physics:CollidesWith(COLLISION.SMALLOBSTACLES)
    physics:CollidesWith(COLLISION.CHARACTERS)
    physics:CollidesWith(COLLISION.GIANTS)
end
--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		if data.color ~= nil then
            common_setcolor(inst, data.color)
        end
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.colour = inst.colour
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.CARRAT)
	
	inst.mobsleep = false	
	
	inst:AddTag("moreaggro") --TODO remove this when FF rework releases
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("hostile")
    inst:AddTag("monster")
    inst:AddTag("notraptrigger")
    inst:AddTag("shadow_aligned")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst)
	--Stats--
    inst.sounds = sounds
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	----------------------------------
	--Variables	
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = true
	inst.specialsleep = true
    inst.AnimState:SetSymbolLightOverride("red_art", 1.0)

	inst.debuffimmune = true
	
    inst._RemoveCharacterPhysics = remove_character_physics
    inst._ResetCharacterPhysics = reset_character_physics
	
	local body_symbol = "red_art"
	inst.poisonsymbol = body_symbol
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
    PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
    inst:AddComponent("planarentity")
    inst:AddComponent("planardamage")
    inst.components.planardamage:SetBaseDamage(TUNING.FUSED_SHADELING_PLANAR_DAMAGE)
	----------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
    MakeCharacterPhysics(inst, 10, 1.2)
    inst.Transform:SetSixFaced()
    inst.DynamicShadow:SetSize(2.0, 1.25)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)