local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "alterguardianp"

local assets = 
{
    Asset("ANIM", "anim/alterguardian_phase1.zip"),
	Asset("ANIM", "anim/alterguardian_phase2.zip"),
	Asset("ANIM", "anim/alterguardian_phase3.zip"),
    Asset("ANIM", "anim/alterguardian_spawn_death.zip"),
}

local prefabs =
{
    
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end

local BURN_OFFSET = Vector3(0, 1.5, 0)
-----------------------
--Stats--
local mob = 
{
	health = BOSS_STATS and TUNING.ALTERGUARDIAN_PHASE1_HEALTH or 2000,
	hunger = 300,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 800,
	
	runspeed = TUNING.ALTERGUARDIAN_PHASE1_WALK_SPEED,
	walkspeed = TUNING.ALTERGUARDIAN_PHASE1_WALK_SPEED,
	
	attackperiod = TUNING.ALTERGUARDIAN_PHASE1_ATTACK_PERIOD,
	damage = TUNING.ALTERGUARDIAN_PHASE1_ROLLDAMAGE,
	range = 15,
	hit_range = TUNING.ALTERGUARDIAN_PHASE1_AOERANGE,
	
	bank = "alterguardian_phase1",
	build = "alterguardian_phase1",
	shiny = "alterguardian_phase1",
	
	scale = 1,
	stategraph = "SGalterguardian_phase1p",
	minimap = prefabname..".tex",	
}

local mob2 = {
	health = BOSS_STATS and TUNING.ALTERGUARDIAN_PHASE2_MAXHEALTH or 2500,
	hunger = 225,
	sanity = 800,
	
	walkspeed = TUNING.ALTERGUARDIAN_PHASE2_WALK_SPEED,
	
	bank = "alterguardian_phase2",
	build = "alterguardian_phase2",
	stategraph = "SGalterguardian_phase2p",
	
	damage = TUNING.ALTERGUARDIAN_PHASE2_DAMAGE,
	range = TUNING.ALTERGUARDIAN_PHASE2_CHOP_RANGE,
	hit_range = TUNING.ALTERGUARDIAN_PHASE2_CHOP_RANGE,
	attackperiod = TUNING.ALTERGUARDIAN_PHASE2_ATTACK_PERIOD,		
}

local mob3 = {
	health = BOSS_STATS and TUNING.ALTERGUARDIAN_PHASE3_MAXHEALTH or 3000,
	hunger = 225,
	sanity = 800,
	
	walkspeed = TUNING.ALTERGUARDIAN_PHASE3_WALK_SPEED,
	
	bank = "alterguardian_phase3",
	build = "alterguardian_phase3",
	stategraph = "SGalterguardian_phase3p",
	
	damage = TUNING.ALTERGUARDIAN_PHASE3_DAMAGE,
	range = TUNING.ALTERGUARDIAN_PHASE3_STAB_HITRANGE,
	hit_range = TUNING.ALTERGUARDIAN_PHASE3_STAB_HITRANGE,
	attackperiod = TUNING.ALTERGUARDIAN_PHASE3_ATTACK_PERIOD,	
}

--Loot that drops when you die, duh.
SetSharedLootTable("alterguardianp",
{
    {"moonrocknugget",      1.00},
    {"moonrocknugget",      1.00},
    {"moonrocknugget",      1.00},
    {"moonrocknugget",      1.00},
    {"moonrocknugget",      1.00},
    {"moonrocknugget",      0.66},
    {"moonrocknugget",      0.66},
})

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "brightmare"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "brightmare"}
	end
end
--==============================================
--					Mob Functions
--==============================================
--Phase 3--
local function NoHoles(pt)
    return not TheWorld.Map:IsPointNearHole(pt)
end

local function SpawnTrapProjectile(inst, target_positions)
    local target_position = table.remove(target_positions)

    local projectile = SpawnPrefab("alterguardian_phase3trapprojectile")
    projectile.Transform:SetPosition(target_position:Get())
    projectile:SetGuardian(inst)

    if #target_positions > 0 then
        -- Spawn the projectiles, in a tail-recursive-style way.
        inst:DoTaskInTime(0.5, SpawnTrapProjectile, target_positions)
    else
        inst:PushEvent("endtraps")
    end
end

local function teleport_override_fn(inst)
    local ipos = inst:GetPosition()
    local offset = FindWalkableOffset(ipos, 2*PI*math.random(), 8, 8, true, false)
        or FindWalkableOffset(ipos, 2*PI*math.random(), 12, 8, true, false)

    return (offset ~= nil and ipos + offset) or ipos
end

local TRAP_PLAYERCOUNT_DSQ = TUNING.ALTERGUARDIAN_PHASE3_TARGET_DIST^2
local function do_traps(inst)
    local position = inst:GetPosition()
    local px, py, pz = position:Get()

    -- Add extra traps based on the number of nearby players.
    local num_traps = 3
    for _, v in ipairs(AllPlayers) do
        if not v:HasTag("playerghost") and v.entity:IsVisible()
                and v:GetDistanceSqToPoint(px, py, pz) < TRAP_PLAYERCOUNT_DSQ then
            num_traps = num_traps + 2
        end
    end

    -- Space out the traps mostly evenly, but a little randomly (leaves a little pocket of sorts sometimes)
    local delta = (1.5 + math.random() * 0.5) * PI / num_traps
    local initial_offset = PI2 * math.random()

    local angles = {}
    for i = 1, num_traps do
        table.insert(angles, i*delta + initial_offset)
    end
    shuffleArray(angles)

    local target_positions = {}
    for i = 1, #angles do
        local range = TUNING.ALTERGUARDIAN_PHASE3_TRAP_MINRANGE + math.sqrt(math.random()) * TUNING.ALTERGUARDIAN_PHASE3_TRAP_MAXRANGE
        local offset = FindWalkableOffset(position, angles[i], range, 12, true, true, NoHoles)
        if offset ~= nil then
            -- Turn the offset into a world position.
            offset.x = offset.x + position.x
            offset.y = 0
            offset.z = offset.z + position.z
            table.insert(target_positions, offset)
        end
    end

    if #target_positions > 0 then
        -- Spawn the projectiles, in a tail-recursive-style way.
        inst:DoTaskInTime(0.5, SpawnTrapProjectile, target_positions)
    else
        inst:PushEvent("endtraps")
    end
end

local function track_trap(inst, trap)
    local function ontrapremoved()
        if inst._traps then
            inst._traps[trap] = nil
        end
    end
    inst._traps[trap] = true
    inst:ListenForEvent("onremove", ontrapremoved, trap)
end

local function CalcSanityAura(inst, observer)
    return (inst.components.combat.target ~= nil and TUNING.SANITYAURA_HUGE) or TUNING.SANITYAURA_LARGE
end


--Phase 2--
local function do_boat_spike(inst, target)
end

local RETARGET_MUST_TAGS = { "_combat" }
local RETARGET_CANT_TAGS = { "INLIMBO", "playerghost" }
local RETARGET_ONEOF_TAGS = { "character", "monster" }

local function spawn_spike_with_target(inst, data)
    if not data then
        return
    end

    local spawn_vec = data.spawn_pos or inst:GetPosition()

    local spike = SpawnPrefab("alterguardian_phase2spiketrail")
    spike.Transform:SetPosition(spawn_vec.x, 0, spawn_vec.z)
    spike.Transform:SetRotation(data.angle)
    spike:SetOwner(inst)
end

local MIN_SPIKE_COUNT, MAX_SPIKE_COUNT = 3, 4
local SPIKE_DSQ = TUNING.ALTERGUARDIAN_PHASE2_SPIKE_RANGE * TUNING.ALTERGUARDIAN_PHASE2_SPIKE_RANGE
local SPIKE_SPAWN_DELAY = 15*FRAMES
local SPIKEATTACK_CANT_TAGS = {}
for _, tag in ipairs(RETARGET_CANT_TAGS) do
    table.insert(SPIKEATTACK_CANT_TAGS, tag)
end
table.insert(SPIKEATTACK_CANT_TAGS, "player")

local function do_spike_attack(inst)
    local ipos = inst:GetPosition()

    -- Yes, we could decrement here, but using an increment serves our
    -- frame delaying of the spawns better, since the max count is random.
    local spikes_to_spawn = GetRandomMinMax(MIN_SPIKE_COUNT, MAX_SPIKE_COUNT)
    local spikes_spawned = 0

    -- Prioritize nearby players first.
    for _, p in ipairs(AllPlayers) do
        if not p:HasTag("playerghost") and p.entity:IsVisible()
                and (p.components.health ~= nil and not p.components.health:IsDead())
                and p:GetDistanceSqToPoint(ipos:Get()) < SPIKE_DSQ and p ~= inst then
            local firing_angle = inst.Transform:GetRotation()
            local spawn_data =
            {
                spawn_pos = ipos,
                angle = firing_angle,
            }
            inst:DoTaskInTime(SPIKE_SPAWN_DELAY*spikes_spawned, spawn_spike_with_target, spawn_data)
            spikes_spawned = spikes_spawned + 1
            if spikes_spawned >= spikes_to_spawn then
                break
            end
        end
    end

    if spikes_spawned >= spikes_to_spawn then
        return
    end

    -- There are still spikes we could spawn, so look for other entities that are targetable.

    local ix, iy, iz = ipos:Get()
    local targetable_entities = TheSim:FindEntities(
        ix, iy, iz, TUNING.ALTERGUARDIAN_PHASE2_SPIKE_RANGE,
        RETARGET_MUST_TAGS, GetExcludeTags(inst), RETARGET_ONEOF_TAGS
    )
    if #targetable_entities <= 0 then
        return
    end

    for _, p in ipairs(targetable_entities) do
        if p.components.health ~= nil and not p.components.health:IsDead() then
            local firing_angle = inst:GetAngleToPoint(p.Transform:GetWorldPosition())
            local spawn_data =
            {
                spawn_pos = ipos,
                angle = firing_angle,
            }
            inst:DoTaskInTime(SPIKE_SPAWN_DELAY*spikes_spawned, spawn_spike_with_target, spawn_data)
            spikes_spawned = spikes_spawned + 1
            if spikes_spawned >= spikes_to_spawn then
                break
            end
        end
    end
end

--Phase 1--
local function play_custom_hit(inst)
    if not inst.components.timer:TimerExists("hitsound_cd") then
        if inst._is_shielding then
            inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/hit")
        else
            inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/onothercollide")
        end

        inst.components.timer:StartTimer("hitsound_cd", 5*FRAMES)
    end
end

local function onothercollide(inst, other)
    if not other:IsValid() then
        return

    elseif other:HasTag("smashable") and other.components.health ~= nil then
        other.components.health:Kill()

    elseif other.components.workable ~= nil
            and other.components.workable:CanBeWorked()
            and other.components.workable.action ~= ACTIONS.NET then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        other.components.workable:Destroy(inst)

    elseif other.components.health ~= nil and not other.components.health:IsDead() then
        inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/onothercollide")
        inst.components.combat:DoAttack(other)

    end
end

local COLLISION_DSQ = 42
local function oncollide(inst, other)
    if inst._collisions[other] == nil and other ~= nil and other:IsValid()
            and Vector3(inst.Physics:GetVelocity()):LengthSq() > COLLISION_DSQ then
        ShakeAllCameras(CAMERASHAKE.SIDE, .5, .05, .1, inst, 40)
        inst:DoTaskInTime(2 * FRAMES, onothercollide, other)
        inst._collisions[other] = true

        if other and other == inst.roll_target then
            inst:PushEvent("rollcollidedwithplayer")
        end
    end
end

local function EnableRollCollision(inst, enable)
    if enable then
        inst.Physics:SetCollisionCallback(oncollide)
        inst._collisions = {}
    else
        inst.Physics:SetCollisionCallback(nil)
        inst._collisions = nil
    end
end

local function find_gestalt_target(inst, gestalt)
    local gx, gy, gz = gestalt.Transform:GetWorldPosition()
	local ent = FindEntity(inst, 8, function(guy) return guy ~= inst end, {"_health", "_combat"}, GetExcludeTags(inst))
    return ent
end

local MIN_GESTALTS, MAX_GESTALTS = 6, 10
local EXTRA_GESTALTS_BYHEALTH = 12
local MIN_SUMMON_RANGE, MAX_SUMMON_RANGE = 5, 7

local function DoGestaltSummon(inst)
    local ix, iy, iz = inst.Transform:GetWorldPosition()

    local spawn_warning = SpawnPrefab("alterguardian_summon_fx")
    spawn_warning.Transform:SetScale(1.2, 1.2, 1.2)
    spawn_warning.Transform:SetPosition(ix, iy, iz)

    -- A random amount of spawns plus a base amount based on missing health.
    local num_gestalts = GetRandomMinMax(MIN_GESTALTS, MAX_GESTALTS) + math.ceil((1 - inst.components.health:GetPercent()) * EXTRA_GESTALTS_BYHEALTH)

    local angle_increment = 3.75*PI / num_gestalts -- almost 2pi twice; loop 2 times, but slightly offset
    local initial_angle = 2*PI*math.random()

    for i = 1, num_gestalts do
        -- Spawn a collection of gestalts in a haphazard ring around the boss.
        -- The gestalts are undirected, but will target somebody if they're nearby.

        inst:DoTaskInTime(2.0 + (i*4*FRAMES), function(inst2)
            local gestalt = SpawnPrefab("gestalt_alterguardian_projectile")
            if gestalt ~= nil then
                -- NOTE: Deliberately not square rooting this radius;
                -- clustering closer to the boss is fine behaviour.
                local r = GetRandomMinMax(MIN_SUMMON_RANGE, MAX_SUMMON_RANGE)
                local angle = initial_angle + GetRandomWithVariance((i - 1) * angle_increment, PI/8)
                local x, z = r * math.cos(angle), r * math.sin(angle)

                gestalt.Transform:SetPosition(ix + x, iy + 0, iz + z)

                local target = find_gestalt_target(inst, gestalt)
                if target ~= nil then
                    gestalt:ForceFacePoint(target:GetPosition())
                    gestalt:SetTargetPosition(target:GetPosition())
                end
            end
        end)
    end

    inst:DoTaskInTime(2.0 + (num_gestalts*4*FRAMES) + 1.0, function(inst2)
        spawn_warning:PushEvent("endloop")
    end)

    inst.components.timer:StartTimer("summon_cooldown", TUNING.ALTERGUARDIAN_PHASE1_SUMMONCOOLDOWN)
end

local function EnterShield(inst)
    inst._is_shielding = true

    inst.components.health:SetAbsorptionAmount(TUNING.ALTERGUARDIAN_PHASE1_SHIELDABSORB)

    if not inst.components.timer:TimerExists("summon_cooldown") then
        DoGestaltSummon(inst)
    end
end

local function ExitShield(inst)
    inst._is_shielding = nil

    inst.components.health:SetAbsorptionAmount(0)
end

local function CalcSanityAura(inst, observer)
    return (inst.components.combat.target ~= nil and TUNING.SANITYAURA_HUGE) or TUNING.SANITYAURA_LARGE
end

--==============================================
--				Custom Common Functions
--==============================================
local function SetStats(inst, stats)
	
end

local function SetPhase(inst, phase, onload)
	if not inst:HasTag("playerghost") then
		--yes I'm hardcoding these
		local healthpercent = inst.components.health:GetPercent()
		inst.attacks = {} --empty out the table on each phase change
		if phase == 2 then
			inst.components.health:SetMaxHealth(mob2.health)
			inst.components.combat:SetDefaultDamage(mob2.damage)
			inst.components.combat:SetRange(mob2.range, mob2.hit_range)
			inst.components.combat:SetAttackPeriod(mob2.attackperiod)
		
			inst.AnimState:SetBank(mob2.bank)
			inst.AnimState:SetBuild(mob2.build)
			inst:SetStateGraph(mob2.stategraph)
			
			inst.Light:SetIntensity(0)
			inst.Light:SetRadius(0)
			inst.Light:SetFalloff(0)
			inst.Light:SetColour(0.01, 0.35, 1)
		
			inst.attacks.spin = true
			inst.attacks.spike = true
		
			inst.canhide = nil
			inst.mobsleep = nil
			inst.specialsleep = true
			inst.taunt = true
			inst.taunt2 = true
		
			inst.Transform:SetSixFaced()
		
			inst.DoSpikeAttack = do_spike_attack
			inst.DoBoatSpike = do_boat_spike
		elseif phase >= 3 then
			inst.components.health:SetMaxHealth(mob3.health)
			inst.components.combat:SetDefaultDamage(mob3.damage)
			inst.components.combat:SetRange(mob3.range, mob3.hit_range)
			inst.components.combat:SetAttackPeriod(mob3.attackperiod)
		
			inst.AnimState:SetBank(mob3.bank)
			inst.AnimState:SetBuild(mob3.build)
			inst:SetStateGraph(mob3.stategraph)
		
			inst.Light:SetIntensity(0)
			inst.Light:SetRadius(0)
			inst.Light:SetFalloff(0)
			inst.Light:SetColour(0.01, 0.35, 1)
            inst.Light:Enable(true)
		
			inst.attacks.spin = nil
			inst.attacks.spike = nil
		
			inst.specialsleep = true
			inst.mobsleep = nil
			inst.canhide = true
			inst.taunt = true
			inst.taunt2 = nil
			inst.taunt3 = true
			
			inst.Transform:SetSixFaced()
		
			inst.DoTraps = do_traps
			inst.TrackTrap = track_trap
			inst._traps = {}
		
			inst.DoSpikeAttack = nil
			inst.DoBoatSpike = nil
			PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
		else
			PlayablePets.SetCommonStats(inst, mob)
			inst.DoTraps = nil
			inst.TrackTrap = nil
			inst._traps = nil
		
			inst.attacks.spin = nil
			inst.attacks.spike = nil
		
			inst.specialsleep = nil
			inst.mobsleep = nil
			inst.taunt = true
			inst.taunt2 = true
			inst.taunt3 = nil
			inst.Transform:SetFourFaced()
		end
		inst.current_phase = phase
		if onload then
			inst.components.health:SetPercent(healthpercent)
		end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.current_phase = data.current_phase or 1
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.current_phase = inst.current_phase or 1
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.ALTERGUARDIAN)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "darts", "staves", "melee"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("epic")
    inst:AddTag("hostile")
    inst:AddTag("largecreature")
    inst:AddTag("mech")
    inst:AddTag("monster")
    inst:AddTag("scarytoprey")
    inst:AddTag("soulless")
	inst:AddTag("brightmareboss")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	--PlayablePets.SetSandstormImmunity(inst)
	--PlayablePets.SetLunarstormImmunity_Client(inst)
end

local function OnDeath(inst)
	--SetPhase(1)
	inst.amphibious = nil
end

local function OnRevive()
	
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	inst.components.combat.noimpactsound = true
	inst.components.combat:SetHurtSound("moonstorm/creatures/boss/alterguardian1/onothercollide")
	PlayablePets.SetPassiveHeal(inst)
	----------------------------------
	--Variables	
	inst.mobsleep = false
	inst.taunt = true
	inst.canhide = true
	inst.shouldwalk = true
	inst.current_phase = 1
	
	inst.attacks = {}
	
	--SetPhase(inst, 1)
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, nil, BURN_OFFSET)
    inst.components.burnable:SetBurnTime(5)
    MakeMediumFreezableCharacter(inst, body_symbol)
	PlayablePets.SetCommonStatResistances(inst, 0, 0.5, 0, 8) --fire, acid, poison, freeze
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	PlayablePets.SetStormImmunity(inst)
	
	inst.EnableRollCollision = EnableRollCollision
    inst.EnterShield = EnterShield
    inst.ExitShield = ExitShield
	inst.SetPhase = SetPhase
	
	
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	inst.components.lootdropper.min_speed = 4.0
    inst.components.lootdropper.max_speed = 6.0
    inst.components.lootdropper.y_speed = 14
    inst.components.lootdropper.y_speed_variance = 2
	
	inst:AddComponent("sanityaura")
    inst.components.sanityaura.aurafn = CalcSanityAura
    inst.components.sanityaura.max_distsq = 225
	
	inst:AddComponent("timer")
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeGiantCharacterPhysics(inst, 500, 1.25)
    inst.DynamicShadow:SetSize(5.00, 1.50)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("blocked", play_custom_hit)
	inst:ListenForEvent("death", OnDeath)
    --inst:ListenForEvent("phasetransition", OnPhaseTransition)

    inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian1/idle_LP", "idle_LP")
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) SetPhase(inst, inst.current_phase, true) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) SetPhase(inst, inst.current_phase) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
