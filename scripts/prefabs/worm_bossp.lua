local MakePlayerCharacter = require "prefabs/player_common"
local WORMBOSS_UTILS = require("prefabs/worm_boss_util")
local easing = require("easing")

---------------------------
----------==Notes==--------
--Klei has an invisible prefab that handles the worm.
--So the question is how we do this with the player? 
--Do we make the head playable or make it an invisible prefab as well.
--Making it invisible might be redundant, might also break inspection.
--But it might also make control slightly easier or smooth. Not sure.
---------------------------
local prefabname = "worm_bossp"

local assets = 
{
	Asset("ANIM", "anim/worm_boss.zip"),
    Asset("ANIM", "anim/worm_boss_segment.zip"),
    Asset("ANIM", "anim/worm_boss_segment_2_build.zip"),
    Asset("SCRIPT", "scripts/prefabs/worm_boss_util.lua"),
}

local prefabs =
{
    "worm_boss_dirt",
    "worm_boss_dirt_ground_fx",
    "worm_boss_head",
    "worm_boss_segment",
}
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local MOB_TUNING = TUNING[string.upper(prefabname)]
local mob = 
{
	health       = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger       = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate   = TUNING.WILSON_HUNGER_RATE, 
	sanity       = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed     = MOB_TUNING.RUNSPEED,
	walkspeed    = MOB_TUNING.WALKSPEED,
	damage       = MOB_TUNING.DAMAGE,
	range        = MOB_TUNING.RANGE, 
	hit_range    = MOB_TUNING.HIT_RANGE, --range is 1.8 in stategraph.
	attackperiod = MOB_TUNING.ATTACK_PERIOD,
	bank         = "worm_boss",
	build        = "worm_boss",
	scale        = 1,
	--build2 = "alternate build here",
	stategraph   = "SG"..prefabname,
	minimap      = prefabname..".tex",	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
    { "monstermeat",  1.00 },
    { "monstermeat",  1.00 },
    { "monstermeat",  1.00 },
    { "monstermeat",  0.66 },
    { "monstermeat",  0.66 },
    { "wormlight",    1.00 },
})

local sounds =
{
    
}

local FORGE_STATS = PPROT_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
--Written by klei on the original prefab, altered as needed.

local function GenerateLoot(inst, pos)
    local loottable = {
        boneshard = 25,
        rocks = 20,
        flint = 15,
        nitre = 15,
        monstermeat = 15,
        goldnugget = 4,
        slurtle_shellpieces = 2,
        tentaclespots = 2,
        lightbulb = 2,
        wormlight = 2,
        guano = 2,
        redgem = 2,
        bluegem = 2,
        purplegem = 2,
        trinket_17 = 1,
        trinket_12 = 1,
        orangegem = 1,
        yellowgem = 1,
        greengem = 1,
        thulecite = 1,
        fossil_piece = 1,
    }

    local choice = weighted_random_choice(loottable)

    if choice ~= nil then
        inst.components.lootdropper:FlingItem(SpawnPrefab(choice), pos)
    end
end

local SHAKE_DIST = 40

local function OnUpdate(inst, dt)
    if #inst.chunks < 1 then
        WORMBOSS_UTILS.CreateNewChunk(inst, inst:GetPosition())
    end

    for _, chunk in ipairs(inst.chunks) do
        WORMBOSS_UTILS.UpdateChunk(inst, chunk, dt)
    end
end

-----------------------------------------------------------------------------------------------------------------------

local DECAY_THORNS_EFFECT_TIME = 3
local NUM_THORNS_TO_KNOCKBACK = 3

local function ProcessThornDamage(inst, target)
    inst.SoundEmitter:PlaySound("rifts4/worm_boss/spike_slice")

    target.components.combat:GetAttacked(inst, TUNING.WORM_BOSS_SPINES)

    local owner = inst.worm or inst
    owner._thorns_targets = owner._thorns_targets or {}

    local targets = owner._thorns_targets

    local currenttime = GetTime()

    -- Limit hits... do knockback.
    if targets[target] == nil then
        targets[target] = { [currenttime] = true }
    else
        targets[target][currenttime] = true
    end

    for time, _ in pairs(shallowcopy(targets[target])) do
        if currenttime > (time + DECAY_THORNS_EFFECT_TIME) then
            targets[target][time] = nil
        end
    end

    if GetTableSize(targets[target]) >= NUM_THORNS_TO_KNOCKBACK then
        targets[target] = nil

        WORMBOSS_UTILS.Knockback(inst, target)
    end
end

-----------------------------------------------------------------------------------------------------------------------

local function OnDeath(inst, data)
    inst.state = WORMBOSS_UTILS.STATE.DEAD

    if inst.new_crack ~= nil then
        inst.new_crack:Remove()
        inst.new_crack= nil
    end

    local freehead  = false  -- The regular head is present.
    local headchunk = nil    -- The head is in the a segmented form.

    for _, chunk in ipairs(inst.chunks) do
        if chunk.lastsegment ~= nil then
            chunk.lastsegment:Remove()
        end
        if chunk.dirt_start ~= nil and chunk.dirt_start:IsValid() then
            chunk.dirt_start:AddTag("notarget")
            chunk.dirt_start.AnimState:PlayAnimation("dirt_idle")
            chunk.dirt_start.persists = false
        end
    
        if chunk.dirt_end ~= nil and chunk.dirt_end:IsValid() then
            chunk.dirt_end:AddTag("notarget")
            chunk.dirt_end.AnimState:PlayAnimation("dirt_idle")
            chunk.dirt_end.persists = false
        end
    
        for _, segment in ipairs(chunk.segments) do
            if segment.head then
                headchunk = chunk
                break
            end
        end
        if chunk.head then
           freehead = true
        end
    end

    inst.headlootdropped = nil
    if inst.head then
        inst.headlootdropped = Vector3(inst.head.Transform:GetWorldPosition())
    elseif inst.createnewchunktask then
        inst.headlootdropped = inst.createnewchunktask._target_pt
    elseif inst.chunks and inst.chunks[#inst.chunks] and inst.chunks[#inst.chunks].groundpoint_start then
        inst.headlootdropped = inst.chunks[#inst.chunks].groundpoint_start
    end

    if headchunk ~= nil then
       WORMBOSS_UTILS.SpawnAboveGroundHeadCorpse(inst, headchunk)
    elseif freehead and inst.head ~= nil then
       inst.head:PushEvent("death")       
       
    else
       WORMBOSS_UTILS.SpawnUnderGroundHeadCorpse(inst)
    end

    inst.current_death_chunk = math.max(1, #inst.chunks - 1)
end

local function _PlayDirstPstSlowAnim(dirt)
    dirt.AnimState:PlayAnimation("dirt_pst_slow")
end

local SEGMENT_ERODE_TIME = 6

local function OnDeathEnded(inst)
    if inst.devoured then
        WORMBOSS_UTILS.SpitAll(inst, nil, true)
    end

    if inst.head ~= nil then
        inst.head:PushEvent("death_ended")
    end

    for _, chunk in ipairs(inst.chunks) do
        for i,segment in ipairs(chunk.segments)do
            if not segment.AnimState:IsCurrentAnimation("segment_death_pst") then
                segment.AnimState:PlayAnimation("segment_death_pst")
            end
        end

        if chunk.dirt_start ~= nil then
            chunk.dirt_start:DoTaskInTime(math.random()*3 + 4, _PlayDirstPstSlowAnim)
        end

        if chunk.dirt_end ~= nil then
            chunk.dirt_end:DoTaskInTime(math.random()*3 + 4, _PlayDirstPstSlowAnim)
        end

        --for _, segment in ipairs(chunk.segments) do
            --ErodeAway(segment, SEGMENT_ERODE_TIME)
        --end
    end


    inst.Transform:SetPosition(inst.chunks[#inst.chunks].dirt_start.Transform:GetWorldPosition()) -- For inventory:DropEverything position.

    inst.components.inventory:DropEverything(true)
    inst.components.lootdropper:DropLoot()
    inst.headlootdropped = nil

    inst:Remove()
end

-----------------------------------------------------------------------------------------------------------------------

local function SerializePosition(pos)
    return pos ~= nil and { x = pos.x, z = pos.z } or nil
end

local function DeserializePosition(data)
    return Vector3(data.x, 0, data.z)
end

local function OnSave(inst, data)
    data.state = inst.state

    if inst.state == WORMBOSS_UTILS.STATE.DEAD then

        if inst.headlootdropped then
            data.headlootdropped =SerializePosition(inst.headlootdropped)
        end

        data.lootspots = {}
        for i, chunk in ipairs(inst.chunks) do
            for s,segment in ipairs(chunk.segments)do
                if not segment:IsInLimbo() and not segment.AnimState:IsCurrentAnimation("segment_death_pst") then
                    table.insert(data.lootspots,SerializePosition(Vector3(segment.Transform:GetWorldPosition()) ))
                end
            end
        end

    else
        if inst.createnewchunktask ~= nil   then
            data.new_chunk_pos = SerializePosition(inst.createnewchunktask._target_pt)
        end

        if #inst.chunks <= 1 then
            return -- Not really worth saving any chunk...
        end

        data.chunks = {}
        data.lootspots = {}

        for i, chunk in ipairs(inst.chunks) do
            local savechunk = {}

            savechunk.groundpoint_start = SerializePosition(chunk.groundpoint_start)
            savechunk.groundpoint_end   = SerializePosition(chunk.groundpoint_end  )

            table.insert(data.chunks, savechunk)
        end
    end
end

local function OnLoad(inst, data)

    if data == nil then
        return
    end

    inst:SetState(data.state)
    
    if inst.state == WORMBOSS_UTILS.STATE.DEAD then
        -- all the prefabs don't persist, just spawning the unspawned loot in post pass, then removing. 
        return
    end

    local headchunk = nil

    if data.chunks ~= nil then
        local head = data.new_chunk_pos == nil and #data.chunks or nil -- The head is always the last chunk, if it exist...
        local tail = #data.chunks > WORMBOSS_UTILS.WORM_LENGTH and 1 or nil -- The tail is always the first chunk, when it's complete.

        for i, chunkdata in ipairs(data.chunks) do
            local newchunk = WORMBOSS_UTILS.CreateNewChunk(inst, DeserializePosition(chunkdata.groundpoint_start), true)

            if chunkdata.groundpoint_end ~= nil then
                newchunk.groundpoint_end = DeserializePosition(chunkdata.groundpoint_end)
            end

            if i == tail then
                newchunk.lastrun = true

                -- Create the things that would be there if the chunk is a tail.
                WORMBOSS_UTILS.SpawnDirt(inst, newchunk, newchunk.groundpoint_end, false, true)
                newchunk.dirt_start:Remove()
            end

            if i == head then
                headchunk = newchunk
            else
                newchunk.state = WORMBOSS_UTILS.CHUNK_STATE.MOVING

                -- Update the chunks until we have a regular chunk or we have turned into a tail prefab.
                while not newchunk.loopcomplete and newchunk.tail == nil do
                    WORMBOSS_UTILS.UpdateChunk(inst, newchunk, FRAMES, true)
                end
            end
        end
    end

    if data.new_chunk_pos ~= nil then
        WORMBOSS_UTILS.SetCreateChunkTask(inst, DeserializePosition(data.new_chunk_pos))
    end

    if headchunk ~= nil then
        WORMBOSS_UTILS.EmergeHead(inst, headchunk, true) -- Deferred head creation because the head emergion changes the worm state.
    end
end


local function OnLoadPostPass(inst, newents, data)
    if data then
        if data.lootspots then
            for i,spot in ipairs(data.lootspots) do
                local pos = DeserializePosition(spot)
                GenerateLoot(inst, pos)
                GenerateLoot(inst, pos)
                GenerateLoot(inst, pos)
            end
        end
        if data.headlootdropped then
            local pos = DeserializePosition(data.headlootdropped) 
            inst.Transform:SetPosition(pos.x,pos.y,pos.z)
            inst.components.lootdropper:DropLoot()
            inst.components.inventory:DropEverything(true)
        end
    end
    if inst.state == WORMBOSS_UTILS.STATE.DEAD then
        inst:Remove()
    end
end

-----------------------------------------------------------------------------------------------------------------------

local function SetState(inst, state)
    if inst.state ~= WORMBOSS_UTILS.STATE.DEAD then
        inst.state = state
    end
end

local function OnRemoveEntity(inst)
    if inst.segment_pool ~= nil then
        for i, v in ipairs(inst.segment_pool) do
            v:Remove()
        end

        inst.segment_pool = {}
    end

    if inst.components.health:IsDead() then
        return -- Let the death logic remove the chunks.
    end

    if inst.new_crack ~= nil then
        inst.new_crack:Remove()
        inst.new_crack= nil
    end

    for _, chunk in ipairs(inst.chunks) do
        if chunk.lastsegment ~= nil then
            chunk.lastsegment:Remove()
        end

        if chunk.dirt_start ~= nil then
            chunk.dirt_start:Remove()
        end

        if chunk.dirt_end ~= nil then
            chunk.dirt_end:Remove()
        end

        if chunk.head ~= nil then
            chunk.head:Remove()
        end

        if chunk.tail ~= nil then
            chunk.tail:Remove()
        end

        for _, segment in ipairs(chunk.segments) do
            segment:Remove()
        end
    end
end

local OFFSCREEN_REMOVAL_DELAY = 30

local function Worm_TestForRemoval(inst)
    if inst._sleeptime == nil or (GetTime() - inst._sleeptime <= OFFSCREEN_REMOVAL_DELAY) then
        return
    end

    for _, chunk in ipairs(inst.chunks) do
        if chunk.dirt_start ~= nil and chunk.dirt_start.entity:IsAwake() then
            return
        end

        if chunk.dirt_end ~= nil and chunk.dirt_end.entity:IsAwake() then
            return
        end
    end

    inst:Remove()
end

-----------------------------------------------------------------------------------------------------------------------

local DEBUG_POOLING_DISABLED = true -- FIXME(DiogoW): Have a solution for client position interpolation without physics teleport.

local function Worm_GetSegmentFromPool(inst)
    local segment = not DEBUG_POOLING_DISABLED and table.remove(inst.segment_pool) or nil

    if segment == nil then
        segment = SpawnPrefab("worm_boss_segment")
    else
        segment:Restart()
    end

    return segment
end

local function Worm_ReturnSegmentToPool(inst, segment)
    if DEBUG_POOLING_DISABLED then
        segment:Remove()
    end

    if segment:IsInLimbo() then
        return
    end

    segment:RemoveFromScene()
    segment:SetHighlightOwners(nil, nil)

    table.insert(inst.segment_pool, segment)
end

local function OnSyncOwnerDirty(inst)
    inst:OnSetHighlightOwners(inst.syncowner1:value(), inst.syncowner2:value())
end

function HighlightHandler_OnRemoveEntity(inst)
    for _, owner in pairs(inst._owners) do
        if owner.components.colouradder ~= nil then
            owner.components.colouradder:DetachChild(inst)
        end

        if owner.highlightchildren ~= nil then
            table.removearrayvalue(owner.highlightchildren, inst)
        end
    end
end

function SetHighlightOwners(inst, owner1, owner2)
    if inst.syncowner1 ~= nil then
        inst.syncowner1:set(owner1)
    end

    if inst.syncowner2 ~= nil then
        inst.syncowner2:set(owner2)
    end

    inst:OnSetHighlightOwners(owner1, owner2)
end

local function HighlightHandler_SetOwner(inst, index, owner)
    if inst._owners[index] ~= nil then
        if not TheNet:IsDedicated() then
            inst.AnimState:SetHighlightColour()

            table.removearrayvalue(inst._owners[index].highlightchildren, inst)
        end

        if inst._owners[index].components.colouradder ~= nil then
            inst._owners[index].components.colouradder:DetachChild(inst)
        end
    end

    inst._owners[index] = owner ~= nil and owner:IsValid() and owner or nil

    if inst._owners[index] ~= nil then
        if not TheNet:IsDedicated() then
            if inst._owners[index].highlightchildren == nil then
                inst._owners[index].highlightchildren = { inst }
            else
                table.insert(inst._owners[index].highlightchildren, inst)
            end
        end

        if inst._owners[index].components.colouradder ~= nil then
            inst._owners[index].components.colouradder:AttachChild(inst)
        end
    end
end

function OnSetHighlightOwners(inst, owner1, owner2)
    HighlightHandler_SetOwner(inst, inst.syncowner1, owner1)
    HighlightHandler_SetOwner(inst, inst.syncowner2, owner2)

    if not TheNet:IsDedicated() then
        inst.client_forward_target = owner1 or owner2
    end
end

local function AddHighlightHandler(inst)
    if inst.Network == nil then
        return
    end

    inst._owners = {}

    inst.syncowner1 = net_entity(inst.GUID, "syncowner1", "hightlighownerdirty")
    inst.syncowner2 = net_entity(inst.GUID, "syncowner2", "hightlighownerdirty")

    inst.OnSetHighlightOwners = OnSetHighlightOwners

    inst:ListenForEvent("onremove", HighlightHandler_OnRemoveEntity)

    if not TheWorld.ismastersim then
        inst:ListenForEvent("hightlighownerdirty", OnSyncOwnerDirty)
    else
        inst.SetHighlightOwners = SetHighlightOwners
    end
end

--==============================================
--				Custom Common Functions
--==============================================	

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	
end

--==============================================
--					Reforged
--==============================================
local FORGE_STATS = PP_FORGE[string.upper(prefabname)]
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
        if ThePlayer then
            inst:EnableMovementPrediction(false) --mob players don't use client stategraphs, so movement prediction needs to be disabled.
            if MONSTERHUNGER== "Disable" then
                --ThePlayer.HUD.controls.status.stomach:Hide()
            end
        end
    end)
    inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
    AddHighlightHandler(inst)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("NOCLICK")
    inst:AddTag("INLIMBO")
    inst:AddTag("NOBLOCK")
    inst:AddTag("groundpound_immune")
    inst:AddTag("worm_boss_piece")
    inst:AddTag("epic")
    inst:AddTag("wet")
	----------------------------------

	inst:WatchWorldState( "issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	PlayablePets.SetSandstormImmunity(inst)

end

local master_postinit = function(inst)  
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
    inst.components.combat.forcefacing = false
	inst.components.combat.hiteffectsymbol = "body_top"
	----------------------------------
	--Variables		
	inst.shouldwalk = true
	inst.mobplayer = true

    inst.child_scale = 1
    inst.chunks = {}
    inst.segment_pool = {}
    inst.sounds = sounds

    inst.SetState = SetState
    inst.GetSegmentFromPool = Worm_GetSegmentFromPool
    inst.ReturnSegmentToPool = Worm_ReturnSegmentToPool
    inst._ondeath = OnDeath
    inst._ondeathended = OnDeathEnded

    inst.AnimState:SetFinalOffset(-3)

    AddHighlightHandler(inst)

    inst:SetState(WORMBOSS_UTILS.STATE.EMERGE)
	
	inst.ghostbuild = "ghost_monster_build"
	
	local body_symbol = "body_top"
	inst.poisonsymbol = body_symbol
	--MakeLargeBurnableCharacter(inst, body_symbol)
    --MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetStormImmunity(inst)
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	inst.components.lootdropper.y_speed = 4
	inst.components.lootdropper.y_speed_variance = 3
	inst.components.lootdropper.spawn_loot_inside_prefab = true

    if not inst.components.updatelooper then --TODO remove this if player common already adds it.
        inst:AddComponent("updatelooper")
    end
    inst.components.updatelooper:AddOnUpdateFn(OnUpdate)
	----------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	--Physics and Scale--
    inst:SetPhysicsRadiusOverride(.75)
	MakeCharacterPhysics(inst, 50, inst.physicsradiusoverride)
	inst.DynamicShadow:SetSize(2, 1)
	inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
    inst:ListenForEvent("death", inst._ondeath)
    inst:ListenForEvent("death_ended", inst._ondeathended)
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true) 
		end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    inst.OnLoadPostPass = OnLoadPostPass
    inst.OnRemoveEntity = OnRemoveEntity
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
