local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "stageusherp"

local assets = 
{
    Asset("ANIM", "anim/stagehand.zip"),
    Asset("ANIM", "anim/stagehand_sts.zip"),
}

local prefabs = 
{	
	
}
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE/2, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 4,
	walkspeed = 4,
	damage = TUNING.STAGEUSHER_ATTACK_DAMAGE,
	range = TUNING.STAGEUSHER_ATTACK_RANGE,
	hit_range = TUNING.STAGEUSHER_ATTACK_RANGE,
	attackperiod = TUNING.STAGEUSHER_ATTACK_PERIOD,
	bank = "stagehand",
	build = "stagehand_sts",
	shiny = "stagehand",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGstageusherp",
	minimap = "stagehandp.tex",	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('stagehandp',
-----Prefab---------------------Chance------------
{
   --{'endtable_blueprint', 1.0},
    
   
})

local sounds =
{
    hit              = "dontstarve/creatures/together/stagehand/hit",
	awake_pre        = "dontstarve/creatures/together/stagehand/awake_pre",
	footstep         = "dontstarve/creatures/together/stagehand/footstep",
}

local FORGE_STATS = PPROT_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local function SetPhysicsState(inst, set_to_standing)
    local is_blocker = inst:HasTag("blocker")
    if set_to_standing then
        if is_blocker then
            inst:RemoveTag("blocker")
            inst:RemoveTag("notarget")
            inst.Physics:SetMass(100)
            inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.WORLD)
        end
    else
        if not is_blocker then
            inst:AddTag("blocker")
            inst:AddTag("notarget")
            inst.Physics:SetMass(0)
            inst.Physics:SetCollisionGroup(COLLISION.OBSTACLES)
            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.ITEMS)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)
        end
    end
end

local function IsStanding(inst)
    return inst._is_standing
end

local function ChangeStanding(inst, new_standing)
    new_standing = new_standing or not inst._is_standing
    if new_standing and not inst._is_standing then
        inst._is_standing = true
        inst.components.combat.canattack = true
        SetPhysicsState(inst, inst._is_standing)
    elseif not new_standing and inst._is_standing then
        inst._is_standing = false
        inst.components.combat.canattack = false
        SetPhysicsState(inst, inst._is_standing)

        -- Reset our work and health when we sit down.
        inst.components.workable:SetWorkLeft(TUNING.STAGEHAND_HITS_TO_GIVEUP)
        inst.components.health:SetPercent(1)
    end
end

local function StartAttackingTarget(inst, target)
    if target == nil or not target:IsValid() then
        return false
    end

    local ipos = inst:GetPosition()
    local tpos = target:GetPosition()
    local unit_target_vec = (tpos - ipos):GetNormalized()

    local attack_hand = SpawnPrefab("stageusher_attackhand")
    attack_hand.Transform:SetPosition((ipos + unit_target_vec*0.5):Get())
    attack_hand:SetOwner(inst)
    attack_hand:SetCreepTarget(target)

    if inst._on_hand_removed == nil then
        inst._on_hand_removed = function(hand) inst:PushEvent("handfinished") end
    end
    inst:ListenForEvent("onremove", inst._on_hand_removed, attack_hand)

    return true
end
--==============================================
--				Custom Common Functions
--==============================================	
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
    inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("monster")
    inst:AddTag("stageusher")
	----------------------------------

	inst:WatchWorldState( "issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)
end

local master_postinit = function(inst)  
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 1, 0, 0, 9999) --fire, acid, poison
    inst.components.combat.ignorehitrange = true

    inst.AnimState:OverrideSymbol("dark_spew", "stagehand", "dark_spew")
    inst.AnimState:OverrideSymbol("fx", "stagehand", "fx")
    inst.AnimState:OverrideSymbol("stagehand_fingers", "stagehand", "stagehand_fingers")
	----------------------------------
	--Variables		
	inst.mobsleep = true
	inst.shouldwalk = false
	inst.mobplayer = true
	
	inst.ghostbuild = "ghost_monster_build"
	
	inst.sounds = sounds
    inst._is_standing = false
    inst.IsStanding = IsStanding
    inst.ChangeStanding = ChangeStanding
    inst.StartAttackingTarget = StartAttackingTarget
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    --MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5) --This might multiply food stats.
	---------------------------------
	--Physics and Shadows--
	--Physics and Scale--
	inst.CharacterPhyscisMass = 150
	MakeCharacterPhysics(inst, 10, .5)

    inst.DynamicShadow:SetSize(1.5, .5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
