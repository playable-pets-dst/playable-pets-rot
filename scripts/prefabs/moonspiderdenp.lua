local assets =
{
    Asset("ANIM", "anim/spider_mound_mutated.zip"),
    Asset("MINIMAP_IMAGE", "spidermoonden"),
}

local prefabs =
{
    "spider_moon",

    --loot
    "rocks",
    "moonglass",
    "silk",
    "spidergland",
    "silk",
    "moonrocknugget",

    --fx
    "rock_break_fx",
}

SetSharedLootTable('moon_spider_holep',
{
    {"rocks",           1.00},
    {"moonglass",       1.00},
    {"silk",            1.00},
    {"moonrocknugget",  1.00},
    {"moonrocknugget",  0.50},
    {"spidergland",     0.25},
    {"silk",            0.50},
})

local SMALL = 1
local MEDIUM = 2
local LARGE = 3

local function set_stage(inst, workleft, play_grow_sound)
    local new_stage = (workleft * 4 <= TUNING.MOONSPIDERDEN.WORK and SMALL)
            or (workleft * 2 <= TUNING.MOONSPIDERDEN.WORK and MEDIUM)
            or LARGE

    if inst._stage ~= nil and inst._stage == (new_stage - 1) then
        if inst._stage == SMALL and new_stage == MEDIUM then
            inst.AnimState:PlayAnimation("grow_low_to_med")
            inst.AnimState:PushAnimation("med")
        elseif inst._stage == MEDIUM and new_stage == LARGE then
            inst.AnimState:PlayAnimation("grow_med_to_full")
            inst.AnimState:PushAnimation("full")
        end

        if play_grow_sound then
            inst.SoundEmitter:PlaySound("dontstarve/creatures/spider/spiderLair_grow")
        end
    else
        inst.AnimState:PlayAnimation((new_stage == SMALL and "low") or (new_stage == MEDIUM and "med") or "full")
    end

    inst.GroundCreepEntity:SetRadius(TUNING.MOONSPIDERDEN.CREEPRADIUS[new_stage])
    inst._num_investigators = TUNING.MOONSPIDERDEN.MAX_INVESTIGATORS[new_stage]

    inst._stage = new_stage
end

---------------------------------------------------------------------------
---- WORKABLE REGENERATION TASK MANAGEMENT ----
---------------------------------------------------------------------------

local function stop_regen_task(inst)
    if inst._regen_task ~= nil then
        inst._regen_task:Cancel()
        inst._regen_task = nil
    end
end

local function try_work_regen(inst)
    if inst.components.workable ~= nil and inst.components.workable.workleft < TUNING.MOONSPIDERDEN.WORK then
        local new_work_amount = inst.components.workable.workleft + 1
        set_stage(inst, new_work_amount, true)
        inst.components.workable:SetWorkLeft(new_work_amount)

        -- If we've regenerated back to our max work amount, end the task. We'll start it up again the next time we get worked on.
        if new_work_amount == TUNING.MOONSPIDERDEN.WORK then
            stop_regen_task(inst)
        end
    end
end

local function start_regen_task(inst)
    if inst._regen_task == nil then
        inst._regen_task = inst:DoPeriodicTask(TUNING.MOONSPIDERDEN.WORK_REGENTIME, try_work_regen)
    end
end

---------------------------------------------------------------------------

local function push_twitch_idle(inst)
    if inst.components.workable ~= nil and (inst.components.workable.workleft * 2) > TUNING.MOONSPIDERDEN.WORK then
        inst.AnimState:PlayAnimation("twitch")
        inst.AnimState:PushAnimation("full")
    end
end

local function start_twitch_idle(inst)
    if inst._idle_task == nil then
        inst._idle_task = inst:DoPeriodicTask(15 + math.random() * 5, push_twitch_idle)
    end
end

local function stop_twitch_idle(inst)
    if inst._idle_task ~= nil then
        inst._idle_task:Cancel()
        inst._idle_task = nil
    end
end

---------------------------------------------------------------------------

local function spawner_onworked(inst, worker, workleft)
    if workleft <= 0 then
        local pos = inst:GetPosition()
        SpawnPrefab("rock_break_fx").Transform:SetPosition(pos:Get())
        inst.components.lootdropper:DropLoot(pos)

        stop_regen_task(inst)
        inst:Remove()
    else
        set_stage(inst, workleft, false)
        start_regen_task(inst)
    end

    if inst.components.multisleepingbag ~= nil then
        inst.components.multisleepingbag:DoWakeUpAll()
    end
end

local function on_workable_load(inst)
    set_stage(inst, inst.components.workable.workleft, false)

    if inst.components.workable.workleft < TUNING.MOONSPIDERDEN.WORK then
        start_regen_task(inst)
    end
end

---------------------------------------------------------------------------

local function on_save(inst, data)
    if inst._sleep_start_time ~= nil and (inst.components.workable.workleft < inst.components.workable.maxwork) then
        local time_since_sleep = GetTime() - inst._sleep_start_time

        -- Do 'proper' rounding, so that we're closer to the correct amount of work than we would be
        -- just from a floor or ceiling.
        local work_regen_during_sleep = math.floor((time_since_sleep / TUNING.MOONSPIDERDEN.WORK_REGENTIME) + 0.5)

        if work_regen_during_sleep > 0 then
            data.sleeping_work_regen = work_regen_during_sleep
        end
    end
end

local function on_load(inst, data)
    if data ~= nil and data.sleeping_work_regen ~= nil then
        local new_work_amount = math.min(inst.components.workable.maxwork, inst.components.workable.workleft + data.sleeping_work_regen)
        inst.components.workable:SetWorkLeft(new_work_amount)
        set_stage(inst, new_work_amount, true)

        -- If we've regenerated back to our max work amount, end the regeneration task (we may have started it in on_workable_load)
        -- We'll start it up again the next time we get worked on.
        if new_work_amount == TUNING.MOONSPIDERDEN.WORK then
            stop_regen_task(inst)
        end
    end
end

local function on_sleep(inst)
    stop_twitch_idle(inst)

    inst._sleep_start_time = GetTime()
    stop_regen_task(inst)
end

local function on_wake(inst)
    start_twitch_idle(inst)

    if inst._sleep_start_time ~= nil and inst.components.workable ~= nil and (inst.components.workable.workleft < inst.components.workable.maxwork) then
        local time_since_sleep = GetTime() - inst._sleep_start_time

        -- Do 'proper' rounding, so that we're closer to the correct amount of work than we would be
        -- just from a floor or ceiling.
        local work_regen_during_sleep = math.floor((time_since_sleep / TUNING.MOONSPIDERDEN.WORK_REGENTIME) + 0.5)

        if work_regen_during_sleep > 0 then
            local new_work_amount = math.min(inst.components.workable.maxwork, inst.components.workable.workleft + work_regen_during_sleep)
            inst.components.workable:SetWorkLeft(new_work_amount)

            set_stage(inst, new_work_amount, true)
        end

        -- If we still haven't hit our maxwork, we need to restart the regen task.
        if inst.components.workable.workleft < inst.components.workable.maxwork then
            start_regen_task(inst)
        end
    end

    inst._sleep_start_time = nil
end

local function onwake(inst, sleeper, nostatechange)

    if not nostatechange then
        if sleeper.sg:HasStateTag("house_sleep") then
            sleeper.sg.statemem.iswaking = true
        end
		--inst.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
        sleeper.sg:GoToState("taunt")
    end

    if inst.sleep_anim ~= nil then
        --inst.AnimState:PushAnimation("idle", true)
    end
end

local function OnKilled(inst)
    inst.AnimState:PlayAnimation("death", false)
    --inst.SoundEmitter:KillSound("loop")
    inst.components.lootdropper:DropLoot(inst:GetPosition())
end

local function onsleeptick(inst, sleepers)
	if sleepers and #sleepers > 0 then
		for i, sleeper in ipairs(sleepers) do
			local isstarving = sleeper.components.beaverness ~= nil and sleeper.components.beaverness:IsStarving()
	
			if not sleeper:HasTag(inst.components.multisleepingbag.musthavetag) then
				inst.components.multisleepingbag:DoWakeUp(sleeper)
			end
	
			if sleeper.components.hunger ~= nil then
				sleeper.components.hunger:DoDelta(inst.hunger_tick, true, true)
				isstarving = sleeper.components.hunger:IsStarving()
			end

			if sleeper.components.sanity ~= nil and sleeper.components.sanity:GetPercentWithPenalty() < 1 then
				sleeper.components.sanity:DoDelta(TUNING.SLEEP_SANITY_PER_TICK * 2, true)
			end

			if not isstarving and sleeper.components.health ~= nil then
				sleeper.components.health:DoDelta(TUNING.SLEEP_HEALTH_PER_TICK, true, inst.prefab, true)
			end

			if sleeper.components.temperature ~= nil then
				if inst.is_cooling then
					if sleeper.components.temperature:GetCurrent() > TUNING.SLEEP_TARGET_TEMP_TENT then
						sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() - TUNING.SLEEP_TEMP_PER_TICK)
					end
				elseif sleeper.components.temperature:GetCurrent() < TUNING.SLEEP_TARGET_TEMP_TENT then
					sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() + TUNING.SLEEP_TEMP_PER_TICK)
				end
			end

			if isstarving then
				inst.components.multisleepingbag:DoWakeUp(sleeper)
			end
		end	
	end
end

local function onsleep(inst, sleeper)

end

local function moonspiderden_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    inst.entity:AddGroundCreepEntity()
    inst.GroundCreepEntity:SetRadius(TUNING.MOONSPIDERDEN.CREEPRADIUS[LARGE])

    MakeObstaclePhysics(inst, 2)

    inst.AnimState:SetBank("spider_mound_mutated")
    inst.AnimState:SetBuild("spider_mound_mutated")
    inst.AnimState:PlayAnimation("full")

    inst.MiniMapEntity:SetIcon("spidermoonden.png")

    inst:AddTag("spiderden")
	inst:AddTag("mobhome")

	inst:SetPrefabNameOverride("moondspiderden")
	
    MakeSnowCoveredPristine(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

	inst:AddComponent("multisleepingbag")
	inst.components.multisleepingbag:MustHaveTag("spider")
	inst.components.multisleepingbag.onsleep = onsleep
    inst.components.multisleepingbag.onwake = onwake
    --convert wetness delta to drying rate
    inst.components.multisleepingbag.dryingrate = math.max(0, -TUNING.SLEEP_WETNESS_PER_TICK / TUNING.SLEEP_TICK_PERIOD)
	inst.hunger_tick = TUNING.SLEEP_HUNGER_PER_TICK / 3
	inst.sleeptask = inst:DoPeriodicTask(TUNING.SLEEP_TICK_PERIOD, onsleeptick, nil, inst.components.multisleepingbag.sleepers)
	
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.MINE)
    inst.components.workable:SetMaxWork(TUNING.MOONSPIDERDEN.WORK)
    inst.components.workable:SetWorkLeft(TUNING.MOONSPIDERDEN.WORK)
    inst.components.workable:SetOnWorkCallback(spawner_onworked)
    inst.components.workable.savestate = true
    inst.components.workable:SetOnLoadFn(on_workable_load)

    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('moon_spider_holep')

    MakeHauntableWork(inst)

    start_twitch_idle(inst)

    set_stage(inst, TUNING.MOONSPIDERDEN.WORK, false)

    inst.OnEntitySleep = on_sleep
    inst.OnEntityWake = on_wake

    inst.OnSave = on_save
    inst.OnLoad = on_load

    MakeSnowCovered(inst)

    return inst
end

return Prefab("moonspiderdenp", moonspiderden_fn, assets, prefabs),
MakePlacer("moonspiderdenp_placer","spider_mound_mutated", "spider_mound_mutated", "full")
