local easing = require("easing")
local WORMBOSS_UTILS = require("prefabs/worm_boss_util")
local WBU = {}
--Current aim is to use the head as the "controller"
--Seems like the original worm is on "autopilot" so
--we have to make things more controlled.
--Hopefully we don't need to write too much new stuff,
--but we'll see.

----------------------------------------------------------------------------------------------------------------------------------------
--copied locals over incase I need them.
local STATE = {
    MOVING = 1,
    IDLE = 2,
    DEAD = 3,
    DIGESTING = 4,
}

local CHUNK_STATE ={
    EMERGE = 1,
    MOVING = 2,
    IDLE = 3,
    TRANSITION_TO_SEGMENTED = 4,
}

local DIGESTING_STATE = {
    DOING = 1,
    WAITING = 2,
}

local CHUNK_TEMPLATE = {
    state = CHUNK_STATE.MOVING,
    startpos = nil,
    segments = {},
    rotation = nil,
    groundpoint_start = nil,
    groundpoint_end = nil,
    ease = 1,
    nextseg = 0,
    segtimeMax = 1,
    segmentstotal = 0,
}

----------------------------------------------------------------------------------------------------------------------------------------

local WORM_LENGTH = 3
local DELAY_TO_MOVE_UNDERGROUND = 0.4
local SHAKE_DIST = 40
local THORN_EASE_THRESHOLD = 0.5

----------------------------------------------------------------------------------------------------------------------------------------


