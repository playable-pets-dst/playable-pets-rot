local function ondeploy(inst, pt)
    --inst.SoundEmitter:PlaySound("dontstarve/creatures/spider/spider_egg_sack")
    local tree = SpawnPrefab(home)
    if tree ~= nil then
        tree.Transform:SetPosition(pt:Get())
        inst.components.stackable:Get():Remove()
        inst:RemoveComponent("hauntable")
    end
end


local function onpickup(inst)
    --inst.SoundEmitter:PlaySound("dontstarve/creatures/spider/spider_egg_sack")
end

local function MakeHomeItem(name, bank, build, anim)
	local assets =
    {
        
    }
	
    local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("spider_egg_sac")
    inst.AnimState:SetBuild("spider_egg_sac")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("cattoy")
    --MakeDragonFlyBait(inst, 3)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst.home = name.."p"
	
    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_LARGEITEM    
    inst:AddComponent("inspectable")

    inst:AddComponent("fuel")
    inst.components.fuel.fuelvalue = TUNING.LARGE_FUEL

    MakeSmallBurnable(inst, TUNING.LARGE_BURNTIME)
    MakeSmallPropagator(inst)
    MakeHauntableLaunchAndIgnite(inst)

    inst:AddComponent("inventoryitem")
    inst:AddComponent("tradable")

	inst.components.inventoryitem.atlasname = "images/inventoryimages/pprot_homes.xml"
    inst.components.inventoryitem:SetOnPickupFn(onpickup)

    inst:AddComponent("deployable")
	
    inst.components.deployable:SetDeployMode(DEPLOYMODE.ANYWHERE)
    inst.components.deployable.ondeploy = ondeploy
	end

    return Prefab(name.."p", fn, assets),
		name.."p_placer", bank, build, anim)
end

-----------------------------------------------------------
-- 						Actual Homes
-----------------------------------------------------------

local function common_fn(bank, build, icon, tag, onbuiltfn)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 1)

    inst:AddTag("tent")
    inst:AddTag("structure")
    if tag ~= nil then
        inst:AddTag(tag)
    end

    inst.AnimState:SetBank(bank)
    inst.AnimState:SetBuild(build)
    inst.AnimState:PlayAnimation("idle", true)

    inst.MiniMapEntity:SetIcon(icon)

    MakeSnowCoveredPristine(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("lootdropper")
	
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)

    inst:AddComponent("multisleepingbag")
    inst.components.multisleepingbag.onsleep = onsleep
    inst.components.multisleepingbag.onwake = onwake
    --convert wetness delta to drying rate
    inst.components.multisleepingbag.dryingrate = math.max(0, -TUNING.SLEEP_WETNESS_PER_TICK / TUNING.SLEEP_TICK_PERIOD)

    MakeSnowCovered(inst)
    inst:ListenForEvent("onbuilt", onbuiltfn)

    MakeLargeBurnable(inst, nil, nil, true)
    MakeMediumPropagator(inst)

    inst.OnSave = OnSave 
    inst.OnLoad = OnLoad

    MakeHauntableWork(inst)

    return inst
end

local function spiderden_fn()

end

return MakeHomeItem("spidermoondenp", "spider_mound_mutated", "spider_mound_mutated", "full")
