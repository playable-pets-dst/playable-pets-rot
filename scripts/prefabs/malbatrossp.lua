local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "malbatrossp"

local assets = 
{
	Asset("ANIM", "anim/malbatross_basic.zip"),
    Asset("ANIM", "anim/malbatross_actions.zip"),    
    Asset("ANIM", "anim/malbatross_build.zip"),
}

local prefabs = 
{	
	"malbatross_ripple",
    "wave_med",
    "splash_teal",    
    "splash_green",
    "malbatross_beak",
    "mast_malbatross",
    "mast_malbatross_item",
    "malbatross_feather",
    "malbatross_feather_fall",
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.MALBATROSS_HEALTH,
	hunger = 200,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 50,
	
	runspeed = 8,
	walkspeed = 3,
	
	attackperiod = 4,
	damage = 150,
	range = TUNING.MALBATROSS_ATTACK_RANGE,
	hit_range = TUNING.MALBATROSS_ATTACK_RANGE,
	
	bank = "malbatross",
	build = "malbatross_build",
	shiny = "malbatross",
	
	scale = 1.3,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable("malbatrossp",
{
    {'meat',                                1.00},
    {'meat',                                1.00},
    {'meat',                                1.00},
    {'meat',                                1.00},
    {'meat',                                1.00},
    {'meat',                                1.00},
    {'meat',                                1.00},    
    {'malbatross_beak',                     1.00},
    {'mast_malbatross_item_blueprint',      1.00},    
    {'malbatross_feathered_weave_blueprint',1.00},        
    {'bluegem',                             1},
    {'bluegem',                             1},
    {'bluegem',                             0.3},
    {'yellowgem',                           0.05}, 
})

local FORGE_STATS = PPROT_FORGE.MALBATROSS
--==============================================
--					Mob Functions
--==============================================
local function OnHealthChange(inst,data)
    if data.newpercent <= 0.66 and not inst.willswoop then
        inst.willswoop = true
		inst.components.combat:SetRange(8)
	elseif data.newpercent >= 0.80 and inst.willswoop then
		inst.willswoop = false
		inst.components.combat:SetRange(TUNING.MALBATROSS_ATTACK_RANGE)
    end
end

local function resetdivetask(inst)
    
    if inst.divetask then
        inst.divetask:Cancel()
        inst.divetask = nil
    end
    inst.divetask = inst:DoTaskInTime(5, 
        function(inst) 
            inst.willdive = true 
            inst.divetask:Cancel()
            inst.divetask = nil
        end)
end

local function spawnwaves(inst, numWaves, totalAngle, waveSpeed, wavePrefab, initialOffset, idleTime, instantActivate, random_angle)
    SpawnAttackWaves(
        inst:GetPosition(),
        (not random_angle and inst.Transform:GetRotation()) or nil,
        initialOffset or (inst.Physics and inst.Physics:GetRadius()) or nil,
        numWaves,
        totalAngle,
        waveSpeed,
        wavePrefab,
        idleTime,
        instantActivate
    )
end

local function spawnfeather(inst,time)
	if TheNet:GetServerGameMode() ~= "lavaarena" then
    local feather = SpawnPrefab("malbatross_feather_fall")
    local pos = Vector3(inst.Transform:GetWorldPosition())
    local angle = math.random() * 2* PI
    local offset = Vector3(math.cos(angle), 0, -math.sin(angle)):Normalize() * (math.random()*2+ 1)
    pos = pos + offset
    feather.Transform:SetPosition(pos.x,pos.y,pos.z)

    if time then
        local set = time * 79/30
        feather.AnimState:SetTime( set )
    end

    feather.Transform:SetRotation(math.random()*360)
	end
end

local function OnAttacked(inst, data)
    for i=1,4 do
        if math.random() < 0.05 then    
            inst.spawnfeather(inst,0.4)        
        end
    end

    if not inst.divetask and not inst.readytodive then
        inst.resetdivetask(inst)
    end
end

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function onothercollide(inst, other)
    if not other:IsValid() or inst.recentlycharged[other] or (not other:HasTag("tree") and not other:HasTag("mast") and not other.components.health) or other == inst then
        return
    elseif other:HasTag("smashable") and other.components.health ~= nil then
        --other.Physics:SetCollides(false)
        other.components.health:Kill()
    elseif other.components.workable ~= nil
        and other.components.workable:CanBeWorked()
        and other.components.workable.action ~= ACTIONS.NET then

        if other:HasTag("mast") then
            local vx, vy, vz = inst.Physics:GetVelocity()
            local velocity = VecUtil_Length(vx, vz) * 3
            local x,y,z = inst.Transform:GetWorldPosition()
            local boat = other:GetCurrentPlatform()
            if boat then
                local boat_physics = boat.components.boatphysics 
                vx,vz = VecUtil_Normalize(vx,vz)
                boat_physics:ApplyForce(vx, vz, velocity)         
            end

            spawnfeather(inst,0.4)
            spawnfeather(inst,0.4)
            spawnfeather(inst,0.4)
            if math.random() < 0.3 then spawnfeather(inst,0.4) end
            if math.random() < 0.3 then spawnfeather(inst,0.4) end
        end

        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        other.components.workable:Destroy(inst)
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end    
    elseif other.components.health ~= nil and not other.components.health:IsDead() then
        inst.recentlycharged[other] = true
        inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        inst.components.combat:DoAttack(other)
    end
end

local function oncollide(inst, other)
    if not (other ~= nil and other:IsValid() and inst:IsValid())
        or inst.recentlycharged[other]
        then
        return
    end
    inst:DoTaskInTime(2 * FRAMES, onothercollide, other)
end
--==============================================
--				Custom Common Functions
--==============================================
local function NoHoles(pt)
    return not TheWorld.Map:IsVisualGroundAtPoint(pt.x, pt.y, pt.z)
end

local function ReticuleTargetFn(inst)
    local rotation = inst.Transform:GetRotation() * DEGREES
    local pos = inst:GetPosition()
    pos.y = 0
    for r = 13, 4, -.5 do
        local offset = FindSwimmableOffset(pos, rotation, r, 1, false, true, NoHoles)
        if offset ~= nil then
            pos.x = pos.x + offset.x
            pos.z = pos.z + offset.z
            return pos
        end
    end
    for r = 13.5, 16, .5 do
        local offset = FindSwimmableOffset(pos, rotation, r, 1, false, true, NoHoles)
        if offset ~= nil then
            pos.x = pos.x + offset.x
            pos.z = pos.z + offset.z
            return pos
        end
    end
    pos.x = pos.x + math.cos(rotation) * 13
    pos.z = pos.z - math.sin(rotation) * 13
    return pos
end

local function GetPointSpecialActions(inst, pos, useitem, right)
    if right and useitem == nil then
        local rider = inst.replica.rider
        if rider == nil or not rider:IsRiding() and not TheWorld.Map:IsVisualGroundAtPoint(pos.x,pos.y,pos.z) and not TheWorld.Map:GetPlatformAtPoint(pos.x,pos.y,pos.z) then
            return { ACTIONS.MALBATROSS_TELEPORT}
        end
    end
    return {}
end

local function OnSetOwner(inst)
    if inst.components.playeractionpicker ~= nil and TheNet:GetServerGameMode() ~= "lavaarena" then
        inst.components.playeractionpicker.pointspecialactionsfn = GetPointSpecialActions
    end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.cannoteat = data.cannoteat or nil
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.cannoteat = inst.cannoteat or nil
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function OnHitOtherForge(inst, target)
	target:PushEvent("flipped", {flipper = inst})
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.MALBATROSS)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees"})
	end)
	
	inst.acidmult = 2.5
	inst.healmult = 2
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:RemoveEventCallback("healthdelta", OnHealthChange)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
	inst.components.combat.onhitotherfn = OnHitOtherForge
	inst.willswoop = true
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("epic")
	inst:AddTag("monster")
	inst:AddTag("flying")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	MakeInventoryFloatable(inst, "large")
	
	PlayablePets.SetNightVision(inst)
	
	--inst:ListenForEvent("setowner", OnSetOwner)
	--if inst.components.playeractionpicker ~= nil then
        --inst.components.playeractionpicker.pointspecialactionsfn = GetPointSpecialActions
   -- end
	
	inst:AddComponent("reticule")
    inst.components.reticule.targetfn = ReticuleTargetFn
    inst.components.reticule.ease = true
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5, 1, 1) --fire, acid, poison
	inst.components.locomotor:EnableGroundSpeedMultiplier(false)
    inst.components.locomotor:SetTriggersCreep(false)
	inst.components.combat:SetAreaDamage(TUNING.MALBATROSS_AOE_RANGE, TUNING.MALBATROSS_AOE_SCALE)
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = true --dummy way to use flying speed
	
	inst.spawnwaves = spawnwaves
    inst.spawnfeather = spawnfeather
    inst.resetdivetask = resetdivetask

    inst.readytoswoop = true
    inst.readytosplash = true
	inst.willdive = true
    inst.willswoop = false -- changed when health is lowered.
	
	inst.recentlycharged = {}
    --inst.Physics:SetCollisionCallback(oncollide)
    inst.oncollide = oncollide
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)	
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(2,3,3) --This multiplies food stats.
	inst.components.eater.preferseating = { FOODGROUP.OMNI, FOODGROUP.TRINKET }
    inst.components.eater.caneat = { FOODGROUP.OMNI, FOODGROUP.TRINKET }
	---------------------------------
	--Physics and Shadows--
	MakeTinyFlyingCharacterPhysics(inst,1000, 1.5)
	
	inst.DynamicShadow:SetSize(6, 2)
    inst.Transform:SetSixFaced()
	
	PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("healthdelta", OnHealthChange)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)