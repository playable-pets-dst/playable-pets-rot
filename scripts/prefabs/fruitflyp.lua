local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "fruitflyp"

local assets = 
{
	Asset("ANIM", "anim/fruitfly.zip"),
    Asset("ANIM", "anim/fruitfly_evil_minion.zip"),
}

local prefabs = 
{	
	"fruitflyfruit",
    "fruitfly",
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.FRUITFLY_HEALTH,
	hunger = 125,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	
	runspeed = TUNING.FRUITFLY_WALKSPEED*2,
	walkspeed = TUNING.FRUITFLY_WALKSPEED*2,
	
	attackperiod = 0,
	damage = TUNING.FRUITFLY_DAMAGE * 2,
	range = TUNING.FRUITFLY_ATTACK_DIST,
	hit_range = TUNING.FRUITFLY_ATTACK_DIST,
	
	bank = "fruitfly",
	build = "fruitfly_evil_minion",
	shiny = "fruitfly_evil_minion",
	
	scale = 0.5,
	stategraph = "SGfruitflyp",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.

local FORGE_STATS = PPROT_FORGE.FRUITFLYP
--==============================================
--					Mob Functions
--==============================================
local sounds = {
    flap = "farming/creatures/minion_fruitfly/LP",
    hurt = "farming/creatures/minion_fruitfly/hit",
    attack = "farming/creatures/minion_fruitfly/attack",
    die = "farming/creatures/minion_fruitfly/die",
    die_ground = "farming/creatures/minion_fruitfly/hit",
    sleep = "farming/creatures/minion_fruitfly/sleep",
    buzz = "farming/creatures/minion_fruitfly/hit",
    spin = "farming/creatures/minion_fruitfly/spin",
    plant_attack = "farming/creatures/minion_fruitfly/plant_attack"
}

local PLANT_DEFS = require("prefabs/farm_plant_defs").PLANT_DEFS
require "prefabs/veggies"
local function pickseed()
    local season = TheWorld.state.season
    local weights = {}
    local season_mod = TUNING.SEED_WEIGHT_SEASON_MOD

    for k, v in pairs(VEGGIES) do
        weights[k] = v.seed_weight * ((PLANT_DEFS[k] and PLANT_DEFS[k].good_seasons[season]) and season_mod or 1)
    end

    return weighted_random_choice(weights).."_seeds"
end

local function LordLootSetupFunction(lootdropper)
    lootdropper.chanceloot = nil
    for i = 1, 2 do
        lootdropper:AddChanceLoot(pickseed(), 1.0)
        lootdropper:AddChanceLoot(pickseed(), 0.25)
    end
end

local function NumFruitFliesToSpawn(inst)
    local numFruitFlies = TUNING.LORDFRUITFLY_FRUITFLY_AMOUNT
    local numFollowers = inst.components.leader:CountFollowers()
    
    local num = math.min(numFollowers+numFruitFlies/2, numFruitFlies) -- only spawn half the fruit flies per buzz
    num = (math.log(num)/0.4)+1 -- 0.4 is approx log(1.5)
    num = RoundToNearest(num, 1)

    return num - numFollowers
end
--==============================================
--				Custom Common Functions
--==============================================

local function SetRotPref(inst, stale, spoiled)
	if stale then
		inst.components.eater.stale_health = stale
		inst.components.eater.stale_hunger = stale
		inst.components.eater.stale_sanity = stale
	end
	if spoiled then
		inst.components.eater.spoiled_health = spoiled
		inst.components.eater.spoiled_hunger = spoiled
		inst.components.eater.spoiled_sanity = spoiled
	end
end

local function OnEat(inst, food)
	if food:HasTag("show_spoiled") then
		inst.components.health:DoDelta(2, false)
		inst.components.hunger:DoDelta(10, false)
		inst.components.sanity:DoDelta(3, false)
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "melees"})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 0.5
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
	inst:AddTag("flying")
    inst:AddTag("fruitfly")
    inst:AddTag("hostile")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst)
	--Stats--
	inst.sounds = sounds
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 3, 3, 3) --fire, acid, poison
	inst.components.locomotor:EnableGroundSpeedMultiplier(false)
    inst.components.locomotor:SetTriggersCreep(false)
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = true
	
	
	local body_symbol = "fruit2"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, "lordfruitfly") --inst, prefaboverride
    inst.components.lootdropper:SetLootSetupFn(LordLootSetupFunction)
    LordLootSetupFunction(inst.components.lootdropper)
	
	--inst:AddComponent("sanityaura")
    --inst.components.sanityaura.aura = -TUNING.SANITYAURA_SMALL
	----------------------------------
	--Eater--
	--inst.components.eater:SetDiet({ FOODTYPE.VEGGIE }, { FOODTYPE.VEGGIE })
    inst.components.eater:SetAbsorptionModifiers(0.5,0.5,0.5) --Gets less benefits from fresh food, prefers stale/rotting food.
	SetRotPref(inst, 2, 2.5)
	inst.components.eater:SetOnEatFn(OnEat)
	---------------------------------
	--Physics and Shadows--
	MakeGhostPhysics(inst, 1, 0.5)
	inst.DynamicShadow:SetSize(2, 0.75)
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)