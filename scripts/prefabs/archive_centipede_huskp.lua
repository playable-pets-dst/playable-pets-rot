local assets =
{

}

local prefabs =
{

}


local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
		inst.owner = data.owner or nil --for husk
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
	data.owner = inst.owner or nil
end

--Custom Husk
local MED_THRESHOLD_DOWN = 0.66
local LOW_THRESHOLD_DOWN = 0.33

local MED_THRESHOLD_UP = 0.66 -- 0.75
local LOW_THRESHOLD_UP = 0.33 --0.50
local BOTTOM_THRESHOLD = 0.2 --0.50

local function playidle2(inst)
    if inst.AnimState:IsCurrentAnimation("idle_full") or
       inst.AnimState:IsCurrentAnimation("idle_med") or
       inst.AnimState:IsCurrentAnimation("idle_low") then    
        if inst.components.health:GetPercent() < LOW_THRESHOLD_DOWN then
            inst.AnimState:PlayAnimation("idle2_low")
            inst.AnimState:PushAnimation("idle_low")
        elseif inst.components.health:GetPercent() < MED_THRESHOLD_DOWN then
            inst.AnimState:PlayAnimation("idle2_med")
            inst.AnimState:PushAnimation("idle_med")
        else
            inst.AnimState:PlayAnimation("idle2_full")
            inst.AnimState:PushAnimation("idle_full")
        end
    end
end

local function onpossess(inst, data)
    if data.possesser and data.possesser:HasTag("power_point") then
        data.possesser:Remove()
        local x,y,z = inst.Transform:GetWorldPosition()
        local centipede = SpawnPrefab("archive_centipede")
        centipede.Transform:SetPosition(x,y,z)
        centipede.sg:GoToState("spawn")
        if inst.idle2task then
            inst.idle2task:Cancel()
            inst.idle2task = nil
        end        
        inst:Remove()
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 1)

    inst.AnimState:SetBank("archive_centipede")
    inst.AnimState:SetBuild("archive_centipede_build")
    inst.AnimState:PlayAnimation("idle_full")
	
	--inst.nameoverride = "archive_centipede_husk"

    inst:AddTag("security_powerpoint")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    PlayablePets.SetCommonLootdropper(inst, "archive_centipedep") --inst, prefaboverride
	inst:AddComponent("named")
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.ARCHIVE_CENTIPEDE.HUSK_HEALTH)

    inst.possessable = true

    inst.MED_THRESHOLD_DOWN = MED_THRESHOLD_DOWN

    inst:AddComponent("inspectable")

    inst:AddComponent("combat")

    inst:ListenForEvent("possess", onpossess)
    inst:ListenForEvent("healthdelta", playidle2)
	inst:ListenForEvent("death", function(inst) inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition())) end)
	
	
    MakeHauntableWork(inst)
	
	inst:DoTaskInTime(1, function(inst)
		if inst.owner then
			inst.components.named:SetName(inst.owner)
		end
		if inst.isshiny and inst.isshiny ~= 0 then
			inst.AnimState:SetBuild("archive_centipede_shiny_build_0"..inst.isshiny)
		end
	end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    return inst
end

return Prefab("archive_centipede_huskp", fn, prefabs, assets)
