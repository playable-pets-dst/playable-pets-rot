local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "crabkingp"

local assets = 
{
	Asset("ANIM", "anim/crab_king_basic.zip"),
    Asset("ANIM", "anim/crab_king_actions.zip"),    
    Asset("ANIM", "anim/crab_king_build.zip"),
	
	Asset("ANIM", "anim/crabking_rockchip.zip"),  
}

local prefabs =
{
    "crabking_geyserspawnerp",
    "crabking_clawp",
    "crab_king_shine",
    "crabking_freezep",
    "crab_king_shine_orange",
    "crabking_ring_fx",
    "crabking_chip_high",
    "crabking_chip_med",
    "crabking_chip_low",
    "crabking_yellow_whirl_front",
    "crabking_yellow_whirl_back",
    "moon_altar_cosmic",
    "hermit_cracked_pearl",
	"chesspiece_crabking_sketch",
	"trident_blueprint",
	"meat",
	"singingshell_octave5",
	"singingshell_octave4",
	"singingshell_octave3",
}

local geyserprefabs =
{
    "crab_king_bubble1",
    "crab_king_bubble2",
    "crab_king_bubble3",
    "crab_king_waterspout",
}

local freezeprefabs =
{
    "mushroomsprout_glow",
    "crab_king_icefx",
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end

local MOB_HEALTH = BOSS_STATS and TUNING.CRABKING_HEALTH or  7000
-----------------------
--Stats--
local mob = 
{
	health = MOB_HEALTH,
	hunger = 300,
	hungerrate = TUNING.WILSON_HUNGER_RATE/2, 
	sanity = 130,
	
	runspeed = 3,
	walkspeed = 3 ,
	
	attackperiod = TUNING.CRABKING_ATTACK_PERIOD,
	damage = 50 * 2, --it attacks 3 times
	range = 25,
	hit_range = 25,
	
	bank = "king_crab",
	build = "crab_king_build",
	shiny = "crab_king",
	
	scale = 0.7,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('crabkingp',
{
	{"chesspiece_crabking_sketch",			1.00},
    {"trident_blueprint",                   1.00},
    {'meat',                                1.00},
    {'meat',                                1.00},
    {'meat',                                1.00},
    {'meat',                                1.00},
    {'meat',                                1.00},
    {'meat',                                1.00},
    {'meat',                                1.00},
    {"singingshell_octave5",                1.00},
    {"singingshell_octave5",                1.00},
    {"singingshell_octave5",                1.00},
    {"singingshell_octave5",                1.00},
    {"singingshell_octave5",                0.50},
    {"singingshell_octave5",                0.25},
    {"singingshell_octave4",                1.00},
    {"singingshell_octave4",                1.00},
    {"singingshell_octave4",                1.00},
    {"singingshell_octave4",                0.50},
    {"singingshell_octave4",                0.25},
    {"singingshell_octave3",                1.00},
    {"singingshell_octave3",                1.00},
    {"singingshell_octave3",                0.50},
})

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "crabking", "crabking_claw"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "crabking", "crabking_claw"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "crabking", "crabking_claw"}
	end
end

--==============================================
--					Mob Functions
--==============================================
local function getfreezerange(inst)
    return 5 + (inst.countgems(inst).blue)
end

local TARGET_DIST = 16

local MAX_SOCKETS = 9

local ARMTIME = {
    0,
    0.25,
    0.1,
    0.2,
    0.15,
    0.3,
    0,
    0.25,
    0.1,
    0.2,
    0.15,
    0.3,
    0,
    0.25,
    0.1,
}

local function removecrab(inst) 
    inst:RemoveEventCallback("onremove", function() removecrab( inst ) end, inst.crab)
    inst.crab = nil
    inst:Remove()
end

local function RemoveDecor(inst, data)
    inst.AnimState:ClearOverrideSymbol("gems_blue")    
end

local function AddDecor(inst, data)
    if data == nil or data.slot == nil or data.itemprefab == nil then
        return
    end
    local symbol = "gems_blue"
    if data.itemprefab == "redgem" then
        symbol = "gems_red"
    elseif data.itemprefab == "purplegem" then
        symbol = "gems_purple"
    elseif data.itemprefab == "orangegem" then
        symbol = "gems_orange"
    elseif data.itemprefab == "yellowgem" then
        symbol = "gems_yellow"
    elseif data.itemprefab == "greengem" then
        symbol = "gems_green"
    elseif data.itemprefab == "opalpreciousgem" then
        symbol = "gems_opal"
    elseif data.itemprefab == "hermit_pearl" then
        symbol = "hermit_pearl"
    end
    
    inst.AnimState:OverrideSymbol("gem"..data.slot, "crab_king_build", symbol)
    inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/gem_place")
    inst.shinefx = SpawnPrefab("crab_king_shine")
    inst.shinefx.entity:AddFollower()
    --inst.shinefx.entity:SetParent(inst)
    inst.shinefx.Follower:FollowSymbol(inst.GUID, "gem"..data.slot, 0, 0, 0)
    inst:PushEvent("socket")
end

local function clearsocketart(inst)
    inst.AnimState:ClearOverrideSymbol("gems_blue")
    for i=1,9 do
        inst.AnimState:ClearOverrideSymbol("gem"..i)
    end
end

local function socketitem(inst,item,slot)
    -- find open slot.     
    local socketnum = slot 
    if slot then
        for i=#inst.socketlist,1 do
            if inst.socketlist[i] == slot then
                table.remove(inst.socketlist,i)
                break
            end
        end
    else
        if #inst.socketlist <=0 or item.prefab == "hermit_pearl" then
            socketnum = 5
        else
            local idx = math.random(1,#inst.socketlist)
            socketnum = inst.socketlist[idx]
            table.remove(inst.socketlist,idx)
        end
    end
    local data = {slot = socketnum, itemprefab = item.prefab}
    table.insert(inst.socketed,data)
    AddDecor(inst, data)    
    item:RemoveTag("irreplaceable")
	
	local health = inst.components.health:GetPercent()
    inst.components.health:SetMaxHealth(mob.health + ((inst.countgems(inst).red) * (BOSS_STATS and 2000 or 500)))
    inst.components.health:SetPercent(health)
	
	inst.components.combat:SetDefaultDamage(mob.damage + (10 * inst.countgems(inst).purple/2))
	
	inst.components.locomotor.runspeed = mob.runspeed + (inst.countgems(inst).orange * 0.5)
end

local function doshine(inst,slot)    
    inst.shinefx = SpawnPrefab("crab_king_shine")
    inst.shinefx.entity:AddFollower()
    --inst.shinefx.entity:SetParent(inst)
    inst.shinefx.Follower:FollowSymbol(inst.GUID, "gem"..slot, 0, 0, 0)
end

local function gemshine(inst,color)
    if inst.socketed then
        local t = 0
        for i,data in ipairs(inst.socketed)do
        
            if (data.itemprefab == "bluegem" and color == "blue") or
               (data.itemprefab == "redgem" and color == "red") or
               (data.itemprefab == "purplegem" and color == "purple") or
               (data.itemprefab == "orangegem" and color == "orange") or
               (data.itemprefab == "yellowgem" and color == "yellow") or
               (data.itemprefab == "greengem" and color == "green") or
               (data.itemprefab == "opalpreciousgem" or data.itemprefab == "hermit_pearl") then
                inst:DoTaskInTime( t*0.15 ,function()
                    doshine(inst,data.slot)
                end)
                t = t+1
            end
        end
    end
end

local function OnAttacked(inst, data)
    if data.attacker then
        local x, y, z = inst.Transform:GetWorldPosition()
        local x1, y1, z1 = data.attacker.Transform:GetWorldPosition()
        local r = data.attacker:GetPhysicsRadius(.5)
        r = r / (r + 1)
        SpawnPrefab("mining_fx").Transform:SetPosition(x1 + (x - x1) * r, 0, z1 + (z - z1) * r)
    end
end

local function startcastspell(inst, freeze)
    if freeze then
        local x,y,z = inst.Transform:GetWorldPosition()
        local fx = SpawnPrefab("crabking_freezep")
        fx.crab = inst
        fx:ListenForEvent("onremove", function() removecrab(fx) end, inst)
        fx.Transform:SetPosition(x,y,z)
        local scale = 0.75 + inst.countgems(inst).blue
        fx.Transform:SetScale(scale,scale,scale)
    else
        local x,y,z = inst.Transform:GetWorldPosition()
        local ents = TheSim:FindEntities(x,y,z, 20, nil, GetExcludeTags(inst), {"boat", "hostile", "monster", "character", "oceanfish", "animal", "flying"})
        if #ents >0 then
            for i,boat in pairs(ents)do
                -- find position around boat and spawn the attack prefab
				if boat:HasTag("boat") then
					
					local theta = math.random()*2*PI
					local radius = 6
					local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))
					local boatpt = Vector3(boat.Transform:GetWorldPosition())
					local fx = SpawnPrefab("crabking_geyserspawnerp")
					fx.crab = inst
					fx:ListenForEvent("onremove", function() removecrab(fx) end, inst)
					fx.Transform:SetPosition(boatpt.x,boatpt.y,boatpt.z)
					fx.dogeyserburbletask(fx)
				elseif not boat:HasTag("boat") then
					local pos = boat:GetPosition()
					if boat.components.health and boat.components.combat and not boat.components.health:IsDead() and not TheWorld.Map:GetPlatformAtPoint(pos.x, pos.z) then
						local theta = math.random()*2*PI
						local radius = 6
						local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))
						local boatpt = Vector3(boat.Transform:GetWorldPosition())
						local fx = SpawnPrefab("crabking_geyserspawnerp")
						fx.crab = inst
						fx:ListenForEvent("onremove", function() removecrab(fx) end, inst)
						fx.Transform:SetPosition(boatpt.x,boatpt.y,boatpt.z)
						fx.dogeyserburbletask(fx)					
					end
				end
            end
        end
    end
end

local function endcastspell(inst, lastwasfreeze)
    inst.dofreezecast = nil
    inst.wantstocast = nil
    
    local x,y,z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x,y,z, getfreezerange(inst), nil, nil,{"crabking_spellgenerator"})
    if #ents > 0 then
        for i,ent in pairs(ents)do
            if not inst.components.freezable:IsFrozen() then
                ent:PushEvent("endspell")
            else
                ent:Remove()
            end
        end
    end    
    if lastwasfreeze then
        inst.gemshine(inst,"blue")
    else
        inst.gemshine(inst,"purple")
    end
end

local function spawnstacks(inst)
    local pos = Vector3(inst.Transform:GetWorldPosition())
    local stacks =  math.max(0,TUNING.CRABKING_STACKS - #TheSim:FindEntities(pos.x,0,pos.z, 20,{"seastack"}))    
    local pos = Vector3(inst.Transform:GetWorldPosition())
    if stacks > 0 then
        for i=1,stacks do
            local theta = math.random()*2*PI
            local radius = 9+ (math.pow(math.random(),0.8)* (17-9) )
            local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))
            if not TheWorld.Map:GetPlatformAtPoint(pos.x+offset.x, pos.z+offset.z) and #TheSim:FindEntities(pos.x+offset.x,0,pos.z+offset.z, 3,{"seastack"}) <= 0 then
                inst:DoTaskInTime(math.random()*0.5,function()
                    local stack = SpawnPrefab("seastack")
                    stack.Transform:SetPosition(pos.x+offset.x,0,pos.z+offset.z)
                    stack.AnimState:PlayAnimation(stack.stackid.."_emerge")
                    stack.AnimState:PushAnimation(stack.stackid.."_full")
                    SpawnPrefab("splash_green_large").Transform:SetPosition(pos.x+offset.x,0,pos.z+offset.z)   
                end)
            end
        end
    end
end

local function removearm(inst,armpos)
    
end

local function spawnarm(inst,armpos, fx)

    local clawsnum = TUNING.CRABKING_BASE_CLAWS + (math.floor(inst.countgems(inst).green/2))

    local theta = armpos*(2*PI/clawsnum )
    local radius = 8
    local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))

    local pos = Vector3(inst.Transform:GetWorldPosition()) + offset
    local boat =  TheWorld.Map:GetPlatformAtPoint(pos.x, pos.z)
    local tries = 0
    while boat and tries < 5 do
           
        if boat then
            pos = Vector3(boat.Transform:GetWorldPosition())
            local theta = math.random()*2*PI
            local radius = 5
            local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))
            pos = pos + offset
        end
        boat =  TheWorld.Map:GetPlatformAtPoint(pos.x, pos.z)
        tries = tries + 1
    end
    if not boat then
        local arm = SpawnPrefab("crabking_clawp") 
        arm.Transform:SetPosition(pos.x, 0, pos.z)
		arm.crabking = inst
        arm.armpos = armpos
        inst.arms[armpos] = arm    
        local health = TUNING.CRABKING_CLAW_HEALTH  -- + (math.ceil(inst.countgems(inst).green/2)* TUNING.CRABKING_CLAW_HEALTH_BOOST )
        arm.components.health:SetMaxHealth(health)
        arm.components.health:SetCurrentHealth(health)
        arm:PushEvent("emerge")
		if TheNet:GetServerGameMode() ~= "lavaarena" and TheWorld.Map:IsVisualGroundAtPoint(arm:GetPosition():Get()) then
			inst.arms[armpos] = nil
			arm:Remove()		
		end
    end
    if fx then
        inst.gemshine(inst,"green")
    end
end

local function spawnarms(inst)
    local clawsnum = TUNING.CRABKING_BASE_CLAWS + (math.floor(inst.countgems(inst).green/2))

	if not inst.arms then
		inst.arms = {}
	end
	if inst.arms and #inst.arms <= 0 then
		for i=1,clawsnum do
			if not inst.arms[i] or not inst.arms[i].prefab or not inst.arms[i]:IsValid() then
				inst:DoTaskInTime(ARMTIME[i],function() inst.spawnarm(inst,i)end)
			end
		end
	end
end

local function regenarm(inst)
    if inst.arms then
        for i, arm in ipairs(inst.arms) do
            if not arm.prefab or not arm:IsValid() and not inst.components.timer:TimerExists("claw_regen_delay"..i) then
                inst.spawnarm(inst,i,true)
                break
            end
        end
        inst.components.timer:StartTimer("claw_regen_timer",TUNING.CRABKING_CLAW_REGEN_DELAY)
    end
end

local function finishfixing(inst)
    --print("FINISHED FIXING. STARTING HEAL COOLDOWN")
    inst.wantstoheal = nil
    inst.components.timer:StartTimer("heal_cooldown",TUNING.CRABKING_HEAL_DELAY)
    if inst.components.timer:TimerExists("claw_regen_timer") then
        inst.components.timer:StopTimer("claw_regen_timer")
    end
end

local function dropgems(inst)
    for i, socket in ipairs (inst.socketed) do
        local gem = SpawnPrefab(socket.itemprefab)
        inst.components.lootdropper:FlingItem(gem)
    end
    inst.socketed = {}
    inst.socketlist = {1,2,3,4,6,7,8,9}

    clearsocketart(inst)
	
	if not inst.components.health:IsDead() then
		local health = inst.components.health:GetPercent()
		inst.components.health:SetMaxHealth(mob.health)
		inst.components.health:SetPercent(health)
	end
	inst.components.combat:SetDefaultDamage(mob.damage)
	inst.components.locomotor.runspeed = mob.runspeed
end

local function removegem(inst,gemname)
    for i=#inst.socketed,1, -1 do    
        if inst.socketed[i].itemprefab == gemname then
            table.remove(inst.socketed,i)
        end
    end
    --print("REMOVE GEM",gemname)
    dumptable(inst.socketed)
end
local function addgem(inst,gemname)
    table.insert(inst.socketed,{itemprefab = gemname})
end

local function countgems(inst)

    local gems = {
        red = 0,
        blue = 0,
        purple = 0,
        orange = 0,
        yellow = 0,
        green = 0,
        pearl = 0,
    }

    if inst.socketed then
        for i,data in ipairs(inst.socketed)do
            if data.itemprefab == "bluegem" then
                gems.blue = gems.blue + 1
            elseif data.itemprefab == "redgem" then
                gems.red = gems.red + 1
            elseif data.itemprefab == "purplegem" then
                gems.purple = gems.purple + 1
            elseif data.itemprefab == "orangegem" then
                gems.orange = gems.orange + 1
            elseif data.itemprefab == "yellowgem" then
                gems.yellow = gems.yellow + 1
            elseif data.itemprefab == "greengem" then
                gems.green = gems.green + 1
            elseif data.itemprefab == "opalpreciousgem" then
                gems.green =  gems.green + 1
                gems.yellow = gems.yellow + 1
                gems.orange = gems.orange + 1
                gems.red =    gems.red + 1
                gems.blue =   gems.blue + 1
                gems.purple = gems.purple + 1
            elseif data.itemprefab == "hermit_pearl" then
                gems.green =  gems.green + 3
                gems.yellow = gems.yellow + 3
                gems.orange = gems.orange + 3
                gems.red =    gems.red + 3
                gems.blue =   gems.blue + 3
                gems.purple = gems.purple + 3
                gems.pearl = gems.pearl +1
            end
        end
    end

    return gems
end

local function countarms(inst)
    local count = 0
    if inst.arms then
        for i,arm in ipairs(inst.arms)do
            if arm.prefab and arm:IsValid() then
                count = count+1
            end
        end
    end
    return count
end

local function spawnsparkle(inst,symbol)
    inst.shinefx = SpawnPrefab("crab_king_shine_orange")
    inst.shinefx.entity:AddFollower()
    inst.shinefx.Follower:FollowSymbol(inst.GUID, symbol, 0, 0, 0)
end

local function spawnchunk(inst,prefab,pos)
    print("DAMAGE CHUNK POS",pos.x,pos.y,pos.z)
    local chip = SpawnPrefab(prefab)
    if chip and pos then
        print("pOS:",pos.x,pos.y,pos.z, "inst:",inst.Transform:GetWorldPosition())
        local pos = Vector3(inst.Transform:GetWorldPosition())
        chip.Transform:SetPosition(pos.x,0,pos.z)
    end
    return chip
end

local function setdamageart(inst)
    local index = math.random(1,#inst.nondamagedsymbollist)
    local art = inst.nondamagedsymbollist[index]
    table.remove(inst.nondamagedsymbollist,index)
    table.insert(inst.damagedsymbollist,art)

    local fx = SpawnPrefab("round_puff_fx_lg")
    fx.entity:AddFollower()
    fx.entity:SetParent(inst.entity)
    fx.Follower:FollowSymbol(inst.GUID, "damage"..art, 0, 0, 0)

    local pos = Vector3(inst.AnimState:GetSymbolPosition("damage"..art,0,0,0))  
    if art == 7 or art == 8 then
       inst.spawnchunk(inst,"crabking_chip_high",pos)
    elseif art == 1 or art == 3 or art == 5 or art == 6 or art == 9 or art == 10 then
        inst.spawnchunk(inst,"crabking_chip_med",pos)
    elseif art == 2 or art == 4 then
        inst.spawnchunk(inst,"crabking_chip_low",pos)
    end
    inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/rock_hit")
    inst.AnimState:OverrideSymbol("damage"..art, "crab_king_build", "nil")
    --print("CRABKING: BREAK!")
end

local function setrepairedart(inst)
    local index = math.random(1,#inst.damagedsymbollist)
    local art = inst.damagedsymbollist[index]
    table.remove(inst.damagedsymbollist,index)
    table.insert(inst.nondamagedsymbollist,art)
    inst.AnimState:OverrideSymbol("damage"..art, "crab_king_build", "damage"..art)
    --print("CRABKING: FIX!")
end

local function onHealthChange(inst,data)
    local current = data.oldpercent 

    local done = nil
    while data.newpercent and not done do
        if data.oldpercent > data.newpercent then
            current = math.max(current - 0.1,data.newpercent)
        else 
            current = math.min(current + 0.1,data.newpercent)
        end        
        if (current <= 0.9 and current > 0.8 and #inst.nondamagedsymbollist >= 10) or
           (current <= 0.8 and current > 0.7 and #inst.nondamagedsymbollist >= 9) or
           (current <= 0.7 and current > 0.6 and #inst.nondamagedsymbollist >= 8) or
           (current <= 0.6 and current > 0.5 and #inst.nondamagedsymbollist >= 7) or
           (current <= 0.5 and current > 0.4 and #inst.nondamagedsymbollist >= 6) or
           (current <= 0.4 and current > 0.3 and #inst.nondamagedsymbollist >= 5) or
           (current <= 0.3 and current > 0.2 and #inst.nondamagedsymbollist >= 4) or
           (current <= 0.2 and current > 0.1 and #inst.nondamagedsymbollist >= 3) or
           (current <= 0.1 and current > 0.0 and #inst.nondamagedsymbollist >= 2) or
           (current <= 0.0                   and #inst.nondamagedsymbollist >= 1) then

            setdamageart(inst)
        end
      
        if (current >= 1.0                   and #inst.nondamagedsymbollist < 10) or
           (current >= 0.9 and current < 1.0 and #inst.nondamagedsymbollist < 9) or
           (current >= 0.8 and current < 0.9 and #inst.nondamagedsymbollist < 8) or
           (current >= 0.7 and current < 0.8 and #inst.nondamagedsymbollist < 7) or
           (current >= 0.6 and current < 0.7 and #inst.nondamagedsymbollist < 6) or
           (current >= 0.5 and current < 0.6 and #inst.nondamagedsymbollist < 5) or
           (current >= 0.4 and current < 0.5 and #inst.nondamagedsymbollist < 4) or
           (current >= 0.3 and current < 0.4 and #inst.nondamagedsymbollist < 3) or
           (current >= 0.2 and current < 0.3 and #inst.nondamagedsymbollist < 2) or
           (current >= 0.1 and current < 0.2 and #inst.nondamagedsymbollist < 1) then
           
            setrepairedart(inst)
        end      
        if current == data.newpercent then
            done = true
        end
    end
end


--==============================================
--				Custom Common Functions
--==============================================
local function OnEat(inst, food)
	if food:HasTag("gem") and #inst.socketed < MAX_SOCKETS then
		if food.prefab ~= "hermit_pearl" or (food.prefab == "hermit_pearl" and inst.countgems(inst).pearl <= 0) then
			inst.sg:GoToState("socket", food)
		end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0		
	end
	
	clearsocketart(inst)
    -- reset sockets
    if data then
        inst.socketlist = data.socketlist
        if data.socketed then
            for k,v in ipairs(data.socketed) do
                local gem = SpawnPrefab(v)
                socketitem(inst,gem,data.socketedslot[k])
				gem:DoTaskInTime(0, function(inst) gem:Remove() end)
            end
        end
    end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	local ents = {}
    data.socketlist = inst.socketlist

    data.socketed = {}
    data.socketedslot = {}
    if #inst.socketed > 0 then
        for k,v in ipairs(inst.socketed)do
            table.insert(data.socketed,v.itemprefab)
            table.insert(data.socketedslot,v.slot)
        end
    end

    if inst.arms then
        data.arms = {}
        for i,arm in pairs(inst.arms)do
            if arm.prefab then
                data.arms[i] = arm.GUID
                table.insert(ents, arm.GUID)
            end
        end
    end
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.GNARWAIL)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("epic")
	inst:AddTag("crabking")
	inst:AddTag("monster")
	----------------------------------
	AddDefaultRippleSymbols(inst, true, false)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local function RemoveArms(inst)
	if inst.arms and #inst.arms > 0 then
		for i, v in ipairs(inst.arms) do
			if v.boat and v.boat.components.debuffable then
				v.boat.components.debuffable:RemoveDebuff("crabkingclaw_hold_debuff")
				v.boat.heldclaws = nil
			end
			v.sg:GoToState("submerge")
		end
	end
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	--inst.components.combat:SetAreaDamage(TUNING.CRABKING_AOE_RANGE, TUNING.CRABKING_AOE_SCALE)
	PlayablePets.SetCommonStatResistances(inst, 0) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.cansummonclaws = true
	inst.getskins = getskins
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	inst:AddComponent("timer")
	----------------------------------
	--Eater--	
	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.VEGGIE, FOODTYPE.ELEMENTAL}, { FOODTYPE.MEAT, FOODTYPE.VEGGIE, FOODTYPE.ELEMENTAL })
    inst.components.eater:SetAbsorptionModifiers(2,2,2) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater:SetOnEatFn(OnEat)
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1000, 3.4)
	inst.Transform:SetNoFaced()
	
	inst.DynamicShadow:SetSize(1, 3.4)
	inst.DynamicShadow:Enable(false)  
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank, false, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("healthdelta", onHealthChange)
	inst:ListenForEvent("onremove", RemoveArms)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) 
			inst.components.health:SetMaxHealth(mob.health)
			inst.components.locomotor.runspeed = mob.runspeed
		end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end)
    end)
	
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	inst.socketlist = {1,2,3,4,6,7,8,9}
    inst.damagedsymbollist = {}
    inst.nondamagedsymbollist = {1,2,3,4,5,6,7,8,9,10}
    inst.socketed = {}
    inst.startcastspell = startcastspell
    inst.endcastspell = endcastspell
    inst.spawnarms = spawnarms
    inst.regenarm = regenarm
    inst.spawnarm = spawnarm
    inst.spawnstacks = spawnstacks
    inst.countarms = countarms
    inst.countgems = countgems
    inst.dropgems = dropgems
    inst.finishfixing = finishfixing
    inst.gemshine = gemshine
    inst.spawnchunk = spawnchunk
    inst.removegem = removegem
    inst.addgem = addgem
	inst.socketitem = socketitem

    clearsocketart(inst)
	
    return inst	
end

--========================================
-- GEYSER 
--========================================

local function dogeyserburbletask(inst)
    if inst.burbletask then
        inst.burbletask:Cancel()
        inst.burbletask = nil
    end
    local totalcasttime = TUNING.CRABKING_CAST_TIME - (inst.crab and inst.crab:IsValid() and math.floor(inst.crab.countgems(inst.crab).yellow or 0)/2)
    local time = Remap(inst.components.age:GetAge(),0,totalcasttime,0.2,0.01)
    inst.burbletask = inst:DoTaskInTime(time,function() inst.burble(inst) end) -- 0.01+ math.random()*0.1
end

local function burble(inst)
    local MAXRADIUS = 6
    local x,y,z = inst.Transform:GetWorldPosition()
    local theta = math.random()*2*PI
    local radius = math.pow(math.random(),0.8)* MAXRADIUS
    local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))   
    local prefab = "crab_king_bubble"..math.random(1,3)

    if TheWorld.Map:IsOceanAtPoint(x+offset.x, 0, z+offset.z) then
        local fx = SpawnPrefab(prefab)
        fx.Transform:SetPosition(x+offset.x,y+offset.y,z+offset.z)  
    elseif TheWorld.Map:GetPlatformAtPoint(x+offset.x, z+offset.z) then
        local boat = TheWorld.Map:GetPlatformAtPoint(x+offset.x, z+offset.z)
        if boat then
            ShakeAllCameras(CAMERASHAKE.VERTICAL, 0.1, 0.01, 0.3, boat, boat:GetPhysicsRadius(4))
        end
    end

    dogeyserburbletask(inst)
end

local function endgeyser(inst)
    inst:DoTaskInTime(2.4,function()
        if inst.burbletask then
            inst.burbletask:Cancel()
            inst.burbletask = nil
        end
    end)
    local extrageysers = 0
    if inst.crab and inst.crab:IsValid() then
        extrageysers = math.floor(inst.crab.countgems(inst.crab).purple/2)
    end
    for i=1,TUNING.CRABKING_DEADLY_GEYSERS + extrageysers do
        inst:DoTaskInTime(math.random()*0.4,function()
            local MAXRADIUS = 4.5
            local x,y,z = inst.Transform:GetWorldPosition()
            local theta = math.random()*2*PI
            local radius = math.pow(math.random(),0.8)* MAXRADIUS
            local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))   
            local prefab = "crab_king_waterspout"
            if TheWorld.Map:IsOceanAtPoint(x+offset.x, 0, z+offset.z) then
                local fx = SpawnPrefab(prefab)
                fx.Transform:SetPosition(x+offset.x,y+offset.y,z+offset.z)                 

                local INITIAL_LAUNCH_HEIGHT = 0.1
                local SPEED = 8
                local CANT_HAVE_TAGS = {"INLIMBO", "outofreach", "DECOR"}
                local function launch_away(inst, position)
                    local ix, iy, iz = inst.Transform:GetWorldPosition()
                    inst.Physics:Teleport(ix, iy + INITIAL_LAUNCH_HEIGHT, iz)

                    local px, py, pz = position:Get()
                    local angle = (180 - inst:GetAngleToPoint(px, py, pz)) * DEGREES
                    local sina, cosa = math.sin(angle), math.cos(angle)
                    inst.Physics:SetVel(SPEED * cosa, 4 + SPEED, SPEED * sina)
                end                
                local affected_entities = TheSim:FindEntities(x,0,z, 6, {"_combat"}, GetExcludeTags(inst))
                for _, v in ipairs(affected_entities) do
                    if v.components.oceanfishable ~= nil then
                        -- Launch fishable things because why not.
                        local projectile = v.components.oceanfishable:MakeProjectile()
                        local position = Vector3(x+offset.x,y+offset.y,z+offset.z) 
                        if projectile.components.complexprojectile then
                            projectile.components.complexprojectile:SetHorizontalSpeed(16)
                            projectile.components.complexprojectile:SetGravity(-30)
                            projectile.components.complexprojectile:SetLaunchOffset(Vector3(0, 0.5, 0))
                            projectile.components.complexprojectile:SetTargetOffset(Vector3(0, 0.5, 0))

                            local v_position = v:GetPosition()
                            local launch_position = v_position + (v_position - position):Normalize() * SPEED
                            projectile.components.complexprojectile:Launch(launch_position, projectile)
                        else
                            launch_away(projectile, position)
                        end
					elseif v.components.combat and v.components.health and not v.components.health:IsDead() then
						local pos = v:GetPosition()
						if not TheWorld.Map:GetPlatformAtPoint(pos.x, pos.z) and v ~= inst.crab then
							inst.crab.components.combat:DoAttack(v)
						end
                    end
                end
            else
                local boat = TheWorld.Map:GetPlatformAtPoint(x+offset.x, z+offset.z)
                if boat then
                    local pt = Vector3(x+offset.x,0,z+offset.z)
                    boat.components.health:DoDelta(-TUNING.CRABKING_GEYSER_BOATDAMAGE)

                    -- look for patches
                    local nearpatch = TheSim:FindEntities(pt.x,0,pt.z, 2,{"boat_repaired_patch"})
                    for i,patch in pairs(nearpatch)do
                        pt = Vector3(patch.Transform:GetWorldPosition())
                        patch:Remove()
                        break
                    end

                    boat:PushEvent("spawnnewboatleak", {pt = pt, leak_size = "small_leak", playsoundfx = true})        
                end
            end        
        end)
    end

    inst:DoTaskInTime(1,function() inst:Remove() end)
end

local function geyserfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
    
    inst:AddTag("NOCLICK")
    inst:AddTag("fx")
    inst:AddTag("crabking_spellgenerator")

    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("age")

    inst.persists = false

    inst.burble = burble
    inst.dogeyserburbletask = dogeyserburbletask

    inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/bubble_LP","burble")
    inst.SoundEmitter:SetParameter("burble", "intensity", 0)
    inst.burblestarttime = GetTime()
    inst.burbleintensity = inst:DoPeriodicTask(1,function()
            local totalcasttime = TUNING.CRABKING_CAST_TIME - (inst.crab and inst.crab:IsValid() and math.floor(inst.crab.countgems(inst.crab).yellow or 0)/2)
            local intensity = math.min(1,( GetTime() - inst.burblestarttime ) / totalcasttime)

            inst.SoundEmitter:SetParameter("burble", "intensity", intensity)
      end)
    inst:ListenForEvent("onremove", function()
        if inst.burbletask then
            inst.burbletask:Cancel()
            inst.burbletask = nil
        end
        if inst.burbleintensity then
            inst.burbleintensity:Cancel()
            inst.burbleintensity = nil
        end        
        inst.SoundEmitter:KillSound("burble")
    end)

    inst:ListenForEvent("endspell", function()
        endgeyser(inst)
    end)
    
    inst:DoTaskInTime(TUNING.CRABKING_CAST_TIME+2,function()
        endgeyser(inst)
    end)

    
    
    return inst
end
--========================================
-- FREEZE FX
--========================================
local function onfreeze(inst, target)
    if not target:IsValid() then
        --target killed or removed in combat damage phase
        return
    end

    if target.components.burnable ~= nil then
        if target.components.burnable:IsBurning() then
            target.components.burnable:Extinguish()
        elseif target.components.burnable:IsSmoldering() then
            target.components.burnable:SmotherSmolder()
        end
    end

    if target.components.combat ~= nil and inst.crab and inst.crab:IsValid() then
        target.components.combat:SuggestTarget(inst.crab)
    end

    if target.sg ~= nil and not target.sg:HasStateTag("frozen") and inst.crab and inst.crab:IsValid() then
        target:PushEvent("attacked", { attacker = inst.crab, damage = 0, weapon = inst })
    end

    if target.components.freezable ~= nil and not target.components.freezable:IsFrozen() and not target.components.freezable:IsThawing() then
        target.components.freezable:AddColdness(10,10 + Remap((inst.crab and inst.crab:IsValid() and inst.crab.countgems(inst.crab).blue or 0),0,9,0,10) )
        target.components.freezable:SpawnShatterFX()
    end
end

local function dofreezefz(inst)
    if inst.freezetask then
        inst.freezetask:Cancel()
        inst.freezetask = nil
    end
    local time = 0.1 
    inst.freezetask = inst:DoTaskInTime(time,function() inst.freezefx(inst) end)
end

local function freezefx(inst)
    local function spawnfx()
        local MAXRADIUS = inst.crab and inst.crab:IsValid() and getfreezerange(inst.crab) or (TUNING.CRABKING_FREEZE_RANGE * 0.75)
        local x,y,z = inst.Transform:GetWorldPosition()
        local theta = math.random()*2*PI
        local radius = 4+ math.pow(math.random(),0.8)* MAXRADIUS
        local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))   

        local prefab = "crab_king_icefx"
        local fx = SpawnPrefab(prefab)
        fx.Transform:SetPosition(x+offset.x,y+offset.y,z+offset.z)
    end

    local MAXFX =  1 + (inst.crab and inst.crab:IsValid() and inst.crab.countgems(inst.crab).blue or 0)
   
    local fx = MAXFX
    for i=1,fx do
        if math.random()<0.2 then
            spawnfx()
        end
    end

    dofreezefz(inst)
end

local function dofreeze(inst)
    local interval = 0.2
    local pos = Vector3(inst.Transform:GetWorldPosition())
    local range = inst.crab and inst.crab:IsValid() and getfreezerange(inst.crab) or (TUNING.CRABKING_FREEZE_RANGE * 0.75)
    local ents = TheSim:FindEntities(pos.x, pos.y, pos.z, range, nil, {"crabking_claw","crabking","flying", "shadow", "ghost", "playerghost", "FX", "NOCLICK", "DECOR", "INLIMBO"})
    for i,v in pairs(ents)do
        if v.components.temperature then

            local rate = (TUNING.CRABKING_BASE_FREEZE_AMOUNT + ((inst.crab and inst.crab:IsValid() and inst.crab.countgems(inst.crab).blue or 0) * TUNING.CRABKING_FREEZE_INCRAMENT)) /( (TUNING.CRABKING_CAST_TIME_FREEZE - (inst.crab and inst.crab:IsValid() and math.floor(inst.crab.countgems(inst.crab).yellow) or 0/2)) /interval)
            if v.components.moisture then
                rate = rate * Remap(v.components.moisture:GetMoisture(),0,v.components.moisture.maxmoisture,1,3)
            end
            
            local mintemp = v.components.temperature.mintemp
            local curtemp = v.components.temperature:GetCurrent()
            if mintemp < curtemp then
                v.components.temperature:DoDelta(math.max(-rate, mintemp - curtemp))
            end
        end
    end     
        
    local time = 0.2 
    inst.lowertemptask = inst:DoTaskInTime(time,function() inst.dofreeze(inst) end)
end

local function endfreeze(inst)
    if inst.freezetask then
        inst.freezetask:Cancel()
        inst.freezetask = nil
    end

    if inst.lowertemptask then
        inst.lowertemptask:Cancel()
        inst.lowertemptask = nil
    end

    local pos = Vector3(inst.Transform:GetWorldPosition())  
    local range = inst.crab and inst.crab:IsValid() and getfreezerange(inst.crab) or (TUNING.CRABKING_FREEZE_RANGE * 0.75)
    local ents = TheSim:FindEntities(pos.x, pos.y, pos.z, range, nil, {"crabking_claw","crabking", "shadow", "ghost", "playerghost", "FX", "NOCLICK", "DECOR", "INLIMBO"})
    for i,v in pairs(ents)do
        onfreeze(inst, v)
    end    
    SpawnPrefab("crabking_ring_fx").Transform:SetPosition(pos.x,pos.y,pos.z)
    inst:DoTaskInTime(1,function() inst:Remove() end)
end

local function freezefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
    
    inst:AddTag("NOCLICK")
    inst:AddTag("fx")
    inst:AddTag("crabking_spellgenerator")

    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("age")

    inst.persist = false

    inst.freezefx = freezefx
    inst.dofreeze = dofreeze
    inst:DoTaskInTime(0,function()
        dofreezefz(inst)
        dofreeze(inst)
    end)

    inst.SoundEmitter:PlaySound("hookline_2/creatures/boss/crabking/ice_attack")

    inst:ListenForEvent("onremove", function()
        if inst.burbletask then
            inst.burbletask:Cancel()
            inst.burbletask = nil
        end
    end)

    inst:ListenForEvent("endspell", function()
        endfreeze(inst)
    end)

    inst:DoTaskInTime(TUNING.CRABKING_CAST_TIME+2,function()
        endfreeze(inst)
    end)
    
    return inst
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv),
	Prefab("crabking_geyserspawnerp", geyserfn, nil, geyserprefabs),
	Prefab("crabking_freezep", freezefn, nil, freezeprefabs)