local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "dustmothp"

local assets = 
{
    Asset("ANIM", "anim/dustmoth.zip"),
}

local prefabs =
{

}
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.DUSTMOTH.HEALTH,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	
	runspeed = TUNING.DUSTMOTH.WALK_SPEED,
	walkspeed = TUNING.DUSTMOTH.WALK_SPEED,
	
	attackperiod = 0,
	damage = 30,
	range = 2,
	hit_range = 2.5,
	
	bank = "dustmoth",
	build = "dustmoth",
	shiny = "dustmoth",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('dustmothp',
{
    {'smallmeat',  1.0},
})

local sounds =
{
    slide_in = "grotto/creatures/dust_moth/slide_in",
    slide_out = "grotto/creatures/dust_moth/slide_out",
    pickup = "grotto/creatures/dust_moth/mumble",
    hit = "grotto/creatures/dust_moth/hit",
    death = "grotto/creatures/dust_moth/death",
    sneeze = "grotto/creatures/dust_moth/sneeze",
    dustoff = "grotto/creatures/dust_moth/dustoff",
    mumble = "grotto/creatures/dust_moth/mumble",
    clean = "grotto/creatures/dust_moth/clean",
    eat = "grotto/creatures/dust_moth/eat",
    fall = "grotto/creatures/dust_moth/bodyfall",
    eat_slide = "grotto/creatures/dust_moth/eat_slide",
}
--==============================================
--					Mob Functions
--==============================================

--==============================================
--				Custom Common Functions
--==============================================
local function GetDustValue(food)

end

local generic_rock = {hunger = 10, health = 10}
local food_items = {
	dustmeringue = {hunger = mob.hunger, health = 20},
	refined_dust = {hunger = mob.hunger/4, health = 3},
	rocks = generic_rock,
	flint = {hunger = 12, health = generic_rock.health},
	nitre = {hunger = 15, health = generic_rock.health},
	flint = {hunger = 12, health = generic_rock.health},
	goldnugget = {hunger = 20, health = generic_rock.health},
	cutstone = {hunger = 30, health = generic_rock.health},
	thulecite_pieces = {hunger = 1, health = -20},
	thulecite = {hunger = 1, health = -20*4},
}

local function OnEat(inst, food)
	if food and food_items[food.prefab] then
		local fv = food_items[food.prefab]
		inst.components.hunger:DoDelta(fv.hunger or 0)
		inst.components.health:DoDelta(fv.health or 0, nil, food.prefab) --TODO make food item the cause.
	end
end

local MAXDUST = 3
local function CollectDust(inst)
	inst.dust = inst.dust + 1
	if inst.dust >= MAXDUST then
		local dust = SpawnPrefab("refined_dust")
		dust.Transform:SetPosition(inst:GetPosition():Get())
		Launch(dust, inst, 1)
		inst.dust = 0
	end
end
local function OnDustOther(inst, target)
	if not target:HasTag("dusted") then
		target:AddTag("dusted") --so the action can check for it
		target:DoTaskInTime(480, function(target) target:RemoveTag("dusted") end)
		CollectDust(inst)
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.dust = data.dust or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.dust = inst.dust or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.DUSTMOTH)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("cavedweller")
    inst:AddTag("animal")
	inst:AddTag("dustmoth")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = true
	
	inst._sounds = sounds
	
	inst.dust = 0
	inst.DustOther = OnDustOther
	
	inst.getskins = getskins
	--inst:RemoveComponent("inkable")
	
	local body_symbol = "dm_body"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.ELEMENTAL }, { FOODTYPE.ELEMENTAL }) 
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater:SetOnEatFn(OnEat)
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 50, .75)
	inst.DynamicShadow:SetSize(2.8, 2.5)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	--inst:ListenForEvent("death", RemoveAllies) 
	--disabled because it would just force other players to focus the player instead of the allies
	--plus I don't think it'd be exploited much.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
