local thrall_color_symbols = {
	{
		symbol = "mouth_inner",
		colour = {1, 0, 0, 1},
	},
	{
		symbol = "body_top",
		colour = {1, 0, 0, 1},
	},
	{
		symbol = "body_low",
		colour = {1, 0, 0, 1},
	},
	{
		symbol = "mouth_edge",
		colour = {1, 0, 0, 1},
	},
	{
		symbol = "tent_arm",
		colour = {1, 0, 0, 1},
	},
	{
		symbol = "tent_hand",
		colour = {1, 0, 0, 1},
	},
}
local SKINS = {
	wobsterp = {
		val = {
			name = "val", --this is for the purpose of printing
			fname = "Valentine Wobster",
			build = "lobster_valentines",
		},
	},
	dustmothp = {
		blue = {
			name = "blue",
			fname = "Blue Dustmoth",
			build = "dustmoth",
			hue = 0.5,
		},
		pink = {
			name = "pink",
			fname = "Pink Dustmoth",
			build = "dustmoth",
			hue = 0.7,
		},
		rgb = {
			name = "rgb",
			fname = "Rainbow Dustmoth",
			build = "dustmoth",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	lightcrabp = {
		red = {
			name = "red",
			fname = "Red Crab",
			build = "lightcrab",
			hue = 0.5,
		},
		rgb = {
			name = "rgb",
			fname = "RGB Crab",
			build = "lightcrab",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	striderp = {
		fluffy = {
			name = "fluffy", --this is for the purpose of printing
			fname = "Fluffy Hazbin",
			build = "spider_fluffy",
			build2 = "spider_fluffy_water",
		},
		odd = {
			name = "odd",
			fname = "Odd Strider",
			build = "spider_water",
			build2 = "spider_water_water",
			hue = 0.2,
		},
		rgb = {
			name = "rgb",
			fname = "RGB Strider",
			build = "spider_water",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	fused_shadelingp = {
		blue = {
			name = "blue",
			fname = "Blue Shadeling",
			build = "fused_shadeling",
			hue = 0.6,
			sat = 1.5,
		},
		pink = {
			name = "pink",
			fname = "Pink Shadeling",
			build = "fused_shadeling",
			hue = 0.8,
			sat = 1.5,
		},
		green = {
			name = "green",
			fname = "Green Shadeling",
			build = "fused_shadeling",
			hue = 0.4,
			sat = 1.5,
		},
		purple = {
			name = "purple",
			fname = "Purple Shadeling",
			build = "fused_shadeling",
			hue = 0.68,
			sat = 1.5,
		},
		rgb = {
			name = "rgb",
			fname = "RGB Shadeling",
			build = "fused_shadeling",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	grassgatorp = {
		val = {
			name = "val", --this is for the purpose of printing
			fname = "Valentine Gator",
			build = "grass_gator_valentines",
		},
	},
	powdermonkeyp = {
		sw = {
			name = "sw", --this is for the purpose of printing
			fname = "Shipwrecked",
			build = "monkey_small_sw",
		},
		spider = {
			name = "spider", --this is for the purpose of printing
			fname = "Spider",
			build = "monkey_small_spider",
		},
	},
	hound_hedgep = {
		blue = {
			name = "blue", --this is for the purpose of printing
			fname = "Blue Briar Hound",
			build = "hound_hedge_ocean",
			hue = 0.6,
		},
		purple = {
			name = "purple", --this is for the purpose of printing
			fname = "Purple Briar Hound",
			build = "hound_hedge_ocean",
			hue = 0.8,
		},
		green = {
			name = "green", --this is for the purpose of printing
			fname = "Green Briar Hound",
			build = "hound_hedge_ocean",
			hue = 0.4,
		},
		rgb = {
			name = "rgb",
			fname = "RGB Briar Hound",
			build = "hound_hedge_ocean",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	--[[
	shadowthrall_hornsp = {
		red = {
			name = "red", --this is for the purpose of printing
			fname = "Red Rasp",
			build = "shadow_thrall_horns",
			hue = 1,
			symbol_data = thrall_color_symbols,
		},
		pink = {
			name = "pink", --this is for the purpose of printing
			fname = "Pink Rasp",
			build = "shadow_thrall_horns",
			hue = 0.85,
			symbol_data = thrall_color_symbols,
		},
		blue = {
			name = "blue", --this is for the purpose of printing
			fname = "Blue Rasp",
			build = "shadow_thrall_horns",
			hue = 0.6,
			symbol_data = thrall_color_symbols,
		},
		purple = {
			name = "purple", --this is for the purpose of printing
			fname = "Purple Rasp",
			build = "shadow_thrall_horns",
			hue = 0.7,
			symbol_data = thrall_color_symbols,
		},
		green = {
			name = "green", --this is for the purpose of printing
			fname = "Green Rasp",
			build = "shadow_thrall_horns",
			hue = 0.4,
			symbol_data = thrall_color_symbols,
		},
	},
	shadowthrall_wingsp = {
		pink = {
			name = "pink",
			fname = "Pink Shriek",
			build = "shadow_thrall_wings",
			hue = 0.85,
		},
		blue = {
			name = "blue",
			fname = "Blue Shriek",
			build = "shadow_thrall_wings",
			hue = 0.6,
		},
		purple = {
			name = "purple",
			fname = "Purple Shriek",
			build = "shadow_thrall_wings",
			hue = 0.7,
		},
		green = {
			name = "green",
			fname = "Green Shriek",
			build = "shadow_thrall_wings",
			hue = 0.4,
		},
	},
	shadowthrall_handsp = {
		pink = {
			name = "pink",
			fname = "Pink Jitters",
			build = "shadow_thrall_hands",
			hue = 0.85,
		},
		blue = {
			name = "blue",
			fname = "Blue Jitters",
			build = "shadow_thrall_hands",
			hue = 0.6,
		},
		purple = {
			name = "purple",
			fname = "Purple Jitters",
			build = "shadow_thrall_hands",
			hue = 0.7,
		},
		green = {
			name = "green",
			fname = "Green Jitters",
			build = "shadow_thrall_hands",
			hue = 0.4,
		},
	},]]
}

return SKINS